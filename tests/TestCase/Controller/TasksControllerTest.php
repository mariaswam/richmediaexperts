<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TasksController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\TasksController Test Case
 */
class TasksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tasks',
        'app.users',
        'app.milestones',
        'app.projects',
        'app.companies',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.invoices',
        'app.permissions',
        'app.permissions_users',
        'app.items',
        'app.milestones_users',
        'app.task_types',
        'app.timesheets'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'first_name' => 'Manuel',
                    'last_name' => 'Arias',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'A'
                ]
            ]
        ]);
        $this->get('tasks');
        $this->assertResponseOk();
        $this->assertResponseContains('Tasks');
    }

    public function testIndexData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('/tasks?page=1');

      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);
        $this->get('tasks/view/4');
        $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);
      /* Information
        Project:
          budget: 1000,
          total_cost: 300

        User:
          hour_cost: 50

        Task:
          duration: 3 hours

      */
        $data = [
                	'name' => 'T7',
                	'description' => '',
                	'start_date' => [
                		'year' => '2017',
                		'month' => '09',
                		'day' => '10',
                		'hour' => '08',
                		'minute' => '00'
                	],
                	'duration' => '3',
                	'priority' => 'A',
                	'user_id' => '1',
                	'project_id' => '1',
                	'milestone_id' => '2',
                	'task_type_id' => '1'
                ];

        $this->post('tasks/add', $data);
        $this->assertResponseSuccess();

        $tasks = TableRegistry::get('Tasks');
        $projects = TableRegistry::get('Projects');
        $task = $tasks->find()->where(['name' => 'T7']);
        $project = $projects->get('1');

        $this->assertEquals(6, $task->toArray()[0]['id']);
        //Verify if the total_cost of the project change when create a new task
        $this->assertEquals(450, $project->total_cost);

    }

    public function testAddWrongDates()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

        $data = [
                  'name' => 'T7',
                  'description' => '',
                  'start_date' => [
                    'year' => '2018',
                    'month' => '09',
                    'day' => '10',
                    'hour' => '08',
                    'minute' => '00'
                  ],
                  'duration' => '3',
                  'priority' => 'A',
                  'user_id' => '1',
                  'project_id' => '1',
                  'milestone_id' => '2',
                  'task_type_id' => '1'
                ];

        $this->post('tasks/add', $data);
        $this->assertResponseSuccess();

        $tasks = TableRegistry::get('Tasks');
        $task = $tasks->find()->where(['name' => 'T7']);

        $this->assertEquals(0, $task->count());

    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);
        /*Preview record id -> 3
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 10:48:55',
            'duration' => 1,
            'priority' => 'Lorem ipsum dolor sit ame',
            'user_id' => 1,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 10:48:55',
            'state' => 'P'
        ]
        */
        $data = [
                  'name' => 'T6',
                  'description' => '',
                  'start_date' => [
                    'year' => '2018',
                    'month' => '09',
                    'day' => '10',
                    'hour' => '08',
                    'minute' => '00'
                  ],
                  'duration' => '1',
                  'priority' => 'A',
                  'user_id' => '1',
                  'project_id' => '1',
                  'milestone_id' => '2',
                  'task_type_id' => '1'
                ];

        $this->put('tasks/edit/3', $data);
        $this->assertResponseSuccess();

        $tasks = TableRegistry::get('Tasks');
        $projects = TableRegistry::get('Projects');
        $task = $tasks->find()->where(['id' => '3']);
        $project = $projects->get('1');

        $this->assertEquals('T6', $task->toArray()[0]['name']);
        $this->assertEquals(250, $project->total_cost);

    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->post('tasks/delete/1');
      $this->assertResponseSuccess();

      $tasks = TableRegistry::get('Tasks');
      $projects = TableRegistry::get('Projects');
      $task = $tasks->find()->where(['id' => '1']);
      $project = $projects->get('1');

      $this->assertEquals(0, $task->count());
      $this->assertEquals(200, $project->total_cost);
    }

    public function testFinishTask(){
      /*Project information:
          id: 1,
          total_cost: 300,
          budget: 1000

        User information:
          id: 1,
          hour_cost: 50

        Task information:
          start_date: 2017-09-27 12:00:00,
          end_date: 2017-09-27 14:00:00,
          duration: 2
      */
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
        'id' => '1',
        'end_date' => date('2017-09-27 13:00')
      ];

      $this->post('tasks/finishTask', $data);

      $tasks = TableRegistry::get('Tasks');
      $projects = TableRegistry::get('Projects');
      $task = $tasks->get('1');
      $project = $projects->get('1');

      $this->assertEquals('TA', $task->state);
      $this->assertEquals(250, $project->total_cost);

    }
}
