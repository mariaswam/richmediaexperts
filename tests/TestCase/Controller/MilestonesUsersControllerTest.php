<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MilestonesUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MilestonesUsersController Test Case
 */
class MilestonesUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.milestones_users',
        'app.milestones',
        'app.projects',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.invoices',
        'app.items'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
