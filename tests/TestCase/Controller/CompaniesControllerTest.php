<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CompaniesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\CompaniesController Test Case
 */
class CompaniesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.milestones_users',
        'app.contacts',
        'app.projects',
        'app.permissions',
        'app.permissions_users',
        'app.tags',
        'app.companies_tags'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'R'
              ]
          ]
      ]);
      $this->get('/companies');

      $this->assertResponseOk();
      $this->assertResponseContains('Companies');
    }

    public function testIndexData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'R'
              ]
          ]
      ]);

      $this->get('/companies?page=1');

      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('/companies/view/3');

      $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
      $data = [
          'name' => 'CTest',
          'phone' => 'Lorem ipsum dolor sit amet',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'Lorem ipsum dolor sit amet',
          'sector' => 'Lorem ipsum dolor sit amet',
          'payment_terms' => 'Lorem ipsum dolor ',
          'user_id' => 1
      ];

      $this->post('companies/add', $data);
      $this->assertResponseSuccess();
      $companies = TableRegistry::get('Companies');
      $company = $companies->find()->where(['name' => 'CTest']);
      $this->assertEquals(7, $company->toArray()[0]['id']);
    }

    public function testAddWhitWrongData(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      //Name is a required value
      $data = [
          'phone' => 'Lorem ipsum dolor sit amet',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'Lorem ipsum dolor sit amet',
          'sector' => 'Lorem ipsum dolor sit amet',
          'payment_terms' => 'Lorem ipsum dolor ',
          'user_id' => 1
      ];

      $this->post('companies/add', $data);
      //Post successful
      $this->assertResponseSuccess();
      $companies = TableRegistry::get('Companies');
      //No insert into database
      $company = $companies->find()->where(['name' => 'CTest']);
      $this->assertEquals(0, $company->count());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
          'name' => 'CTest',
          'phone' => 'Lorem ipsum dolor sit amet',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'San Pedro',
          'sector' => 'Lorem ipsum dolor sit amet',
          'payment_terms' => 'Lorem ipsum dolor ',
          'user_id' => 1
      ];

      $this->put('companies/edit/1', $data);
      $this->assertResponseSuccess();
      $companies = TableRegistry::get('Companies');
      $company = $companies->find()->where(['id' => '1']);
      $this->assertEquals('CTest', $company->toArray()[0]['name']);
      $this->assertEquals('San Pedro', $company->toArray()[0]['address']);
    }

    public function testEditWithWrongData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
      //Original record
      /*[
          'name' => 'Lorem ipsum dolor sit amet',
          'phone' => 'Lorem ipsum dolor sit amet',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'Lorem ipsum dolor sit amet',
          'sector' => 'Lorem ipsum dolor sit amet',
          'payment_terms' => 'Lorem ipsum dolor ',
          'user_id' => 1
      ]*/
      $data = [
          'name' => null,
          'phone' => 'Lorem ipsum dolor sit amet',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'San Pedro',
          'sector' => 'Lorem ipsum dolor sit amet',
          'payment_terms' => 'Lorem ipsum dolor ',
          'user_id' => 1
      ];

      $this->put('companies/edit/1', $data);
      $this->assertResponseSuccess();
      $companies = TableRegistry::get('Companies');
      $company = $companies->find()->where(['id' => '1']);
      //No changes in the record
      $this->assertEquals('Lorem ipsum dolor sit amet', $company->toArray()[0]['name']);
      $this->assertEquals('Lorem ipsum dolor sit amet', $company->toArray()[0]['address']);
    }

    /*
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
        $this->post('companies/delete/3');
        $this->assertResponseSuccess();
        $companies = TableRegistry::get('Companies');
        $company = $companies->find()->where(['id' => 3]);
        $this->assertEquals(0, $company->count());
    }

    public function testDeleteWrongCompany()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
        $this->post('companies/delete/30');
        $this->assertResponseError();

    }
}
