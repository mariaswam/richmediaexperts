<?php
namespace App\Test\TestCase\Controller;

use App\Controller\InvoicesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\InvoicesController Test Case
 */
class InvoicesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.invoices',
        'app.projects',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.milestones_users',
        'app.contacts',
        'app.permissions',
        'app.permissions_users',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.items'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('invoices');

      $this->assertResponseOk();
    }

    public function testIndexData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('/companies?page=1');

      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('invoices/view/1');
      $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
            	'date' => [
            		'year' => '2017',
            		'month' => '07',
            		'day' => '09'
            	],
            	'project_id' => '1',
            	'itemsArray' => [
            		'description' => [
            			(int) 0 => 'Banner',
            			(int) 1 => 'Sitio'
            		],
            		'price' => [
            			(int) 0 => '200',
            			(int) 1 => '300'
            		],
            		'quantity' => [
            			(int) 0 => '2',
            			(int) 1 => '1'
            		],
            		'vat' => [
            			(int) 0 => '0',
            			(int) 1 => '13'
            		]
            	],
            	'total_amount' => '739'
      ];

      $this->post('invoices/add', $data);
      $this->assertResponseSuccess();

      $invoices = TableRegistry::get('Invoices');
      $invoicesItems = TableRegistry::get('Items');
      $invoice = $invoices->find()->where(['id' => '6']);
      $items = $invoicesItems->find()->where(['invoice_id' => $invoice->toArray()[0]['id']]);

      $this->assertEquals(1, $invoice->count());
      $this->assertEquals(2, $items->count());

    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      /*
        [
          ID => 3
            'date' => '2017-09-27',
            'state' => 'Lorem ipsum dolor sit ame',
            'total_amount' => 1000,
            'project_id' => 3,
            'due_date' => '2017-10-17',
            'outstanding_amount' => 1000
        ]
      */

      $data = [
              'date' => [
                'year' => '2017',
                'month' => '07',
                'day' => '09'
              ],
              'project_id' => '1',
              'itemsArray' => [
                'description' => [
                  (int) 0 => 'Banner',
                  (int) 1 => 'Sitio'
                ],
                'price' => [
                  (int) 0 => '200',
                  (int) 1 => '300'
                ],
                'quantity' => [
                  (int) 0 => '2',
                  (int) 1 => '1'
                ],
                'vat' => [
                  (int) 0 => '0',
                  (int) 1 => '13'
                ]
              ],
              'total_amount' => '739'
      ];

      $this->post('invoices/edit/3', $data);
      $this->assertResponseSuccess();

      $invoices = TableRegistry::get('Invoices');
      $invoice = $invoices->find()->where(['id' => '3']);

      $this->assertEquals('2017-07-09', $invoice->toArray()[0]['date']->format('Y-m-d'));

    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->post('invoices/delete/1');
      $this->assertResponseSuccess();

      $invoices = TableRegistry::get('Invoices');
      $invoice = $invoices->find()->where(['id' => '1']);

      $this->assertEquals(0, $invoice->count());

    }

    public function testMakePaidComplete(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
          	'paidType' => '0',
          	'sendNotification' => 'true',
          	'invoice_id' => '2'
          ];

      $this->post('invoices/makePaid', $data);
      $this->assertResponseSuccess();

      $invoices = TableRegistry::get('Invoices');
      $invoice = $invoices->get('2');

      $this->assertEquals(0, $invoice->outstanding_amount);
      $this->assertEquals('C', $invoice->state);
    }

    public function testMakePaidPartial(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
            	'paidType' => '1',
            	'paidAmount' => '200',
            	'sendNotification' => 'true',
            	'invoice_id' => '2'
            ];

      $this->post('invoices/makePaid', $data);
      $this->assertResponseSuccess();

      $invoices = TableRegistry::get('Invoices');
      $invoice = $invoices->get('2');

      $this->assertEquals(800, $invoice->outstanding_amount);
      $this->assertEquals('P', $invoice->state);
    }
}
