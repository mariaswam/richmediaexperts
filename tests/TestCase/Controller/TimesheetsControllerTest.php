<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TimesheetsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\TimesheetsController Test Case
 */
class TimesheetsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.timesheets',
        'app.tasks',
        'app.users',
        'app.milestones',
        'app.projects',
        'app.companies',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.permissions',
        'app.permissions_users',
        'app.companies_tags',
        'app.invoices',
        'app.items',
        'app.milestones_users',
        'app.task_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('timesheets');
      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('timesheets/view/3');
      $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
            	'project_id' => '1',
            	'milestone_id' => '2',
            	'task_id' => '1',
            	'performed_activity' => '3',
            	'description' => 'Bla bla bla',
            	'start_date' => [
            		'year' => '2017',
            		'month' => '09',
            		'day' => '27',
            		'hour' => '12',
            		'minute' => '30'
            	],
            	'duration' => [
            		'hour' => '01',
            		'minute' => '00'
            	]
      ];

      $this->post('timesheets/add', $data);
      $this->assertResponseSuccess();

      $timesheets = TableRegistry::get('Timesheets');
      $timesheet = $timesheets->find()->where(['id' => '5']);

      $this->assertEquals('Bla bla bla', $timesheet->toArray()[0]['description']);
    }

    public function testAddWrongTime()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
              'project_id' => '1',
              'milestone_id' => '2',
              'task_id' => '1',
              'performed_activity' => '3',
              'description' => 'Bla bla bla',
              'start_date' => [
                'year' => '2017',
                'month' => '09',
                'day' => '27',
                'hour' => '12',
                'minute' => '30'
              ],
              'duration' => [
                'hour' => '05',
                'minute' => '00'
              ]
      ];

      $this->post('timesheets/add', $data);
      $this->assertResponseSuccess();

      $timesheets = TableRegistry::get('Timesheets');
      $timesheet = $timesheets->find()->where(['id' => '5']);

      $this->assertEquals(0, $timesheet->count());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      /*
        [ ID => 3
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 1,
            'performed_activity' => 1
        ]
      */

      $data = [
              'task_id' => '1',
              'performed_activity' => '3',
              'description' => 'Bla bla bla',
              'start_date' => [
                'year' => '2017',
                'month' => '09',
                'day' => '27',
                'hour' => '12',
                'minute' => '30'
              ],
              'duration' => [
                'hour' => '01',
                'minute' => '00'
              ]
      ];

      $this->post('timesheets/edit/3', $data);
      $this->assertResponseSuccess();

      $timesheets = TableRegistry::get('Timesheets');
      $timesheet = $timesheets->find()->where(['id' => '3']);

      $this->assertEquals('Bla bla bla', $timesheet->toArray()[0]['description']);

    }

    public function testEditWrongTime()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      /*
        [ ID => 3
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 1,
            'performed_activity' => 1
        ]
      */

      $data = [
              'task_id' => '1',
              'performed_activity' => '3',
              'description' => 'Bla bla bla',
              'start_date' => [
                'year' => '2017',
                'month' => '09',
                'day' => '27',
                'hour' => '12',
                'minute' => '30'
              ],
              'duration' => [
                'hour' => '05',
                'minute' => '00'
              ]
      ];

      $this->post('timesheets/edit/3', $data);
      $this->assertResponseSuccess();

      $timesheets = TableRegistry::get('Timesheets');
      $timesheet = $timesheets->find()->where(['id' => '3']);

      $this->assertEquals('Lorem ipsum dolor sit amet', $timesheet->toArray()[0]['description']);

    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->post('timesheets/delete/4');

      $timesheets = TableRegistry::get('Timesheets');
      $timesheet = $timesheets->find()->where(['id' => '4']);

      $this->assertEquals(0, $timesheet->count());
    }

    public function testTimesheetByUser(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A',
                  'organization_id' => '1'
              ]
          ]
      ]);

      $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

      $data = ['id' => '1'];
      $expected = '[{"id":1,"description":"Prueba","start_date":"2017-09-28T10:16:00-06:00","duration":"2017-09-29T10:16:00-06:00","end_date":"2017-09-28T10:16:00-06:00","task_id":1,"performed_activity":1,"task":{"id":1,"name":"Lorem ipsum dolor ","description":"Lorem ipsum dolor sit amet","start_date":"2017-09-27T12:00:00-06:00","duration":2,"priority":"A","user_id":1,"milestone_id":1,"task_type_id":1,"end_date":"2017-09-27T14:00:00-06:00","state":"P"}},{"id":2,"description":"Lorem ipsum dolor sit amet","start_date":"2017-09-28T10:16:00-06:00","duration":"2017-09-29T10:16:00-06:00","end_date":"2017-09-28T10:16:00-06:00","task_id":2,"performed_activity":1,"task":{"id":2,"name":"Lorem ipsum dolor ","description":"Lorem ipsum dolor sit amet","start_date":"2017-09-27T10:48:55-06:00","duration":1,"priority":"L","user_id":1,"milestone_id":1,"task_type_id":1,"end_date":"2017-09-27T10:48:55-06:00","state":"P"}},{"id":4,"description":"Lorem ipsum dolor sit amet","start_date":"2017-09-28T10:16:00-06:00","duration":"2017-09-29T10:16:00-06:00","end_date":"2017-09-28T10:16:00-06:00","task_id":2,"performed_activity":1,"task":{"id":2,"name":"Lorem ipsum dolor ","description":"Lorem ipsum dolor sit amet","start_date":"2017-09-27T10:48:55-06:00","duration":1,"priority":"L","user_id":1,"milestone_id":1,"task_type_id":1,"end_date":"2017-09-27T10:48:55-06:00","state":"P"}}]';
      $this->post('timesheets/getTimesheetByUser', $data);
      $this->assertResponseSuccess();
      $this->assertEquals($expected, $this->_response->body());
      unset($_ENV['HTTP_X_REQUESTED_WITH']);
    }
}
