<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProjectsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\ProjectsController Test Case
 */
class ProjectsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.projects',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.milestones_users',
        'app.contacts',
        'app.permissions',
        'app.permissions_users',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.invoices'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'role' => 'R'
                ]
            ]
        ]);

        $this->get('projects');

        $this->assertResponseOk();
        $this->assertResponseContains('Projects');
    }

    public function testIndexData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'R'
              ]
          ]
      ]);

      $this->get('/projects?page=1');

      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'role' => 'R'
                ]
            ]
        ]);

        $this->get('projects/view/4');
        $this->assertResponseOk();
        $this->assertResponseContains('Projects');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddOnlyProject()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'first_name' => 'Manuel',
                    'last_name' => 'Arias',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'R'
                ]
            ]
        ]);

        $data = [
            'name' => 'P1',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => ['year' => '2017', 'month' => '08', 'day' => '12'],
            'budget' => 1000,
            'invoiced' => 0,
            'company_id' => 1,
            'total_cost' => 0,
            'end_date' => ['year' => '2017', 'month' => '11', 'day' => '12']
        ];

        $this->post('projects/add', $data);
        $this->assertResponseSuccess();
        $projects = TableRegistry::get('Projects');
        $project = $projects->find()->where(['name' => 'P1']);
        $this->assertEquals(8, $project->toArray()[0]['id']);
    }

    public function testAddWithMilestones(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'R'
              ]
          ]
      ]);

      $data = [
              	'name' => 'P2',
              	'description' => 'Bla bla bla',
              	'start_date' => [
              		'year' => '2017',
              		'month' => '04',
              		'day' => '02'
              	],
              	'end_date' => [
              		'year' => '2017',
              		'month' => '11',
              		'day' => '08'
              	],
              	'budget' => '2300',
              	'company_id' => '5',
              	'invoiced' => '0',
              	'itemsInvoices' => '',
              	'milestonesArray' => [
              		'name' => [
              			(int) 0 => 'M1',
              			(int) 1 => 'M2'
              		],
              		'start_date' => [
              			(int) 0 => [
              				'year' => '2017'
              			],
              			(int) 1 => [
              				'month' => '05'
              			],
              			(int) 2 => [
              				'day' => '07'
              			],
              			(int) 3 => [
              				'year' => '2017'
              			],
              			(int) 4 => [
              				'month' => '07'
              			],
              			(int) 5 => [
              				'day' => '07'
              			]
              		],
              		'end_date' => [
              			(int) 0 => [
              				'year' => '2017'
              			],
              			(int) 1 => [
              				'month' => '06'
              			],
              			(int) 2 => [
              				'day' => '03'
              			],
              			(int) 3 => [
              				'year' => '2017'
              			],
              			(int) 4 => [
              				'month' => '08'
              			],
              			(int) 5 => [
              				'day' => '02'
              			]
              		],
              		'user' => [
              			(int) 0 => '1,',
              			(int) 1 => '2,1,'
              		]
              	]
              ];

        $this->post('projects/add', $data);
        $this->assertResponseSuccess();
        $projects = TableRegistry::get('Projects');
        $project = $projects->find()->where(['name' => 'P2']);
        $this->assertEquals(8, $project->toArray()[0]['id']);
        $milestones = TableRegistry::get('Milestones');
        $milestoneArray = $milestones->find()->where(['project_id' => 8]);
        $this->assertEquals(2, $milestoneArray->count());
        $usersMilestones = TableRegistry::get('MilestonesUsers');
        $milestoneUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[0]['id']]);
        $this->assertEquals(1, $milestoneUser->count());
        $milestonesUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[1]['id']]);
        $this->assertEquals(2, $milestonesUser->count());
    }

    public function testAddWithMilestonesWrongDates(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'R'
              ]
          ]
      ]);

      $data = [
                'name' => 'P2',
                'description' => 'Bla bla bla',
                'start_date' => [
                  'year' => '2017',
                  'month' => '04',
                  'day' => '02'
                ],
                'end_date' => [
                  'year' => '2017',
                  'month' => '11',
                  'day' => '08'
                ],
                'budget' => '2300',
                'company_id' => '5',
                'invoiced' => '0',
                'itemsInvoices' => '',
                'milestonesArray' => [
                  'name' => [
                    (int) 0 => 'M1',
                    (int) 1 => 'M2'
                  ],
                  'start_date' => [
                    (int) 0 => [
                      'year' => '2017'
                    ],
                    (int) 1 => [
                      'month' => '03'
                    ],
                    (int) 2 => [
                      'day' => '07'
                    ],
                    (int) 3 => [
                      'year' => '2017'
                    ],
                    (int) 4 => [
                      'month' => '07'
                    ],
                    (int) 5 => [
                      'day' => '07'
                    ]
                  ],
                  'end_date' => [
                    (int) 0 => [
                      'year' => '2017'
                    ],
                    (int) 1 => [
                      'month' => '06'
                    ],
                    (int) 2 => [
                      'day' => '03'
                    ],
                    (int) 3 => [
                      'year' => '2017'
                    ],
                    (int) 4 => [
                      'month' => '08'
                    ],
                    (int) 5 => [
                      'day' => '02'
                    ]
                  ],
                  'user' => [
                    (int) 0 => '1,',
                    (int) 1 => '2,1,'
                  ]
                ]
              ];

        $this->post('projects/add', $data);
        $this->assertResponseSuccess();
        $projects = TableRegistry::get('Projects');
        $project = $projects->find()->where(['name' => 'P2']);
        $this->assertEquals(8, $project->toArray()[0]['id']);
        $milestones = TableRegistry::get('Milestones');
        $milestoneArray = $milestones->find()->where(['project_id' => 8]);
        $this->assertEquals(1, $milestoneArray->count());
        $usersMilestones = TableRegistry::get('MilestonesUsers');
        $milestoneUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[0]['id']]);
        $this->assertEquals(2, $milestoneUser->count());
    }


    // public function testAddWithInvoice(){
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'first_name' => 'Manuel',
    //               'last_name' => 'Arias',
    //               'email' => 'manuelarias185@gmail.com',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //   $itemsInvoices = TableRegistry::get('Items');
    //   // $itemsInvoices->deleteAll(['invoice_id' => 2]);
    //   $data = [
    //           	'name' => 'P3',
    //           	'description' => '',
    //           	'start_date' => [
    //           		'year' => '2017',
    //           		'month' => '05',
    //           		'day' => '02'
    //           	],
    //           	'end_date' => [
    //           		'year' => '2017',
    //           		'month' => '10',
    //           		'day' => '02'
    //           	],
    //           	'budget' => '2400',
    //           	'company_id' => '1',
    //           	'invoiced' => '1',
    //           	'itemsInvoices' => '[{"name":"description","value":"Bla bla bla "},{"name":"price","value":"120"},{"name":"quantity","value":"1"},{"name":"vat","value":"0"},{"name":"description","value":"Bla bla bla "},{"name":"price","value":"20"},{"name":"quantity","value":"2"},{"name":"vat","value":"0"}]'
    //         ];
    //
    //   $this->post('projects/add', $data);
    //   $this->assertResponseSuccess();
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['name' => 'P3']);
    //   $this->assertEquals(8, $project->toArray()[0]['id']);
    //   $invoices = TableRegistry::get('Invoices');
    //   $invoice = $invoices->find()->where(['project_id' => $project->toArray()[0]['id']]);
    //   $this->assertEquals(2, $invoice->toArray()[0]['id']);
    //   $itemArray = $itemsInvoices->find()->where(['invoice_id' => $invoice->toArray()[0]['id']]);
    //   $this->assertEquals(2, $itemArray->count());
    //
    // }

    // public function testAddWithMilestonesInvoices(){
    //   $itemsInvoices = TableRegistry::get('Items');
    //   // $itemsInvoices->deleteAll(['invoice_id' => 2]);
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'first_name' => 'Manuel',
    //               'last_name' => 'Arias',
    //               'email' => 'manuelarias185@gmail.com',
    //               'role' => 'R'
    //           ]
    //       ]
    //   ]);
    //   $data = [
    //         	'name' => 'PTest',
    //         	'description' => 'bla bla ',
    //         	'start_date' => [
    //         		'year' => '2017',
    //         		'month' => '05',
    //         		'day' => '05'
    //         	],
    //         	'end_date' => [
    //         		'year' => '2017',
    //         		'month' => '09',
    //         		'day' => '02'
    //         	],
    //         	'budget' => '2300',
    //         	'company_id' => '1',
    //         	'invoiced' => '1',
    //         	'itemsInvoices' => '[{"name":"description","value":"Bla bla bla "},{"name":"price","value":"200"},{"name":"quantity","value":"2"},{"name":"vat","value":"0"},{"name":"description","value":"Bla bla bla "},{"name":"price","value":"120"},{"name":"quantity","value":"1"},{"name":"vat","value":"13"}]',
    //         	'milestonesArray' => [
    //         		'name' => [
    //         			(int) 0 => 'M1',
    //         			(int) 1 => 'M2'
    //         		],
    //         		'start_date' => [
    //         			(int) 0 => [
    //         				'year' => '2017'
    //         			],
    //         			(int) 1 => [
    //         				'month' => '06'
    //         			],
    //         			(int) 2 => [
    //         				'day' => '04'
    //         			],
    //         			(int) 3 => [
    //         				'year' => '2017'
    //         			],
    //         			(int) 4 => [
    //         				'month' => '07'
    //         			],
    //         			(int) 5 => [
    //         				'day' => '05'
    //         			]
    //         		],
    //         		'end_date' => [
    //         			(int) 0 => [
    //         				'year' => '2017'
    //         			],
    //         			(int) 1 => [
    //         				'month' => '06'
    //         			],
    //         			(int) 2 => [
    //         				'day' => '20'
    //         			],
    //         			(int) 3 => [
    //         				'year' => '2017'
    //         			],
    //         			(int) 4 => [
    //         				'month' => '08'
    //         			],
    //         			(int) 5 => [
    //         				'day' => '11'
    //         			]
    //         		],
    //         		'user' => [
    //         			(int) 0 => '1,',
    //         			(int) 1 => '2,1,'
    //         		]
    //         	]
    //         ];
    //
    //   $this->post('projects/add', $data);
    //   $this->assertResponseSuccess();
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['name' => 'PTest']);
    //   $this->assertEquals(8, $project->toArray()[0]['id']);
    //   $milestones = TableRegistry::get('Milestones');
    //   $milestoneArray = $milestones->find()->where(['project_id' => $project->toArray()[0]['id']]);
    //   $this->assertEquals(2, $milestoneArray->count());
    //   $usersMilestones = TableRegistry::get('MilestonesUsers');
    //   $milestoneUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[0]['id']]);
    //   $this->assertEquals(1, $milestoneUser->count());
    //   $milestonesUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[1]['id']]);
    //   $this->assertEquals(2, $milestonesUser->count());
    //   $invoices = TableRegistry::get('Invoices');
    //   $invoice = $invoices->find()->where(['project_id' => $project->toArray()[0]['id']]);
    //   $this->assertEquals(2, $invoice->toArray()[0]['id']);
    //   $itemArray = $itemsInvoices->find()->where(['invoice_id' => $invoice->toArray()[0]['id']]);
    //   $this->assertEquals(2, $itemArray->count());
    // }
    //
    // public function testAddWithMilestonesInvoicesWrongDates(){
    //   $itemsInvoices = TableRegistry::get('Items');
    //   // $itemsInvoices->deleteAll(['invoice_id' => 2]);
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'first_name' => 'Manuel',
    //               'last_name' => 'Arias',
    //               'email' => 'manuelarias185@gmail.com',
    //               'role' => 'R'
    //           ]
    //       ]
    //   ]);
    //   $data = [
    //           'name' => 'PTest',
    //           'description' => 'bla bla ',
    //           'start_date' => [
    //             'year' => '2017',
    //             'month' => '05',
    //             'day' => '05'
    //           ],
    //           'end_date' => [
    //             'year' => '2017',
    //             'month' => '09',
    //             'day' => '02'
    //           ],
    //           'budget' => '2300',
    //           'company_id' => '1',
    //           'invoiced' => '1',
    //           'itemsInvoices' => '[{"name":"description","value":"Bla bla bla "},{"name":"price","value":"200"},{"name":"quantity","value":"2"},{"name":"vat","value":"0"},{"name":"description","value":"Bla bla bla "},{"name":"price","value":"120"},{"name":"quantity","value":"1"},{"name":"vat","value":"13"}]',
    //           'milestonesArray' => [
    //             'name' => [
    //               (int) 0 => 'M1',
    //               (int) 1 => 'M2'
    //             ],
    //             'start_date' => [
    //               (int) 0 => [
    //                 'year' => '2017'
    //               ],
    //               (int) 1 => [
    //                 'month' => '06'
    //               ],
    //               (int) 2 => [
    //                 'day' => '04'
    //               ],
    //               (int) 3 => [
    //                 'year' => '2017'
    //               ],
    //               (int) 4 => [
    //                 'month' => '07'
    //               ],
    //               (int) 5 => [
    //                 'day' => '05'
    //               ]
    //             ],
    //             'end_date' => [
    //               (int) 0 => [
    //                 'year' => '2017'
    //               ],
    //               (int) 1 => [
    //                 'month' => '06'
    //               ],
    //               (int) 2 => [
    //                 'day' => '20'
    //               ],
    //               (int) 3 => [
    //                 'year' => '2018'
    //               ],
    //               (int) 4 => [
    //                 'month' => '08'
    //               ],
    //               (int) 5 => [
    //                 'day' => '11'
    //               ]
    //             ],
    //             'user' => [
    //               (int) 0 => '1,',
    //               (int) 1 => '2,1,'
    //             ]
    //           ]
    //         ];
    //
    //   $this->post('projects/add', $data);
    //   $this->assertResponseSuccess();
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['name' => 'PTest']);
    //   $this->assertEquals(8, $project->toArray()[0]['id']);
    //   $milestones = TableRegistry::get('Milestones');
    //   $milestoneArray = $milestones->find()->where(['project_id' => $project->toArray()[0]['id']]);
    //   $this->assertEquals(1, $milestoneArray->count());
    //   $usersMilestones = TableRegistry::get('MilestonesUsers');
    //   $milestoneUser = $usersMilestones->find()->where(['milestone_id' => $milestoneArray->toArray()[0]['id']]);
    //   $this->assertEquals(1, $milestoneUser->count());
    //   $invoices = TableRegistry::get('Invoices');
    //   $invoice = $invoices->find()->where(['project_id' => $project->toArray()[0]['id']]);
    //   $this->assertEquals(2, $invoice->toArray()[0]['id']);
    //   $itemArray = $itemsInvoices->find()->where(['invoice_id' => $invoice->toArray()[0]['id']]);
    //   $this->assertEquals(2, $itemArray->count());
    // }
    //
    //
    // public function testAddWhitWrongData(){
    //     $this->session([
    //         'Auth' => [
    //             'User' => [
    //                 'id' => 1,
    //                 'username' => 'testing',
    //                 'first_name' => 'Manuel',
    //                 'last_name' => 'Arias',
    //                 'email' => 'manuelarias185@gmail.com',
    //                 'role' => 'R'
    //             ]
    //         ]
    //     ]);
    //   //name, budget and company_id are required values
    //   $data = [
    //       'description' => 'Lorem ipsum dolor sit amet',
    //       'start_date' => ['year' => '2017', 'month' => '08', 'day' => '12'],
    //       'invoiced' => 0,
    //       'company_id' => 1,
    //       'total_cost' => 0,
    //       'end_date' => ['year' => '2017', 'month' => '11', 'day' => '12']
    //   ];
    //
    //   $this->post('projects/add', $data);
    //   $this->assertResponseSuccess();
    //
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['id' => '8']);
    //   $this->assertEquals(0, $project->count());
    //
    // }
    //
    // public function testBadDates(){
    //     $this->session([
    //         'Auth' => [
    //             'User' => [
    //                 'id' => 1,
    //                 'username' => 'testing',
    //                 'first_name' => 'Manuel',
    //                 'last_name' => 'Arias',
    //                 'email' => 'manuelarias185@gmail.com',
    //                 'role' => 'R'
    //             ]
    //         ]
    //     ]);
    //   $data = [
    //       'name' => 'P1',
    //       'description' => 'Lorem ipsum dolor sit amet',
    //       'start_date' => ['year' => '2017', 'month' => '08', 'day' => '12'],
    //       'budget' => 1000,
    //       'invoiced' => 0,
    //       'company_id' => 1,
    //       'total_cost' => 0,
    //       'end_date' => ['year' => '2017', 'month' => '05', 'day' => '12']
    //   ];
    //
    //   $this->post('projects/add', $data);
    //   $this->assertResponseSuccess();
    //
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['id' => '8']);
    //   $this->assertEquals(0, $project->count());
    //
    // }
    //
    // /**
    //  * Test edit method
    //  *
    //  * @return void
    //  */
    // public function testEdit()
    // {
    //   /* Preview record ID -> 2
    //   [
    //       'name' => 'Lorem ipsum dolor sit amet',
    //       'description' => 'Lorem ipsum dolor sit amet',
    //       'start_date' => '2017-08-11',
    //       'budget' => 1,
    //       'invoiced' => 0,
    //       'company_id' => 2,
    //       'total_cost' => 0,
    //       'end_date' => '2017-12-11'
    //   ]
    //   */
    //     $this->session([
    //         'Auth' => [
    //             'User' => [
    //                 'id' => 1,
    //                 'username' => 'testing',
    //                 'first_name' => 'Manuel',
    //                 'last_name' => 'Arias',
    //                 'email' => 'manuelarias185@gmail.com',
    //                 'role' => 'R'
    //             ]
    //         ]
    //     ]);
    //   $data = [
    //       'name' => 'P1',
    //       'description' => 'Lorem ipsum dolor sit amet',
    //       'start_date' => ['year' => '2017', 'month' => '08', 'day' => '12'],
    //       'budget' => 1000,
    //       'invoiced' => 0,
    //       'company_id' => 1,
    //       'total_cost' => 0,
    //       'end_date' => ['year' => '2017', 'month' => '11', 'day' => '12']
    //   ];
    //
    //   $this->put('projects/edit/2', $data);
    //   $this->assertResponseSuccess();
    //
    //   $projects = TableRegistry::get('Projects');
    //   $project = $projects->find()->where(['id' => '2']);
    //   $this->assertEquals('P1', $project->toArray()[0]['name']);
    //   $this->assertEquals(1000, $project->toArray()[0]['budget']);
    //   $this->assertEquals(1, $project->toArray()[0]['company_id']);
    //
    // }
    //
    // /**
    //  * Test delete method
    //  *
    //  * @return void
    //  */
    // public function testDelete()
    // {
    //     $this->session([
    //         'Auth' => [
    //             'User' => [
    //                 'id' => 1,
    //                 'username' => 'testing',
    //                 'first_name' => 'Manuel',
    //                 'last_name' => 'Arias',
    //                 'email' => 'manuelarias185@gmail.com',
    //                 'role' => 'A'
    //             ]
    //         ]
    //     ]);
    //     $this->post('projects/delete/3');
    //     $this->assertResponseSuccess();
    //
    //     $projects = TableRegistry::get('Projects');
    //     $project = $projects->find()->where(['id' => '3']);
    //
    //     $this->assertEquals(0, $project->count());
    // }
    //
    public function testProfiability(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = ['id' => '1'];

      $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

      $this->post('projects/profiability', $data);
      $this->assertResponseSuccess();

      // debug($this->_controller->View->viewVars['project']);
      // die();
      $projectExpected = TableRegistry::get('Projects')->get('1');
      $projectExpected->profiability = 70;
      $projectExpected->earning = 700;
      $projectExpected->dataCosts = ['milestones' => ['M1', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet'], 'costs' => ['230', '0', '0']];

      $this->assertEquals($projectExpected, $this->_controller->View->viewVars['project']);

      unset($_ENV['HTTP_X_REQUESTED_WITH']);
    }

    public function testPerformance(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = ['id' => '1'];

      $this->post('projects/performance', $data);
      $this->assertResponseSuccess();

      $usersTable = TableRegistry::get('Users');
      $user1 = $usersTable->find()->select(['Users.username', 'Users.id', 'Users.first_name', 'Users.last_name'])->autofields(false)->where(['id' => '1'])->toArray();
      $user2 = $usersTable->find()->select(['Users.username', 'Users.id', 'Users.first_name', 'Users.last_name'])->autofields(false)->where(['id' => '2'])->toArray();
      $user3 = $usersTable->find()->select(['Users.username', 'Users.id', 'Users.first_name', 'Users.last_name'])->autofields(false)->where(['id' => '3'])->toArray();

      $performanceInformationExpected = ['0' => ['totalTasks' => 2, 'user' => $user1[0], 'pendingTasks' => 2, 'doneTasks' => 0, 'delayedTasks' => 2, 'completeOnTime' => 0, 'completeAfterTime' => 0, 'tasksForToday' => 0],
                                        '1' => ['totalTasks' => 1, 'user' => $user2[0], 'pendingTasks' => 1, 'doneTasks' => 0, 'delayedTasks' => 1, 'completeOnTime' => 0, 'completeAfterTime' => 0, 'tasksForToday' => 0],
                                        '2' => ['totalTasks' => 2, 'user' => $user3[0], 'pendingTasks' => 1, 'doneTasks' => 1, 'delayedTasks' => 1, 'completeOnTime' => 1, 'completeAfterTime' => 0, 'tasksForToday' => 0]];

      // debug($this->_controller->View->viewVars['performanceInformation']);
      // die();
      $this->assertEquals($performanceInformationExpected, $this->_controller->View->viewVars['performanceInformation']);

    }

    public function testProjectStatusInformation(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';
      $data = ['project_id' => '1', 'end_date' => '2017-09-26'];

      $this->post('projects/getProjectStatusInformation', $data);

      $projectInformationExpected = ['completeMilestones' => 1, 'incompleteMilestones' => 2, 'percentageCompletion' => 6.67, 'remainingDays' => 7, 'pendingTasks' => 4, 'completeTasks' => 1, 'totalTasks' => 5, 'profiability' => 70.00];

      $this->assertEquals($projectInformationExpected, $this->_controller->View->viewVars['projectInformation']);

      unset($_ENV['HTTP_X_REQUESTED_WITH']);
    }
}
