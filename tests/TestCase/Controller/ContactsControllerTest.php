<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ContactsController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\ContactsController Test Case
 */
class ContactsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contacts',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.milestones_users',
        'app.projects',
        'app.permissions',
        'app.permissions_users',
        'app.tags',
        'app.companies_tags',
        'app.roles'
    ];

    public $import = array('model' => 'Contacts', 'records' => false);
    public $dropTables = false;

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
        $this->get('/contacts');

        $this->assertResponseOk();
        $this->assertResponseContains('Contacts');
    }

    public function testIndexData()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('/contacts?page=1');

      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

        $this->get('contacts/view/6');

        $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

        $data = [
            'first_name' => 'Peter',
            'last_name' => 'Lorem ipsum dolor sit amet',
            'email' => 'peter@mail.com',
            'mobile' => 'Lorem ipsum dolor ',
            'gender' => 'H',
            'date_of_birth' => '2017-08-11',
            'country' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'language' => 'EN',
            'company_id' => 3,
            'role_id' => 1
        ];

        $this->post('contacts/add', $data);
        $this->assertResponseSuccess();
        $contacts = TableRegistry::get('Contacts');
        $contact = $contacts->find()->where(['first_name' => 'Peter']);
        $this->assertEquals(7, $contact->toArray()[0]['id']);
    }

    public function testAddWhitWrongData(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);
      //first_name, email, mobile is a required value
      $data = [
          'last_name' => 'Lorem ipsum dolor sit amet',
          'gender' => 'H',
          'date_of_birth' => '2017-08-11',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'Lorem ipsum dolor sit amet',
          'language' => 'EN',
          'company_id' => 1,
          'role_id' => 1
      ];

      $this->post('contacts/add', $data);
      //Post successful
      $this->assertResponseSuccess();
      $contacts = TableRegistry::get('Contacts');
      //No insert into database
      $contact = $contacts->find()->where(['first_name' => 'Peter']);
      $this->assertEquals(0, $contact->count());
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      $data = [
          'first_name' => 'Peter',
          'last_name' => 'Lorem ipsum dolor sit amet',
          'email' => 'peter@mail.com',
          'mobile' => 'Lorem ipsum dolor ',
          'gender' => 'H',
          'date_of_birth' => '2017-08-11',
          'country' => 'Lorem ipsum dolor sit amet',
          'address' => 'Lorem ipsum dolor sit amet',
          'language' => 'EN',
          'company_id' => 1,
          'role_id' => 1
      ];

      $this->put('contacts/edit/3', $data);
      $this->assertResponseSuccess();
      $contacts = TableRegistry::get('Contacts');
      $contact = $contacts->find()->where(['id' => '3']);
      $this->assertEquals('Peter', $contact->toArray()[0]['first_name']);
      $this->assertEquals('peter@mail.com', $contact->toArray()[0]['email']);
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->post('contacts/delete/3');
      $this->assertResponseSuccess();

      $contacts = TableRegistry::get('Contacts');
      $contact = $contacts->find()->where(['id' => '3']);

      $this->assertEquals(0, $contact->count());
    }
}
