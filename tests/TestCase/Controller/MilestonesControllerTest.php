<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MilestonesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\MilestonesController Test Case
 */
class MilestonesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.milestones',
        'app.projects',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones_users',
        'app.contacts',
        'app.roles',
        'app.permissions',
        'app.permissions_users',
        'app.tags',
        'app.companies_tags',
        'app.invoices',
        'app.items'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    // public function testIndex()
    // {
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //
    //     $this->get('milestones/');
    //     $this->assertResponseOk();
    //     $this->assertResponseContains('Milestones');
    // }
    //
    // public function testIndexData()
    // {
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //   $this->get('/milestones?page=1');
    //
    //   $this->assertResponseOk();
    // }
    //
    // /**
    //  * Test view method
    //  *
    //  * @return void
    //  */
    // public function testView()
    // {
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //
    //   $this->get('/milestones/view/3');
    //
    //   $this->assertResponseOk();
    // }
    //
    // /**
    //  * Test add method
    //  *
    //  * @return void
    //  */
    // public function testAddOnlyMilestone()
    // {
    //     $this->session([
    //         'Auth' => [
    //             'User' => [
    //                 'id' => 1,
    //                 'username' => 'testing',
    //                 'role' => 'A'
    //             ]
    //         ]
    //     ]);
    //
    //     $data = [
    //           	'name' => 'M1',
    //           	'start_date' => [
    //           		'year' => '2017',
    //           		'month' => '08',
    //           		'day' => '07'
    //           	],
    //           	'end_date' => [
    //           		'year' => '2017',
    //           		'month' => '08',
    //           		'day' => '23'
    //           	],
    //           	'project_id' => '1',
    //           	'users' => [
    //           		'_ids' => [
    //           			(int) 0 => '1'
    //           		]
    //           	]];
    //
    //   $this->post('milestones/add', $data);
    //   $this->assertResponseSuccess();
    //
    //   $milestones = TableRegistry::get('Milestones');
    //   $milestone = $milestones->find()->where(['name' => 'M1']);
    //
    //   $this->assertEquals(7, $milestone->toArray()[0]['id']);
    //
    // }
    //
    // public function testAddWithTasks(){
    //       $this->session([
    //           'Auth' => [
    //               'User' => [
    //                   'id' => 1,
    //                   'username' => 'testing',
    //                   'role' => 'A'
    //               ]
    //           ]
    //       ]);
    //   $data = [
    //       	'name' => 'M1',
    //       	'project_id' => '1',
    //       	'start_date' => [
    //       		'year' => '2017',
    //       		'month' => '05',
    //       		'day' => '08'
    //       	],
    //       	'end_date' => [
    //       		'year' => '2017',
    //       		'month' => '08',
    //       		'day' => '07'
    //       	],
    //       	'users' => [
    //       		'_ids' => [
    //       			(int) 0 => '1'
    //       		]
    //       	],
    //       	'tasksArray' => [
    //       		'name' => [
    //       			(int) 0 => 'T1',
    //       			(int) 1 => 'T5'
    //       		],
    //       		'description' => [
    //       			(int) 0 => '',
    //       			(int) 1 => ''
    //       		],
    //       		'start_date' => [
    //       			(int) 0 => [
    //       				'year' => '2017'
    //       			],
    //       			(int) 1 => [
    //       				'month' => '06'
    //       			],
    //       			(int) 2 => [
    //       				'day' => '02'
    //       			],
    //       			(int) 3 => [
    //       				'hour' => '08'
    //       			],
    //       			(int) 4 => [
    //       				'minute' => '00'
    //       			],
    //       			(int) 5 => [
    //       				'year' => '2017'
    //       			],
    //       			(int) 6 => [
    //       				'month' => '08'
    //       			],
    //       			(int) 7 => [
    //       				'day' => '04'
    //       			],
    //       			(int) 8 => [
    //       				'hour' => '08'
    //       			],
    //       			(int) 9 => [
    //       				'minute' => '00'
    //       			]
    //       		],
    //       		'duration' => [
    //       			(int) 0 => '2',
    //       			(int) 1 => '2'
    //       		],
    //       		'priority' => [
    //       			(int) 0 => 'A',
    //       			(int) 1 => 'M'
    //       		],
    //       		'task_type' => [
    //       			(int) 0 => '2',
    //       			(int) 1 => '4'
    //       		],
    //       		'user' => [
    //       			(int) 0 => '1',
    //       			(int) 1 => '2'
    //       		]
    //       	]
    //       ];
    //     $this->post('milestones/add', $data);
    //     $this->assertResponseSuccess();
    //
    //     $milestones = TableRegistry::get('Milestones');
    //     $milestone = $milestones->find()->where(['name' => 'M1']);
    //     $this->assertEquals(7, $milestone->toArray()[0]['id']);
    //
    //     $tasks = TableRegistry::get('Tasks');
    //     $taskArray = $tasks->find()->where(['milestone_id' => $milestone->toArray()[0]['id']]);
    //     $this->assertEquals(2, $taskArray->count());
    //
    // }
    //
    // public function testAddWithTasksWrongDates(){
    //       $this->session([
    //           'Auth' => [
    //               'User' => [
    //                   'id' => 1,
    //                   'username' => 'testing',
    //                   'role' => 'A'
    //               ]
    //           ]
    //       ]);
    //   $data = [
    //         'name' => 'M1',
    //         'project_id' => '1',
    //         'start_date' => [
    //           'year' => '2017',
    //           'month' => '05',
    //           'day' => '08'
    //         ],
    //         'end_date' => [
    //           'year' => '2017',
    //           'month' => '08',
    //           'day' => '07'
    //         ],
    //         'users' => [
    //           '_ids' => [
    //             (int) 0 => '1'
    //           ]
    //         ],
    //         'tasksArray' => [
    //           'name' => [
    //             (int) 0 => 'T1',
    //             (int) 1 => 'T5'
    //           ],
    //           'description' => [
    //             (int) 0 => '',
    //             (int) 1 => ''
    //           ],
    //           'start_date' => [
    //             (int) 0 => [
    //               'year' => '2019'
    //             ],
    //             (int) 1 => [
    //               'month' => '06'
    //             ],
    //             (int) 2 => [
    //               'day' => '02'
    //             ],
    //             (int) 3 => [
    //               'hour' => '08'
    //             ],
    //             (int) 4 => [
    //               'minute' => '00'
    //             ],
    //             (int) 5 => [
    //               'year' => '2017'
    //             ],
    //             (int) 6 => [
    //               'month' => '08'
    //             ],
    //             (int) 7 => [
    //               'day' => '04'
    //             ],
    //             (int) 8 => [
    //               'hour' => '08'
    //             ],
    //             (int) 9 => [
    //               'minute' => '00'
    //             ]
    //           ],
    //           'duration' => [
    //             (int) 0 => '2',
    //             (int) 1 => '2'
    //           ],
    //           'priority' => [
    //             (int) 0 => 'A',
    //             (int) 1 => 'M'
    //           ],
    //           'task_type' => [
    //             (int) 0 => '2',
    //             (int) 1 => '4'
    //           ],
    //           'user' => [
    //             (int) 0 => '1',
    //             (int) 1 => '2'
    //           ]
    //         ]
    //       ];
    //     $this->post('milestones/add', $data);
    //     $this->assertResponseSuccess();
    //
    //     $milestones = TableRegistry::get('Milestones');
    //     $milestone = $milestones->find()->where(['name' => 'M1']);
    //     $this->assertEquals(7, $milestone->toArray()[0]['id']);
    //
    //     $tasks = TableRegistry::get('Tasks');
    //     $taskArray = $tasks->find()->where(['milestone_id' => $milestone->toArray()[0]['id']]);
    //     $this->assertEquals(1, $taskArray->count());
    //
    // }
    //
    // /**
    //  * Test edit method
    //  *
    //  * @return void
    //  */
    // public function testEdit()
    // {
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //
    //   /* Preview record id -> 4
    //   [
    //       'name' => 'Lorem ipsum dolor sit amet',
    //       'start_date' => '2017-08-11',
    //       'end_date' => '2017-08-11',
    //       'project_id' => 4
    //   ]*/
    //
    //   $data = [
    //       'name' => 'TaskTest',
    //       'start_date' => [
    //         'year' => '2017',
    //         'month' => '08',
    //         'day' => '07'
    //       ],
    //       'end_date' => [
    //         'year' => '2017',
    //         'month' => '08',
    //         'day' => '23'
    //       ],
    //       'project_id' => 4
    //   ];
    //     $this->put('milestones/edit/4', $data);
    //     $this->assertResponseSuccess();
    //
    //     $milestones = TableRegistry::get('Milestones');
    //     $milestone = $milestones->find()->where(['id' => '4']);
    //     $this->assertEquals('TaskTest', $milestone->toArray()[0]['name']);
    //
    // }
    //
    // public function testBadDates(){
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //   $data = [
    //           'name' => 'M1',
    //           'start_date' => [
    //             'year' => '2017',
    //             'month' => '08',
    //             'day' => '07'
    //           ],
    //           'end_date' => [
    //             'year' => '2015',
    //             'month' => '08',
    //             'day' => '23'
    //           ],
    //           'project_id' => '1',
    //           'users' => [
    //             '_ids' => [
    //               (int) 0 => '1'
    //             ]
    //           ]];
    //
    //   $this->post('milestones/add', $data);
    //   $this->assertResponseSuccess();
    //
    //   $milestones = TableRegistry::get('Milestones');
    //   $milestone = $milestones->find()->where(['id' => '7']);
    //   $this->assertEquals(0, $milestone->count());
    //
    // }
    //
    // /**
    //  * Test delete method
    //  *
    //  * @return void
    //  */
    // public function testDelete()
    // {
    //   $this->session([
    //       'Auth' => [
    //           'User' => [
    //               'id' => 1,
    //               'username' => 'testing',
    //               'role' => 'A'
    //           ]
    //       ]
    //   ]);
    //     $this->post('milestones/delete/2');
    //     $this->assertResponseSuccess();
    //
    //     $milestones = TableRegistry::get('Milestones');
    //     $milestone = $milestones->find()->where(['id' => '2']);
    //     $this->assertEquals(0, $milestone->count());
    // }

      // public function testFinishMilestones(){
      //   $this->session([
      //       'Auth' => [
      //           'User' => [
      //               'id' => 1,
      //               'username' => 'testing',
      //               'first_name' => 'Manuel',
      //               'last_name' => 'Arias',
      //               'email' => 'manuelarias185@gmail.com',
      //               'role' => 'A'
      //           ]
      //       ]
      //   ]);
      //
      //   $data = [
      //     'id' => '3',
      //     'end_date' => date('Y-m-d H:i')
      //   ];
      //
      //   $this->post('milestones/finishMilestone', $data);
      //   $this->assertResponseSuccess();
      //
      //   $milestones = TableRegistry::get('Milestones');
      //   $milestone = $milestones->find()->where(['id' => '3'])->toArray();
      //   $this->assertEquals('C', $milestone[0]['state']);
      // }

      public function testGetMilestonesProject(){
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 7,
                    'username' => 'testing',
                    'first_name' => 'Manuel',
                    'last_name' => 'Arias',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'A'
                ]
            ]
        ]);

        $_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest';

        $data = ['id' => '1'];

        $this->post('milestones/getMilestonesProject', $data);
        $this->assertResponseSuccess();

        $milestonesExpected = TableRegistry::get('Milestones')->find()->where(['project_id' => '1']);

        $this->assertEquals($milestonesExpected, $this->_controller->View->viewVars['milestones']);
        unset($_SERVER['HTTP_X_REQUESTED_WITH']);
      }
}
