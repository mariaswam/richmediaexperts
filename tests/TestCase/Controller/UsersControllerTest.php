<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\UsersController Test Case
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.permissions',
        'app.permissions_users',
        'app.milestones_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('users');
      $this->assertResponseOk();
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A'
              ]
          ]
      ]);

      $this->get('users/view/2');
      $this->assertResponseOk();
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'first_name' => 'Manuel',
                    'last_name' => 'Arias',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'A',
                    'organization_id' => '1'
                ]
            ]
        ]);

        $data = [
              	'username' => 'Plop',
              	'first_name' => 'Us',
              	'last_name' => 'U',
              	'email' => 'manuelarias185@gmail.com',
              	'role' => 'R',
              	'hour_cost' => '120'
      ];

      $this->post('users/add', $data);
      $this->assertResponseSuccess();

      $users = TableRegistry::get('Users');
      $user = $users->find()->where(['id' => '3']);
      $usersPermissions = TableRegistry::get('PermissionsUsers');
      $permissions = $usersPermissions->find()->where(['user_id' => '3']);

      $this->assertEquals(1, $user->count());
      //User is a resource. Default permissions are Milestones, Tasks, Timesheets
      $this->assertEquals(3, $permissions->count());
  }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        /*
          [
              'username' => 'Spiderman',
              'first_name' => 'Peter',
              'last_name' => 'Parker',
              'email' => 'manuelarias185@gmail.com',
              'role' => 'A',
              'password' => 'Lorem ipsum dolor sit amet',
              'hour_cost' => 50,
              'organization_id' => 1,
              'first_login' => 1,
              'password_reset_token' => 'Lorem ipsum dolor sit amet',
              'hashval' => 'Lorem ipsum dolor sit amet'
          ]
        */

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    'username' => 'testing',
                    'first_name' => 'Manuel',
                    'last_name' => 'Arias',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'A',
                    'organization_id' => '1'
                ]
            ]
        ]);

        $data = [
                'username' => 'Plop',
                'first_name' => 'Us',
                'last_name' => 'U',
                'email' => 'manuelarias185@gmail.com',
                'role' => 'R',
                'hour_cost' => '120'
        ];

        $this->post('users/edit/1', $data);
        $this->assertResponseSuccess();

        $users = TableRegistry::get('Users');
        $user = $users->find()->where(['id' => '1']);

        $this->assertEquals('Plop', $user->toArray()[0]['username']);

    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A',
                  'organization_id' => '1'
              ]
          ]
      ]);

      $this->post('users/delete/1');
      $this->assertResponseSuccess();

      $users = TableRegistry::get('Users');
      $user = $users->find()->where(['id' => '1']);

      $this->assertEquals(0, $user->count());
    }

    public function testRegister(){
      $data = [
            	'username' => 'superwdpm',
            	'first_name' => 'WDPM',
            	'last_name' => 'WDPM',
            	'email' => 'manuelarias185@gmail.com',
            	'organization_name' => 'Patito Feliz',
            	'password' => 'q1w2e3r4',
            	'confirm_password' => 'q1w2e3r4'
      ];

      $this->post('users/register', $data);
      $this->assertResponseSuccess();

      $users = TableRegistry::get('Users');
      $organizations = TableRegistry::get('Organizations');
      $permissionsUsers = TableRegistry::get('PermissionsUsers');
      $user = $users->find()->where(['username' => 'superwdpm']);
      $permissions = $permissionsUsers->find()->where(['user_id' => $user->toArray()[0]['id']]);
      $organizations->deleteAll(['id' => $user->toArray()[0]['organization_id']]);
      $organization = $organizations->find()->where(['name' => 'Patito Feliz']);
      $this->assertEquals(1, $user->count());
      $this->assertEquals(1, $organization->count());
      $this->assertEquals(8, $permissions->count());
    }

    public function testLoginSuccess(){
      $data = [
                  'username' => 'Spiderman',
                  'first_name' => 'Peter',
                  'last_name' => 'Parker',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A',
                  'password' => 'q1w2e3r4',
                  'hour_cost' => 50,
                  'organization_id' => 1,
                  'first_login' => 1,
                  'password_reset_token' => 'Lorem ipsum dolor sit amet',
                  'hashval' => 'Lorem ipsum dolor sit amet'
              ];
      $sessionExpected = [
        'id' => 1,
        'username' => 'Spiderman',
        'first_name' => 'Peter',
        'last_name' => 'Parker',
        'email' => 'manuelarias185@gmail.com',
        'role' => 'A',
        'hour_cost' => 50,
        'organization_id' => 1,
        'first_login' => 1,
        'password_reset_token' => 'Lorem ipsum dolor sit amet',
        'hashval' => 'Lorem ipsum dolor sit amet'
      ];

      $this->post('/users/login', $data);
      $this->assertSession($sessionExpected, 'Auth.User');

    }

    public function testLoginFailed(){
      $data = [
                  //Username: Spiderman Password: q1w2e3r4
                  'username' => 'Spiderman',
                  'first_name' => 'Peter',
                  'last_name' => 'Parker',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A',
                  'password' => '123456',
                  'hour_cost' => 50,
                  'organization_id' => 1,
                  'first_login' => 1,
                  'password_reset_token' => 'Lorem ipsum dolor sit amet',
                  'hashval' => 'Lorem ipsum dolor sit amet'
              ];
      $sessionExpected = [
        'id' => 1,
        'username' => 'Spiderman',
        'first_name' => 'Peter',
        'last_name' => 'Parker',
        'email' => 'manuelarias185@gmail.com',
        'role' => 'A',
        'hour_cost' => 50,
        'organization_id' => 1,
        'first_login' => 1,
        'password_reset_token' => 'Lorem ipsum dolor sit amet',
        'hashval' => 'Lorem ipsum dolor sit amet'
      ];

      $this->post('/users/login', $data);
      $this->assertResponseContains('El nombre y/o contraseña son inválidos');

    }

    public function testAccountSettings(){
      /*
        [
            'username' => 'Spiderman',
            'first_name' => 'Peter',
            'last_name' => 'Parker',
            'email' => 'manuelarias185@gmail.com',
            'role' => 'A',
            'password' => 'Lorem ipsum dolor sit amet',
            'hour_cost' => 50,
            'organization_id' => 1,
            'first_login' => 1,
            'password_reset_token' => 'Lorem ipsum dolor sit amet',
            'hashval' => 'Lorem ipsum dolor sit amet'
        ]
      */

      $this->session([
          'Auth' => [
              'User' => [
                    'id' => 1,
                    'username' => 'Spiderman',
                    'first_name' => 'Peter',
                    'last_name' => 'Parker',
                    'email' => 'manuelarias185@gmail.com',
                    'role' => 'A',
                    'hour_cost' => 50,
                    'organization_id' => 1,
                    'first_login' => 1,
                    'password_reset_token' => 'Lorem ipsum dolor sit amet',
                    'hashval' => 'Lorem ipsum dolor sit amet'
              ]
          ]
      ]);

      $data = [
              'username' => 'Plop',
              'first_name' => 'Us',
              'last_name' => 'U',
              'email' => 'manuelarias185@gmail.com',
              'role' => 'R',
              'hour_cost' => '120'
      ];

      $this->post('users/account-settings', $data);
      $this->assertResponseSuccess();

      $users = TableRegistry::get('Users');
      $user = $users->find()->where(['id' => '1']);

      $this->assertEquals('Plop', $user->toArray()[0]['username']);
    }

    public function testPermissionsUser(){
      $this->session([
          'Auth' => [
              'User' => [
                  'id' => 1,
                  'username' => 'testing',
                  'first_name' => 'Manuel',
                  'last_name' => 'Arias',
                  'email' => 'manuelarias185@gmail.com',
                  'role' => 'A',
                  'organization_id' => '1'
              ]
          ]
      ]);

      $this->configRequest([
        'headers' => ['Accept' => 'application/json']
      ]);

      $data = [
          	'user_id' => '1',
          	'permissions' => [
          		(int) 0 => [
          			'module' => '1',
          			'permission' => 'true'
          		],
          		(int) 1 => [
          			'module' => '2',
          			'permission' => 'false'
          		],
          		(int) 2 => [
          			'module' => '3',
          			'permission' => 'true'
          		],
          		(int) 3 => [
          			'module' => '4',
          			'permission' => 'true'
          		],
          		(int) 4 => [
          			'module' => '5',
          			'permission' => 'false'
          		],
          		(int) 5 => [
          			'module' => '6',
          			'permission' => 'true'
          		],
          		(int) 6 => [
          			'module' => '7',
          			'permission' => 'false'
          		],
          		(int) 7 => [
          			'module' => '8',
          			'permission' => 'false'
          		]
          	]
          ];
        $this->_request = ['isAjax' => true];
        // $this->post('/users/permissions', $data);
        // $this->_sendRequest('users/per');
        $this->assertResponseSuccess();
        $this->assertEquals('Puta', $this->_response->body());


    }
}
