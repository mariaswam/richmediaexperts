<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuotesItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuotesItemsTable Test Case
 */
class QuotesItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QuotesItemsTable
     */
    public $QuotesItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.quotes_items',
        'app.quotes',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.projects',
        'app.invoices',
        'app.items',
        'app.milestones',
        'app.milestones_users',
        'app.task_types',
        'app.timesheets',
        'app.organizations',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.companies_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QuotesItems') ? [] : ['className' => QuotesItemsTable::class];
        $this->QuotesItems = TableRegistry::get('QuotesItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuotesItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
