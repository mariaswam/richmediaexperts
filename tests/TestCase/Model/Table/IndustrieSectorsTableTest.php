<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IndustrieSectorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IndustrieSectorsTable Test Case
 */
class IndustrieSectorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IndustrieSectorsTable
     */
    public $IndustrieSectors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.industrie_sectors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IndustrieSectors') ? [] : ['className' => IndustrieSectorsTable::class];
        $this->IndustrieSectors = TableRegistry::get('IndustrieSectors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IndustrieSectors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
