<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompaniesTagsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompaniesTagsTable Test Case
 */
class CompaniesTagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompaniesTagsTable
     */
    public $CompaniesTags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.companies_tags',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.milestones',
        'app.milestones_users',
        'app.contacts',
        'app.projects',
        'app.tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompaniesTags') ? [] : ['className' => CompaniesTagsTable::class];
        $this->CompaniesTags = TableRegistry::get('CompaniesTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompaniesTags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
