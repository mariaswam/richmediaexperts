<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentTermsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentTermsTable Test Case
 */
class PaymentTermsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentTermsTable
     */
    public $PaymentTerms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.payment_terms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentTerms') ? [] : ['className' => PaymentTermsTable::class];
        $this->PaymentTerms = TableRegistry::get('PaymentTerms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentTerms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
