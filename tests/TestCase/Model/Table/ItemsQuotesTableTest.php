<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ItemsQuotesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ItemsQuotesTable Test Case
 */
class ItemsQuotesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ItemsQuotesTable
     */
    public $ItemsQuotes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.items_quotes',
        'app.quotes',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.projects',
        'app.invoices',
        'app.items',
        'app.milestones',
        'app.milestones_users',
        'app.task_types',
        'app.timesheets',
        'app.organizations',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.quotes_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ItemsQuotes') ? [] : ['className' => ItemsQuotesTable::class];
        $this->ItemsQuotes = TableRegistry::get('ItemsQuotes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ItemsQuotes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
