<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MilestonesUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MilestonesUsersTable Test Case
 */
class MilestonesUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MilestonesUsersTable
     */
    public $MilestonesUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.milestones_users',
        'app.milestones',
        'app.projects',
        'app.companies',
        'app.users',
        'app.tasks',
        'app.contacts',
        'app.roles',
        'app.tags',
        'app.companies_tags',
        'app.invoices',
        'app.items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MilestonesUsers') ? [] : ['className' => MilestonesUsersTable::class];
        $this->MilestonesUsers = TableRegistry::get('MilestonesUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MilestonesUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
