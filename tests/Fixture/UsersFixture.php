<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'username' => ['type' => 'string', 'length' => 30, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'first_name' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'last_name' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'email' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'role' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'hour_cost' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'organization_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'first_login' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'password_reset_token' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'hashval' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'fk_users_organizations1_idx' => ['type' => 'index', 'columns' => ['organization_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'username_UNIQUE' => ['type' => 'unique', 'columns' => ['username'], 'length' => []],
            'fk_users_organizations1' => ['type' => 'foreign', 'columns' => ['organization_id'], 'references' => ['organizations', 'id'], 'update' => 'noAction', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'username' => 'Spiderman',
            'first_name' => 'Peter',
            'last_name' => 'Parker',
            'email' => 'manuelarias185@gmail.com',
            'role' => 'A',
            'password' => '$2y$10$65awW08rYV2w6FJFLGqknuztX9PgzHdUCH2wBE/QtopT2moQzgAnO',
            'hour_cost' => 50,
            'organization_id' => 1,
            'first_login' => 1,
            'password_reset_token' => 'Lorem ipsum dolor sit amet',
            'hashval' => 'Lorem ipsum dolor sit amet'
        ],
        [
            'username' => 'Batman',
            'first_name' => 'Bruce',
            'last_name' => 'Wayne',
            'email' => 'a.arias-12@hotmail.com',
            'role' => 'A',
            'password' => 'Lorem ipsum dolor sit amet',
            'hour_cost' => 20,
            'organization_id' => 1,
            'first_login' => 1,
            'password_reset_token' => 'Lorem ipsum dolor sit amet',
            'hashval' => 'Lorem ipsum dolor sit amet'
        ],
        [
            'username' => 'BatmanBeyond',
            'first_name' => 'Bruce',
            'last_name' => 'Wayne',
            'email' => 'manuel.ariasalfaro@hotmail.com',
            'role' => 'A',
            'password' => 'Lorem ipsum dolor sit amet',
            'hour_cost' => 20,
            'organization_id' => 1,
            'first_login' => 1,
            'password_reset_token' => 'Lorem ipsum dolor sit amet',
            'hashval' => 'Lorem ipsum dolor sit amet'
        ]
    ];
}
