<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TasksFixture
 *
 */
class TasksFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'start_date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'duration' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'priority' => ['type' => 'string', 'fixed' => true, 'length' => 1, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'milestone_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'task_type_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'end_date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'state' => ['type' => 'string', 'fixed' => true, 'length' => 5, 'null' => true, 'default' => 'P', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Tasks_Users1_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'fk_Tasks_Milestones1_idx' => ['type' => 'index', 'columns' => ['milestone_id'], 'length' => []],
            'fk_tasks_task_types1_idx' => ['type' => 'index', 'columns' => ['task_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_Tasks_Milestones1' => ['type' => 'foreign', 'columns' => ['milestone_id'], 'references' => ['milestones', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_Tasks_Users1' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'setNull', 'length' => []],
            'fk_tasks_task_types1' => ['type' => 'foreign', 'columns' => ['task_type_id'], 'references' => ['task_types', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 12:00:00',
            'duration' => 2,
            'priority' => 'A',
            'user_id' => 1,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 14:00:00',
            'state' => 'P'
        ],
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 10:48:55',
            'duration' => 1,
            'priority' => 'Lorem ipsum dolor sit ame',
            'user_id' => 1,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 10:48:55',
            'state' => 'P'
        ],
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 10:48:55',
            'duration' => 2,
            'priority' => 'Lorem ipsum dolor sit ame',
            'user_id' => 3,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 10:48:55',
            'state' => 'TA'
        ],
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 10:48:55',
            'duration' => 1,
            'priority' => 'Lorem ipsum dolor sit ame',
            'user_id' => 2,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 10:48:55',
            'state' => 'P'
        ],
        [
            'name' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27 10:48:55',
            'duration' => 1,
            'priority' => 'Lorem ipsum dolor sit ame',
            'user_id' => 3,
            'milestone_id' => 1,
            'task_type_id' => 1,
            'end_date' => '2017-09-27 10:48:55',
            'state' => 'P'
        ]
    ];
}
