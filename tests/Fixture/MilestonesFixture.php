<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MilestonesFixture
 *
 */
class MilestonesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 40, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'start_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'end_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'project_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'state' => ['type' => 'string', 'fixed' => true, 'length' => 5, 'null' => true, 'default' => 'O', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Milestones_Projects1_idx' => ['type' => 'index', 'columns' => ['project_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_Milestones_Projects1' => ['type' => 'foreign', 'columns' => ['project_id'], 'references' => ['projects', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'M1',
            'start_date' => '2017-09-17',
            'end_date' => '2017-10-01',
            'project_id' => 1,
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-06-27',
            'end_date' => '2017-10-27',
            'project_id' => 1,
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27',
            'end_date' => '2017-09-27',
            'project_id' => 3,
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27',
            'end_date' => '2017-09-27',
            'project_id' => 4,
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-27',
            'end_date' => '2017-09-27',
            'project_id' => 1,
            'state' => 'C'
        ]
    ];
}
