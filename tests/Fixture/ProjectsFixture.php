<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProjectsFixture
 *
 */
class ProjectsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'name' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'start_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'budget' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'invoiced' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'company_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'total_cost' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'end_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'state' => ['type' => 'string', 'fixed' => true, 'length' => 5, 'null' => true, 'default' => 'O', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Projects_Companies1_idx' => ['type' => 'index', 'columns' => ['company_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_Projects_Companies1' => ['type' => 'foreign', 'columns' => ['company_id'], 'references' => ['companies', 'id'], 'update' => 'noAction', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-04-26',
            'budget' => 1000,
            'invoiced' => 1,
            'company_id' => 1,
            'total_cost' => 300,
            'end_date' => '2017-09-26',
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-12',
            'budget' => 1,
            'invoiced' => 1,
            'company_id' => 1,
            'total_cost' => 1,
            'end_date' => '2017-11-26',
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-12-26',
            'budget' => 1,
            'invoiced' => 1,
            'company_id' => 1,
            'total_cost' => 1,
            'end_date' => '2018-09-26',
            'state' => 'C'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-03-26',
            'budget' => 1,
            'invoiced' => 0,
            'company_id' => 1,
            'total_cost' => 1,
            'end_date' => '2017-05-26',
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-02-12',
            'budget' => 1,
            'invoiced' => 1,
            'company_id' => 1,
            'total_cost' => 1,
            'end_date' => '2017-05-23',
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-26',
            'budget' => 1,
            'invoiced' => 1,
            'company_id' => 1,
            'total_cost' => 1,
            'end_date' => '2017-12-26',
            'state' => 'O'
        ],
        [
            'name' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-02-26',
            'budget' => 1,
            'invoiced' => 0,
            'company_id' => 1,
            'total_cost' => 100,
            'end_date' => '2017-09-26',
            'state' => 'O'
        ]
    ];
}
