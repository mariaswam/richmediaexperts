<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TimesheetsFixture
 *
 */
class TimesheetsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'description' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'start_date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'duration' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'end_date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'task_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'performed_activity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_Timesheets_Tasks1_idx' => ['type' => 'index', 'columns' => ['task_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_Timesheets_Tasks1' => ['type' => 'foreign', 'columns' => ['task_id'], 'references' => ['tasks', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'description' => 'Prueba',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 1,
            'performed_activity' => 1
        ],
        [
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 2,
            'performed_activity' => 1
        ],
        [
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 3,
            'performed_activity' => 1
        ],
        [
            'description' => 'Lorem ipsum dolor sit amet',
            'start_date' => '2017-09-28 10:16:00',
            'duration' => '10:16:00',
            'end_date' => '2017-09-28 10:16:00',
            'task_id' => 2,
            'performed_activity' => 1
        ]
    ];
}
