<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;
use Cake\I18n\Time;
use Cake\Routing\Router;
use Cake\I18n\FrozenTime;

/**
 * Tasks shell command.
 */
class TasksShell extends Shell
{

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function verifyDelayedTasks(){
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->select(['Tasks.user_id', 'Tasks.name', 'Tasks.end_date'])
                                          ->autofields(false)
                                          ->where(['Tasks.end_date <' => date('Y-m-d') ])
                                          ->contain(['Users' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Users.email', 'Users.id'])->autofields(false);
                                          }]);
      if ($tasks->count() != 0) {
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2><br>Existen tareas cuya fecha de fin es hoy. <a href='".Router::url('/', true )."tasks'>¡Por favor revísalo!</a>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Tareas pendientes', $to, $content);
        $array_users = array();
        $messageToDisplay = "<h2>¡Hola!</h2>Las siguientes tareas su fecha de entrega ha sido sobrepasada y aún no han sido marcadas como concluidas:";

        foreach($tasks as $task) {
          if (!isset($array_users[$task->user->email])) {
            $array_users[$task->user->email] = "<li>".$task->name.' - Fecha de entrega: '.$task->end_date->format('d/m/Y H:i').'</li>';
          }
          else {
            $array_users[$task->user->email] .= "<li>".$task->name.' - Fecha de entrega: '.$task->end_date->format('d/m/Y H:i').'</li>';
          }
        }

        foreach ($array_users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Tasks', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);

      }
    }

    public function verifyTasksForToday(){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->select(['Tasks.user_id', 'Tasks.name', 'Tasks.end_date'])
                                          ->autofields(false)
                                          ->where(['Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'Tasks.end_date >=' => $today->format('Y-m-d H:i:s'), 'OR' => [['Tasks.state' => 'P'], ['Tasks.state' => 'PA']]])
                                          ->contain(['Users' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Users.email', 'Users.id'])->autofields(false);
                                          }]);

      if($tasks->count()!=0){
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2><br>Existen tareas cuya fecha de fin es hoy. <a href='".Router::url('/', true )."tasks'>¡Por favor revísalo!</a>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Tareas del día', $to, $content);
        $array_users = array();
        foreach($tasks as $task) {
          if (!isset($array_users[$task->user->email])) {
            $array_users[$task->user->email] = "<li>".$task->name.' - Hora de entrega: '.$task->end_date->format('H:i').'</li>';
          }
          else {
            $array_users[$task->user->email] .= "<li>".$task->name.' - Hora de entrega: '.$task->end_date->format('H:i').'</li>';
          }
        }
        $messageToDisplay = "<h2>¡Hola!</h2>Las tareas cuya fecha de fin es hoy son las siguientes:";
        foreach ($array_users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Tasks', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }

}
