<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Routing\Router;

/**
 * Projects shell command.
 */
class ProjectsShell extends Shell
{
      public function initialize(){
          parent::initialize();
          $this->loadModel('Projects');
      }
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function verifyProjectsForToday(){
      $projects = $this->Projects->find()->select(['Projects.end_date', 'Projects.name'])
                                        ->autofields(false)
                                        ->where(['Projects.end_date' => date('Y-m-d'), 'Projects.state' => 'O'])
                                        ->contain(['Companies' => function(\Cake\ORM\Query $q){
                                          return $q->select(['Companies.user_id', 'Companies.name'])
                                                    ->autofields(false)
                                                    ->contain(['Users'=> function(\Cake\ORM\Query $q){
                                                      return $q->select(['Users.email', 'Users.id'])->autofields(false);
                                                    }]);
                                        }]);

      if($projects->count() != 0){
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Proyectos del día', $to, $content);
        $users = array();
        $messageToDisplay = "<h2>¡Hola!</h2>Los siguientes proyectos deben ser entregados el día de hoy:";

        foreach ($projects as $project) {
          if (!isset($users[$project->company->user->email])) {
            $users[$project->company->user->email] = "<li>".$project->name.' - Compañía: '.$project->company->name.'</li>';
          }
          else {
            $users[$project->company->user->email] .= "<li>".$project->name.' - Compañía: '.$project->company->name.'</li>';
          }
        }

        foreach ($users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Projects', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }

        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }

    public function verifyDelayedProjects(){
      $projects = $this->Projects->find()->select(['Projects.end_date', 'Projects.name'])
                                        ->autofields(false)
                                        ->where(['Projects.end_date <' => date('Y-m-d'), 'Projects.state' => 'O'])
                                        ->contain(['Companies' => function(\Cake\ORM\Query $q){
                                          return $q->select(['Companies.user_id', 'Companies.name'])
                                                    ->autofields(false)
                                                    ->contain(['Users'=> function(\Cake\ORM\Query $q){
                                                      return $q->select(['Users.email', 'Users.id'])->autofields(false);
                                                    }]);
                                        }]);

      if($projects->count() != 0){
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Proyectos atrasados', $to, $content);
        $users = array();
        $messageToDisplay = "<h2>¡Hola!</h2>Los siguientes proyectos su fecha de entrega ha sido sobrepasada y aún permanecen sin entregar o cerrar:";

        foreach ($projects as $project) {
          if (!isset($users[$project->company->user->email])) {
            $users[$project->company->user->email] = "<li>".$project->name.' - Compañía: '.$project->company->name.'</li>';
          }
          else {
            $users[$project->company->user->email] .= "<li>".$project->name.' - Compañía: '.$project->company->name.'</li>';
          }
        }

        foreach ($users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Projects', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }

        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }
}
