<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;
use Cake\Routing\Router;

/**
 * Milestones shell command.
 */
class MilestonesShell extends Shell
{

    public function initialize(){
        parent::initialize();
        $this->loadModel('MilestonesUsers');
    }
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function verifyDelayedMilestones(){
      $milestonesUsers = $this->MilestonesUsers->find()
                                          ->contain(['Milestones' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Milestones.id', 'Milestones.name', 'Milestones.end_date'])
                                                      ->autofields(false)
                                                      ->where(['Milestones.end_date <' => date('Y-m-d'), 'Milestones.state' => 'O'])
                                                      ->contain(['Projects'=> function(\Cake\ORM\Query $q){
                                                          return $q->select(['Projects.name'])->autofields(false);
                                                      }]);
                                          }, 'Users' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Users.id', 'Users.email'])->autofields(false);
                                          }]);
      if ($milestonesUsers->count() != 0) {
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Milestones pendientes', $to, $content);
        $array_users = array();
        $messageToDisplay = "<h2>¡Hola!</h2>Los siguientes milestones su fecha de entrega ha sido sobrepasada y aún no han sido marcados como concluidos:";

        foreach($milestonesUsers as $milestones) {
          if (!isset($array_users[$milestones->user->email])) {
            $array_users[$milestones->user->email] = "<li>".$milestones->milestone->name.' - Fecha de entrega: '.(($milestones->milestone->end_date != null) ? $milestones->milestone->end_date->format('d/m/Y'): 'no-date')." - Proyecto: ".$milestones->milestone->project->name."</li>";
          }
          else {
            $array_users[$milestones->user->email] .= "<li>".$milestones->milestone->name.' - Fecha de entrega: '.(($milestones->milestone->end_date != null) ? $milestones->milestone->end_date->format('d/m/Y'): 'no-date')." - Proyecto: ".$milestones->milestone->project->name."</li>";
          }
        }

        foreach ($array_users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Milestones', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }

    public function verifyMilestonesForToday(){
      $milestonesUsers = $this->MilestonesUsers->find()
                                          ->contain(['Milestones' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Milestones.id', 'Milestones.name', 'Milestones.end_date'])
                                                      ->autofields(false)
                                                      ->where(['Milestones.end_date' => date('Y-m-d'), 'Milestones.state' => 'O'])
                                                      ->contain(['Projects'=> function(\Cake\ORM\Query $q){
                                                          return $q->select(['Projects.name'])->autofields(false);
                                                      }]);
                                          }, 'Users' => function(\Cake\ORM\Query $q){
                                            return $q->select(['Users.id', 'Users.email'])->autofields(false);
                                          }]);

      if($milestonesUsers->count()!= 0){
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $message = "<h2>¡Hola!</h2>";
        $content = new \SendGrid\Content("text/plain", $message);
        $mail = new \SendGrid\Mail($from, 'Milestones del día', $to, $content);
        $array_users = array();
        $messageToDisplay = "<h2>¡Hola!</h2>Los siguientes milestones deben ser concluidos el día de hoy:";

        foreach($milestonesUsers as $milestones) {
          if (!isset($array_users[$milestones->user->email])) {
            $array_users[$milestones->user->email] = "<li>".$milestones->milestone->name." - Proyecto: ".$milestones->milestone->project->name."</li>";
          }
          else {
            $array_users[$milestones->user->email] .= "<li>".$milestones->milestone->name." - Proyecto: ".$milestones->milestone->project->name."</li>";
          }
        }

        foreach ($array_users as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Milestones', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }
}
