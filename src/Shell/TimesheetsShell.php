<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Timesheets shell command.
 */
class TimesheetsShell extends Shell
{

    public function initialize(){
        parent::initialize();
        $this->loadModel('Timesheets');
    }

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function verifyTimesheetsOfWeek(){
      //Get the current week
      $monday = strtotime("last monday");
      $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
      $week = array();
      array_push($week, date("Y-m-d",$monday));
      for ($i=1; $i <= 4 ; $i++) {
        $day = strtotime(date("Y-m-d",$monday)." +".$i." days");
        array_push($week, date("Y-m-d",$day));
      }
      $friday = strtotime(date("Y-m-d",$monday)." +4 days");
      $this_week_sd = date("Y-m-d",$monday);
      $this_week_ed = date("Y-m-d",$friday);
      $users = TableRegistry::get('Users')->find()->select(['Users.id', 'Users.email'])->autofields(false);

      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email(null, 'wdpminfo@gmail.com'); //What email?
      $message = "<h2>¡Hola!</h2>";
      $content = new \SendGrid\Content("text/plain", $message);
      $mail = new \SendGrid\Mail($from, 'Horas de trabajo sin llenar', $to, $content);

      foreach ($users as $user) {
        $userID = $user->id;
        $timesheets = $this->Timesheets->find()->contain(['Tasks' => function(\Cake\ORM\Query $q) use($userID){
                                                            return $q->select(['Tasks.id'])
                                                                      ->autofields(false)
                                                                      ->contain(['Users'=> function(\Cake\ORM\Query $q) use($userID){
                                                                          return $q->select()->where(['Users.id' => $userID]);
                                                                      }]);
                                                          }])->where(['Timesheets.start_date >= ' => $this_week_sd, 'Timesheets.start_date <=' => $this_week_ed]);
        if($timesheets->count() != 0){
          $completedDays = [false, false, false, false, false];
          foreach ($timesheets as $timesheet) {
            for ($i=0; $i < count($week); $i++) {
              if ($timesheet->start_date == $week[$i] && $completedDays[$i] == false) {
                $completedDays[$i] = true;
              }
            }
          }
          $days = 0;
          for ($i=0; $i < count($completedDays); $i++) {
            if ($completedDays[$i] == false) {
              $days++;
            }
          }
          if($days != 0){
            $personalization = new \SendGrid\Personalization();
            $email = new \SendGrid\Email(null, $user->email);
            $personalization->addTo($email);
            $personalization->addSubstitution('%html_content%', '<p>Parece que no has llenado todas las horas de trabajo de esta semana.</p><br> <p>¡Recorda hacerlo!</p>');
            $mail->addPersonalization($personalization);
          }
        }
        else{
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $user->email);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', '<p>Parece que no has llenado las horas de trabajo de esta semana.</p><br> <p>¡Recorda hacerlo!</p>');
          $mail->addPersonalization($personalization);
        }
      }
      $template = file_get_contents(WWW_ROOT.'reminder_templates/reminder_timesheets.html');
      $content = new \SendGrid\Content("text/html", $template);
      $mail->addContent($content);

      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
      debug($response);
      die();
    }
}
