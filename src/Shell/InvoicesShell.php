<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;
use Cake\Routing\Router;

/**
 * Invoices shell command.
 */
class InvoicesShell extends Shell
{

  public function initialize()
    {
        parent::initialize();
        $this->loadModel('Invoices');
    }

    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out($this->OptionParser->help());
    }

    public function verifyDueDatesInvoices(){
      $invoices = $this->Invoices->find()
                                  ->where(['due_date <' => date('Y-m-d'), 'state' => 'P'])
                                  ->orwhere(['due_date <' => date('Y-m-d'), 'state' => 'V'])
                                  ->contain(['Companies' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Companies.name'])
                                              ->autofields(false)
                                              ->contain(['Users' => function(\Cake\ORM\Query $q){
                                                return $q->select(['Users.email', 'Users.organization_id'])
                                                          ->autofields(false);
                                              }]);
                                  }]);

      $projectTable = TableRegistry::get('Projects');
      $companyTable = TableRegistry::get('Companies');
      $userTable = TableRegistry::get('Users');
      $usersToNotify = array();

      if($invoices->count() != 0){
        $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $content = new \SendGrid\Content("text/plain", 'Facturas pendientes');
        $mail = new \SendGrid\Mail($from, 'Facturas pendientes', $to, $content);
        $messageToDisplay = "<h2>¡Hola!</h2>Existen factura(s) pendiente(s) cuya fecha de cobro ya fue sobrepasada y no han sido canceladas:";

        foreach ($invoices as $invoice) {
          $project = $projectTable->find()
                                  ->select(['Projects.name'])
                                  ->autofields(false)
                                  ->where(['Projects.id' => $invoice->project_id]);

          $user = $userTable->find()->select(['Users.email'])->autofields(false)->where(['organization_id' => $invoice->company->user->organization_id, 'role' => 'A'])->first();
          $projectName = $project->count() != 0 ? $project->toArray()[0]->name : 'no-name';

          if (!isset($usersToNotify[$user->email])) {
            $usersToNotify[$user->email] = "<li> Factura #".$invoice->id.', proyecto '.$projectName.', monto de la factura: $'.$invoice->total_amount.", monto pendiente: $".$invoice->outstanding_amount.", compañía: ".$invoice->company->name.", fecha de pago: ".$invoice->due_date."</li>";
          }
          else {
            $usersToNotify[$user->email] .= "<li> Factura #".$invoice->id.', proyecto '.$projectName.', monto de la factura: $'.$invoice->total_amount.", monto pendiente: $".$invoice->outstanding_amount.", compañía: ".$invoice->company->name.", fecha de pago: ".$invoice->due_date."</li>";
          }

          if ($invoice->state == 'P') {
            $invoice->state = 'V';
            $this->Invoices->save($invoice);
          }
        }

        foreach ($usersToNotify as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $messageToDisplay);
          $personalization->addSubstitution('%button_content%', Router::url(['controller' => 'Tasks', 'action' => 'index'], TRUE));
          $mail->addPersonalization($personalization);
        }

        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);

        $sg = new \SendGrid("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");
        $response = $sg->client->mail()->send()->post($mail);
      }
    }
}
