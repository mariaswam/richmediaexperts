<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentTerms Model
 *
 * @method \App\Model\Entity\PaymentTerm get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentTerm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentTerm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentTerm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentTerm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentTerm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentTerm findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentTermsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_terms');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        return $validator;
    }
}
