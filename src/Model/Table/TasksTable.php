<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\FrozenTime;

/**
 * Tasks Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\MilestonesTable|\Cake\ORM\Association\BelongsTo $Milestones
 * @property \App\Model\Table\TaskTypesTable|\Cake\ORM\Association\BelongsTo $TaskTypes
 * @property \App\Model\Table\TimesheetsTable|\Cake\ORM\Association\HasMany $Timesheets
 *
 * @method \App\Model\Entity\Task get($primaryKey, $options = [])
 * @method \App\Model\Entity\Task newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Task[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Task|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Task patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Task[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Task findOrCreate($search, callable $callback = null, $options = [])
 */
class TasksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tasks');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Projects', [
            'foreignKey' => 'project_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Milestones', [
            'foreignKey' => 'milestone_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TaskTypes', [
            'foreignKey' => 'task_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Timesheets', [
            'foreignKey' => 'task_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('type');

        $validator
            ->datetime('start_date')
            ->allowEmpty('start_date');

        $validator
            ->numeric('duration')
            ->allowEmpty('duration');

        $validator
            ->allowEmpty('priority');

        $validator
            ->datetime('end_date')
            ->allowEmpty('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        // $rules->add($rules->existsIn(['milestone_id'], 'Milestones'));
        $rules->add($rules->existsIn(['task_type_id'], 'TaskTypes'));

        return $rules;
    }

    public function getEndDate($task)
    {

      if ($task->duration <= 8) {
        $endDate = $task->start_date->modify('+'.$task->duration.' hours');
        if (($endDate->hour >= 17) || (($endDate->hour >= 17) && ($endDate->minute >= 0))) {
          $difference = (strtotime($endDate->format('Y-m-d H:i'))-strtotime(date($endDate->format('Y-m-d').' 17:00')))/3600;
          $hour = 8+$difference;
          $minutes = $task->start_date->minute;
          $date = $endDate->modify('+1 day');
          $endDate = new FrozenTime(($date->format('Y-m-d').' '.number_format($hour, 0).':'.$minutes));
        }
      }
      else {
        $duration = $task->duration/8;
        $hours = ($duration-floor($duration))*8;
        $endDate = $task->start_date->modify('+'.floor($duration).' days '.$hours.' hours');
        if (($endDate->hour >= 17) || (($endDate->hour >= 17) && ($endDate->minute >= 0))) {
          $difference = (strtotime($endDate->format('Y-m-d H:i'))-strtotime(date($endDate->format('Y-m-d').' 17:00')))/3600;
          $hour = 8+$difference;
          $minutes = $task->start_date->minute;
          $date = $endDate->modify('+1 day');
          $endDate = new FrozenTime(($date->format('Y-m-d').' '.number_format($hour, 0).':'.$minutes));
        }
      }

      return $endDate;
    }
}
