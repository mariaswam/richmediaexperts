<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property \Cake\I18n\FrozenDate $start_date
 * @property \Cake\I18n\FrozenTime $duration
 * @property string $priority
 * @property int $user_id
 * @property int $milestone_id
 * @property int $task_type_id
 * @property \Cake\I18n\FrozenDate $end_date
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Milestone $milestone
 * @property \App\Model\Entity\TaskType $task_type
 * @property \App\Model\Entity\Timesheet[] $timesheets
 */
class Task extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
