<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Attachment Entity
 *
 * @property int $id
 * @property int $task_id
 * @property string $file
 *
 * @property \App\Model\Entity\Task $task
 */
class Attachment extends Entity
{

}
