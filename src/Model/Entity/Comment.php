<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property int $task_id
 * @property string $comment
 *
 * @property \App\Model\Entity\Task $task
 */
class Comment extends Entity
{

}
