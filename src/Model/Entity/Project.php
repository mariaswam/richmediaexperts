<?php
namespace App\Model\Entity;
use Cake\Validation\Validator;

use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property \Cake\I18n\FrozenDate $start_date
 * @property float $budget
 * @property int $invoiced
 * @property int $company_id
 * @property float $total_cost
 * @property \Cake\I18n\FrozenDate $end_date
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Invoice[] $invoices
 * @property \App\Model\Entity\Milestone[] $milestones
 */
class Project extends Entity
{


    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

}
