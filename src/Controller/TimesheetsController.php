<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\Event\Event;
use Firebase\JWT\JWT;

/**
 * Timesheets Controller
 *
 * @property \App\Model\Table\TimesheetsTable $Timesheets
 *
 * @method \App\Model\Entity\Timesheet[] paginate($object = null, array $settings = [])
 */
class TimesheetsController extends AppController
{

  public function beforeFilter(Event $event){
      $this->Auth->allow(['saveTimesheetApp']);

      return parent::beforeFilter($event);
  }

  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Timesheets']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete', 'view', 'report', 'timeTracking', 'getTotalHoursByUser', 'getTimesheetsUser', 'downloadTimesheets'])){
          return true;
      }
    }

    if($user['role'] == 'E' && in_array($this->request->getParam('action'), ['report'])){
        return true;
    }

  return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index(){
        $timesheets = $this->Timesheets->find()
                                        ->contain(['Tasks' => function(\Cake\ORM\Query $q) {
                                          return $q->find('all')->where(['user_id' => $this->Auth->user('id')]);
                                        }]);
        $timesheets = json_encode($timesheets->toArray());
        $milestonesUsers = TableRegistry::get('MilestonesUsers')->find()->select(['milestone_id'])->autofields(false)->where(['user_id' => $this->Auth->user('id')]);
        $milestones = TableRegistry::get('Milestones')->find()
                                                      ->select(['Milestones.project_id'])
                                                      ->where(['Milestones.id IN' => $milestonesUsers]);

        if ($milestonesUsers->count() > 0) {
           //debug($milestonesUsers->toArray());die();
          $projects = TableRegistry::get('Projects')->find('list', ['keyField' => 'id', 'valueField' => 'name'])
                                                    ->select(['Projects.name', 'Projects.id'])
                                                    ->where(['Projects.id IN' => $milestones]);
        }
        else {
          $projects = array();
        }

        $taskTypes = TableRegistry::get('TaskTypes')->find('list', ['limit' => 200,  'keyField' => 'id', 'valueField' => 'type']);
        $users = TableRegistry::get('Users')->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $tasks = TableRegistry::get('Tasks')->find('list')->where(['user_id' => $this->Auth->user('id')]);
        $this->set(compact('timesheets', 'projects', 'taskTypes', 'users', 'tasks'));
        $this->set('_serialize', ['timesheets']);
    }

    /**
     * View method
     *
     * @param string|null $id Timesheet id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $timesheet = $this->Timesheets->get($id, [
            'contain' => ['Tasks']
        ]);

        $this->set('timesheet', $timesheet);
        $this->set('_serialize', ['timesheet']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $timesheet = $this->Timesheets->newEntity();
        $taskTable = TableRegistry::get('Tasks');
        $usersTable = TableRegistry::get('Users');
        $projectTable = TableRegistry::get('Projects');

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if ($data['project'] != "") {
              $timesheet = $this->Timesheets->patchEntity($timesheet, $this->request->getData());
              $project = $projectTable->find()->select(['id'])->where(['Projects.name LIKE' => '%' . $data['project'] . '%'])->first();
              $timesheet->project_id = $project->id;
              $timesheet->date = new FrozenTime($timesheet->date);
              $timesheet->user_id = $this->Auth->user('id');
              if ($this->Timesheets->save($timesheet)) {
                $this->validateTimesheetsFeedbackProject($timesheet->project_id);
                  $this->Flash->success(__('Las horas han sido salvadas exitosamente.'));
                  return $this->redirect(['action' => 'add']);
              }
              $this->Flash->error(__('Las horas no pudieron ser salvadas. Por favor intente nuevamente.'));
            }
            else{
              $this->Flash->error(__('The project is empty'));
            }
        }

        $tasks = $taskTable->find("list", array("fields"=>array("Tasks.name")))->where(['user_id' => $this->Auth->user('id')]);
        $users = $usersTable->find()->where(['id !=' => '8']);
        $projects = $projectTable->find("list", array("fields"=>array("Projects.name")));
        $taskTypes = TableRegistry::get('TaskTypes')->find('list', ['limit' => 200,  'keyField' => 'id', 'valueField' => 'type']);
        $this->set(compact('timesheet', 'projects', 'taskTypes', 'tasks', 'users', 'projects'));
        $this->set('_serialize', ['timesheet']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Timesheet id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){
        $timesheet = $this->Timesheets->get($id, [
            'contain' => ['Projects']
        ]);
        $projectTable = TableRegistry::get('Projects');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            if ($data['project'] != "") {
              $timesheet = $this->Timesheets->patchEntity($timesheet, $this->request->getData());
              // $date = strtotime($timesheet->date);
              // debug($date);die();
              $timesheet->date = new FrozenTime($timesheet->date);
              $project = $projectTable->find()->select(['id'])->where(['Projects.name LIKE' => '%' . $data['project'] . '%'])->first();
              $timesheet->project_id = $project->id;

              if ($this->Timesheets->save($timesheet)) {
                    $this->Flash->success(__('Las horas fueron salvadas exitosamente.'));

                    return $this->redirect('/tracking');
              }
              $this->Flash->error(__('Las horas no pudieron ser salvadas. Por favor intente nuevamente'));
            }
            else {
                $this->Flash->error(__('The project is empty'));
            }
        }

        $projects = $projectTable->find("list", array("fields"=>array("Projects.name")));
        $this->set(compact('timesheet', 'projects', 'taskTypes', 'projects', 'task'));
        $this->set('_serialize', ['timesheet']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Timesheet id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $timesheet = $this->Timesheets->get($id);
        if ($this->Timesheets->delete($timesheet)) {
            $this->Flash->success(__('La hora ha sido eliminada correctamente.'));
        } else {
            $this->Flash->error(__('La hora de trabajo no pudo ser eliminada. Por favor intente nuevamente.'));
        }

        return $this->redirect(['action' => 'add']);
    }

    public function getTimesheetByUser(){
      if($this->request->is('ajax')){
        $userID = $this->request->data['id'];
        $timesheets = $this->Timesheets->find()
                                        ->contain(['Tasks' => function(\Cake\ORM\Query $q) use($userID){
                                          return $q->find('all')->where(['user_id' => $userID]);
                                        }]);
        $timesheets = json_encode($timesheets->toArray());
        $this->set(compact('timesheets'));
        $this->set('_serialize', ['timesheets']);
      }
    }

    public function getTotalHoursByUser(){
      if ($this->request->is('ajax')) {
        $userID = $this->request->data['id'];
        $startDate = $this->request->data['start_date'];
        $endDate = $this->request->data['end_date'];
        $tableUsers = TableRegistry::get('Users');
        if ($userID != 'null') {
          $timesheets = $this->Timesheets->find()
                                          ->where(['Timesheets.start_date >=' => $startDate, 'Timesheets.end_date <= ' => $endDate])
                                          ->contain(['Tasks' => function(\Cake\ORM\Query $q) use($userID){
                                            return $q->find('all')->where(['user_id' => $userID]);
                                          }]);
          $user = $tableUsers->find()->select(['Users.hour_cost'])->autofields(false)->where(['Users.id' => $userID])->first();
        }
        else {
          $timesheets = $this->Timesheets->find()
                                          ->where(['Timesheets.start_date >=' => $startDate, 'Timesheets.end_date <= ' => $endDate])
                                          ->contain(['Tasks' => function(\Cake\ORM\Query $q) {
                                            return $q->find('all')->where(['user_id' => $this->Auth->user('id')]);
                                          }]);
          $user = $tableUsers->find()->select(['Users.hour_cost'])->autofields(false)->where(['Users.id' => $this->Auth->user('id')])->first();
        }

        $totalHours = 0;
        $totalMinutes = 0;
        foreach ($timesheets as $timesheet) {
          $totalHours += $timesheet->duration->format('H');
          $totalMinutes += $timesheet->duration->format('i');

          if($totalMinutes == 60){
           $totalMinutes = 0;
           $totalHours++;
          }
        }

        $totalHoursUser = $totalHours.':'.($totalMinutes < 10 ? ('0'.$totalMinutes):$totalMinutes);
        $totalAmount = ($totalHours*$user->hour_cost)+(($totalMinutes/60)*$user->hour_cost);

        $this->set(compact('totalHoursUser', 'totalAmount'));
        $this->set('_serialize', ['totalHoursUser', 'totalAmount']);
      }
    }

    public function report(){
      $projectTable = TableRegistry::get('Projects');
      $invoiceTable = TableRegistry::get('Invoices');
      $usersTable = TableRegistry::get('Users');

      if ($this->request->is('ajax')) {
        $data = $this->request->getData();
        $isProject = false;
        $conditions = array();

        foreach ($data as $key => $value) {
          switch ($key) {
            case 'date':
              $value != null ? $conditions['Timesheets.date >= '] = $value : $conditions = $conditions;
            break;
            case 'endDate':
              $value != null ? $conditions['Timesheets.date <= '] = $value : $conditions = $conditions;
            break;
            case 'user':
              $value != '0' ? $conditions['Timesheets.user_id '] = $value : $conditions = $conditions;
            break;
          }
        }
// debug($conditions);die();
        if ($data['project'] != '') {
          $timesheets = $this->Timesheets->find()->contain(['Projects' => function(\Cake\ORM\Query $q) use($data){
                                                                    return $q->find('all')
                                                                              ->where(['Projects.name LIKE' => $data['project']]);
                                                                }, 'Users'])->where($conditions);

          if ($timesheets->count() > 0) {
            $invoices = $invoiceTable->find()->where(['project_id' => $timesheets->toArray()[0]->project->id])->contain(['Companies', 'Items']);
          }
          $isProject = true;
        }
        else{
          $timesheets = $this->Timesheets->find()->contain(['Projects', 'Users'])->where($conditions);
        }

        $regulartime = 0;
        $feedbacktime = 0;
        $overtime = 0;
        $userStatistic = array();
        $users = array();
        $feedbackEstimatedTime = 0;
        $projects_ids = array();

        foreach ($timesheets as $timesheet) {
          switch ($timesheet->performed_activity) {
            case 'regular':
              $regulartime += $timesheet->duration;
              break;
            case 'overtime':
                $overtime+= $timesheet->duration;
              break;
            case 'feedback':
                $feedbacktime += $timesheet->duration;
              break;
          }

          if (!isset($userStatistic[$timesheet->user->username])) {
            $project_id = $data['project'] != '' ? $timesheet->project->id : 0;
            $userStatistic[$timesheet->user->username] = $this->getStatisticUser($timesheet->user->id, $project_id, $data);
            array_push($users, $timesheet->user->username);
          }

          if (!$isProject) {
            array_push($projects_ids, $timesheet->project->id);
          }

        }

        if (!$isProject) {
          $projects_ids = array_unique($projects_ids);
          if (!empty($projects_ids)) {
            $invoices = $invoiceTable->find()->where(['project_id IN' => $projects_ids])->contain(['Companies', 'Items']);
          }
        }

        if (isset($invoices)) {
          foreach ($invoices as $invoice) {
            foreach ($invoice->items as $item) {
              if (strpos(strtolower($item->description), 'feedback') !== false) {
                $feedbackEstimatedTime += $item->quantity;
              }
            }
          }
        }

        $dataHotFeedback = $this->getLastHotProjects();

        $this->set(compact('timesheets', 'regulartime', 'feedbacktime', 'overtime', 'users', 'userStatistic', 'feedbackEstimatedTime', 'dataHotFeedback'));
      }

      $projects = $projectTable->find("list", array("fields"=>array("Projects.name")));
      $usersArray = $usersTable->find()->where(['id NOT IN' => ['50', '44']]);
      $this->set(compact('projects', 'usersArray'));
      $this->set('_serialize', ['projects', 'timesheets', 'regulartime', 'feedbacktime', 'overtime', 'users', 'userStatistic', 'feedbackEstimatedTime', 'dataHotFeedback', 'usersArray']);
    }

    private function getStatisticUser($id, $project_id, $data){
      $conditions = ['user_id' => $id];
      $conditions += $project_id != 0 ? ['project_id' => $project_id] : $conditions;

      foreach ($data as $key => $value) {
        switch ($key) {
          case 'date':
            $value != null ? $conditions['Timesheets.date >= '] = $value : $conditions = $conditions;
          break;
          case 'endDate':
            $value != null ? $conditions['Timesheets.date <= '] = $value : $conditions = $conditions;
          break;
        }
      }

      $timesheets = $this->Timesheets->find()->where($conditions);
      $regular = 0;
      $feedback = 0;
      $overtime = 0;

      foreach ($timesheets as $timesheet) {
        switch ($timesheet->performed_activity) {
          case 'regular':
            $regular += $timesheet->duration;
            break;
          case 'overtime':
              $overtime+= $timesheet->duration;
            break;
          case 'feedback':
              $feedback += $timesheet->duration;
            break;
        }
      }

      return [$regular, $feedback, $overtime];
    }

    private function getLastHotProjects(){
      $invoicesTable = TableRegistry::get('Invoices');
      $timesheetsTable = TableRegistry::get('Timesheets');
      $invoices = $invoicesTable->find()->contain(['Projects', 'Items'])->where(['Invoices.state !=' => 'C']);
      $projectData = array();

      foreach ($invoices as $invoice) {
        foreach ($invoice->items as $item) {
          if (strpos(strtolower($item->description), 'feedback') !== false) {
            //Get sum of timesheets
            $query = $timesheetsTable->find();
            $timesheetsSum = $query->select(['total' => $query->func()->sum('duration')])->where(['performed_activity' => 'feedback', 'project_id' => $invoice->project_id])->first();
            if ($timesheetsSum->total != 0 && $item->quantity - $timesheetsSum->total <= 3) {
              if (!isset($projectData[$invoice->project->name])) {
                $projectData[$invoice->project->name] = [$item->quantity, $timesheetsSum->total];
              }
              else{
                $projectData[$invoice->project->name][0] += $item->quantity;
                $projectData[$invoice->project->name][1] += $timesheetsSum->total;
              }
            }
          }
        }
      }

      return $projectData;
    }

    public function validateStateTask($timesheet){
      if (isset($timesheet->task_name)) {
        $task = TableRegistry::get('Tasks')->find()->where(['Tasks.name LIKE ' => $timesheet->task_name, 'Tasks.user_id' => $this->Auth->user('id')])->first();
      }
      else if (isset($timesheet->task_id)) {
        $task = TableRegistry::get('Tasks')->get($timesheet->task_id);
      }
      if ($task->state == 'P' || $task->state == 'PA' || $task->state == 'IP') {
        return true;
      }
      return false;
    }

    private function validateShockOfHours($timesheet){
      if (isset($timesheet->task_name)) {
        $task = TableRegistry::get('Tasks')->find()->where(['Tasks.name LIKE ' => $timesheet->task_name, 'Tasks.user_id' => $this->Auth->user('id')])->first();
      }
      else if (isset($timesheet->task_id)) {
        $task = TableRegistry::get('Tasks')->get($timesheet->task_id);
      }
      debug($timesheet);die();
      $timesheets = $this->Timesheets->find()->where(['task_id' => $timesheet->task_id]);

      foreach ($timesheets as $timesheetSave) {
        if ($timesheet->start_date <= $timesheetSave->end_date && $timesheet->end_date >= $timesheetSave->start_date) {
          debug('Entra');
          //return false;
        }
      }
      die();
      return true;
    }

    private function sendEmails($subject, $message, $user){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($user['first_name'].' '.$user['last_name'], $user['email']);
      $content = new \SendGrid\Content("text/html", $message);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    public function downloadReportTimesheetsByProject($timesheets_id = null){
      $totalHours = 0;
      $totalRegularHours = 0;
      $totalOvertimeHours = 0;
      $totalFeedbackHours = 0;
      $resources = array();
      $timesheets_id = explode(',', $timesheets_id);

      $timesheets = $this->Timesheets->find()->contain(['Projects', 'Users'])->where(['Timesheets.id IN' => $timesheets_id]);

      $excelFile = new PHPExcel();
      $excelFile->getProperties()->setCreator("RME")->setLastModifiedBy("RME")->setTitle("Timesheets report")
                         ->setSubject("Timesheets")->setDescription("Document report")->setKeywords("office")
                         ->setCategory("Report");
      $excelFile->setActiveSheetIndex(0)->setCellValue('A1', __('Timesheets report'))->setCellValue('A3', __('Date'))
                                        ->setCellValue('B3', Time::now()->format('d-m-Y H:i'))->setCellValue('A4', __('Project'))
                                        ->setCellValue('B4', $timesheets->toArray()[0]->project->name)
                                        ->setCellValue('A5', __('Total hours'))->setCellValue('A6', __('Total regular hours'))
                                        ->setCellValue('A7', __('Total feedback hours'))->setCellValue('A8', __('Total overtime hours'))
                                        ->setCellValue('A9', __('Number of resources'));

      $excelFile->setActiveSheetIndex(0)->setCellValue('A11', __('Resource'))->setCellValue('B11', __('Date'))->setCellValue('C11', __('Description'))->setCellValue('D11', __('Hours'))
                                        ->setCellValue('E11', __('Type'));

      $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      foreach ($timesheets as $timesheet) {
        $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+1), $timesheet->user->username)->setCellValue('B'.($lastRow+1), $timesheet->date->format('m-d-Y'))
                                          ->setCellValue('C'.($lastRow+1), $timesheet->description)->setCellValue('D'.($lastRow+1), $timesheet->duration)
                                          ->setCellValue('E'.($lastRow+1), $timesheet->performed_activity);

        switch ($timesheet->performed_activity) {
          case 'regular':
            $totalRegularHours += $timesheet->duration;
          break;
          case 'overtime':
            $totalOvertimeHours += $timesheet->duration;
          break;
          case 'feedback':
            $totalFeedbackHours += $timesheet->duration;
          break;
        }

        if (!isset($resources[$timesheet->user->username])) {
          $resources[$timesheet->user->username] = true;
        }

        $totalHours += $timesheet->duration;
        $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      }

      $excelFile->setActiveSheetIndex(0)->setCellValue('B5', $totalHours)->setCellValue('B6', $totalRegularHours)
                                        ->setCellValue('B7', $totalFeedbackHours)->setCellValue('B8', $totalOvertimeHours)
                                        ->setCellValue('B9', count($resources));
      $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A5')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A7')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A8')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A9')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('B11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('C11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('D11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('E11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

      $excelFile->getActiveSheet()->setTitle(__('Timesheets report'));
      $excelFile->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="timesheets_report_'.date('m-d-Y').'.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');

      ob_end_clean();
      $writer->save('php://output');
    }

    public function timeTracking($taskID = null){
      $this->viewBuilder()->setLayout('');
      $timesheet = $this->Timesheets->newEntity();
      if ($this->request->is('post')) {
        $timesheet = $this->Timesheets->patchEntity($timesheet, $this->request->getData());
        $timesheet->start_date = new FrozenTime($timesheet->start_date);
        $timesheet->duration = new FrozenTime($timesheet->duration);
        $timesheet->end_date = $timesheet->start_date->modify('+'.$timesheet->duration->hour.' hours '.$timesheet->duration->minute.' minutes');
        if($this->validateStateTask($timesheet)){
          if ($this->validateShockOfHours($timesheet)) {
            if ($this->Timesheets->save($timesheet)) {
                $this->Flash->success(__('Las horas han sido salvadas exitosamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Las horas no pudieron ser salvadas. Por favor intente nuevamente.'));
          }
          else {
            $this->Flash->error(__('Ya existen horas registradas durante ese lapso. Por favor intente nuevamente.'));
          }
        }
        else{
          $this->Flash->error(__('Las horas no pudieron ser salvadas ya la tarea se encuentra finalizada.'));
        }
      }
      $taskTypes = TableRegistry::get('TaskTypes')->find('list', ['keyField' => 'id', 'valueField' => 'type']);
      $task = TableRegistry::get('Tasks')->get($taskID);
      $this->set(compact('taskTypes', 'task'));
      $this->set('_serialize', ['taskTypes', 'task']);
    }

    public function saveTimesheetApp(){
      $this->autoRender = false;
      header('Access-Control-Allow-Origin: *');
      $timesheet = $this->Timesheets->newEntity();

      if ($this->request->is('post')) {
        $user_id = $this->request->data['user_id'];
        $jwt = $this->request->data['jwt'];
        $timesheet = $this->Timesheets->patchEntity($timesheet, $this->request->getData());

        $key = '16cf8e473001a6ceff36ab1a24134a76';
        $data = JWT::decode($jwt, $key, array('HS256'));

        $user = TableRegistry::get('Users')->get($user_id);
        $jwtToken = $data->data->jwtToken;
        $decodeJwtToken = base64_decode($jwtToken);
        $explodeToken = explode('.', $decodeJwtToken);

        if ($explodeToken[0] == $user->id && $explodeToken[1] == $user->username) {
          $timesheet->start_date = new FrozenTime($timesheet->start_date);
          $timesheet->duration = new FrozenTime($timesheet->duration);
          $timesheet->end_date = $timesheet->start_date->modify('+'.$timesheet->duration->hour.' hours '.$timesheet->duration->minute.' minutes');

          if($this->validateStateTask($timesheet)){
            if ($this->validateShockOfHours($timesheet)) {
              if ($this->Timesheets->save($timesheet)) {
                  $result = ['message' => 'Las horas han sido salvadas exitosamente.', 'type' => 'success'];
              }
              else {
                $result = ['message' => 'Las horas no pudieron ser salvadas. Por favor intente nuevamente.', 'type' => 'error'];
              }
            }
            else {
              $result = ['message' => 'Ya existen horas registradas durante ese lapso. Por favor intente nuevamente.', 'type' => 'error'];
            }
          }
          else{
            $result = ['message' => 'La tarea se encuentra cerrada', 'type' => 'error'];
          }


          $resultJ = json_encode(array('result' => $result));
          $this->response->type('json');
          $this->response->body($resultJ);
          return $this->response;
        }
      }
    }

    public function getTimesheetsUser(){
      if ($this->request->is('ajax')) {
        $data = $this->request->getData();
        if ($data['date'] == null && $data['endDate'] == null) {
          $timesheets = $this->Timesheets->find()->where(['user_id' => $data['user']])->contain(['Projects'])->order(['date' => 'DESC']);
        }
        else {
          $timesheets = $this->Timesheets->find()->where(['user_id' => $data['user'], 'date >=' => $data['date'], 'date <=' => $data['endDate']])->contain(['Projects'])->order(['date' => 'DESC']);
        }
        $regulartime = 0;
        $feedbacktime = 0;
        $overtime = 0;

        foreach ($timesheets as $timesheet) {
          switch ($timesheet->performed_activity) {
            case 'regular':
              $regulartime += $timesheet->duration;
              break;
            case 'overtime':
                $overtime+= $timesheet->duration;
              break;
            case 'feedback':
                $feedbacktime += $timesheet->duration;
              break;
          }
        }

        $this->set(compact('timesheets', 'regulartime', 'feedbacktime', 'overtime'));
        $this->set('_serialize', ['timesheets', 'regulartime', 'feedbacktime', 'overtime']);
      }
    }

    public function downloadTimesheets($ids){
      $ids = explode(',', $ids);
      $timesheets = $this->Timesheets->find()->where(['Timesheets.id IN' => $ids])->contain(['Projects', 'Users']);
      $totalHours = 0;
      $totalRegularHours = 0;
      $totalOvertimeHours = 0;
      $totalFeedbackHours = 0;

      $excelFile = new PHPExcel();
      $excelFile->getProperties()->setCreator("RME")->setLastModifiedBy("RME")->setTitle("Timesheets report")
                         ->setSubject("Timesheets")->setDescription("Document report")->setKeywords("office")
                         ->setCategory("Report");
      $excelFile->setActiveSheetIndex(0)->setCellValue('A1', __('Timesheets report'))->setCellValue('A3', __('Date'))
                                        ->setCellValue('B3', Time::now()->format('d-m-Y H:i'))->setCellValue('A4', __('Resource'))
                                        ->setCellValue('B4', $timesheets->toArray()[0]->user->username)
                                        ->setCellValue('A5', __('Total hours'))->setCellValue('A6', __('Total regular hours'))
                                        ->setCellValue('A7', __('Total feedback hours'))->setCellValue('A8', __('Total overtime hours'));
      $excelFile->setActiveSheetIndex(0)->setCellValue('A10', __('Project'))->setCellValue('B10', __('Date'))->setCellValue('C10', __('Description'))
                                        ->setCellValue('D10', __('Hours'))->setCellValue('E10', __('Type'));

      $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      foreach ($timesheets as $timesheet) {
        $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+1), $timesheet->project->name)->setCellValue('B'.($lastRow+1), $timesheet->date->format('m-d-Y'))
                                          ->setCellValue('C'.($lastRow+1), $timesheet->description)->setCellValue('D'.($lastRow+1), $timesheet->duration)
                                          ->setCellValue('E'.($lastRow+1), $timesheet->performed_activity);

        switch ($timesheet->performed_activity) {
          case 'regular':
            $totalRegularHours += $timesheet->duration;
          break;
          case 'overtime':
            $totalOvertimeHours += $timesheet->duration;
          break;
          case 'feedback':
            $totalFeedbackHours += $timesheet->duration;
          break;
        }

        $totalHours += $timesheet->duration;
        $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      }

      $excelFile->setActiveSheetIndex(0)->setCellValue('B5', $totalHours)->setCellValue('B6', $totalRegularHours)
                                        ->setCellValue('B7', $totalFeedbackHours)->setCellValue('B8', $totalOvertimeHours);
      $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A5')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A7')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A8')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('B10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('C10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('D10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('E10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

      $excelFile->getActiveSheet()->setTitle(__('Timesheets report'));
      $excelFile->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="timesheets_report_'.date('m-d-Y').'.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');

      ob_end_clean();
      $writer->save('php://output');
    }

    private function validateTimesheetsFeedbackProject($project_id){
      $invoicesTable = TableRegistry::get('Invoices');
      $invoices = $invoicesTable->find()->contain(['Projects', 'Items'])->where(['Invoices.project_id' => $project_id]);
      $projectData = array();
      $estimatedFeedback = 0;
      $query = $this->Timesheets->find();
      $timesheetsSum = $query->select(['total' => $query->func()->sum('duration')])->where(['performed_activity' => 'feedback', 'project_id' => $project_id])->first();
      foreach ($invoices as $invoice) {
        foreach ($invoice->items as $item) {
          if (strpos(strtolower($item->description), 'feedback') !== false) {
            //Get sum of timesheets
            $estimatedFeedback += $item->quantity;
          }
        }
      }

      //Check if the current feedback is greater than estimated
      if ($timesheetsSum->total > $estimatedFeedback) {
        //Send slack notification
        $this->alertFeedbackOverflow($project_id, $estimatedFeedback, $timesheetsSum->total);
      }

      return $projectData;
    }

    private function alertFeedbackOverflow($project_id, $feedback, $current){
      $project = TableRegistry::get('Projects')->get($project_id);
      $adminUser = TableRegistry::get('Users')->find()->where(['role' => 'A'])->first();
      $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');

      //Content
      $message = 'The estimated feedback has been exceeded.' . PHP_EOL . "> Project: " . $project->name . PHP_EOL . "> Estimated feedback: " . $feedback. PHP_EOL . "> Current feedback: " . $current;

      $client->send($message);
    }
}
