<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use App\Model\Entity\Notification;
use Cake\Routing\Router;
use App\Model\Entity\Attachment;
use Cake\Event\Event;
use Firebase\JWT\JWT;

/**
 * Tasks Controller
 *
 * @property \App\Model\Table\TasksTable $Tasks
 *
 * @method \App\Model\Entity\Task[] paginate($object = null, array $settings = [])
 */
class TasksController extends AppController
{
    public function beforeFilter(Event $event){
        $this->Auth->allow(['getTasksUserApp']);

        return parent::beforeFilter($event);
    }

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Tasks']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete', 'view', 'getTasksProject', 'availability', 'getTasksByMilestone', 'finishTask', 'reopenTask', 'sendTaskForApproval', 'getTaskById', 'changeStatusTask', 'sendMessageSlack'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $projects = TableRegistry::get('Projects')->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
        if ($this->Auth->user('role') == 'A') {
          $tasks = $this->Tasks->find()->contain(['Users']);
        }
        else {
          $tasks = $this->Tasks->find()->where(['user_id' => $this->Auth->user('id')]);
        }

        $this->set(compact('tasks'));
        $this->set('_serialize', ['tasks']);
    }

    /**
     * View method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $task = $this->Tasks->get($id, [
            'contain' => ['Users', 'TaskTypes', 'Timesheets', 'Projects']
        ]);

        $attachments = TableRegistry::get('Attachments')->find()->where(['task_id' => $task->id]);
        $comments = TableRegistry::get('Comments')->find()->where(['task_id' => $task->id])->contain(['Users'])->order(['date' => 'ASC']);
        $loggedUser = $this->Auth->user();
        $this->set(compact('task', 'attachments', 'comments', 'loggedUser'));
        $this->set('_serialize', ['task']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */

     public function add(){
           $task = $this->Tasks->newEntity();
           $tableProjects = TableRegistry::get('Projects');
           if ($this->request->is('post')) {
               $task = $this->Tasks->patchEntity($task, $this->request->getData());
               $project = $tableProjects->find()->select(['Projects.id', 'Projects.name'])->autofields(false)->where(['Projects.name LIKE' => $task->project_name])->first();
               $task->project_id = $project->id;
               $task->created_by = $this->Auth->user('id');

               if ($task->task_type_id == 6) {
                 if ($this->Tasks->save($task)) {
                   $this->Flash->success(__('La tarea ha sido salvado exitosamente'));
                   return $this->redirect(['action' => 'index']);
                 }
                 else {
                   $this->Flash->error(__('La tarea no pudo ser salvada. Intente nuevamente'));
                   return $this->redirect(['action' => 'index']);
                 }
               }

                 if ($this->Tasks->save($task)) {
                     if (count($_FILES['attachments']['size']) != 0) {
                       $this->saveAttachments($_FILES['attachments'], $task);
                     }
                     $user = TableRegistry::get('Users')->get($task->user_id);
                     $link =Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
                     $message = "Se ha creado la tarea: *".$task->name.'*, perteneciente al proyecto:  _'.$project->name.'_ el cual se te ha sido asignado. ' . $link;
                     $this->sendMessageSlack($user, $message);
                     $this->Flash->success(__('La tarea ha sido salvado exitosamente'));

                     return $this->redirect(['action' => 'index']);
                 }
                 $this->Flash->error(__('La tarea no pudo ser salvada. Intente nuevamente'));
           }
           $users = $this->Tasks->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
           $projects = $tableProjects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
           $taskTypes = $this->Tasks->TaskTypes->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'type']);
           $this->set(compact('task', 'users', 'projects', 'taskTypes'));
           $this->set('_serialize', ['task']);
     }


     private function saveAttachments($files, $task){
       $attachmentsPath = WWW_ROOT . 'attachments/' . $task->id;
       $numberFiles = count($files['size']);
       $attachmentsTable = TableRegistry::get('Attachments');

       if (!file_exists($attachmentsPath)) {
         mkdir($attachmentsPath, 0777, true);
       }

       for ($i=0; $i < $numberFiles; $i++) {
         $fileName = addslashes($files['tmp_name'][$i]);
         $name = str_replace(' ', '_', $files['name'][$i]);
         $pathFile = $attachmentsPath . DS . $name;
         if (move_uploaded_file($fileName, $pathFile)) {
           $attachment = new Attachment(['task_id' => $task->id, 'file' => $pathFile, 'name' => $name]);
           $attachmentsTable->save($attachment);
         }
       }
     }

     public function edit($id = null){
       $task = $this->Tasks->get($id, [
           'contain' => ['Projects']
       ]);
       if ($this->request->is(['patch', 'post', 'put'])) {
           $task = $this->Tasks->patchEntity($task, $this->request->getData());

           if($task->dirty('state')){
             if ($task->state == 'F') {
               $date = Time::now();
               if (new FrozenTime($task->end_date) <= $date) {
                 $task->state = 'FA';
               }
               else {
                 $task->state = 'FB';
               }
             }
           }

           if ($this->Tasks->save($task)) {

             if (count($_FILES['attachments']['size']) != 0) {
               $this->saveAttachments($_FILES['attachments'], $task);
             }

             if ($task->dirty('duration')) {
               $this->changeCostProject($task);
             }

              $this->Flash->success(__('La tarea ha sido salvado exitosamente'));

              return $this->redirect(['action' => 'view', $task->id]);
           }
           $this->Flash->error(__('La tarea no pudo ser salvada. Intente nuevamente'));
       }
       $users = $this->Tasks->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
       $projects = TableRegistry::get('Projects');
       $project = $projects->get($task->project_id);
       $projects = TableRegistry::get('Projects')->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
       $milestones = TableRegistry::get('Milestones')->find('list')->where(['project_id' => $task->project_id]);
       $taskTypes = $this->Tasks->TaskTypes->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'type']);
       $this->set(compact('task', 'users', 'projects', 'taskTypes', 'milestones', 'project'));
       $this->set('_serialize', ['task']);
     }

    /**
     * Delete method
     *
     * @param string|null $id Task id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $task = $this->Tasks->get($id);
        if ($task->task_type_id == 6) {
          if ($this->Tasks->delete($task)) {
            $this->Flash->success(__('La tarea ha sido eliminada'));
          }
          else {
            $this->Flash->error(__('La tarea no pudo ser eliminada. Intente nuevamente'));
          }
        }
        else {
          if ($this->Tasks->delete($task)) {
            $projectTable = TableRegistry::get('Projects');
            $milestones = TableRegistry::get('Milestones')->get($task->milestone_id);
            $user = TableRegistry::get('Users')->get($task->user_id);
            $project = $projectTable->get($milestones->project_id);
            $project->total_cost = ($project->total_cost-($task->duration*$user->hour_cost));
            $projectTable->save($project);
              $this->Flash->success(__('La tarea ha sido eliminada'));
          } else {
              $this->Flash->error(__('La tarea no pudo ser eliminada. Intente nuevamente'));
          }
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getTasksProject($id = null){
      if($this->request->is('ajax')){
        $requestData = $this->request->data;
        $projectName = $requestData['id'];
        if($this->Auth->user('role') == 'A'){
          if ($projectName == '0') {
            $tasks = $this->Tasks->find()
                                  ->select(['Tasks.name', 'Tasks.id', 'Tasks.end_date', 'Tasks.state', 'Tasks.duration'])
                                  ->autofields(false)
                                  ->where(['Tasks.task_type_id' => '6'])
                                  ->contain(['Users' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Users.username']);
                                  }, 'TaskTypes' => function(\Cake\ORM\Query $q){
                                    return $q->select('TaskTypes.type');
                                  }]);
          }
          else {
            $tasks = $this->Tasks->find()
                                  ->select(['Tasks.name', 'Tasks.id', 'Tasks.end_date', 'Tasks.state', 'Tasks.duration'])
                                  ->autofields(false)
                                  ->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectName){
                                    return $q->select(['Projects.name'])->where(['Projects.name LIKE' => $projectName]);
                                  }, 'TaskTypes' => function(\Cake\ORM\Query $q){
                                    return $q->select('TaskTypes.type');
                                  }, 'Users' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Users.username']);
                                  }]);
          }
        }
        else{
          if ($projectName == '0') {
            $tasks = $this->Tasks->find()
                                  ->select(['Tasks.name','Tasks.id', 'Tasks.end_date', 'Tasks.state', 'Tasks.duration'])
                                  ->autofields(false)
                                  ->where(['Tasks.task_type_id' => '6', 'Tasks.user_id' => $this->Auth->user('id')])
                                  ->contain(['Users' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Users.username']);
                                  }, 'TaskTypes' => function(\Cake\ORM\Query $q){
                                    return $q->select('TaskTypes.type');
                                  }]);
          }
          else {
            $tasks = $this->Tasks->find()
                                  ->select(['Tasks.name', 'Tasks.id', 'Tasks.end_date', 'Tasks.state', 'Tasks.duration'])
                                  ->where(['Tasks.user_id' => $this->Auth->user('id')])
                                  ->autofields(false)
                                  ->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectName){
                                    return $q->select(['Projects.name'])->where(['Projects.name LIKE' => $projectName]);
                                  }, 'TaskTypes' => function(\Cake\ORM\Query $q){
                                    return $q->select('TaskTypes.type');
                                  }, 'Users' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Users.username']);
                        }]);
          }
        }

        $this->set('tasks', $tasks->toArray());
        $this->set('_serialize', ['tasks']);
      }
    }
    /*This method increase the cost of the project when create a new task
    */
    public function changeCostProject($task){
      $projectTable = TableRegistry::get('Projects');
      $userTable = TableRegistry::get('Users')->find()
                                        ->select(['hour_cost'])
                                        ->autofields(false)
                                        ->where(['id' => $task->user_id])
                                        ->first();

      $project = $projectTable->get($task->project_id);
      $user = $userTable->toArray();

      if(isset($user['hour_cost'])){
          $project->total_cost = $project->total_cost + ($task->duration*$user['hour_cost']);

          if($projectTable->save($project)){
            return true;
          }
      }
      return false;
    }

    /*This method increase or decrease the cost of the project if the task
      is edited in the duration value
    */
    public function updateCostProject($prevDuration, $task){
      $differenceDuration = ($prevDuration-$task->duration);
      $user = TableRegistry::get('Users')->get($task->user_id);
      $projectsTable = TableRegistry::get('Projects');
      $project = $projectsTable->get($task->project_id);

      if ($differenceDuration < 0) {
        //Increase cost of project
        $costToIncrease = abs($differenceDuration*$user->hour_cost);
        $project->total_cost = ($project->total_cost+$costToIncrease);
        $projectsTable->save($project);
      }
      else {
        //Decrease cost of project
        $costToDecrease = (($prevDuration*$user->hour_cost)-($task->duration*$user->hour_cost));
        $project->total_cost = ($project->total_cost-$costToDecrease);
        $projectsTable->save($project);
      }
    }

    public function getTasksMilestones(){
      if($this->request->is('ajax')){
        $requestData = $this->request->data;
        $milestoneID = $requestData['id'];
        $milestoneDate = $requestData['start_date'];
        $projectID = $requestData['project_id'];

        $taskTable = TableRegistry::get('Tasks');
        $tasks = $taskTable->find()->select(['Tasks.id', 'Tasks.name', 'Tasks.state', 'Tasks.milestone_id'])
                                    ->autofields(false)
                                    ->where(['milestone_id' => $milestoneID])
                                    ->toArray();

        $pendingTasks = $taskTable->find()
                                  ->select(['Tasks.id', 'Tasks.name', 'Tasks.state'])
                                  ->autofields(false)
                                  ->where(['Tasks.end_date <' => date('Y-m-d'), 'Tasks.state' => 'P', 'm.project_id' => $projectID])
                                  ->join([
                                    'table' => 'milestones',
                                    'alias' => 'm',
                                    'type' => 'INNER',
                                    'conditions' => ['Tasks.milestone_id = m.id']
                                    ])->toArray();

        $this->set(compact('tasks', 'pendingTasks'));
        $this->set('_serialize', ['tasks', 'pendingTasks']);
      }
    }

    public function getTasksByMilestone(){
      if($this->request->is('ajax')){
        $milestoneID = $this->request->data['id'];

        $tasks = $this->Tasks->find()->select(['Tasks.id', 'Tasks.name'])->autofields(false)->where(['milestone_id' => $milestoneID, 'user_id' => $this->Auth->user('id')]);
        $this->set('tasks', $tasks);
        $this->set('_serialize', ['tasks']);
      }
    }

    public function getTaskById(){
      if($this->request->is('ajax')){
        $taskID = $this->request->data['id'];
        $task = $this->Tasks->get($taskID, ['contain' => ['Users']]);

        if ($task->task_type_id != 6) {
          $milestone = TableRegistry::get('Milestones')->find()
                                                        ->select(['Milestones.name'])
                                                        ->autofields(false)
                                                        ->where(['Milestones.id' => $task->milestone_id])
                                                        ->contain(['Projects'=> function(\Cake\ORM\Query $q){
                                                          return $q->select(['Projects.name'])->autofields(false);
                                                        }])
                                                        ->first();
          $task->milestone = $milestone;
        }

        $this->set('task', $task);
        $this->set('_serialize', ['task']);
      }
    }

    public function finishTask(){
      if($this->request->is('post')){
        $projectTable = TableRegistry::get('Projects');
        $taskID = $this->request->getData()['id'];
        $endDate = $this->request->getData()['end_date'];
        $task = $this->Tasks->find()->select(['Tasks.id', 'Tasks.end_date', 'Tasks.milestone_id', 'Tasks.duration', 'Tasks.user_id', 'Tasks.state', 'Tasks.name'])
                                    ->autofields(false)
                                    ->where(['Tasks.id' => $taskID])
                                    ->contain(['Users' => function(\Cake\ORM\Query $q){
                                        return $q->select(['Users.hour_cost', 'Users.id', 'Users.email', 'Users.first_name', 'Users.last_name'])
                                                  ->autofields(false);
                                    }]);
        $task = $task->toArray();
        $milestone = TableRegistry::get('Milestones')->find()
                                                    ->select(['Milestones.project_id'])
                                                    ->autofields(false)
                                                    ->where(['Milestones.id' => $task[0]->milestone_id])
                                                    ->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                      return $q->select(['Projects.total_cost', 'Projects.id'])
                                                              ->autofields(false);
                                                    }]);
        $milestone = $milestone->toArray();
        //days between the current end_date and the estimated
        $start = strtotime($endDate);
        $end = strtotime($task[0]->end_date != null ? $task[0]->end_date->format('Y-m-d H:i'):$task[0]->end_date);
        $days = ($start-$end)/86400;
        //remaining hours to the estimated end date
        if (($days < 0 && $days > -1) || ($days > 0 && $days < 1)) { //Verify if the difference is hours or minutes
          $date = new FrozenTime($endDate);
          if ($date->day == $task[0]->end_date->day) {
            $hours = abs(($start-$end)/3600);
          }
          else {
            $difference = 0;
            $differenceEndDate = 0;
            if (($date->hour < 17) || ($date->hour ==17 && $date->minute == 0)) {
              $difference = abs((strtotime($date->format('Y-m-d H:i'))-strtotime(date($date->format('Y-m-d').' 17:00')))/3600);
            }
            if($task[0]->end_date->hour > 8 || ($task[0]->end_date->hour == 8 && $task[0]->end_date->minute > 0)){
              $differenceEndDate = abs((strtotime($task[0]->end_date->format('Y-m-d H:i'))-strtotime(date($task[0]->end_date->format('Y-m-d').' 8:00')))/3600);
            }
            $hours = abs($difference+$differenceEndDate);
          }
        }
        else{ //The difference is in days
          $hours = $days*8;
        }

        $project = $milestone[0]->project;

        if($days < 0){
          //Decrease cost of project
          $initialCostTask = ($task[0]->duration*$task[0]->user->hour_cost);
          $costToReduce = (abs($hours*$task[0]->user->hour_cost)); //the cost of the remaining hours to the end date
          $project->total_cost = ($project->total_cost-$costToReduce);
          $projectTable->save($project);
          $task[0]->state = 'FB';
          if($this->Tasks->save($task[0])){
            $message = "<p>La tarea: <strong>".$task[0]->name."</strong> ha sido marcada como <em>Terminada</em></p>";
            $link =Router::url(['controller' => 'Tasks', 'action' => 'index'], TRUE);
            $this->sendEmails('Tarea Finalizada', $message, $task[0]->user, $link);
            $message = 'Se ha finalizado la tarea: '.$task[0]->name;
            $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task[0]->id], TRUE);
            $notification = new Notification(['message' => $message, 'user_id' => $task[0]->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
            $notificationTable = TableRegistry::get('Notifications');
            $notificationTable->save($notification);
            $this->Flash->success(__('La tarea ha sido marcada como finalizada.'));
            return $this->redirect(['action' => 'index']);
          }
          else {
            $this->Flash->error(__('La tarea no pudo ser marcada como finalizada.'));
            return $this->redirect(['action' => 'index']);
          }
        }
        else{
          //Increase cost of project
          $costToIncrease = ($task[0]->user->hour_cost*$hours);
          $project->total_cost = ($project->total_cost+$costToIncrease);
          $projectTable->save($project);
          $task[0]->state = 'FA';
          if($this->Tasks->save($task[0])){
            $message = "<h2>¡Hola!</h2><p>La tarea: <strong>".$task[0]->name."</strong> ha sido marcada como <em>Terminada</em></p>";
            $link =Router::url(['controller' => 'Tasks', 'action' => 'view', $task[0]->id], TRUE);
            $this->sendEmails('Tarea Finalizada', $message, $task[0]->user, $link);
            $messageNot = 'Se ha finalizado la tarea: '.$task[0]->name;
            $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task[0]->id], TRUE);
            $notification = new Notification(['message' => $messageNot, 'user_id' => $task[0]->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
            $notificationTable = TableRegistry::get('Notifications');
            $notificationTable->save($notification);
            $this->Flash->success(__('La tarea ha sido marcada como finalizada.'));
            return $this->redirect(['action' => 'index']);
          }
          else {
            $this->Flash->error(__('La tarea no pudo ser marcada como finalizada.'));
            return $this->redirect(['action' => 'index']);
          }
        }
      }
    }

    private function sendEmails($subject, $message, $user, $url){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($user['first_name'].' '.$user['last_name'], $user['email']);
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
      $content = new \SendGrid\Content("text/html", $template);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $mail->personalization[0]->addSubstitution('%html_content%', $message);
      $mail->personalization[0]->addSubstitution('%text_content%', '<h2>¡Hola!</h2>');
      $mail->personalization[0]->addSubstitution('%button_content%', $url);
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    public function getTasksByUser(){
      if($this->request->is('post')){
        $userID = $this->request->data['user_id'];
        $projectName = $this->request->data['project_name'];
        $tasks = $this->Tasks->find()->select(['Tasks.id', 'Tasks.name', 'end' => 'Tasks.end_date', 'Tasks.state'])
                                    ->autofields(false)
                                    ->where(['user_id' => $userID])
                                    ->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectName){
                                        return $q->select(['Projects.id'])->autofields(false)->where(['Projects.name LIKE' => $projectName]);
                                    }])
                                    ->order(['Tasks.end_date' => 'ASC']);
        $tasksForGantt = array();
        foreach ($tasks as $task) {
          $daysUntilToday = abs((strtotime(Time::now()->format('Y-m-d'))-strtotime($task->start))/86400);
          $durationDays = abs((strtotime($task->end)-strtotime($task->start))/86400);
          $progress = $durationDays != 0 ? round(($daysUntilToday/$durationDays)*100): 0;
          array_push($tasksForGantt, ['id' => $task->id, 'name' => $task->name, 'start' => (($task->start != null) ? $task->start->format('Y-m-d'):$task->start), 'end' => (($task->end != null) ? $task->end->format('Y-m-d'):$task->end),'duration' => $task->duration]);
        }
        $this->set('tasks', $tasksForGantt);
        $this->set('_serialize', ['tasks']);
      }
    }

    public function correctDates($task){
      if (isset($task->milestone_id) && $task->milestone_id != '') {
        $milestone = TableRegistry::get('Milestones')->get($task->milestone_id);
        if (isset($task->end_date)) {
          if ($milestone->start_date != null && $milestone->end_date) {
            if(new FrozenTime($task->end_date) >= $milestone->start_date &&  new FrozenTime($task->end_date) <= $milestone->end_date){
              return true;
            }
            else {
              return false;
            }
          }
        }
      }
      else {
        $project = TableRegistry::get('Projects')->get($task->project_id);

        if (($project->start_date != '' || $project->start_date != NULL) && ($project->end_date != '' || $project->end_date != NULL)) {
          if(new FrozenTime($task->end_date) >= $project->start_date && new FrozenTime($task->end_date) <= $project->end_date){
            return true;
          }
          else {
            return false;
          }
        }
      }

      return true;
    }

    public function availability(){
      //Get the current week
      $monday = strtotime("last monday");
      $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
      $friday = strtotime(date("Y-m-d",$monday)." +4 days");
      $this_week_sd = date("Y-m-d",$monday);
      $this_week_ed = date("Y-m-d",$friday);

      $workOfTheWeek = array();
      $tasks = $this->Tasks->find()->where(['user_id' => $this->Auth->user('id'), 'Tasks.start_date >=' => $this_week_sd, 'Tasks.start_date <=' => $this_week_ed]);

      if($this->request->is('post')){
        $userID = $this->request->data['id'];
        $tasks = $this->Tasks->find()->where(['user_id' => $userID, 'Tasks.start_date >=' => $this_week_sd, 'Tasks.start_date <=' => $this_week_ed]);
      }

      foreach ($tasks as $task) {
        array_push($workOfTheWeek, ['title' => $task->name, 'start' => (($task->start_date != null) ? $task->start_date->format('Y-m-d H:i'):$task->start_date), 'end' => (($task->end_date != null) ? $task->end_date->format('Y-m-d H:i'):$task->end_date)]);
      }
      $users = TableRegistry::get('Users')->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
      $this->set('workOfTheWeek', json_encode($workOfTheWeek));
      $this->set('users', $users);
      $this->set('_serialize', ['workOfTheWeek']);
    }

    public function reopenTask(){
      if ($this->request->is('post')) {
        $task = $this->Tasks->get($this->request->data['task_id'], ['contain' => ['Milestones', 'Users']]);

        if ($this->request->data['duration'] != '') {
          $previousDuration = $task->duration;
          $task->duration = $this->request->data['duration'];
          $previousEndDate = $task->end_date;
          $task->end_date = $this->Tasks->getEndDate($task);
          if ($previousEndDate <= $task->end_date) {
            if ($this->correctDates($task)){
              $task->state = 'PA';
              if ($this->Tasks->save($task)) {
                $task->project_id = $task->milestone->project_id;
                $this->updateCostProject($previousDuration, $task);
                $messageNot = 'Se ha reabierto la tarea: '.$task->name;
                $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
                $notification = new Notification(['message' => $messageNot, 'user_id' => $task->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
                $notificationTable = TableRegistry::get('Notifications');
                $notificationTable->save($notification);
                $message = "<p>La tarea: <strong>".$task->name."</strong> ha sido reabierta con una fecha de fin de: ".$task->end_date."</p>";
                $link =Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
                $this->sendEmails('Tarea reabierta', $message, $task->user, $link);
                $this->Flash->success(__('La tarea ha sido reabierta correctamente.'));
                return $this->redirect(['action' => 'index']);
              }
            }
            else {
              $this->Flash->error(__('La tarea no pudo ser salvada, existen conflictos entre las fechas y el milestone. Intente nuevamente'));
            }
          }
          else {
            $this->Flash->error(__('La fecha de fin es menor a la fecha de fin previa. Intente nuevamente'));
          }
        }
        else {
          $task->state = 'P';
          if ($this->Tasks->save($task)) {
            $messageNot = 'Se ha reabierto la tarea: '.$task->name;
            $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
            $notification = new Notification(['message' => $messageNot, 'user_id' => $task->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
            $notificationTable = TableRegistry::get('Notifications');
            $notificationTable->save($notification);
            $message = "<p>La tarea: <strong>".$task->name."</strong> ha sido reabierta con una fecha de fin de: ".$task->end_date."</p>";
            $link =Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
            $this->sendEmails('Tarea reabierta', $message, $task->user, $link);
            $this->Flash->success(__('La tarea ha sido reabierta correctamente.'));
            return $this->redirect(['action' => 'index']);
          }
        }
        return $this->redirect(['action' => 'index']);
      }
    }

    public function sendTaskForApproval(){
      if ($this->request->is('ajax')) {
        $id = $this->request->data['id'];
        $task = $this->Tasks->get($id, ['contain' => ['Users', 'Milestones']]);
        $adminUser = TableRegistry::get('Milestones')->find()
                                                      ->select(['Milestones.id'])
                                                      ->autofields(false)
                                                      ->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                        return $q->select(['Projects.id'])
                                                                  ->autofields(false)
                                                                  ->contain(['Companies' => function(\Cake\ORM\Query $q){
                                                                    return $q->select(['Companies.user_id'])
                                                                              ->autofields(false);
                                                                  }]);
                                                      }])->first();

        $task->state = 'PA';
        if ($this->Tasks->save($task)) {
          $messageNot = 'Se ha enviado la tarea: '.$task->name.' para su aprobación';
          $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
          $notification = new Notification(['message' => $messageNot, 'user_id' => $adminUser->project->company->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
          $notificationTable = TableRegistry::get('Notifications');
          $notificationTable->save($notification);
          $message = "<h2>¡Hola!</h2><br><br>La tarea: <strong>".$task->name."</strong> ha sido marcada reabierta con una fecha de fin de: ".$task->end_date;
          $this->sendEmails('Tarea Finalizada', $message, $task->user, $url);
          $this->Flash->success(__('La tarea ha sido enviada para su aprobación'));
          $this->redirect(['action' => 'index']);
        }
      }
    }

    public function sendFeedbackTask(){
      $task = $this->Tasks->newEntity();
      if ($this->request->is('post')) {
        $task = $this->Tasks->get($this->request->data['task_id']);
        $task->comments = $this->request->data['comments'];
        $task->state = 'P';
        if ($this->Tasks->save($task)) {
          $message = 'Se ha enviado retroalimentación de la tarea: '.$task->name;
          $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
          $notification = new Notification(['message' => $message, 'user_id' => $task->user_id, 'state' => 'NR', 'date' => date('Y-m-d H:i'), 'url' => $url]);
          $notificationTable = TableRegistry::get('Notifications');
          $notificationTable->save($notification);
          $this->Flash->success(__('La tarea ha sido devuelta exitosamente.'));
          $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
      }
      $this->set(compact('task'));
      $this->set('_serialize', ['task']);
    }

    public function changeStatusTask(){
      $result = false;
      if ($this->request->is('post')) {
        $taskID = $this->request->data['id'];
        $state = $this->request->data['state'];
        $task = $this->Tasks->get($taskID);
        $task->state = $state;

        if($this->Tasks->save($task)){
          $result = true;
        }
      }

      $this->set(compact('result'));
      $this->set('_serialize', ['result']);
    }

    public function getTasksUserApp(){
      $this->autoRender = false;
      header('Access-Control-Allow-Origin: *');

      if ($this->request->is('post')) {
        $user_id = $this->request->data['user_id'];
        $jwt = $this->request->data['jwt'];
        $project_id = $this->request->data['project_id'];

        $key = '16cf8e473001a6ceff36ab1a24134a76';
        $data = JWT::decode($jwt, $key, array('HS256'));

        $user = TableRegistry::get('Users')->get($user_id);
        $jwtToken = $data->data->jwtToken;
        $decodeJwtToken = base64_decode($jwtToken);
        $explodeToken = explode('.', $decodeJwtToken);

        if ($explodeToken[0] == $user->id && $explodeToken[1] == $user->username) {

          if ($user->role == 'A') {
            $tasks = $this->Tasks->find()->where(['Tasks.project_id' => $project_id]);

            $resultJ = json_encode(array('tasks' => $tasks));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
          }
          else {
            $tasks = $this->Tasks->find()->where(['Tasks.user_id' => $user_id, 'Tasks.project_id' => $project_id]);

            $resultJ = json_encode(array('tasks' => $tasks));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
          }
        }
      }
    }

    public function sendMessageSlack($user, $message){
      $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
      $client->to($user->slack_user)->send($message);
    }

}
