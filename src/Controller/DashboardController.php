<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;

/**
 * Dashboard Controller
 *
 *
 * @method \App\Model\Entity\Dashboard[] paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{

      public function isAuthorized($user){
        if(($user['role'] == 'R' || $user['role'] == 'E') && in_array($this->request->getParam('action'), ['userIndex'])){
            return true;
        }
        if($user['role'] == 'E' && in_array($this->request->getParam('action'), ['dashboardCustomer'])){
            return true;
        }
      return parent::isAuthorized($user);
      }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tax = TableRegistry::get('Tax')->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'tax_value']);
        $projects = TableRegistry::get('Projects')->find('list', ['limit' => 200]);
        $projectsInformation = $this->getProjectsInformation();
        $milestonesInformation = $this->getMilestonesInformation();
        $tasksInformation = $this->getTasksInformation();
        $invoicesInformation = $this->getInvoicesInformation();
        $costsInformation = $this->getCostsOrganization();
        $pendingTasks = json_encode($this->getPendingTasks());
        $pendingApprovedTasks = json_encode($this->getCompletedTasks());
        $finishTasks = json_encode($this->getFinishedTasks());
        $myTasks = json_encode($this->getPendingTasksByUser($this->Auth->user('id')));
        $myFinishTasks = json_encode($this->getFinishedTasksByUser($this->Auth->user('id')));


        $this->set(compact('projectsInformation', 'milestonesInformation', 'tasksInformation', 'invoicesInformation', 'tax', 'projects', 'costsInformation', 'pendingTasks', 'pendingApprovedTasks', 'finishTasks', 'myTasks', 'myFinishTasks'));
        $this->set('_serialize', ['projectsInformation', 'milestonesInformation', 'tasksInformation', 'invoicesInformation', 'tax', 'projects', 'costsInformation', 'pendingTasks', 'pendingApprovedTasks', 'finishTasks', 'myTasks', 'myFinishTasks']);
    }

    public function userIndex(){
      $milestonesInformation = $this->getMilestonesInformationByUser();
      $tasksInformation = $this->getTasksInformationByUser();
      $pendingTasks = json_encode($this->getPendingTasksByUser($this->Auth->user('id')));
      $pendingApprovedTasks = json_encode($this->getCompletedTasks());
      $finishTasks = json_encode($this->getFinishedTasksByUser($this->Auth->user('id')));

      $this->set(compact('milestonesInformation', 'tasksInformation', 'pendingTasks', 'pendingApprovedTasks', 'finishTasks'));
    }

    public function dashboardCustomer(){

    }

    private function getProjectsInformation(){
      $this->loadModel('Projects');
      $projects = TableRegistry::get('Projects')->find();
      $pendingProjects = 0;
      $projectLowProfiability = array();
      $projectWithoutInvoice = array();
      $projectForToday = array();
      $delayedProjects = array();

      foreach ($projects as $project) {
        if($project->state == 'O'){
          $pendingProjects++;
        }

        if($this->Projects->getProfiabilityProject($project->id) <= 15){
          array_push($projectLowProfiability, $project);
        }

        if($project->invoiced == 0){
          array_push($projectWithoutInvoice, $project);
        }

        if(($project->end_date != null ?$project->end_date->format('Y-m-d'):$project->end_date) == Time::now()->format('Y-m-d')){
          array_push($projectForToday, $project);
        }
        if((($project->end_date != null ?$project->end_date->format('Y-m-d'):$project->end_date) < Time::now()->format('Y-m-d')) && ($project->state === 'O')){
          array_push($delayedProjects, $project);
        }
      }

      return ['pendingProjects' => $pendingProjects, 'projectLowProfiability' => $projectLowProfiability, 'projectWithoutInvoice' => $projectWithoutInvoice, 'projectForToday' => $projectForToday, 'delayedProjects' => $delayedProjects];
    }

    private function getMilestonesInformation(){
      $milestones = TableRegistry::get('Milestones')->find()->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                                            return $q->select('name')->autofields(false);
                                                                      }]);
      $pendingMilestones = 0;
      $delayedMilestones = array();
      $milestonesForToday = array();

      foreach ($milestones as $milestone) {
        if($milestone->state == 'O'){
          $pendingMilestones++;
        }

        if(($milestone->end_date != null ? $milestone->end_date->format('Y-m-d'):$milestone->end_date) == Time::now()->format('Y-m-d')){
          array_push($milestonesForToday, $milestone);
        }

        if(($milestone->end_date != null ? $milestone->end_date->format('Y-m-d'):$milestone->end_date) < Time::now()->format('Y-m-d') && ($milestone->state === 'O')){
          array_push($delayedMilestones, $milestone);
        }
      }

      return ['pendingMilestones' => $pendingMilestones, 'delayedMilestones' => $delayedMilestones, 'milestonesForToday' => $milestonesForToday];
    }

    public function getTasksInformation(){
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->contain(['Milestones' => function(\Cake\ORM\Query $q){
                                              return $q->select('Milestones.name')->autofields(false)
                                                      ->contain(['Projects'=> function(\Cake\ORM\Query $q){
                                                        return $q->select('Projects.name')->autofields(false);
                                                      }]);
                                            }, 'Users' => function(\Cake\ORM\Query $q){
                                              return $q->select('Users.username')->autofields(false);
                                            }]);
      $pendingTasks = 0;
      $delayedTasks = array();
      $tasksForToday = array();

      foreach ($tasks as $task) {
        if($task->state = 'P'){
          $pendingTasks++;
        }

        if(($task->end_date != null ? $task->end_date->format('Y-m-d'):$task->end_date) ==  Time::now()->format('Y-m-d')){
          array_push($tasksForToday, $task);
        }

        if(($task->end_date != null ? $task->end_date->format('Y-m-d'):$task->end_date) < Time::now()->format('Y-m-d')&& ($task->state === 'P')){
          array_push($delayedTasks, $task);
        }
      }

      return ['pendingTasks' => $pendingTasks, 'delayedTasks' => $delayedTasks, 'tasksForToday' => $tasksForToday];
    }

    public function getInvoicesInformation(){
      $this->loadModel('Invoices');
      $invoices = $this->Invoices->find()->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                      return $q->select(['Projects.name'])->autofields(false);
                                                  }]);
      $invoicesForToday = array();
      $delayedInvoices = array();

      foreach ($invoices as $invoice) {
        if(($invoice->due_date != null ? $invoice->due_date->format('Y-m-d'):$invoice->due_date) < Time::now()->format('Y-m-d') && ($invoice->state === 'P' || $invoice->state === 'V')){
          array_push($delayedInvoices, $invoice);
        }

        if(($invoice->due_date != null ? $invoice->due_date->format('Y-m-d'):$invoice->due_date) == Time::now()->format('Y-m-d')){
          array_push($invoicesForToday, $invoice);
        }
      }

      return ['invoicesForToday' => $invoicesForToday, 'delayedInvoices' => $delayedInvoices];
    }

    private function getMilestonesInformationByUser(){
      $milestones = TableRegistry::get('MilestonesUsers')->find()->where(['user_id' => $this->Auth->user('id')])->contain(['Milestones' => function(\Cake\ORM\Query $q){return $q->find('all')->contain(['Projects']); }]);
      $pendingMilestones = 0;
      $milestonesForToday = array();
      $delayedMilestones = array();

      foreach ($milestones as $milestone_user) {
        if($milestone_user->milestone->state == 'O'){
          $pendingMilestones++;
        }

        if(($milestone_user->milestone->end_date != null ? $milestone_user->milestone->end_date->format('Y-m-d'):$milestone_user->milestone->end_date) == Time::now()->format('Y-m-d')){
          array_push($milestonesForToday, $milestone_user->milestone);
        }

        if(($milestone_user->milestone->end_date != null ? $milestone_user->milestone->end_date->format('Y-m-d'):$milestone_user->milestone->end_date) < Time::now()->format('Y-m-d') && ($milestone_user->milestone->state === 'O')){
          array_push($delayedMilestones, $milestone_user->milestone);
        }
      }

      return ['pendingMilestones' => $pendingMilestones, 'delayedMilestones' => $delayedMilestones, 'milestonesForToday' => $milestonesForToday];
    }

    public function getTasksInformationByUser(){
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->where(['user_id' => $this->Auth->user('id')])
                                          ->contain(['Milestones' => function(\Cake\ORM\Query $q){
                                              return $q->select('Milestones.name')->autofields(false)
                                                      ->contain(['Projects'=> function(\Cake\ORM\Query $q){
                                                        return $q->select('Projects.name')->autofields(false);
                                                      }]);
                                            }]);
      $pendingTasks = 0;
      $delayedTasks = array();
      $tasksForToday = array();

      foreach ($tasks as $task) {
        if($task->state = 'P'){
          $pendingTasks++;
        }

        if(($task->end_date != null ? $task->end_date->format('Y-m-d'):$task->end_date) ==  Time::now()->format('Y-m-d')){
          array_push($tasksForToday, $task);
        }

        if(($task->end_date != null ? $task->end_date->format('Y-m-d'):$task->end_date) < Time::now()->format('Y-m-d') && ($task->state === 'P')){
          array_push($delayedTasks, $task);
        }
      }

      return ['pendingTasks' => $pendingTasks, 'delayedTasks' => $delayedTasks, 'tasksForToday' => $tasksForToday];
    }

    public function getCostsOrganization(){
      $end = new FrozenTime(date('Y-m-d'));
      $start = $end->modify('-1 month');
      $projects = TableRegistry::get('Projects')->find()->select(['Projects.total_cost'])->autofields(false)->where(['Projects.start_date >=' => $start, 'Projects.start_date <=' => $end]);
      $invoices = TableRegistry::get('Invoices')->find()->select(['Invoices.total_amount'])->autofields(false)->where(['Invoices.date >=' => $start, 'Invoices.date <=' => $end]);;
      $projectCosts = 0;
      $invoicedCosts = 0;

      foreach ($projects as $project) {
        $projectCosts += $project->total_cost;
      }

      foreach ($invoices as $invoice) {
        $invoicedCosts += $invoice->total_amount;
      }

      return ['projectCost' => $projectCosts, 'invoicedCosts' => $invoicedCosts];
    }

    public function getPendingTasks(){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      if ($this->Auth->user('role') == 'A') {
        $tasks = TableRegistry::get('Tasks')->find()
                                            ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                            ->autofields(false)
                                            ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'Tasks.state' => 'P']);
      }

      return $tasks;
    }

    public function getPendingTasksByUser($userID){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                          ->autofields(false)
                                          ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'Tasks.state' => 'P', 'Tasks.user_id' => $userID]);
      return $tasks;
    }

    public function getCompletedTasks(){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      if ($this->Auth->user('role') == 'A') {
        $tasks = TableRegistry::get('Tasks')->find()
                                            ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                            ->autofields(false)
                                            ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'Tasks.state' => 'PA']);
      }
      else {
        $tasks = TableRegistry::get('Tasks')->find()
                                            ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                            ->autofields(false)
                                            ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'Tasks.state' => 'PA', 'Tasks.user_id' => $this->Auth->user('id')]);
      }

      return $tasks;
    }

    public function getFinishedTasks(){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      if ($this->Auth->user('role') == 'A') {
        $tasks = TableRegistry::get('Tasks')->find()
                                            ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                            ->autofields(false)
                                            ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'OR' => [['Tasks.state' => 'TA'], ['Tasks.state' => 'TD']]]);
      }

      return $tasks;
    }

    public function getFinishedTasksByUser($userID){
      $today = new FrozenTime((date('Y-m-d').' 00:00'));
      $endDay = $today->modify('+23 hours 59 minutes');
      $tasks = TableRegistry::get('Tasks')->find()
                                          ->select(['Tasks.id', 'title' => 'Tasks.name', 'date' => 'Tasks.end_date', 'Tasks.duration'])
                                          ->autofields(false)
                                          ->where(['Tasks.end_date >= ' => $today->format('Y-m-d H:i:s'), 'Tasks.end_date <=' => $endDay->format('Y-m-d H:i:s'), 'OR' => [['Tasks.state' => 'TA'], ['Tasks.state' => 'TD']], 'Tasks.user_id' => $userID]);
      return $tasks;
    }
}
