<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Cache\Cache;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = array('Cookie');

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginAction' => [
              'controller' => 'Users',
              'action' => 'login'
            ],
              'loginRedirect' => [
                  'controller' => 'Dashboard',
                  'action' => 'index'
              ],
              'logoutRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login'
              ],
              'authError' => false,
              'unauthorizedRedirect' => ['controller' => 'Dashboard', 'action' => 'index']
          ]);
        $this->Auth->config('authorize', ['Controller']);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        $this->set('Auth', $this->Auth);
    }

    public function beforeFilter(Event $event){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $this->Auth->user('id')])->contain(['Permissions']);
      $isAuthorizedCRM = false;
      $isAuthorizedProjects = false;
      $isAuthorizedMilestones = false;
      $isAuthorizedTasks = false;
      $isAuthorizedInvoices = false;
      $isAuthorizedTimesheets = false;
      $isAuthorizedReports = false;
      $isAuthorizedConfig = false;
      foreach ($permissions as $permission) {
        switch ($permission->permission->module) {
          case 'CRM':
            $isAuthorizedCRM = true;
            break;
          case 'Projects':
            $isAuthorizedProjects = true;
            break;
          case 'Milestones':
            $isAuthorizedMilestones = true;
            break;
          case 'Tasks':
            $isAuthorizedTasks = true;
            break;
          case 'Invoices':
            $isAuthorizedInvoices = true;
            break;
          case 'Timesheets':
            $isAuthorizedTimesheets = true;
            break;
          case 'Reports':
            $isAuthorizedReports = true;
            break;
          case 'Config':
            $isAuthorizedConfig = true;
            break;
        }

      }

      // $locale = Cache::read('locale');
      $locale = $this->request->session()->read('Config.defaultLocale');
      if($locale !== false){
        I18n::locale($locale);
      }

      $this->set(compact('isAuthorizedCRM', 'isAuthorizedProjects', 'isAuthorizedMilestones', 'isAuthorizedTasks', 'isAuthorizedInvoices', 'isAuthorizedTimesheets', 'isAuthorizedReports', 'isAuthorizedConfig'));
      $this->set('user', $this->Auth->user());
    }

    public function isAuthorized($user){
      if(isset($user['role']) && $user['role'] === 'A'){
        return true;
      }
      return false;
    }
}
