<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Account;

/**
 * Banks Controller
 *
 * @property \App\Model\Table\BanksTable $Banks
 *
 * @method \App\Model\Entity\Bank[] paginate($object = null, array $settings = [])
 */
class BanksController extends AppController
{

  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Config']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'view', 'delete'])){
          return true;
      }
    }

  return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Organizations']
        ];
        $banks = $this->paginate($this->Banks);

        $this->set(compact('banks'));
        $this->set('_serialize', ['banks']);
    }

    /**
     * View method
     *
     * @param string|null $id Bank id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bank = $this->Banks->get($id, [
            'contain' => ['Organizations']
        ]);

        $this->set('bank', $bank);
        $this->set('_serialize', ['bank']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bank = $this->Banks->newEntity();
        if ($this->request->is('post')) {
            $bank = $this->Banks->patchEntity($bank, $this->request->getData());
            $accounts = $bank->accounts;
            if ($this->Banks->save($bank)) {
              $tableAccounts = TableRegistry::get('Accounts');

              for ($i=0; $i < count($accounts['account_number']); $i++) {
                $account = new Account(['account_number' => $accounts['account_number'][$i], 'type' => $accounts['account_type'][$i], 'bank_id' => $bank->id]);
                $tableAccounts->save($account);
              }
                $this->Flash->success(__('El banco ha sido salvado.'));

                return $this->redirect(['controller' => 'Organizations', 'action' => 'view', $bank->organization_id]);
            }
            $this->Flash->error(__('El banco no ha sido salvado. Por favor intente nuevamente.'));
        }
        $organizations = $this->Banks->Organizations->find('list', ['limit' => 200]);
        $this->set(compact('bank', 'organizations'));
        $this->set('_serialize', ['bank']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bank id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bank = $this->Banks->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bank = $this->Banks->patchEntity($bank, $this->request->getData());
            $tableAccount = TableRegistry::get('Accounts');
            if ($this->Banks->save($bank)) {
              for ($i=0; $i < count($bank->accounts['account_number']); $i++) {
                if(isset($bank->accounts['id'][$i])){
                  $account = $tableAccount->get($bank->accounts['id'][$i]);
                  $account->account_number = $bank->accounts['account_number'][$i];
                  $account->type = $bank->accounts['account_type'][$i];
                  $tableAccount->save($account);
                }
                else{
                  $account = new Account(['account_number' => $bank->accounts['account_number'][$i], 'type' => $bank->accounts['account_type'][$i], 'bank_id' => $bank->id]);
                  $tableAccount->save($account);
                }
              }
              if($bank->accounts_delete != ''){
                $accountsToDelete = explode(',',$bank->accounts_delete);
                for ($i=0; $i < count($accountsToDelete); $i++) {
                  $account = $tableAccount->get($accountsToDelete[$i]);
                  $tableAccount->delete($account);
                }
              }
                $this->Flash->success(__('El banco ha sido salvado.'));

                return $this->redirect(['controller' => 'Organizations', 'action' => 'view', $bank->organization_id]);
            }
            $this->Flash->error(__('El banco no ha sido salvado. Por favor intente nuevamente.'));
        }
        $organizations = $this->Banks->Organizations->find('list', ['limit' => 200]);
        $accounts = TableRegistry::get('Accounts')->find()->where(['bank_id' => $bank->id]);
        $this->set(compact('bank', 'organizations', 'accounts'));
        $this->set('_serialize', ['bank']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bank id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bank = $this->Banks->get($id);
        $organization = $bank->organization_id;
        if ($this->Banks->delete($bank)) {
            $this->Flash->success(__('El banco ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El banco no pudo ser eliminado. Por favor intente nuevamente'));
        }

        return $this->redirect(['controller' => 'Organizations', 'action' => 'view', $organization]);
    }
}
