<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Parametrization Controller
 *
 *
 * @method \App\Model\Entity\Parametrization[] paginate($object = null, array $settings = [])
 */
class ParametrizationController extends AppController
{

  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Config']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['settings'])){
          return true;
      }
    }

  return parent::isAuthorized($user);
  }

    public function settings(){
      $tags = TableRegistry::get('Tags')->find();
      $roles = TableRegistry::get('Roles')->find();
      $industries = TableRegistry::get('IndustrieSectors')->find();
      $payment_terms = TableRegistry::get('PaymentTerms')->find();
      $taxes = TableRegistry::get('Tax')->find();
      $tasks = TableRegistry::get('TaskTypes')->find();
      $banks = TableRegistry::get('Banks')->find('list')->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'I']);;
      $banksBeneficiary = TableRegistry::get('Banks')->find('list')->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'B']);;
      $organization = TableRegistry::get('Organizations')->get($this->Auth->user('organization_id'));
      $templates = TableRegistry::get('Templates')->find();
      $customersQuotesTemplates = TableRegistry::get('Customers')->find();
      $cssFile = file_get_contents(WWW_ROOT.'css/ex2.css');
      $this->set(compact('tags', 'roles', 'industries', 'payment_terms', 'taxes', 'tasks', 'banks', 'organization', 'cssFile', 'templates', 'banksBeneficiary', 'customersQuotesTemplates'));
      $this->set('_serialize', ['tags']);
    }

    public function configureInvoices(){
      if($this->request->is('post')){
        $tableOrg = TableRegistry::get('Organizations');
        $organization = $tableOrg->get($this->Auth->user('organization_id'));
        if ($this->request->data['intermediaryBank'] != 0) {
          $organization->default_intermediary_bank = $this->request->data['intermediaryBank'];
        }
        else {
          $organization->default_intermediary_bank = 0;
        }
        if ($this->request->data['beneficiaryBank'] != 0) {
          $organization->default_beneficiary_bank = $this->request->data['beneficiaryBank'];
        }
        else {
          $organization->default_beneficiary_bank = 0;
        }
        if ($this->request->data['beneficiaryClient'] != 0) {
          $organization->client_beneficiary = $this->request->data['beneficiaryClient'];
        }
        else {
          $organization->client_beneficiary = 0;
        }
        $organization->notes_invoices = $this->request->data['notes'];

        if ($tableOrg->save($organization)) {
          $this->Flash->success(__('Los datos se guardaron correctamente'));
          return $this->redirect(['action' => 'settings']);
        }

        $this->Flash->error(__('Los datos no se pudieron guardar correctamente'));
      }
    }

    public function editCssFile(){
      $cssFile = WWW_ROOT.'css/ex2.css';
      if ($this->request->is('post')) {
        $cssFile = $this->request->data['cssFile'];
        $file = WWW_ROOT.'css/ex2.css';
        file_put_contents($file, $cssFile);
        $this->Flash->success(__('El css ha sido editado exitosamente.'));
        return $this->redirect(['action' => 'settings']);
      }
    }
}
