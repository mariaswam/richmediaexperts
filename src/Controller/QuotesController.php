<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Component;
use App\Model\Entity\Project;
use Cake\I18n\Time;
use PHPExcel;
use PHPExcel_IOFactory;

/**
 * Quotes Controller
 *
 * @property \App\Model\Table\QuotesTable $Quotes
 *
 * @method \App\Model\Entity\Quote[] paginate($object = null, array $settings = [])
 */
class QuotesController extends AppController
{

  public function isAuthorized($user){
    if($user['role'] == 'E' && in_array($this->request->getParam('action'), ['index', 'add', 'view', 'resendQuoteForApproval', 'changeStateQuote', 'downloadQuote', 'edit', 'delete', 'getApprovedQuotes', 'report'])){
        return true;
    }
    return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->Auth->user('role') == 'E') {
          $quotes = $this->Quotes->find()->where(['Quotes.user_id' => $this->Auth->user('id'), 'state NOT IN' => ['approved', 'used', 'invoice']])->contain(['Companies']);
          $quotesApproved = $this->Quotes->find()->where(['Quotes.user_id' => $this->Auth->user('id'), 'state' => 'approved'])->contain(['Companies']);
          $invoices = TableRegistry::get('Invoices')->find()->where(['Invoices.company_id' => $this->Auth->user('company_id')])->contain(['Projects', 'Companies', 'Customers']);
          $this->set(compact('quotes', 'quotesApproved', 'invoices'));
          $this->set('_serialize', ['quotes', 'quotesApproved', 'invoices']);
          $this->render('getQuotesClient');
        }
        elseif ($this->Auth->user('role') == 'R' || $this->Auth->user('role') == 'A') {
          $this->paginate = [
              'contain' => ['Companies'],
              'order' => ['date' => 'DESC']
          ];
          $quotes = $this->paginate($this->Quotes->find('all')->where(['state NOT IN' => ['used', 'invoice']]));

          $this->set(compact('quotes'));
          $this->set('_serialize', ['quotes']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $quote = $this->Quotes->get($id, [
            'contain' => ['Companies', 'Components']
        ]);

        $this->set('quote', $quote);
        $this->set('_serialize', ['quote']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $quote = $this->Quotes->newEntity();
        if ($this->request->is('post')) {
            $quote = $this->Quotes->patchEntity($quote, $this->request->getData());
            $quote->user_id = $this->Auth->user('id');
            if ($this->Quotes->save($quote)) {
                if (!empty($quote->itemsArray)) {
                  $tableItem = TableRegistry::get('Components');
                  $itemsQuote = $quote->itemsArray;
                  for ($i=0; $i < count($itemsQuote['description']); $i++) {
                    $description = $itemsQuote['description'][$i];
                    $price = $itemsQuote['price'][$i];
                    $quantity = $itemsQuote['quantity'][$i];
                    $item = new Component(['description' => $description, 'price' => $price, 'quantity' => $quantity, 'subtotal' => $price*$quantity, 'quote_id' => $quote->id]);
                    $tableItem->save($item);
                  }
                }

                if ($this->Auth->user('role') == 'E') {
                  $this->sendNotificationToAdmin($quote, 'A quote has been sent to approval');
                }
                $this->Flash->success(__('The quote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The quote could not be saved. Please, try again.'));
        }
        $companies = $this->Quotes->Companies->find('list', ['limit' => 200]);
        $templates = TableRegistry::get('Templates')->find()->where(['type' => 'header']);
        $customers = TableRegistry::get('Customers')->find();
        $this->set(compact('quote', 'companies', 'templates', 'customers'));
        $this->set('_serialize', ['quote']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $quote = $this->Quotes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $quote = $this->Quotes->patchEntity($quote, $this->request->getData());
            $this->checkNewsItems($quote);
            if ($quote->state == 'approved') {
              $quote->state = 'draft';
            }
            if ($this->Quotes->save($quote)) {
                $this->sendNotificationToAdmin($quote, 'A quote has been modified and sent for approval');
                $this->Flash->success(__('The quote has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The quote could not be saved. Please, try again.'));
        }
        $companies = $this->Quotes->Companies->find('list', ['limit' => 200]);
        $items = $this->Quotes->Components->find()->where(['quote_id' => $id]);
        $this->set(compact('quote', 'companies', 'items'));
        $this->set('_serialize', ['quote']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Quote id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $quote = $this->Quotes->get($id);
        if ($this->Quotes->delete($quote)) {
          if (!$this->request->is('ajax')) {
            $this->Flash->success(__('The quote has been deleted.'));
          }
        } else {
            $this->Flash->error(__('The quote could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function downloadQuote($quoteID = null){
      ini_set("memory_limit","128M");
      $quoteTemplate = file_get_contents(WWW_ROOT.'invoices_templates/quote_template.html');
      $cssQuote = file_get_contents(WWW_ROOT.'css/ex2quote.css');
      $quote = $this->Quotes->get($quoteID, [
                    'contain' => ['Companies']
                ]);
      $organization = TableRegistry::get('Organizations')->get($this->Auth->user('organization_id'));
      $items = TableRegistry::get('Components')->find()->where(['quote_id' => $quoteID]);
      $descriptionContent = '';

      $quoteTemplate = str_replace('$quote_number', (__('Quote') . ' ' . $quoteID), $quoteTemplate);
      $quoteTemplate = str_replace('$logo_address', (WWW_ROOT.'img/'.$organization->logo), $quoteTemplate);
      $quoteTemplate = str_replace('$title_project', __('Project:'), $quoteTemplate);
      $quoteTemplate = str_replace('$project_name', $quote->name, $quoteTemplate);
      $quoteTemplate = str_replace('$title_date', __('Date:'), $quoteTemplate);
      $quoteTemplate = str_replace('$date', $quote->date, $quoteTemplate);
      $quoteTemplate = str_replace('$quote_total', ('$'.$quote->total_amount), $quoteTemplate);
      $quoteTemplate = str_replace('$title_price', __('Price'), $quoteTemplate);
      $quoteTemplate = str_replace('$title_quantity', __('Quantity'), $quoteTemplate);
      $quoteTemplate = str_replace('$header_content', $quote->header, $quoteTemplate);
      $quoteTemplate = str_replace('$description_quote', $quote->description, $quoteTemplate);

      foreach ($items as $item) {
        $descriptionContent .= '<tr><td class="desc">'.$item->description.'</td><td class="unit">$'.$item->price.'</td><td class="qty">'.$item->quantity.'</td><td class="total">$'.$item->subtotal.'</td></tr>';
      }

      $quoteTemplate = str_replace('$quote_description', $descriptionContent, $quoteTemplate);

      $mPDF = new \Mpdf\Mpdf();
      $mPDF->WriteHTML($cssQuote, 1);
      $mPDF->writeHTML($quoteTemplate);
      $mPDF->Output('quote'.$quote->id.'.pdf', 'D');
    }

    public function checkNewsItems($quote){
      $tableComponents = TableRegistry::get('Components');
      $items = $tableComponents->find("list", ['keyField' => 'id','valueField' => 'description'])
                              ->select(['description'])
                              ->where(['quote_id' => $quote->id])->toArray();
      $itemsToAdd = array_diff($quote->itemsArray['description'], $items);
      $itemsToDelete = array_diff($items, $quote->itemsArray['description']);
      $itemsToUpdate = array_intersect($quote->itemsArray['description'], $items);

      //Check for items to add
      if (count($itemsToAdd) > 0) {
        foreach ($itemsToAdd as $key => $value) {
          $subtotal = $quote->itemsArray['price'][$key] * $quote->itemsArray['quantity'][$key];
          $item = new Component(['description' => $quote->itemsArray['description'][$key], 'price' => $quote->itemsArray['price'][$key], 'quantity' => $quote->itemsArray['quantity'][$key], 'subtotal' => $subtotal, 'quote_id' => $quote->id]);
          $tableComponents->save($item);
        }
      }

      //Checks for items to delete
      if (count($itemsToDelete) > 0) {
        $amountToRemove = 0;

        foreach ($itemsToDelete as $key => $value) {
          $item = $tableComponents->get($key);
          $amountToRemove += $item->subtotal;
          $tableComponents->delete($item);
        }
      }

      //Check for items to update
      if (count($itemsToUpdate)) {
        foreach ($itemsToUpdate as $key => $value) {
          $item = $tableComponents->find()->where(['description LIKE' => $value, 'quote_id' => $quote->id])->first();
          $item->description = $quote->itemsArray['description'][$key];
          $item->price = $quote->itemsArray['price'][$key];
          $item->quantity = $quote->itemsArray['quantity'][$key];
          $item->subtotal = ($item->price*$item->quantity);
          $tableComponents->save($item);
        }
      }
    }

    public function convertQuoteInProject(){
      $this->autoRender = false;
      if ($this->request->is('ajax')) {
        $id = $this->request->getData('quote_id');
        $quote = $this->Quotes->get($id, ['contain' => ['Companies']]);
        $tableProjects = TableRegistry::get('Projects');

        $project = new Project(['name' => $quote->name, 'description' => $quote->description,'budget' => $quote->total_amount, 'estimated_cost' => $quote->total_amount, 'company_id' => $quote->company_id, 'created_at' => Time::now(), 'final_end_date' => Time::now()]);
      if($tableProjects->save($project)){
          $this->sendNotificationToClient($quote);
          $quote->state = 'used';
          TableRegistry::get('Quotes')->save($quote);
          $resultJ = json_encode(array('message' => 'success', 'project_id' => $project->id, 'quote_id' => $quote->id));
        }
      }

      $this->response->type('json');
      $this->response->body($resultJ);
      return $this->response;
    }

    public function changeStateQuote(){
      $this->autoRender = false;
      if ($this->request->is('ajax')) {
        $id = $this->request->getData('quote_id');
        $state = $this->request->getData('state');
        $quote = $this->Quotes->get($id, ['contain' => ['Companies']]);
        $quote->state = $state;

        if (TableRegistry::get('Quotes')->save($quote)) {
          if ($state == 'approved') {
            $this->sendNotificationToClient($quote);
          }
          else{
            $this->sendNotificationToAdmin($quote, 'A quote has been approved by client');
          }
          $resultJ = json_encode(array('message' => 'success', 'quote_id' => $quote->id));
        }
      }

      $this->response->type('json');
      $this->response->body($resultJ);
      return $this->response;
    }

    public function sendNotificationToAdmin($quote, $messageHeader){
      $adminUser = TableRegistry::get('Users')->find()->where(['role' => 'A'])->first();
      $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
      $from = new \SendGrid\Email("Richmediaexperts", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($adminUser->first_name.' '.$adminUser->last_name, $adminUser->email);
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
      //Content
      $message = $messageHeader . PHP_EOL . "> Quote ID: " . $quote->id . PHP_EOL . "> Quote name: " . $quote->name;
      $messageEmail = "<p>".$messageHeader.":</p><ul><li><strong>Quote id: ".$quote->id."</strong></li><li><strong>Quote name:</strong>".$quote->name."</li><li><strong>Total amount: </strong>$".$quote->total_amount."</li></ul>";

      $content = new \SendGrid\Content("text/html", $template);
      $mail = new \SendGrid\Mail($from, __('Quote for approval'), $to, $content);
      $mail->personalization[0]->addSubstitution('%html_content%', $messageEmail);
      $mail->personalization[0]->addSubstitution('%text_content%', '<h2>Hi!</h2>');
      $sg = new \SendGrid("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");

      $response = $sg->client->mail()->send()->post($mail);
      $client->to($adminUser->slack_user)->send($message);
    }

    public function sendNotificationToClient($quote){
      $clientUser = TableRegistry::get('Users')->find()->where(['company_id' => $quote->company_id])->first();

      if ($clientUser) {
        $from = new \SendGrid\Email("Richmediaexperts", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email($clientUser->first_name.' '.$clientUser->last_name, $clientUser->email);
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        //Content
        $messageEmail = "<p>The following quote has been approved:</p><ul><li><strong>Quote id: ".$quote->id."</strong></li><li><strong>Quote name:</strong>".$quote->name."</li><li><strong>Total amount: </strong>$".$quote->total_amount."</li></ul>";

        $content = new \SendGrid\Content("text/html", $template);
        $mail = new \SendGrid\Mail($from, __('Approved quote'), $to, $content);
        $mail->personalization[0]->addSubstitution('%html_content%', $messageEmail);
        $mail->personalization[0]->addSubstitution('%text_content%', '<h2>Hi!</h2>');
        $sg = new \SendGrid("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");

        $response = $sg->client->mail()->send()->post($mail);
      }
    }

    public function resendQuoteForApproval($id = null){
        $this->request->allowMethod(['post']);
        $quote = $this->Quotes->get($id);

        $adminUser = TableRegistry::get('Users')->find()->where(['role' => 'A'])->first();
        $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
        $from = new \SendGrid\Email("Richmediaexperts", 'wdpminfo@gmail.com');
        $to = new \SendGrid\Email($adminUser->first_name.' '.$adminUser->last_name, $adminUser->email);
        $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
        //Content
        $message = "This is a reminder to approve the quote. " . PHP_EOL . "> Quote ID: " . $quote->id . PHP_EOL . "> Quote name: " . $quote->name;
        $messageEmail = "<p>This is a reminder to approve the quote:</p><ul><li><strong>Quote id: ".$quote->id."</strong></li><li><strong>Quote name:</strong>".$quote->name."</li><li><strong>Total amount: </strong>$".$quote->total_amount."</li></ul>";

        $content = new \SendGrid\Content("text/html", $template);
        $mail = new \SendGrid\Mail($from, __('Quote reminder'), $to, $content);
        $mail->personalization[0]->addSubstitution('%html_content%', $messageEmail);
        $mail->personalization[0]->addSubstitution('%text_content%', '<h2>Hi!</h2>');
        $sg = new \SendGrid("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");

        $response = $sg->client->mail()->send()->post($mail);
        $client->to($adminUser->slack_user)->send($message);

        $this->Flash->success(__('The quote has been resent.'));

        return $this->redirect(['action' => 'index']);
    }

    public function returnQuoteToApprove($id = null){
      $this->request->allowMethod(['post']);
      $quote = $this->Quotes->get($id);
      $quote->state = 'approved';
      if ($this->Quotes->save($quote)) {
          $this->Flash->success(__('The quote has resent to approval.'));
      } else {
          $this->Flash->error(__('The quote could not be deleted. Please, try again.'));
      }

      return $this->redirect(['action' => 'index']);
    }

    public function getApprovedQuotes(){
      $this->autoRender = false;
      if ($this->request->is('ajax')) {
        $approvedQuotes = $this->Quotes->find()->where(['state' => 'approved', 'Quotes.user_id' => $this->Auth->user('id')])->contain(['Companies']);

        $this->response->type('json');
        $this->response->body(json_encode($approvedQuotes));
        return $this->response;
      }
    }

    public function getPendingInvoiceQuotes(){
      $this->autoRender = false;
      if ($this->request->is('ajax')) {
        $approvedQuotes = $this->Quotes->find()->where(['state' => 'invoice'])->contain(['Companies']);

        $this->response->type('json');
        $this->response->body(json_encode($approvedQuotes));
        return $this->response;
      }
    }

    public function report($invoices = null){
        $quotes_id = $_GET['quotes'];
        $quotes_id = explode(",", $quotes_id);
        $quotes = $this->Quotes->find()
                                    ->contain(['Companies', 'Customers'])
                                    ->where(['Quotes.id IN' => $quotes_id]);


        $excelFile = new PHPExcel();
        $excelFile->getProperties()->setCreator("RME")->setLastModifiedBy("RME")->setTitle("Quotes report")
                           ->setSubject("Quotes")->setDescription("Document report")->setKeywords("office")
                           ->setCategory("Report");
        $excelFile->setActiveSheetIndex(0)->setCellValue('A1', __('Quotes report'))->setCellValue('A3', __('Date'))
                                          ->setCellValue('B3', Time::now()->format('m-d-Y H:i'))->setCellValue('A4', __('Report by:'))
                                          ->setCellValue('B4', 'RME');

        $excelFile->setActiveSheetIndex(0)->setCellValue('A6', __('ID'))->setCellValue('B6', __('Customer'))->setCellValue('C6', __('State'))->setCellValue('D6', __('Total amount'))
                                          ->setCellValue('E6', __('Purchase order'))->setCellValue('F6', __('Total Assets'))
                                          ->setCellValue('G6', __('ETA'));

        $lastRow = $excelFile->getActiveSheet()->getHighestRow();
        $totalRevenues = 0;
        foreach ($quotes as $quote) {
          $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+1), $quote->id)->setCellValue('B'.($lastRow+1), $quote->customer != null ? $quote->customer->name : '')->setCellValue('C'.($lastRow+1), $quote->state)->setCellValue('D'.($lastRow+1), $quote->total_amount)
                                            ->setCellValue('E'.($lastRow+1), (($quote->purchase_order != null ? $quote->purchase_order : '')))->setCellValue('F'.($lastRow+1), $quote->total_assets)
                                            ->setCellValue('G'.($lastRow+1), $quote->eta);
          $lastRow = $excelFile->getActiveSheet()->getHighestRow();
          $totalRevenues += $quote->total_amount;
        }
        $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+2), __('Total quoted'))->setCellValue('B'.($lastRow+2), ('$'.$totalRevenues));
        $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('B6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('C6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('D6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('E6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('F6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('G6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('H6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('I6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

        $excelFile->getActiveSheet()->setTitle(__('Quotes report'));
        $excelFile->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="invoices_report.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');

        // ob_end_clean();
        $writer->save('php://output');
    }

}
