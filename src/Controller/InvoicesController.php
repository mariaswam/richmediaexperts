<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Item;
use App\Model\Entity\Invoice;
use Cake\Mailer\Email;
use Joli\JoliNotif\Notification;
use Joli\JoliNotif\NotifierFactory;
use Cake\I18n\Time;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\I18n\FrozenTime;
use Cake\I18n\I18n;
use DateTime;
use DatePeriod;
use DateInterval;
use Dompdf\Dompdf;
use Cake\Cache\Cache;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 *
 * @method \App\Model\Entity\Invoice[] paginate($object = null, array $settings = [])
 */
class InvoicesController extends AppController
{

    public function isAuthorized($user){
        $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                            return $q->select()->where(['module' => 'Invoices']);
                                                                                                        }])->count();
        if($permissions != 0){
          if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete', 'view', 'statistics', 'getDueDate', 'makePaid', 'getInvoicesProject', 'getInvoicesCompany', 'cancelInvoice', 'discardInvoice', 'sendEmails', 'report', 'downloadInvoice', 'sendInvoiceEmail', 'getContentTemplate', 'fixDueDate'])){
              return true;
          }
        }

        if($user['role'] == 'E' && in_array($this->request->getParam('action'), ['view', 'downloadInvoice', 'getInvoicesCompany', 'report'])){
            return true;
        }

      return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      $tableCompanies = TableRegistry::get('Companies');
      $companies = $tableCompanies->find('list');
      $invoices = $this->Invoices->find()->contain(['Projects']);
      $quotes = TableRegistry::get('Quotes')->find()->where(['state' => 'invoice'])->contain(['Companies', 'Customers']);

      $this->set(compact('invoices', 'companies', 'quotes'));
      $this->set('_serialize', ['invoices']);
    }

    /**
     * View method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $invoice = $this->Invoices->get($id, [
            'contain' => ['Projects', 'Items', 'Companies']
        ]);
        $banks = TableRegistry::get('Banks')->find('list')->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'I']);
        $templates = TableRegistry::get('Templates')->find('list', ['keyField' => 'id', 'valueField' => 'name']);
        $contacts = TableRegistry::get('Contacts')->find('list', ['keyField' => 'id', 'valueField' => 'first_name'])->select(['Contacts.first_name', 'Contacts.id'])->autofields(false)->where(['Contacts.company_id' => $invoice->company_id]);
        $records = Cache::read('invoices_records');
        $prev;
        $next;
        if ($records) {
          $records->rewind();

          while ($records->valid()) {
            if ($invoice->id == intval($records->bottom()) && $invoice->id == intval($records->current())) {
              // debug('Top');
              // debug($records->current());die();
              $prev = $records->top();
              $records->next();
              $next = $records->current();
              break;
            }
            if ($invoice->id == intval($records->top()) && $invoice->id == intval($records->current())) {
              $next = $records->bottom();
              $records->prev();
              $prev = $records->current();
              break;
            }
            if ($invoice->id == intval($records->current())) {
              // debug('Middle');
              // debug($records->current());
              $records->prev();
              $prev = $records->current();
              $records->next();
              $records->next();
              $next = $records->current();
              break;
            }
            $records->next();
          }
        }

        $this->set(compact('invoice', 'banks', 'templates', 'contacts', 'next', 'prev'));
        $this->set('_serialize', ['invoice', 'contacts']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($project_id = null, $quote_id = null)
    {
        $invoice = $this->Invoices->newEntity();
        $tableProject = TableRegistry::get('Projects');
        $tableQuotes = TableRegistry::get('Quotes');
        if ($this->request->is('post')) {
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());
            $invoice->date = $invoice->date != null ? $invoice->date : Time::now();
            $invoice->due_date = $this->getDueDate($invoice);
            $invoice->outstanding_amount = $invoice->total_amount;
            $tableCompanies = TableRegistry::get('Companies');
            $project = $tableProject->get($invoice->project_id);
            $invoice->company_id = $project->company_id;
            $company = $tableCompanies->get($invoice->company_id);
            $tableIndex = TableRegistry::get('InvoicesIndex');
            $invoiceIndex = '';

            if ($company->national_invoice) {
              $invoiceIndex = $tableIndex->find()->where(['type' => 'N'])->first();
              $invoice->code_invoice = 'CRC' . $invoiceIndex->value;
            }
            else {
              $invoiceIndex = $tableIndex->find()->where(['type' => 'I'])->first();
              $invoice->code_invoice = 'INT' . $invoiceIndex->value;
            }

            if ($this->Invoices->save($invoice)) {
              if(!empty($invoice->itemsArray)){
                //Adding the items of the invoices
                $tableItems = TableRegistry::get('Items');

                for ($i=0; $i < count($invoice->itemsArray['description']); $i++) {
                  $item = new Item(['description' => $invoice->itemsArray['description'][$i], 'price' => $invoice->itemsArray['price'][$i], 'quantity' => $invoice->itemsArray['quantity'][$i]]);
                  $item->subtotal = ((($item->price*$item->quantity)*($item->VAT/100))+($item->price*$item->quantity));
                  $item->invoice_id = $invoice->id;
                  $tableItems->save($item);
                }

                $project->invoiced = 1;
                $tableProject->save($project);

                if (isset($invoiceIndex) && $invoiceIndex != '') {
                  $invoiceIndex->value = $invoiceIndex->value + 1;
                  $tableIndex->save($invoiceIndex);
                }
              }

                $message = "<p>Se ha generado una nueva factura: <strong>#".$invoice->id."</strong>, perteneciente al proyecto: <i>".$project->name."</i> por un monto total de: <strong>$".$invoice->total_amount."</strong></p>";
                $this->sendEmails('Nueva Factura', $message, $this->Auth->user());
                $this->Flash->success(__('The invoice has been saved successfully'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('La factura no pudo ser salvada. Por favor intente nuevamente'));
        }

        if ($project_id != null && $quote_id != null) {
          $project = $tableProject->get($project_id);
          $quote = $tableQuotes->get($quote_id, ['contain' => ['Companies', 'Components']]);
        }
        else{
          $project = null;
          $quote = null;
        }

        $taxes = TableRegistry::get('tax')->find('list', ['limit' => 200, 'keyField' => 'tax_value', 'valueField' => 'tax_value']);
        $companies = $this->Invoices->Companies->find("list", array("fields"=>array("Companies.name")))->select('name')->autofields(false);
        $this->set(compact('invoice', 'projects', 'taxes', 'companies', 'quote', 'project'));
        $this->set('_serialize', ['invoice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
     public function edit($id = null)
     {
         $invoice = $this->Invoices->get($id, [
             'contain' => ['Companies']
         ]);
         if ($this->request->is(['patch', 'post', 'put'])) {
             $prevTotalAmount = $invoice->total_amount;
             $invoice = $this->Invoices->patchEntity($invoice, $this->request->getData());
             $invoice->company_id = $invoice->company->id;
             $difference = $invoice->outstanding_amount != 0 ? ($prevTotalAmount-$invoice->outstanding_amount): 0;
             $tableProject = TableRegistry::get('Projects');
             $project = $tableProject->get($invoice->project_id);
             $invoice->company_id = $project->company_id;
             if ($invoice->dirty('date')) {
               $invoice->due_date = $this->getDueDate($invoice);
               $message = "<p>La fecha de pago de la factura <strong>#".$invoice->id."</strong> ha cambiado, la fecha de pago actual es de: <i>$".$invoice->due_date."</i></p>";
             }
             if ($invoice->dirty('total_amount')) {
               $invoice->outstanding_amount = ($invoice->total_amount-$difference);
               $message = "<p>El monto de la factura <strong>#".$invoice->id."</strong> ha cambiado, el monto actual es de: <i>$".$invoice->total_amount."</i></p>";
             }
             if ($this->Invoices->save($invoice)) {
               $tableItems = TableRegistry::get('Items');
               $items = $tableItems->find("list", ['keyField' => 'id','valueField' => 'description'])
                                   ->select(['Items.description'])
                                   ->where(['invoice_id' => $invoice->id]);

               $items = $items->toArray();

               //Check for items to add
               if (count(array_diff($invoice->itemsArray['description'], $items)) > 0) {
                 $itemsToAdd = array_diff($invoice->itemsArray['description'], $items);
                 foreach ($itemsToAdd as $key => $value) {
                   $item = new Item(['description' => $invoice->itemsArray['description'][$key], 'price' => $invoice->itemsArray['price'][$key], 'quantity' => $invoice->itemsArray['quantity'][$key]]);
                   $item->subtotal = ($item->price*$item->quantity);
                   $item->invoice_id = $invoice->id;
                   $tableItems->save($item);
                 }
               }
               //Check for items to delete
               if (count(array_diff($items, $invoice->itemsArray['description'])) > 0) {
                 $itemsToDelete = array_diff($items, $invoice->itemsArray['description']);
                 $amountToDiscount = 0;

                 foreach ($itemsToDelete as $key => $value) {
                   $item = $tableItems->get($key);
                   $amountToDiscount += $item->subtotal;
                   $tableItems->delete($item);
                 }
                 $invoice->total_amount = $invoice->total_amount - $amountToDiscount;
                 $invoice->outstanding_amount = $invoice->outstanding_amount - $amountToDiscount;
                 $this->Invoices->save($invoice);

               }

               //Check for items to update
               if (count(array_intersect($invoice->itemsArray['description'], $items))) {
                 $itemsToUpdate = array_intersect($invoice->itemsArray['description'], $items);

                 foreach ($itemsToUpdate as $key => $value) {
                   $item = $tableItems->find()->where(['description LIKE' => $value, 'invoice_id' => $invoice->id])->first();
                   $item->description = $invoice->itemsArray['description'][$key];
                   $item->price = $invoice->itemsArray['price'][$key];
                   $item->quantity = $invoice->itemsArray['quantity'][$key];
                   $item->subtotal = ($item->price*$item->quantity);
                   $tableItems->save($item);
                 }
               }

                 if(isset($message)){
                   $this->sendEmails('Cambio en Factura', $message, $this->Auth->user());
                 }

                 $this->Flash->success(__('La factura ha sido guardada con éxito'));

                 return $this->redirect(['action' => 'index']);
             }
             $this->Flash->error(__('La factura no pudo ser salvada. Por favor intente nuevamente'));
         }
         $items = TableRegistry::get('Items')->find()->where(['invoice_id' => $id]);
         $taxes = TableRegistry::get('tax')->find('list', ['limit' => 200, 'keyField' => 'tax_value', 'valueField' => 'tax_value']);
         $projects = $this->Invoices->Projects->find('list', ['limit' => 200])->where(['company_id' => $invoice->company->id]);
         $this->set(compact('invoice', 'projects', 'items', 'taxes'));
         $this->set('_serialize', ['invoice']);
     }

    public function statistics(){
      if($this->request->is('post')){
        $company = $this->request->getData()['company'];
        $startDate = $this->request->getData()['start_date'];
        $endDate = $this->request->getData()['end_date'];
        //Get all invoices
        if($company == '0'){
          $invoices = $this->Invoices->find()->select(['Invoices.id', 'Invoices.state', 'Invoices.date', 'Invoices.total_amount', 'Invoices.outstanding_amount'])
                                              ->autofields(false)
                                              ->order(['date' => 'ASC'])
                                              ->where(['date >=' => $startDate, 'date <=' => $endDate]);
        }
        else{
          //Get invoices by company
          $companyObj = $this->Invoices->Companies->find()->select(['Companies.id'])->autofields(false)->where(['Companies.name LIKE' => $company])->first();
          $invoices = $this->Invoices->find()->select(['Invoices.id', 'Invoices.state', 'Invoices.date', 'Invoices.total_amount', 'Invoices.outstanding_amount'])
                                              ->autofields(false)
                                              ->where(['date >=' => $startDate, 'date <=' => $endDate, 'company_id' => $companyObj->id])
                                              ->order(['date' => 'ASC']);
        }
        $pendingInvoices = 0;
        $paidInvoices = 0;
        $dueInvoices = 0;
        $amountOwed = 0;
        $amountPaid = 0;
        $amountsByDate = array();
        $dates = array();

        $dateRange = new DatePeriod(new DateTime($startDate), new DateInterval('P1D'), new DateTime($endDate));

        foreach ($invoices as $invoice) {
          $cont = 0;
          switch ($invoice->state) {
            case 'P':
              $pendingInvoices++;
              $amountOwed += $invoice->outstanding_amount;
              break;
            case 'C':
              $paidInvoices++;
              break;
            case 'V':
              $dueInvoices++;
              $amountOwed += $invoice->outstanding_amount;
              break;
          }

          $amountPaid += ($invoice->total_amount-$invoice->outstanding_amount);
          //Get the amounts by the range of dates (day by day)
          foreach ($dateRange as $date) {
            if(!isset($amountsByDate[$cont])){
              $amountsByDate[$cont] = 0;
            }

            if($date->format('d/m/Y') === $invoice->date->format('d/m/Y')){
              $amountsByDate[$cont] += $invoice->total_amount;
            }
            else {
              $amountsByDate[$cont] += 0;
            }
            $dates[$cont] = $date->format('d-m');
            $cont++;
          }
        }

        $data = ['pendingInvoices' => $pendingInvoices, 'paidInvoices' => $paidInvoices, 'dueInvoices' => $dueInvoices, 'amountOwed' => $amountOwed, 'amountPaid' => $amountPaid, 'totalAmount' => $amountOwed+$amountPaid, 'dates' => $dates, 'amountsByDate' => $amountsByDate];
        $this->set(compact('data'));
      }
      $companies = TableRegistry::get('Companies')->find("list", array("fields"=>array("Companies.name")))->select('name')->autofields(false);
      $this->set(compact('companies'));
      $this->set('_serialize', ['companies', 'data']);
    }

    /* Get the due date based on the payment terms of
    * the company in the project
    */
    private function getDueDate($invoice){
      $project = TableRegistry::get('Projects')->get($invoice->project_id);
      $customer = TableRegistry::get('Companies')->get($project->company_id);
      $payment_terms = TableRegistry::get('PaymentTerms')->get($customer->payment_terms);
      $date = $invoice->date->modify('+'.$payment_terms->name.'days');

      return $date;
    }

    public function makePaid(){
      if ($this->request->is('post')) {
        $requestData = $this->request->getData();
        $invoice = $this->Invoices->get($requestData['invoice_id'], ['contain' => ['Projects']]);

        if($requestData['paidType']==0){
          $invoice->state = 'C';
          $invoice->outstanding_amount = 0;
          if($this->Invoices->save($invoice)){
            if(isset($requestData['sendNotification'])){
              $message = "<p>La factura número: <strong>#".$invoice->id."</strong>, perteneciente al proyecto: <i>".$invoice->project->name."</i> ha sido pagada en su totalidad.";
              $this->sendEmails('Factura cancelada', $message, $this->Auth->user());
            }
            $this->Flash->success(__('Se ha hecho el pago correctamente.'));
            return $this->redirect(['action' => 'index/']);
          }
        }
        else{
          if($requestData['paidAmount']<=$invoice->total_amount){
            $invoice->outstanding_amount = ($invoice->outstanding_amount-$requestData['paidAmount']);
            if($this->Invoices->save($invoice)){
              if(isset($requestData['sendNotification'])){
                $message = "<p>La factura número: <strong>#".$invoice->id."</strong>, perteneciente al proyecto: <i>".$invoice->project->name."</i> ha sido pagada parcialmente, por un monto de: <em>$".$requestData['paidAmount']."</em> el monto de la factura es de: <strong>$".$invoice->outstanding_amount."</strong>";
                $this->sendEmails('Abono de factura', $message, $this->Auth->user());
              }
              $this->Flash->success(__('Se ha hecho el pago correctamente.'));
              return $this->redirect(['action' => 'index/']);
            }
          }
        }
      }
    }

    public function getInvoicesProject(){
      if($this->request->is('ajax')){
        $projectID = $this->request->getData()['id'];

        $invoices = $this->Invoices->find()
                                  ->where(['project_id' => $projectID])
                                  ->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectID){
                                        return $q->select(['Projects.name'])
                                                ->autofields(false)
                                                ->where(['Projects.id' => $projectID]);
                                  }]);
        $this->set('invoices', $invoices->toArray());
        $this->set('_serialize', ['invoices']);
      }
    }

    public function getInvoicesCompany(){
      if($this->request->is('ajax')){
        $data = $this->request->getData();
        $conditions = array();

        switch ($data['type']) {
          case '0':
            $typeInvoice = ['0', '1'];
            break;
          case '1':
            $typeInvoice = ['1'];
            break;
          case '2':
            $typeInvoice = ['0'];
            break;
        }

        foreach ($data as $key => $value) {
          switch ($key) {
            case 'startDate':
              $value != null ? $conditions['Invoices.date >= '] = $value : $conditions = $conditions;
            break;
            case 'endDate':
              $value != null ? $conditions['Invoices.date <= '] = $value : $conditions = $conditions;
            break;
            case 'state':
              $value != null ? $conditions['Invoices.state '] = $value : $conditions = $conditions;
            break;
            case 'downloaded':
              $value != null ? $conditions['Invoices.is_downloaded'] = 'N' : $conditions = $conditions;
            break;
            case 'type':
              $value != null ? $conditions['Companies.national_invoice IN'] = $typeInvoice : $conditions = $conditions;
            break;
          }
        }

        $inputSearch = $this->request->getData()['company'];

        if ($inputSearch != null) {
          if ($this->Auth->user('role') != 'E') {
            $invoices = $this->Invoices->find()
                                        ->contain(['Projects', 'Companies', 'Customers'])
                                        ->where($conditions)
                                        ->andWhere(['Companies.name LIKE ' => ($inputSearch.'%')])
                                        ->andWhere(['Invoices.id !=' => '203'])
                                        ->orWhere(['Projects.name LIKE ' => ('%'.$inputSearch.'%')])
                                        ->order(['Invoices.id' => 'DESC']);;
          }
          else{
            $invoices = $this->Invoices->find()
                                        ->contain(['Projects', 'Companies', 'Customers'])
                                        ->where($conditions)
                                        ->andWhere(['Companies.id ' => $this->Auth->user('company_id')])
                                        ->andWhere(['Invoices.id !=' => '203'])
                                        ->orWhere(['Projects.name LIKE ' => ('%'.$inputSearch.'%')])
                                        ->order(['Invoices.id' => 'DESC']);;
          }
        }
        else {
          if ($this->Auth->user('role') != 'E'){
            $invoices = $this->Invoices->find()
                                        ->contain(['Projects', 'Companies', 'Customers'])
                                        ->where($conditions)
                                        ->andWhere(['Invoices.id !=' => '203'])
                                        ->order(['Invoices.id' => 'DESC']);;
          }
          else{
            $invoices = $this->Invoices->find()
                                        ->contain(['Projects', 'Companies', 'Customers'])
                                        ->where($conditions)
                                        ->andWhere(['Companies.id ' => $this->Auth->user('company_id')])
                                        ->andWhere(['Invoices.id !=' => '203'])
                                        ->order(['Invoices.id' => 'DESC']);;
          }
        }

        $totalCosts = 0;
        $totalPendingCosts = 0;
        foreach ($invoices as $invoice) {
          if ($invoice->state != 'D') {
            $totalCosts += $invoice->total_amount;
            $totalPendingCosts += $invoice->outstanding_amount;
          }
        }

        $invoicesCache = new \SplDoublyLinkedList;
        foreach ($invoices as $key => $value) {
          $invoicesCache->add($key, strval($value->id));
        }

        Cache::write('invoices_records', $invoicesCache);

        $this->set(compact('invoices', 'totalCosts', 'totalPendingCosts'));
        $this->set('_serialize', ['invoices', 'totalCosts', 'totalPendingCosts']);
      }
    }

    public function cancelInvoice(){
        $this->request->allowMethod(['post']);
        $invoice = $this->Invoices->get($this->request->data['id'], ['contain' => ['Projects']]);
        $invoice->state = 'C';
        $invoice->outstanding_amount = 0;
        if($this->Invoices->save($invoice)){
          $message = "<p>La factura: <strong>#".$invoice->id."</strong> perteneciente al proyecto: <i>".$invoice->project->name."</i> ha sido cancelada en su totalidad.";
          $this->sendEmails('Cancelación de factura', $message, $this->Auth->user());
          $this->Flash->success('La factura ha sido cancelada correctamente');
        }
        else {
          $this->Flash->error('Hubo errores al momento de cancelar la factura. Por favor intente nuevamente.');
        }

        return $this->redirect(['action' => 'index']);
    }

    public function discardInvoice(){
        $this->request->allowMethod(['post']);
        $invoice = $this->Invoices->get($this->request->data['id']);
        $invoice->state = 'D';
        if ($this->Invoices->save($invoice)) {
          $this->Flash->success('La factura ha sido descartada correctamente');
        }
        else {
          $this->Flash->error('Hubo errores al momento de descartar la factura. Por favor intente nuevamente.');
        }
        return $this->redirect(['action' => 'index']);
    }

    private function sendEmails($subject, $message, $user){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($user['first_name'].' '.$user['last_name'], $user['email']);
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
      $content = new \SendGrid\Content("text/html", $template);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $mail->personalization[0]->addSubstitution('%html_content%', $message);
      $mail->personalization[0]->addSubstitution('%text_content%', '<h2>¡Hola!</h2>');
      $mail->personalization[0]->addSubstitution('%button_content%', 'localhost/wdpm/invoices/');
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    public function report($invoices = null){
        $invoices_id = $_GET['invoices'];
        $invoices_id = explode(",", $invoices_id);
        $invoices = $this->Invoices->find()
                                    ->contain(['Projects', 'Companies'])
                                    ->where(['Invoices.id IN' => $invoices_id]);


        $projectTable = TableRegistry::get('Projects');
        $organizationTable = TableRegistry::get('Organizations');
        $organization = $organizationTable->get($this->Auth->user('organization_id'));
        $excelFile = new PHPExcel();
        $excelFile->getProperties()->setCreator("WDPM")->setLastModifiedBy("WDPM")->setTitle("ProjectsReport")
                           ->setSubject("Projects")->setDescription("Document report")->setKeywords("office")
                           ->setCategory("Report");
        $excelFile->setActiveSheetIndex(0)->setCellValue('A1', __('Reporte de facturas'))->setCellValue('A3', __('Fecha'))
                                          ->setCellValue('B3', Time::now()->format('d-m-Y H:i'))->setCellValue('A4', __('Compañía'))
                                          ->setCellValue('B4', $organization->name);

        $excelFile->setActiveSheetIndex(0)->setCellValue('A6', __('ID'))->setCellValue('B6', __('Cliente'))->setCellValue('C6', __('Proyecto'))->setCellValue('D6', __('Presupuesto'))
                                          ->setCellValue('E6', __('Fecha'))->setCellValue('F6', __('Fecha de pago'))
                                          ->setCellValue('G6', __('Estado'))->setCellValue('H6', __('Monto total'))->setCellValue('I6', __('Monto pendiente'));

        $lastRow = $excelFile->getActiveSheet()->getHighestRow();
        $totalRevenues = 0;
        foreach ($invoices as $invoice) {
          $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+1), $invoice->code_invoice)->setCellValue('B'.($lastRow+1), $invoice->company->name)->setCellValue('C'.($lastRow+1), $invoice->project != null ? $invoice->project->name:'')
                                            ->setCellValue('D'.($lastRow+1), (($invoice->project != null ? $invoice->project->budget : '')))->setCellValue('E'.($lastRow+1), ($invoice->date != null) ? $invoice->date->format('d-m-Y'): '')
                                            ->setCellValue('F'.($lastRow+1), ($invoice->due_date != null) ? $invoice->due_date->format('d-m-Y'): '')->setCellValue('G'.($lastRow+1), $invoice->state)->setCellValue('H'.($lastRow+1), ($invoice->total_amount))
                                            ->setCellValue('I'.($lastRow+1), ($invoice->outstanding_amount));
          $lastRow = $excelFile->getActiveSheet()->getHighestRow();
          $totalRevenues += $invoice->total_amount;
        }
        $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+2), __('Total'))->setCellValue('B'.($lastRow+2), ('$'.$totalRevenues));
        $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('B6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('C6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('D6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('E6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('F6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('G6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('H6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getStyle('I6')->getFont()->setBold(true)->setSize(12);
        $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $excelFile->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $excelFile->getActiveSheet()->setTitle(__('Reporte de facturas'));
        $excelFile->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="invoices_report.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');

        // ob_end_clean();
        $writer->save('php://output');
    }

    public function reportHistoricByClient(){
      $invoices = $this->Invoices->find()
                                  ->contain(['Projects', 'Companies'])
                                  ->where(['Invoices.state IN' => ['P', 'V']])
                                  ->order(['Invoices.company_id' => 'DESC']);

      $projectTable = TableRegistry::get('Projects');
      $organizationTable = TableRegistry::get('Organizations');
      $organization = $organizationTable->get($this->Auth->user('organization_id'));
      $excelFile = new PHPExcel();
      $pendingInvoices = 0;
      $dueInvoices = 0;
      $totalPending = 0;
      $totalDue = 0;
      $excelFile->getProperties()->setCreator("WDPM")->setLastModifiedBy("WDPM")->setTitle("ProjectsReport")
                                                     ->setSubject("Projects")->setDescription("Document report")->setKeywords("office")
                                                     ->setCategory("Report");
      $excelFile->setActiveSheetIndex(0)->setCellValue('A1', __('Reporte de facturas'))->setCellValue('A3', __('Fecha'))
                                        ->setCellValue('B3', Time::now()->format('d-m-Y H:i'))->setCellValue('A4', __('Compañía'))
                                        ->setCellValue('B4', $organization->name)
                                        ->setCellValue('A6', __('Total de facturas'))->setCellValue('B6', $invoices->count())
                                        ->setCellValue('A7', __('Facturas pendientes'))
                                        ->setCellValue('A8', __('Facturas Vencidas'))->setCellValue('A10', __('Monto Total'))
                                        ->setCellValue('A11', __('Total pendiente'))->setCellValue('A12', __('Total vencido'))
                                        ->setCellValue('A14', __('Cantidad de clientes con facturas abiertas'));

      if ($invoices->count() > 0) {
        $activeCompany = $invoices->toArray()[0]['company_id'];
        $companyName = $invoices->toArray()[0]['company']['name'];
        $activeSheet = 1;
        $totalClients = 1;
        $excelFile->createSheet($activeSheet);
        $excelFile->setActiveSheetIndex($activeSheet);
        $excelFile->getActiveSheet()->setTitle($companyName);
        $totalRevenues = 0;
        $lastRow = 4;
        $excelFile->setActiveSheetIndex($activeSheet)->setCellValue('A1', __('Cliente'))->setCellValue('B1', $companyName);

        foreach ($invoices as $invoice) {
          if ($invoice->company_id != $activeCompany) {
            $activeSheet++;
            $activeCompany = $invoice->company_id;
            $excelFile->createSheet($activeSheet);
            $sheetName = substr($invoice->company->name, 0, 20);
            $excelFile->setActiveSheetIndex($activeSheet);
            $excelFile->getActiveSheet()->setTitle($sheetName);
            $excelFile->setActiveSheetIndex($activeSheet)->setCellValue('A1', __('Cliente'))->setCellValue('B1', $invoice->company->name);
            $totalClients++;
            $totalRevenues = 0;
            $lastRow = 4;
          }
          $excelFile->setActiveSheetIndex($activeSheet)->setCellValue('A4', __('ID'))->setCellValue('B4', __('Proyecto'))->setCellValue('C4', __('Presupuesto'))
                                            ->setCellValue('D4', __('Fecha'))->setCellValue('E4', __('Fecha de pago'))
                                            ->setCellValue('F4', __('Estado'))->setCellValue('G4', __('Monto total'))->setCellValue('H4', __('Monto pendiente'));
          $state = $invoice->state == 'V' ? __('Vencida') : __('Pendiente');
          $excelFile->setActiveSheetIndex($activeSheet)->setCellValue('A'.($lastRow+1), $invoice->code_invoice)->setCellValue('B'.($lastRow+1), $invoice->project != null ? $invoice->project->name:'')
                                            ->setCellValue('C'.($lastRow+1), (($invoice->project != null ? $invoice->project->budget : '')))->setCellValue('D'.($lastRow+1), ($invoice->date != null) ? $invoice->date->format('d-m-Y'): '')
                                            ->setCellValue('E'.($lastRow+1), ($invoice->due_date != null) ? $invoice->due_date->format('d-m-Y'): '')->setCellValue('F'.($lastRow+1), $state)->setCellValue('G'.($lastRow+1), ($invoice->total_amount))
                                            ->setCellValue('H'.($lastRow+1), ($invoice->outstanding_amount));
          $lastRow += 1;
          $totalRevenues += $invoice->total_amount;

          $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('A2')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('B1')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('B4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('C4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('D4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('E4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('F4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('G4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('H4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getStyle('I4')->getFont()->setBold(true)->setSize(12);
          $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
          $excelFile->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
          $excelFile->setActiveSheetIndex($activeSheet)->setCellValue('A2', __('Total'))->setCellValue('B2', ('$'.$totalRevenues));

          switch ($invoice->state) {
            case 'P':
              $pendingInvoices++;
              $totalPending += $invoice->outstanding_amount;
            break;
            case 'V':
              $dueInvoices++;
              $totalDue += $invoice->outstanding_amount;
            break;
          }
        }
      }
      $excelFile->setActiveSheetIndex(0);
      $excelFile->getActiveSheet()->setTitle('General');
      $excelFile->getActiveSheet()->getRowDimension(1)->setRowHeight(-1);
      $excelFile->getActiveSheet()->setCellValue('B7', $pendingInvoices);
      $excelFile->getActiveSheet()->setCellValue('B8', $dueInvoices);
      $excelFile->getActiveSheet()->setCellValue('B11', $totalPending);
      $excelFile->getActiveSheet()->setCellValue('B12', $totalDue);
      $excelFile->getActiveSheet()->setCellValue('B10', $totalDue+$totalPending);
      $excelFile->getActiveSheet()->setCellValue('B14', $totalClients);
      $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(16);
      $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A7')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A8')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A10')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A11')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A12')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A14')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="reportbyclients.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');
      $writer->save('php://output');
    }


    public function downloadInvoice($invoiceID = null, $type = null){
      ini_set("memory_limit","128M");
      $invoiceTemplate = file_get_contents(WWW_ROOT.'invoices_templates/invoice_template.html');
      $cssInvoice = file_get_contents(WWW_ROOT.'css/ex2.css');
      $tableBanks = TableRegistry::get('Banks');
      $tableAccounts = TableRegistry::get('Accounts');
      $invoice = $this->Invoices->find()->where(['Invoices.id' => $invoiceID])->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                      return $q->select(['Projects.name']);
                                                  }, 'Companies'])->first();
      $organization = TableRegistry::get('Organizations')->get($this->Auth->user('organization_id'));
      $itemsInvoices = TableRegistry::get('Items')->find()->where(['Items.invoice_id' => $invoice->id])->toArray();
      $paymentTerms = TableRegistry::get('PaymentTerms')->get($invoice->company->payment_terms);
      $banksOrganizations = $tableBanks->find()->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'B']);

      $invoiceTemplate = str_replace('$organization_name', $organization->name, $invoiceTemplate);
      $invoiceTemplate = str_replace('$logo_address', (WWW_ROOT.'img/'.$organization->logo), $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_address', ($organization->address.', '.$organization->country), $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_phone', $organization->phone, $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_email', $organization->email, $invoiceTemplate);
      $invoiceTemplate = str_replace('$customer_name', $invoice->company->name, $invoiceTemplate);
      $invoiceTemplate = str_replace('$customer_address', ($invoice->company->address.', '.$invoice->company->country), $invoiceTemplate);
      $invoiceTemplate = str_replace('$project_name', $invoice->project!= null ? $invoice->project->name:'', $invoiceTemplate);
      $invoiceTemplate = str_replace('$payment_terms', ($paymentTerms->name.__(' días')), $invoiceTemplate);
      $invoiceTemplate = str_replace('$purchase_order', $invoice->purchase_order, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_date', $invoice->date, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_due_date', $invoice->due_date, $invoiceTemplate);

      //Titles
      $invoiceTemplate = str_replace('$invoice_number', (__('Factura #').$invoice->code_invoice), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_project', __('Proyecto'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_client', __('Cliente'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_address', __('Dirección'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_payment_term', __('Término de pago'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_date', __('Fecha'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_due_date', __('Fecha pago'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_purchase_order', __('Orden de Compra'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_description', __('Descripción'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_price', __('Precio'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_quantity', __('Cantidad'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_info', __('Información'), $invoiceTemplate);
      $invoiceTemplate = str_replace('$title_other_info', __('Otra información'), $invoiceTemplate);
      $descriptionContent = '';

      foreach ($itemsInvoices as $item) {
        $descriptionContent .= '<tr><td class="desc">'.$item->description.'</td><td class="unit">$'.$item->price.'</td><td class="qty">'.$item->quantity.'</td><td class="total">$'.$item->subtotal.'</td></tr>';
      }

      $invoiceTemplate = str_replace('$invoice_description', $descriptionContent, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_total', ('$'.$invoice->total_amount), $invoiceTemplate);

      if ($type == 'international') {
        //Intermediary
        $intermediaryBank = $tableBanks->find()->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'I'])->first();
        $intermediaryBankInfo = '<div>'.__('Banco intermediario:').'<span>'.$intermediaryBank->name.'</span></div><div>SWIFT: <span>'.$intermediaryBank->swift.'</span></div>';
        $intermediaryBankInfo .= '<div>ABA: <span>'.$intermediaryBank->aba.'</span></div><div>'.__('Dirección: ').'<span>'.$intermediaryBank->address.'</span></div>';
        $invoiceTemplate = str_replace('$intermediary_bank_info', $intermediaryBankInfo, $invoiceTemplate);

        //Beneficiary
        $benBank = $banksOrganizations->first();
        $beneficiaryBankInfo = '<div>'.__('Banco beneficiario:').'<span>'.$benBank->name.'</span></div><div>SWIFT: <span>'.$benBank->swift.'</span></div>';
        $beneficiaryBankInfo .= '<div>'.__('Dirección: ').'<span>'.$benBank->address.'</span></div>';
        $invoiceTemplate = str_replace('$beneficiary_bank_info', $beneficiaryBankInfo, $invoiceTemplate);

        $account = $tableAccounts->find()->where(['bank_id' => $intermediaryBank->id, 'type' => $type])->first();
        //Client
        $beneficiaryClientInfo = '<div>'.__('Cliente beneficiario:').'<span>'.$organization->name.'</span></div>';
        $beneficiaryClientInfo .= ($account != null) ? '<div>'.__('Cuenta: ').'<span>'.$account->account_number.'</span></div>' : '';
        $invoiceTemplate = str_replace('$beneficiary_client_info', $beneficiaryClientInfo, $invoiceTemplate);
      }
      else {
        $beneficiaryClientInfo = '<div>'.__('Legal ID: ') . '<span>' . $organization->company_number .'</span></div><br>';
        foreach ($banksOrganizations as $bank) {
          $account = $tableAccounts->find()->where(['bank_id' => $bank->id, 'type' => $type])->first();
          $beneficiaryClientInfo .= '<div>'.$bank->name.' : ';
          $beneficiaryClientInfo .= ($account != null) ? '<span>'.$account->account_number.'</span></div><br>' : '';
        }
        $invoiceTemplate = str_replace('$beneficiary_client_info', $beneficiaryClientInfo, $invoiceTemplate);
        $invoiceTemplate = str_replace('$intermediary_bank_info', '', $invoiceTemplate);
        $invoiceTemplate = str_replace('$beneficiary_bank_info', '', $invoiceTemplate);
      }

      if (($organization->notes_invoices != '' || $organization->notes_invoices != null) && ($invoice->notes != '')) {
        $notes = $organization->notes_invoices . PHP_EOL . $invoice->notes;
        $invoiceTemplate = str_replace('$notes', $notes, $invoiceTemplate);
      }
      else if($invoice->notes != ''){
        $invoiceTemplate = str_replace('$notes', $invoice->notes, $invoiceTemplate);
      }
      else if ($organization->notes_invoices != '' || $organization->notes_invoices != null) {
        $invoiceTemplate = str_replace('$notes', $organization->notes_invoices, $invoiceTemplate);
      }
      else {
        $invoiceTemplate = str_replace('$notes', '', $invoiceTemplate);
      }

      $invoice->is_downloaded = 'Y';
      TableRegistry::get('Invoices')->save($invoice);

      $mPDF = new \Mpdf\Mpdf();
      $mPDF->WriteHTML($cssInvoice, 1);
      $mPDF->writeHTML($invoiceTemplate);
      $mPDF->Output('invoice'.$invoice->code_invoice.'.pdf', 'D');
    }

    public function downloadMultipleInvoices($invoices = array(), $type = null){
      ini_set("memory_limit","256M");
      ob_start();
      $tableBanks = TableRegistry::get('Banks');
      $tableAccounts = TableRegistry::get('Accounts');
      $organization = TableRegistry::get('Organizations')->get($this->Auth->user('organization_id'));
      $banksOrganizations = $tableBanks->find()->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'B']);
      $invoices = explode(',', $invoices);

      for ($i=0; $i < count($invoices); $i++) {
        $invoiceTemplate = file_get_contents(WWW_ROOT.'invoices_templates/invoice_template.html');
        $cssInvoice = file_get_contents(WWW_ROOT.'css/ex2.css');
        $invoice = $this->Invoices->find()->where(['Invoices.code_invoice' => $invoices[$i]])->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                        return $q->select(['Projects.name']);
                                                    }, 'Companies'])->first();

        $itemsInvoices = TableRegistry::get('Items')->find()->where(['Items.invoice_id' => $invoice->id])->toArray();
        $paymentTerms = TableRegistry::get('PaymentTerms')->get($invoice->company->payment_terms);

        $invoiceTemplate = str_replace('$organization_name', $organization->name, $invoiceTemplate);
        $invoiceTemplate = str_replace('$logo_address', (WWW_ROOT.'img/'.$organization->logo), $invoiceTemplate);
        $invoiceTemplate = str_replace('$organization_address', ($organization->address.', '.$organization->country), $invoiceTemplate);
        $invoiceTemplate = str_replace('$organization_phone', $organization->phone, $invoiceTemplate);
        $invoiceTemplate = str_replace('$organization_email', $organization->email, $invoiceTemplate);
        $invoiceTemplate = str_replace('$customer_name', $invoice->company->name, $invoiceTemplate);
        $invoiceTemplate = str_replace('$customer_address', ($invoice->company->address.', '.$invoice->company->country), $invoiceTemplate);
        $invoiceTemplate = str_replace('$project_name', $invoice->project!= null ? $invoice->project->name:'', $invoiceTemplate);
        $invoiceTemplate = str_replace('$payment_terms', ($paymentTerms->name.__(' días')), $invoiceTemplate);
        $invoiceTemplate = str_replace('$purchase_order', $invoice->purchase_order, $invoiceTemplate);
        $invoiceTemplate = str_replace('$invoice_date', $invoice->date, $invoiceTemplate);
        $invoiceTemplate = str_replace('$invoice_due_date', $invoice->due_date, $invoiceTemplate);

        //Titles
        $invoiceTemplate = str_replace('$invoice_number', (__('Factura #').$invoice->code_invoice), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_project', __('Proyecto'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_client', __('Cliente'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_address', __('Dirección'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_payment_term', __('Término de pago'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_date', __('Fecha'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_due_date', __('Fecha pago'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_purchase_order', __('Orden de Compra'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_description', __('Descripción'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_price', __('Precio'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_quantity', __('Cantidad'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_info', __('Información'), $invoiceTemplate);
        $invoiceTemplate = str_replace('$title_other_info', __('Otra información'), $invoiceTemplate);
        $descriptionContent = '';

        foreach ($itemsInvoices as $item) {
          $descriptionContent .= '<tr><td class="desc">'.$item->description.'</td><td class="unit">$'.$item->price.'</td><td class="qty">'.$item->quantity.'</td><td class="total">$'.$item->subtotal.'</td></tr>';
        }

        $invoiceTemplate = str_replace('$invoice_description', $descriptionContent, $invoiceTemplate);
        $invoiceTemplate = str_replace('$invoice_total', ('$'.$invoice->total_amount), $invoiceTemplate);

        if ($type == 'international') {
          //Intermediary
          $intermediaryBank = $tableBanks->find()->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'I'])->first();
          $intermediaryBankInfo = '<div>'.__('Banco intermediario:').'<span>'.$intermediaryBank->name.'</span></div><div>SWIFT: <span>'.$intermediaryBank->swift.'</span></div>';
          $intermediaryBankInfo .= '<div>ABA: <span>'.$intermediaryBank->aba.'</span></div><div>'.__('Dirección: ').'<span>'.$intermediaryBank->address.'</span></div>';
          $invoiceTemplate = str_replace('$intermediary_bank_info', $intermediaryBankInfo, $invoiceTemplate);

          //Beneficiary
          $benBank = $banksOrganizations->first();
          $beneficiaryBankInfo = '<div>'.__('Banco beneficiario:').'<span>'.$benBank->name.'</span></div><div>SWIFT: <span>'.$benBank->swift.'</span></div>';
          $beneficiaryBankInfo .= '<div>'.__('Dirección: ').'<span>'.$benBank->address.'</span></div>';
          $invoiceTemplate = str_replace('$beneficiary_bank_info', $beneficiaryBankInfo, $invoiceTemplate);

          $account = $tableAccounts->find()->where(['bank_id' => $intermediaryBank->id, 'type' => $type])->first();
          //Client
          $beneficiaryClientInfo = '<div>'.__('Cliente beneficiario:').'<span>'.$organization->name.'</span></div>';
          $beneficiaryClientInfo .= ($account != null) ? '<div>'.__('Cuenta: ').'<span>'.$account->account_number.'</span></div>' : '';
          $invoiceTemplate = str_replace('$beneficiary_client_info', $beneficiaryClientInfo, $invoiceTemplate);
        }
        else {
          $beneficiaryClientInfo = '<div>'.__('Cédula Jurídica: ') . '<span>' . $organization->company_number .'</span></div><br>';
          foreach ($banksOrganizations as $bank) {
            $account = $tableAccounts->find()->where(['bank_id' => $bank->id, 'type' => $type])->first();
            $beneficiaryClientInfo .= '<div>'.$bank->name.' : ';
            $beneficiaryClientInfo .= ($account != null) ? '<span>'.$account->account_number.'</span></div><br>' : '';
          }
          $invoiceTemplate = str_replace('$beneficiary_client_info', $beneficiaryClientInfo, $invoiceTemplate);
          $invoiceTemplate = str_replace('$intermediary_bank_info', '', $invoiceTemplate);
          $invoiceTemplate = str_replace('$beneficiary_bank_info', '', $invoiceTemplate);
        }

        if (($organization->notes_invoices != '' || $organization->notes_invoices != null) && ($invoice->notes != '')) {
          $notes = $organization->notes_invoices . PHP_EOL . $invoice->notes;
          $invoiceTemplate = str_replace('$notes', $notes, $invoiceTemplate);
        }
        else if($invoice->notes != ''){
          $invoiceTemplate = str_replace('$notes', $invoice->notes, $invoiceTemplate);
        }
        else if ($organization->notes_invoices != '' || $organization->notes_invoices != null) {
          $invoiceTemplate = str_replace('$notes', $organization->notes_invoices, $invoiceTemplate);
        }
        else {
          $invoiceTemplate = str_replace('$notes', '', $invoiceTemplate);
        }

        $mPDF = new \Mpdf\Mpdf();
        $mPDF->WriteHTML($cssInvoice, 1);
        $mPDF->writeHTML($invoiceTemplate);
        $mPDF->Output(WWW_ROOT.'invoices_pdf/invoice'.$invoice->code_invoice.'.pdf', 'F');
        $invoice->is_downloaded = 'Y';
        TableRegistry::get('Invoices')->save($invoice);
      }

      $invoicesFiles = WWW_ROOT.'invoices_pdf';
      $zip = new \ZipArchive();
      $zipFile = WWW_ROOT.'downloads/invoices.zip';

      if ($zip->open($zipFile, \ZipArchive::CREATE) != TRUE) {
        debug('Entra');
        die('Cannot open');
      }

      //Create zip
      $this->createZip($zip, $invoicesFiles);

      $zip->close();

      if (file_exists($zipFile)) {
        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=invoices.zip");
        header("Pragma: no-cache");
        header("Expires: 0");
        ob_clean();
        flush();
        readfile("$zipFile");
        // delete file
        unlink($zipFile);
        $files = glob($invoicesFiles.'/*'); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file))
            unlink($file); // delete file
        }
        die();
      }
    }

    public function paidMultipleInvoices(){
      if ($this->request->is('post')) {
        $invoices = $this->request->data['invoices'];
        for ($i=0; $i < count($invoices); $i++) {
          $invoice = $this->Invoices->get($invoices[$i]);
          if ($invoice) {
            $invoice->outstanding_amount = 0;
            $invoice->state = 'C';
            $this->Invoices->save($invoice);
          }
        }
      }
    }

    private function createZip($zip, $dir)
    {
      if (is_dir($dir)){
           if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){

             // If file
             if (is_file($dir.'/'.$file)) {
              if($file != '' && $file != '.' && $file != '..' && $file != '.DS_Store'){
               $zip->addFile($dir.'/'.$file, $file);
              }
             }
            }
            closedir($dh);
           }
      }
    }

    public function sendInvoiceEmail($invoiceID = null, $subject = null, $message = null, $contactID = null){
      ini_set("memory_limit","128M");
      $invoiceTemplate = file_get_contents(WWW_ROOT.'invoices_templates/invoice_template.html');
      $cssInvoice = file_get_contents(WWW_ROOT.'css/ex2.css');
      $invoice = $this->Invoices->find()->where(['Invoices.id' => $invoiceID])->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                      return $q->select(['Projects.name']);
                                                  }, 'Companies'])->first();
      $organization = TableRegistry::get('Organizations')->get($this->Auth->user('organization_id'));
      $itemsInvoices = TableRegistry::get('Items')->find()->where(['Items.invoice_id' => $invoice->id])->toArray();
      $paymentTerms = TableRegistry::get('PaymentTerms')->get($invoice->company->payment_terms);
      $banksOrganizations = TableRegistry::get('Banks')->find()->where(['organization_id' => $this->Auth->user('organization_id'), 'type' => 'B'])->first();
      $account = TableRegistry::get('Accounts')->find()->where(['bank_id' => $banksOrganizations->id])->first();

      if($contactID == 'null'){
        $this->Flash->error(__('Debe agregar un correo electrónico de recepción'));
        return $this->redirect(['action' => 'view', $invoice->id]);
      }
      $contact = TableRegistry::get('Contacts')->get($contactID);

      if ($organization->email == null || $organization->email == '') {
        $this->Flash->error(__('Debe configurar el correo electrónico de la organización'));
        return $this->redirect(['action' => 'view', $invoice->id]);
      }

      $invoiceTemplate = str_replace('$organization_name', $organization->name, $invoiceTemplate);
      $invoiceTemplate = str_replace('$logo_address', (WWW_ROOT.'img/'.$organization->logo), $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_address', ($organization->address.', '.$organization->country), $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_phone', $organization->phone, $invoiceTemplate);
      $invoiceTemplate = str_replace('$organization_email', $organization->email, $invoiceTemplate);
      $invoiceTemplate = str_replace('$customer_name', $invoice->company->name, $invoiceTemplate);
      $invoiceTemplate = str_replace('$customer_address', ($invoice->company->address.', '.$invoice->company->country), $invoiceTemplate);
      $invoiceTemplate = str_replace('$project_name', $invoice->project!= null ? $invoice->project->name:'', $invoiceTemplate);
      $invoiceTemplate = str_replace('$payment_terms', ($paymentTerms->name.__(' días')), $invoiceTemplate);
      $invoiceTemplate = str_replace('$purchase_order', $invoice->purchase_order, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_date', $invoice->date, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_due_date', $invoice->due_date, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_number', ('Invoice #'.$invoice->id), $invoiceTemplate);
      $descriptionContent = '';

      foreach ($itemsInvoices as $item) {
        $descriptionContent .= '<tr><td class="desc">'.$item->description.'</td><td class="unit">'.$item->price.'</td><td class="qty">'.$item->quantity.'</td><td class="total">'.$item->subtotal.'</td></tr>';
      }

      $invoiceTemplate = str_replace('$invoice_description', $descriptionContent, $invoiceTemplate);
      $invoiceTemplate = str_replace('$invoice_total', $invoice->total_amount, $invoiceTemplate);

      $beneficiaryBankInfo = '<div>'.__('Banco beneficiario:').'<span>'.$banksOrganizations->name.'</span></div><div>SWIFT: <span>'.$banksOrganizations->swift.'</span></div>';
      $beneficiaryBankInfo .= '<div>ABA: <span>'.$banksOrganizations->aba.'</span></div><div>'.__('Dirección: ').'<span>'.$banksOrganizations->address.'</span></div>';
      $beneficiaryClientInfo = '<div>'.__('Cliente beneficiario:').'<span>'.$organization->name.'</span></div><div>IBAN: <span>'.$banksOrganizations->iban.'</span></div>';
      $beneficiaryClientInfo .= '<div>'.__('Cuenta: ').'<span>'.$account->account_number.'</span></div>';
      if ($organization->default_intermediary_bank != 0) {
        $intermediaryBank = TableRegistry::get('Banks')->find()->where(['id' => $organization->default_intermediary_bank])->first();
        $intermediaryBankInfo = '<div>'.__('Banco intermediario:').'<span>'.$intermediaryBank->name.'</span></div><div>SWIFT: <span>'.$intermediaryBank->swift.'</span></div>';
        $intermediaryBankInfo .= '<div>ABA: <span>'.$intermediaryBank->aba.'</span></div><div>'.__('Dirección: ').'<span>'.$intermediaryBank->address.'</span></div>';
        $invoiceTemplate = str_replace('$intermediary_bank_info', $intermediaryBankInfo, $invoiceTemplate);
      }
      else {
        $invoiceTemplate = str_replace('$intermediary_bank_info', '', $invoiceTemplate);
      }

      if ($organization->notes_invoices != '' || $organization->notes_invoices != null) {
        $invoiceTemplate = str_replace('$notes', $organization->notes_invoices, $invoiceTemplate);
      }
      else {
        $invoiceTemplate = str_replace('$notes', '', $invoiceTemplate);
      }

      $invoiceTemplate = str_replace('$beneficiary_bank_info', $beneficiaryBankInfo, $invoiceTemplate);
      $invoiceTemplate = str_replace('$beneficiary_client_info', $beneficiaryClientInfo, $invoiceTemplate);

      $mPDF = new \Mpdf\Mpdf();
      $mPDF->WriteHTML($cssInvoice, 1);
      $mPDF->writeHTML($invoiceTemplate);
      $mPDF->Output(WWW_ROOT.'invoices/invoice'.$invoice->id.'.pdf', 'F');
      $from = new \SendGrid\Email($organization->name, $organization->email);
      $to = new \SendGrid\Email(($contact->first_name.' '.$contact->last_name), $contact->email);
      $textToSend = '<div><pre>'.base64_decode($message).'</pre></div>';
      $content = new \SendGrid\Content("text/html", $textToSend);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $attachment = new \SendGrid\Attachment();
      $attachment->setContent(base64_encode(file_get_contents(WWW_ROOT.'invoices/invoice'.$invoice->id.'.pdf')));
      $attachment->setType('application/pdf');
      $attachment->setFilename('invoice.pdf');
      $attachment->setDisposition('attachment');
      $attachment->setContentId('Balance Sheet');
      $mail->addAttachment($attachment);
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
      if ($response->statusCode() == 202) {
        unlink(WWW_ROOT.'invoices/invoice'.$invoice->id.'.pdf');
        $this->Flash->success(__('El recordatorio ha sido enviado con éxito.'));
        return $this->redirect(['action' => 'view', $invoice->id]);
      }
    }

    public function getContentTemplate(){
      if ($this->request->is('ajax')) {
        $invoice = $this->Invoices->get($this->request->data['invoice_id']);
        $user = TableRegistry::get('Users')->find()->select(['Users.first_name', 'Users.last_name', 'Users.email'])->autofields(false)->where(['Users.id' => $this->Auth->user('id')])->first();
        $template = TableRegistry::get('Templates')->get($this->request->data['template']);
        $templateContent = $template->content;
        $templateContent = str_replace('$invoice_id', $invoice->id, $templateContent);
        $templateContent = str_replace('$user_name', ($user->first_name.' '.$user->last_name), $templateContent);
        $templateContent = str_replace('$user_email', ($user->email), $templateContent);
        $this->set('template', $templateContent);
        $this->set('_serialize', 'template');
      }
    }

    public function fixDueDate(){
      $invoices = $this->Invoices->find()->contain('Companies');

      foreach ($invoices as $invoice) {
        $invoice->due_date = $this->getDueDate($invoice);
        $this->Invoices->save($invoice);
      }
    }

    public function importInvoices(){
      $file = fopen(WWW_ROOT."files/Invoices.csv", "r");
      $first = true;
      $companies = TableRegistry::get('Companies');
      $projectTable = TableRegistry::get('Projects');
      $numberInvoices = 0;
      while (($line = fgetcsv($file, 1000, ";")) !== FALSE) {
        if ($first) {
          $first=false;
          continue;
        }
        $company = $companies->find()->where(['name LIKE' => utf8_decode($line[0])])->first();
        $company = $company != null ? $company->id : null;
        $project = $projectTable->find()->where(['name LIKE' => utf8_decode($line[5])])->first();
        $project = $project != null ? $project->id : null;
        $invoice = new Invoice();
        $invoice->date = new FrozenTime(date(str_replace('/', '-', $line[6])));
        $invoice->state = $line[8] == 0 ? 'P' : 'C';
        $invoice->total_amount = $line[10];
        $invoice->project_id = $project;
        $invoice->due_date = new FrozenTime(date(str_replace('/', '-', $line[7])));
        $invoice->outstanding_amount = $invoice->state == 'P' ? $line[10] : 0;
        $invoice->company_id = $company;
        $invoice->id = $line[14];
        if ($this->Invoices->save($invoice)) {
          $numberInvoices++;
        }
      }
      debug("Facturas agregadas...".$numberInvoices);
      die();
    }

    public function setCodeInvoice(){
      $invoices = $this->Invoices->find();
      $tableInvoices = TableRegistry::get('Invoices');

      foreach ($invoices as $invoice) {
        $invoice->code_invoice = $invoice->id;

        $tableInvoices->save($invoice);
      }

      die();
    }

    public function verifyDueDatesInvoices(){
      $invoices = $this->Invoices->find()
                                  ->where(['due_date <' => date('Y-m-d'), 'state' => 'P'])
                                  ->orwhere(['due_date <' => date('Y-m-d'), 'state' => 'V'])
                                  ->contain(['Companies' => function(\Cake\ORM\Query $q){
                                    return $q->select(['Companies.name'])
                                              ->autofields(false)
                                              ->contain(['Users' => function(\Cake\ORM\Query $q){
                                                return $q->select(['Users.email', 'Users.organization_id'])
                                                          ->autofields(false);
                                              }]);
                                  }]);

      $projectTable = TableRegistry::get('Projects');
      $companyTable = TableRegistry::get('Companies');
      $userTable = TableRegistry::get('Users');
      $usersToNotify = array();

      if($invoices->count() != 0){

        foreach ($invoices as $invoice) {

          if ($invoice->state == 'P') {
            $invoice->state = 'V';
            $this->Invoices->save($invoice);
          }
        }
      }
      die();
    }
}
