<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Task;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\I18n\FrozenTime;
use Cake\Event\Event;

/**
 * Milestones Controller
 *
 * @property \App\Model\Table\MilestonesTable $Milestones
 *
 * @method \App\Model\Entity\Milestone[] paginate($object = null, array $settings = [])
 */
class MilestonesController extends AppController
{
  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Milestones']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete', 'view', 'getMilestonesProject', 'finishMilestone', 'updateCostProject', 'sendEmails', 'correctDates', 'validateDatesTasks', 'reopenMilestone', 'verifyProject', 'getMilestonesByProjectName'])){
          return true;
      }
    }

  return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->Auth->user('role') == 'R'){
          $projects = array();
          $milestones = TableRegistry::get('MilestonesUsers')->find()->contain(['Milestones' => function(\Cake\ORM\Query $q){
                                                                                  return $q->select()->where(['user_id' => $this->Auth->user('id')])->contain(['Projects' => function(\Cake\ORM\Query $q){
                                                                                    return $q->select(['Projects.id', 'Projects.name']);
                                                                                  }]);
                                                                              }]);
          foreach ($milestones as $milestone) {
            $projects[$milestone->milestone->project->id] = $milestone->milestone->project->name;
          }
        }
        else{
          $tableProjects = TableRegistry::get('Projects');
          $projects = $tableProjects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
        }

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * View method
     *
     * @param string|null $id Milestone id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $milestone = $this->Milestones->get($id, [
            'contain' => ['Projects', 'Users', 'Tasks']
        ]);

        $this->set('milestone', $milestone);
        $this->set('_serialize', ['milestone']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $milestone = $this->Milestones->newEntity();
        if ($this->request->is('post')) {
            $milestone = $this->Milestones->patchEntity($milestone, $this->request->getData());
            $project_id = $this->Milestones->Projects->find()->select(['Projects.id'])->autofields(false)->where(['Projects.name LIKE' => $milestone->project_name])->first();
            $milestone->project_id = $project_id->id;
            if($this->correctDates($milestone)){
                if ($this->Milestones->save($milestone)) {
                    if (!empty($milestone->tasksArray)) {
                      $contDate = 0;
                      $usersToNotify = array();
                      $messageToSend = "<h2>¡Hola!</h2><p>Se han creado las siguientes tareas que te han sido asignadas, pertenecientes al proyecto <i>".$milestone->project_name."</i>";
                      for ($i=0; $i < count($milestone->tasksArray['name']); $i++) {
                        $endDate = new FrozenTime($milestone->tasksArray['end_date'][$i]);
                        $tableTasks = TableRegistry::get('Tasks');
                        $task = new Task(['name' => $milestone->tasksArray['name'][$i], 'description' => $milestone->tasksArray['description'][$i], 'end_date' => $endDate, 'priority' => $milestone->tasksArray['priority'][$i]]);
                        $task->user_id = $milestone->tasksArray['user'][$i];
                        $task->milestone_id = $milestone->id;
                        $task->task_type_id = $milestone->tasksArray['task_type'][$i];

                        $userTask = TableRegistry::get('Users')->get($milestone->tasksArray['user'][$i]);

                        if ($this->validateDatesTasks($task, $milestone)) {
                          if($tableTasks->save($task)){
                            if (count($_FILES['attachments']['size']) != 0) {
                              $this->saveAttachments($_FILES['attachments'], $task);
                            }
                            if (!isset($usersToNotify[$userTask->email])) {
                              $usersToNotify[$userTask->email] = "<li>".$task->name." - Fecha de entrega: ".$task->end_date."</li>";
                            }
                            else {
                              $usersToNotify[$userTask->email] .= "<li>".$task->name." - Fecha de entrega: ".$task->end_date."</li>";
                            }
                            $this->Flash->success(__('La tarea {0} ha sido salvado exitosamente', $task->name));
                          }
                        }
                        else{
                          $this->Flash->error(__('La tarea {0} no pudo ser salvado ya que existen conflictos entre la fechas de la tarea y el milestone. Intente ingresarlo nuevamente desde la sección de tareas.', $task->name));
                        }

                        $contDate += 5;
                      }
                      $this->sendEmails('Nueva Tarea', $messageToSend, $usersToNotify);
                    }
                    $project = TableRegistry::get('Projects')->get($milestone->project_id);
                    $message = "<h2>¡Hola!</h2><p>Se ha creado el milestone ".$milestone->name." que te ha sido asignado.";
                    $users = array();
                    foreach ($milestone->users as $user) {
                        $users[$user->email] = "<li> Fecha de inicio: ".$milestone->start_date." - Fecha de fin: ".$milestone->end_date."</li>";
                    }
                    $this->sendEmails('Nuevo milestone', $message, $users);
                    $this->Flash->success(__('El milestone ha sido salvado exitosamente'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('El milestone no pudo ser salvado. Intente nuevamente'));
            }
            $this->Flash->error(__('Existen conflictos con las fechas del milestone y el proyecto. Intente nuevamente'));

        }
        $taskTypes = TableRegistry::get('TaskTypes')->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'type']);
        $projects = $this->Milestones->Projects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
        $users = $this->Milestones->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $this->set(compact('milestone', 'projects', 'users', 'taskTypes'));
        $this->set('_serialize', ['milestone']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Milestone id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $milestone = $this->Milestones->get($id, [
            'contain' => ['Users']
        ]);
        $project_name = $this->Milestones->Projects->get($milestone->project_id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $prevState = $milestone->state;
            $milestone = $this->Milestones->patchEntity($milestone, $this->request->getData());
            $project_id = $this->Milestones->Projects->find()->select(['Projects.id'])->autofields(false)->where(['Projects.name LIKE' => $milestone->project_name])->first();
            $milestone->project_id = $project_id->id;
            if ($this->Milestones->save($milestone)) {
              if($prevState != $milestone->state && $milestone->state == 'C'){
                $message = "<h2>¡Hola!</h2>El milestone: <strong>".$milestone->name."</strong> ha sido marcado como <em>Cerrado</em>";
                $users = array();
                foreach ($milestone->users as $user) {
                  $users[$user->email] = '';
                }
                $this->sendEmails('Milestone Cerrado', $message, $users);
              }
                $this->Flash->success(__('El milestone ha sido salvado exitosamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El milestone no pudo ser salvado. Intente nuevamente'));
        }
        $projects = $this->Milestones->Projects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
        $users = $this->Milestones->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $this->set(compact('milestone', 'projects', 'users', 'project_name'));
        $this->set('_serialize', ['milestone']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Milestone id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $milestone = $this->Milestones->get($id);
        if ($this->Milestones->delete($milestone)) {
            $this->Flash->success(__('El milestone ha sido eliminado exitosamente'));
        } else {
            $this->Flash->error(__('El milestone no se ha podido eliminar. Intente nuevamente'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getMilestonesProject($id = null){
      if($this->request->is('ajax')){
        $requestData = $this->request->data;
        $projectName = $requestData['name'];

        if($this->Auth->user('role') == 'A'){
          $milestones = $this->Milestones->find()->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectName){
            return $q->select(['Projects.name'])->where(['Projects.name LIKE' => $projectName]);
          }]);
        }
        else{
          $subQuery = TableRegistry::get('MilestonesUsers')->find()->where(['user_id' => $this->Auth->user('id')]);
          $milestones = $this->Milestones->find()->where([function($exp) use($subQuery){
            return $exp->exists($subQuery);
          }])->contain(['Projects' => function(\Cake\ORM\Query $q) use($projectName){
            return $q->select(['Projects.name'])->where(['Projects.name LIKE' => $projectName]);
          }]);
        }
        $this->set('milestones', $milestones->toArray());
        $this->set('_serialize', ['milestones']);
      }
    }


    private function updateCostProject($projectID, $task){
      $projectTable = TableRegistry::get('Projects');
      $users = TableRegistry::get('Users');
      $project = $projectTable->get($projectID);
      $user = $users->get($task->user_id);

      $project->total_cost = $project->total_cost + ($task->duration*$user->hour_cost);

        if ($projectTable->save($project)) {
          return true;
        }
        return false;
    }

    private function sendEmails($subject, $message, $user){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');

      if (is_array($user)) {
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $content = new \SendGrid\Content("text/plain", 'hola');
        $mail = new \SendGrid\Mail($from, $subject, $to, $content);

        foreach ($user as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $message);
          $personalization->addSubstitution('%button_content%', 'localhost/wdpm/tasks');
          $mail->addPersonalization($personalization);
        }
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);
      }
      else {
        $to = new \SendGrid\Email(null, $user->email);
        $content = new \SendGrid\Content("text/html", $template);
        $mail = new \SendGrid\Mail($from, $subject, $to, $content);
        $mail->personalization[0]->addSubstitution('%html_content%', $message);
        $mail->personalization[0]->addSubstitution('%text_content%', '<h2>¡Hola!</h2>');
        $mail->personalization[0]->addSubstitution('%button_content%', 'localhost/wdpm/tasks/');
      }
      //API TEST KEY
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    private function correctDates($milestone){
      $project = TableRegistry::get('Projects')->get($milestone->project_id);

      if ($project->start_date != null && $project->end_date != null) {
        if($milestone->start_date >= $project->start_date && $milestone->start_date < $project->end_date && $milestone->end_date <= $project->end_date){
          return true;
        }
        else {
          return false;
        }
      }

      return true;
    }

    public function finishMilestone(){
      $success = false;
      if($this->request->is('post')){
        $milestoneID = $this->request->getData()['id'];
        $endDate = $this->request->getData()['end_date'];
        $milestone = $this->Milestones->get($milestoneID);
        $tasks = TableRegistry::get('Tasks')->find()
                                            ->select(['Tasks.id', 'Tasks.state'])
                                            ->autofields(false)
                                            ->where(['Tasks.milestone_id' => $milestone->id, 'Tasks.state' => 'P'])
                                            ->count();

        if($tasks == 0){
          $milestone->state = 'C';
          if($this->Milestones->save($milestone)){
            $users = TableRegistry::get('MilestonesUsers')->find()->where(['milestone_id' => $milestone->id])->contain(['Users']);
            $message = "<h2>¡Hola!</h2><br><br>Se ha marcado el milestone: <strong>".$milestone->name."</strong> como <em>Cerrada</em>";
            $users_array = array();
            foreach ($users as $user) {
              $users_array[$user->user->email] = '';
            }
            $this->sendEmails('Milestone Cerrado', $message, $users_array);
            $this->Flash->success(__('El milestone ha sido cerrado correctamente.'));
            return $this->redirect(['action' => 'index']);
          }
        }
        else {
          $this->Flash->error(__('El milestone contiene tareas pendientes por lo que no puede ser cerrado.'));
          return $this->redirect(['action' => 'index']);
        }
      }
      $this->set('success', $success);
      $this->set('_serialize', ['success']);
    }

    public function validateDatesTasks($task, $milestone){
      if (isset($task->end_date)) {
          if(new FrozenTime($task->end_date) >= $milestone->start_date && new FrozenTime($task->end_date) <= $milestone->end_date){
            return true;
          }
      }

      return false;
    }

    public function reopenMilestone(){
      if($this->request->is('post')){
        $milestoneID = $this->request->getData()['id'];
        $milestone = $this->Milestones->get($milestoneID, ['contain' => ['Projects']]);
        $today = new FrozenTime(date('Y-m-d'));

        if ($milestone->end_date > $today) {
          if ($milestone->state === 'C' && $this->verifyProject($milestone)) {
            $milestone->state = 'O';
            if ($this->Milestones->save($milestone)) {
              $users = TableRegistry::get('MilestonesUsers')->find()->where(['milestone_id' => $milestone->id])->contain(['Users']);
              $message = "<h2>¡Hola!</h2><br><br>Se ha reabierto el milestone: <strong>".$milestone->name."</strong>";
              $users_array = array();
              foreach ($users as $user) {
                $users_array[$user->user->email] = '';
              }
              $this->sendEmails('Milestone Cerrado', $message, $users_array);
              $this->Flash->success(__('El milestone ha sido reabierto'));
            }
          }
          else {
            $this->Flash->error(__('El milestone y/o el proyecto ya se encuentra cerrado.'));
          }
        }
        else {
          $this->Flash->error(__('La fecha de entrega del milestone ya fue sobrepasada.'));
        }
      }
    }

    public function verifyProject($milestone){
      if ($milestone->project->state == 'O') {
        return true;
      }
      return false;
    }


    public function getMilestonesByProjectName(){
      if ($this->request->isAjax()) {
        $requestData = $this->request->data;
        if($this->Auth->user('role') == 'A'){
          $milestones = $this->Milestones->find()->contain(['Projects' => function(\Cake\ORM\Query $q) use($requestData){
            return $q->select(['Projects.id'])->where(['Projects.name LIKE' => $requestData['project_name']]);
          }]);
        }
        else{
          $subQuery = TableRegistry::get('MilestonesUsers')->find()->where(['user_id' => $this->Auth->user('id')]);
          $milestones = $this->Milestones->find()->where([function($exp) use($subQuery){
            return $exp->exists($subQuery);
          }])->contain(['Projects' => function(\Cake\ORM\Query $q) use($requestData){
            return $q->select(['Projects.id'])->where(['Projects.name LIKE' => $requestData['project_name']]);
          }]);
        }

        $this->set('milestones', $milestones->toArray());
        $this->set('_serialize', ['milestones']);
      }
    }

    private function saveAttachments($files, $task){
      $attachmentsPath = WWW_ROOT . 'attachments/' . $task->id;
      $numberFiles = count($files['size']);
      $attachmentsTable = TableRegistry::get('Attachments');

      if (!file_exists($attachmentsPath)) {
        mkdir($attachmentsPath, 0777, true);
      }

      for ($i=0; $i < $numberFiles; $i++) {
        $fileName = addslashes($files['tmp_name'][$i]);
        $name = str_replace(' ', '_', $files['name'][$i]);
        $pathFile = $attachmentsPath . DS . $name;
        if (move_uploaded_file($fileName, $pathFile)) {
          $attachment = new Attachment(['task_id' => $task->id, 'file' => $pathFile, 'name' => $name]);
          $attachmentsTable->save($attachment);
        }
      }
    }
}
