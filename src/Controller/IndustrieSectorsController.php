<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * IndustrieSectors Controller
 *
 * @property \App\Model\Table\IndustrieSectorsTable $IndustrieSectors
 *
 * @method \App\Model\Entity\IndustrieSector[] paginate($object = null, array $settings = [])
 */
class IndustrieSectorsController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $industrieSectors = $this->paginate($this->IndustrieSectors);

        $this->set(compact('industrieSectors'));
        $this->set('_serialize', ['industrieSectors']);
    }

    /**
     * View method
     *
     * @param string|null $id Industrie Sector id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $industrieSector = $this->IndustrieSectors->get($id, [
            'contain' => []
        ]);

        $this->set('industrieSector', $industrieSector);
        $this->set('_serialize', ['industrieSector']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $industrieSector = $this->IndustrieSectors->newEntity();
        if ($this->request->is('post')) {
            $industrieSector = $this->IndustrieSectors->patchEntity($industrieSector, $this->request->getData());
            if ($this->IndustrieSectors->save($industrieSector)) {
                $this->Flash->success(__('The industrie sector has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The industrie sector could not be saved. Please, try again.'));
        }
        $this->set(compact('industrieSector'));
        $this->set('_serialize', ['industrieSector']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Industrie Sector id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $industrieSector = $this->IndustrieSectors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $industrieSector = $this->IndustrieSectors->patchEntity($industrieSector, $this->request->getData());
            if ($this->IndustrieSectors->save($industrieSector)) {
                $this->Flash->success(__('The industrie sector has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The industrie sector could not be saved. Please, try again.'));
        }
        $this->set(compact('industrieSector'));
        $this->set('_serialize', ['industrieSector']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Industrie Sector id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $industrieSector = $this->IndustrieSectors->get($id);
        if ($this->IndustrieSectors->delete($industrieSector)) {
            $this->Flash->success(__('The industrie sector has been deleted.'));
        } else {
            $this->Flash->error(__('The industrie sector could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
    }
}
