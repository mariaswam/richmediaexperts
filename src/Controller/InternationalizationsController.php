<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Cache\Cache;

/**
 * Internationalizations Controller
 *
 *
 * @method \App\Model\Entity\Internationalization[] paginate($object = null, array $settings = [])
 */
class InternationalizationsController extends AppController
{
    public $components = array('Cookie');

    public function isAuthorized($user){
      return true;
    }

    public function changeLocale($locale = null){
      if ($this->request->is('post')) {
        // Cache::write('locale', $locale);
        $session = $this->request->session();
        $session->write('Config.defaultLocale', $locale);
      }
      $this->redirect($this->referer());
    }
}
