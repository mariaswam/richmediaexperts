<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Notifications Controller
 *
 *
 * @method \App\Model\Entity\Notification[] paginate($object = null, array $settings = [])
 */
class NotificationsController extends AppController
{
  public function isAuthorized($user){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['verifyNotifications', 'markAsReadNotifications', 'index'])){
        debug("Entro");die();
          return true;
      }

  return parent::isAuthorized($user);
  }

  public function index(){
    $notifications = $this->Notifications->find()->where(['Notifications.user_id' => $this->Auth->user('id')]);

    $this->set('notifications', $notifications);
    $this->set('_serialize', ['notifications']);
  }

  public function verifyNotifications(){
    if ($this->request->is('ajax')) {
      $notifications = $this->Notifications->find()->where(['Notifications.user_id' => $this->Auth->user('id'), 'Notifications.state' => 'NR']);
    }

    $this->set('notifications', $notifications);
    $this->set('_serialize', ['notifications']);
  }

  public function markAsReadNotifications(){
    if ($this->request->is('ajax')) {
      $notifications = $this->Notifications->find()->where(['Notifications.user_id' => $this->Auth->user('id'), 'Notifications.state' => 'NR']);

      foreach ($notifications as $notification) {
        $notification->state = 'R';
        $this->Notifications->save($notification);
      }
    }
  }
}
