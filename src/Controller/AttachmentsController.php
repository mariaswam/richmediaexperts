<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Attachments Controller
 *
 * @property \App\Model\Table\AttachmentsTable $Attachments
 *
 * @method \App\Model\Entity\Attachment[] paginate($object = null, array $settings = [])
 */
class AttachmentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tasks']
        ];
        $attachments = $this->paginate($this->Attachments);

        $this->set(compact('attachments'));
        $this->set('_serialize', ['attachments']);
    }

    /**
     * View method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $attachment = $this->Attachments->get($id, [
            'contain' => ['Tasks']
        ]);

        $this->set('attachment', $attachment);
        $this->set('_serialize', ['attachment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->autoRender = false;
        $attachment = $this->Attachments->newEntity();
        if ($this->request->is('post')) {
            $attachment = $this->Attachments->patchEntity($attachment, $this->request->getData());
            $results = $this->saveAttachments($_FILES['attachments'], $attachment->task_id);
            $attachment->file = $results['file'];
            $attachment->name = $results['name'];
            if ($this->Attachments->save($attachment)) {
                $this->Flash->success(__('The attachment has been saved.'));
                $resultJ = json_encode(array('result' => 'success'));
                $this->response->type('json');
                $this->response->body($resultJ);
                return $this->response;
            }
            $this->Flash->error(__('The attachment could not be saved. Please, try again.'));
            $resultJ = json_encode(array('result' => 'error'));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
        }
        $tasks = $this->Attachments->Tasks->find('list', ['limit' => 200]);
        $this->set(compact('attachment', 'tasks'));
        $this->set('_serialize', ['attachment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $attachment = $this->Attachments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attachment = $this->Attachments->patchEntity($attachment, $this->request->getData());
            if ($this->Attachments->save($attachment)) {
                $this->Flash->success(__('The attachment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attachment could not be saved. Please, try again.'));
        }
        $tasks = $this->Attachments->Tasks->find('list', ['limit' => 200]);
        $this->set(compact('attachment', 'tasks'));
        $this->set('_serialize', ['attachment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Attachment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $attachment = $this->Attachments->get($id);
        $task = $attachment->task_id;
        $fileDelete = $attachment->file;
        if ($this->Attachments->delete($attachment)) {
          if (file_exists($fileDelete)) {
            unlink($fileDelete);
          }
            $this->Flash->success(__('The attachment has been deleted.'));
        } else {
            $this->Flash->error(__('The attachment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Tasks','action' => 'view', $task]);
    }


    public function downloadAttachment($id = null){
      $attachment = $this->Attachments->get($id);

      if (file_exists($attachment->file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($attachment->name).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        flush(); // Flush system output buffer
        readfile($attachment->file);
        exit;
      }
    }

    public function downloadAllFiles($taskID = null){
      $attachments = $this->Attachments->find()->where(['task_id' => $taskID]);
      $zip = new \ZipArchive();
      $zipFile = WWW_ROOT.'downloads/attachments.zip';
      $dir = WWW_ROOT . 'attachments/' . $taskID;

      if (file_exists($dir)) {
        if ($zip->open($zipFile, \ZipArchive::CREATE) != TRUE) {
          die('Cannot open');
        }

        $this->createZip($zip, $dir);

        $zip->close();

        if (file_exists($zipFile)) {
          header("Content-type: application/zip");
          header("Content-Disposition: attachment; filename=attachments.zip");
          header("Pragma: no-cache");
          header("Expires: 0");
          ob_clean();
          flush();
          readfile("$zipFile");
          // delete file
          unlink($zipFile);
        }
      }

    }

    private function createZip($zip, $dir){
      if (is_dir($dir)){
           if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){
             // If file
             if (is_file($dir.'/'.$file)) {
              if($file != '' && $file != '.' && $file != '..' && $file != '.DS_Store'){
               $zip->addFile($dir.'/'.$file, $file);
              }
             }
            }
            closedir($dh);
           }
      }
    }

    private function saveAttachments($files, $task){
      $attachmentsPath = WWW_ROOT . 'attachments/' . $task;
      $numberFiles = count($files['size']);

      if (!file_exists($attachmentsPath)) {
        mkdir($attachmentsPath, 0777, true);
      }

      for ($i=0; $i < $numberFiles; $i++) {
        $fileName = addslashes($files['tmp_name'][$i]);
        $name = str_replace(' ', '_', $files['name'][$i]);
        $pathFile = $attachmentsPath . DS . $name;
        if (move_uploaded_file($fileName, $pathFile)) {
          $attachment = ['file' => $pathFile, 'name' => $name];
        }
      }

      return $attachment;
    }
}
