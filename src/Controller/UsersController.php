<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use App\Model\Entity\Organization;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\Utility\Security;
use Cake\Routing\Router;
use App\Model\Entity\PermissionsUser;
use Firebase\JWT\JWT;
use Cake\I18n\Time;
use \DateTime;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(Event $event){
        $this->Auth->allow(['register', 'forgotPassword', 'resetPassword', 'loginApp']);

        return parent::beforeFilter($event);
    }

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if(($user['role'] == 'R' || $user['role'] == 'E') && in_array($this->request->getParam('action'), ['logout', 'accountSettings', 'add', 'edit', 'view', 'delete', 'index','permissions', 'getUsersPermissions', 'addPermissionsToUser'])){
            return true;
        }
      }
      if(($user['role'] == 'R' || $user['role'] == 'E') && in_array($this->request->getParam('action'), ['logout', 'accountSettings'])){
          return true;
      }


    return parent::isAuthorized($user);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->Users->find()->contain(['Organizations']);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Milestones', 'Tasks']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $randPass = $this->randomPassword();
            $user->password = $randPass;
            $user->confirm_password = $randPass;
            $user->organization_id = $this->Auth->user('organization_id');
            if ($this->Users->save($user)) {
              $header = "<h2>¡Bienvenido a WDPM!</h2><p>Saludos estimado(a) ".$user->first_name." ".$user->last_name.":</p>";
              $message = "<p>Se ha creado una nueva cuenta a su nombre. ";
              $message .= "Las credenciales de ingreso son las siguientes: <ul><li><strong>Usuario</strong>: <em>".$user->username."</em></li><li> <strong>Contraseña:</strong> <em>".$randPass."</em></li></ul></p>";
              $message .= "<p>Por favor cambiar tu contraseña al momento de ingresar.</p>";
              $this->sendEmails('Nueva cuenta', $message, $user, $header, 'localhost/wdpm/users/login');
              $this->addPermissionsToUser($user);
                $this->Flash->success(__('El usuario ha sido salvado exitosamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo ser salvado. Por favor intente nuevamente.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Milestones']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido salvado exitosamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo ser salvado. Por favor intente nuevamente.'));
        }
        $milestones = $this->Users->Milestones->find('list', ['limit' => 200]);
        $permissions = TableRegistry::get('Permissions')->find();
        $this->set(compact('user', 'milestones', 'permissions'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function register(){
      $this->viewBuilder()->setLayout('session');
      $user = $this->Users->newEntity();
      if($this->request->is('post')){
        $user = $this->Users->patchEntity($user, $this->request->getData());
        $user->role = 'A';
        $user->first_login = 1;
        $organization = new Organization(['name' => $user->organization_name]);
        $organization->logo= '/org/default/logo_default.png';
        $tableOrganization = TableRegistry::get('Organizations')->save($organization);
        $user->organization_id = $organization->id;
        if($this->Users->save($user)){
          $header = "<h2>¡Bienvenido a WDPM!</h2><p>Saludos estimado(a) ".$user->first_name." ".$user->last_name.":</p>";
          $message = "<p>Recientemente te has registrado a <em>WDPM</em> comenzá a administrar tus proyectos.";
          $this->sendEmails('Bienvenido a WDPM', $message, $user, $header, 'localhost/users/login');
          $permissions = TableRegistry::get('Permissions')->find();
          $usersPermissionsTable = TableRegistry::get('PermissionsUsers');
          foreach ($permissions as $permission) {
            $permissionUser = new PermissionsUser(['user_id' => $user->id, 'permission_id' => $permission->id]);
            $usersPermissionsTable->save($permissionUser);
          }
          return $this->redirect(['action' => 'login']); //Redirect to login
        }
      }
      $this->set(compact('user'));
      $this->set('_serialize', ['user']);

    }

    public function login(){
      $this->viewBuilder()->setLayout('session');
      if($this->request->is('post')){
        $user = $this->Auth->identify();
        if($user){
          $this->Auth->setUser($user);
          if($user['first_login'] == 0){
            return $this->redirect(['action' => 'accountSettings']);
          }
          if ($user['id'] == 50) {
            return $this->redirect(['controller' => 'Timesheets', 'action' => 'report']);
          }
          switch ($user['role']) {
            case 'R':
              return $this->redirect(['controller' => 'Dashboard', 'action' => 'userIndex']);
              break;
            case 'E':
              return $this->redirect(['controller' => 'Quotes', 'action' => 'index']);
              break;
          }

          return $this->redirect($this->Auth->redirectUrl());
        } else{
          $this->Flash->error('El nombre y/o contraseña son inválidos', ['key' => 'auth']);
        }
      }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function accountSettings(){
      $userID = $this->Auth->user('id');
      $user = $this->Users->get($userID);
      if($this->request->is(['patch', 'post', 'put'])){
        $user = $this->Users->patchEntity($user, $this->request->getData());
        $changePassword = false;
        if($user->dirty('password')){
          $changePassword = true;
        }
        if($this->Users->save($user)){
          if ($changePassword) {
            $this->Flash->success('La contraseña ha sido cambiada exitosamente');
          }
          else {
            $this->Flash->success('El usuario ha sido salvado exitosamente');
          }
          return $this->redirect(['action' => 'accountSettings']);
        }
      }

      $this->set(compact('user'));
      $this->set('_serialize', ['user']);
    }

    public function forgotPassword(){
      $this->viewBuilder()->setLayout('session');
      if($this->request->is('post')){
        $email = $this->request->getData()['email'];
        $user = $this->Users->findByEmail($email)->first();
        if(!empty($user)){
          $password = sha1(Text::uuid());
          $passwordToken = Security::hash($password, 'sha256', true);
          $hashval = sha1($user->username . rand(0, 100));

          $user->password_reset_token = $passwordToken;
          $user->hasval = $hashval;

          $reset_token_link = Router::url(['controller' => 'Users', 'action' => 'resetPassword'], TRUE) . '/' . $passwordToken . '#' . $hashval;
          $header = "<h1>Reestablecimiento de contraseña</h1>";
          $message = "<h2>¡Hola!</h2><p>Parece que olvidaste tu contraseña, para reestablecerla ingresa al siguiente enlace</p> <li>".$reset_token_link."</li>";

          $this->sendEmails('Reestablecer contraseña', $message, $user, $header, $reset_token_link);

          $this->Users->save($user);
          $this->Flash->success('Se ha enviado un enlace para reestablecer la contraseña. Por favor verifica tu correo.', ['key' => 'auth']);
        }
        else{
          $this->Flash->error('El email ingresado no se encuentra registrado. Verifique que haya ingresado el dato correcto.', ['key' => 'auth']);
        }
      }
    }

    public function resetPassword($token = null){
      $this->viewBuilder()->setLayout('session');
      if(!empty($token)){
        $user = $this->Users->findByPasswordResetToken($token)->first();
        if($user){
          if(!empty($this->request->data)){
            $password = $this->request->data['new_password'];
            $confirm_password = $this->request->data['confirm_password'];

            $user->password = $password;
            $user->confirm_password = $confirm_password;
            $hashvalNew = sha1($user->username . rand(0, 100));
            $user->password_reset_token = $hashvalNew;

            if($this->Users->save($user)){
              $header = "<h1>Cambio de contraseña</h1>";
              $message = "<h2>¡Hola!</h2><p>Tu contraseña ha sido modificado de manera exitosa.</p>";
              $this->sendEmails('Nueva contraseña', $message, $user, $header, 'localhost/wdpm/users/login');
              $this->Flash->success('La contraseña ha sido modificada correctamente.', ['key' => 'auth']);
              return $this->redirect(['action' => 'login']);
            }
            else{
              $this->Flash->error('La contraseña no pudo ser modificada. Por favor intente nuevamente.', ['key' => 'auth']);
            }
          }
        }
      }
      $this->set(compact('user'));
      $this->set('_serialize', ['user']);
    }

    public function permissions(){
      if ($this->request->is('ajax')) {
        $permissions = $this->request->data['permissions'];
        $usersPermissionsTable = TableRegistry::get('PermissionsUsers');
        for ($i=0; $i < count($permissions) ; $i++) {
          $permissionUser = new PermissionsUser(['user_id' => $this->request->data['user_id'], 'permission_id' => $permissions[$i]['module']]);
          $exists = $usersPermissionsTable->find()->select(['id'])->autofields(false)->where(['user_id' => $permissionUser->user_id, 'permission_id' => $permissionUser->permission_id]);
          if($exists->count() == 0){
            if($permissions[$i]['permission'] == 'true'){
              $usersPermissionsTable->save($permissionUser);
            }
          }
          else {
            $permission = $usersPermissionsTable->get($exists->toArray()[0]->id);

            if($permissions[$i]['permission'] == 'true'){
              $permission->user_id = $this->request->data['user_id'];
              $permission->permission_id = $permissions[$i]['module'];
              $usersPermissionsTable->save($permission);
            }
            else{
              $usersPermissionsTable->delete($permission);
            }
          }
        }
        $this->Flash->success('Cambios en permisos realizados.');
      }
    }

    private function randomPassword() {
      $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
      $pass = array(); //remember to declare $pass as an array
      $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
      for ($i = 0; $i < 8; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }
      return implode($pass); //turn the array into a string
    }

    private function sendEmails($subject, $message, $user, $header, $link){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($user['first_name'].' '.$user['last_name'], $user['email']);
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
      $content = new \SendGrid\Content("text/html", $template);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $mail->personalization[0]->addSubstitution('%html_content%', $message);
      $mail->personalization[0]->addSubstitution('%text_content%', $header);
      $mail->personalization[0]->addSubstitution('%button_content%', $link);
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    public function getUsersPermissions(){
      if ($this->request->is('ajax')) {
        $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $this->request->data['user_id']]);

        $this->set('permissions', $permissions);
        $this->set('_serialize', 'permissions');
      }
    }

    private function addPermissionsToUser($user){
      $permissionsTable = TableRegistry::get('Permissions');
      $usersPermissionsTable = TableRegistry::get('PermissionsUsers');
      if ($user->role == 'R') {
        $permissions = $permissionsTable->find()->where(['module' => 'Milestones'])->orWhere(['module' => 'Tasks'])->orWhere(['module' => 'Timesheets']);
      }
      else{
        $permissions = $permissionsTable->find();
      }

      foreach ($permissions as $permission) {
        $permissionUser = new PermissionsUser(['user_id' => $user->id, 'permission_id' => $permission->id]);
        $usersPermissionsTable->save($permissionUser);
      }

    }

    public function loginApp(){
      $this->autoRender = false;
      header('Access-Control-Allow-Origin: *');
      $data = [];
      if($this->request->is('post')){
        $user = $this->Auth->identify();
        if($user){
          $userToSave = $this->Users->get($user['id']);
          $time = date(DateTime::ISO8601);
          $timeExp = strtotime($time) + (1200*1200);
          $key = '16cf8e473001a6ceff36ab1a24134a76';
          $jwtToken = base64_encode($user['id'].".".$user['username']);

          $token = array('iat' => strtotime($time), 'exp' => $timeExp, 'data' => ['jwtToken' => $jwtToken]);

          $jwt = JWT::encode($token, $key);

          $userToSave->jwt = $jwtToken;
          $this->Users->save($userToSave);

          $data = JWT::decode($jwt, $key, array('HS256'));

          $resultJ = json_encode(array('jwt' => $jwt, 'user' => $user));
          $this->response->type('json');
          $this->response->body($resultJ);
          return $this->response;

        } else{
          $resultJ = json_encode(array('data' => $data));
          $this->response->type('json');
          $this->response->body($resultJ);
          return $this->response;
        }
      }
    }
}
