<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Tax Controller
 *
 *
 * @method \App\Model\Entity\Tax[] paginate($object = null, array $settings = [])
 */
class TaxController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tax = $this->paginate($this->Tax);

        $this->set(compact('tax'));
        $this->set('_serialize', ['tax']);
    }

    /**
     * View method
     *
     * @param string|null $id Tax id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tax = $this->Tax->get($id, [
            'contain' => []
        ]);

        $this->set('tax', $tax);
        $this->set('_serialize', ['tax']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tax = $this->Tax->newEntity();
        if ($this->request->is('post')) {
            $tax = $this->Tax->patchEntity($tax, $this->request->getData());
            if ($this->Tax->save($tax)) {
                $this->Flash->success(__('El impuesto fue salvado correctamente.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('El impuesto no pudo ser salvado. Por favor intente nuevamente'));
        }
        $this->set(compact('tax'));
        $this->set('_serialize', ['tax']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tax id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tax = $this->Tax->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tax = $this->Tax->patchEntity($tax, $this->request->getData());
            if ($this->Tax->save($tax)) {
                $this->Flash->success(__('The tax has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The tax could not be saved. Please, try again.'));
        }
        $this->set(compact('tax'));
        $this->set('_serialize', ['tax']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tax id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tax = $this->Tax->get($id);
        if ($this->Tax->delete($tax)) {
            $this->Flash->success(__('The tax has been deleted.'));
        } else {
            $this->Flash->error(__('The tax could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
    }
}
