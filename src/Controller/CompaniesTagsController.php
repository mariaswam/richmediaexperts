<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * CompaniesTags Controller
 *
 * @property \App\Model\Table\CompaniesTagsTable $CompaniesTags
 *
 * @method \App\Model\Entity\CompaniesTag[] paginate($object = null, array $settings = [])
 */
class CompaniesTagsController extends AppController
{
  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Config']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])){
          return true;
      }
    }

  return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Companies', 'Tags']
        ];
        $companiesTags = $this->paginate($this->CompaniesTags);

        $this->set(compact('companiesTags'));
        $this->set('_serialize', ['companiesTags']);
    }

    /**
     * View method
     *
     * @param string|null $id Companies Tag id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $companiesTag = $this->CompaniesTags->get($id, [
            'contain' => ['Companies', 'Tags']
        ]);

        $this->set('companiesTag', $companiesTag);
        $this->set('_serialize', ['companiesTag']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $companiesTag = $this->CompaniesTags->newEntity();
        if ($this->request->is('post')) {
            $companiesTag = $this->CompaniesTags->patchEntity($companiesTag, $this->request->getData());
            if ($this->CompaniesTags->save($companiesTag)) {
                $this->Flash->success(__('The companies tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The companies tag could not be saved. Please, try again.'));
        }
        $companies = $this->CompaniesTags->Companies->find('list', ['limit' => 200]);
        $tags = $this->CompaniesTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('companiesTag', 'companies', 'tags'));
        $this->set('_serialize', ['companiesTag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Companies Tag id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $companiesTag = $this->CompaniesTags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $companiesTag = $this->CompaniesTags->patchEntity($companiesTag, $this->request->getData());
            if ($this->CompaniesTags->save($companiesTag)) {
                $this->Flash->success(__('The companies tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The companies tag could not be saved. Please, try again.'));
        }
        $companies = $this->CompaniesTags->Companies->find('list', ['limit' => 200]);
        $tags = $this->CompaniesTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('companiesTag', 'companies', 'tags'));
        $this->set('_serialize', ['companiesTag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Companies Tag id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $companiesTag = $this->CompaniesTags->get($id);
        if ($this->CompaniesTags->delete($companiesTag)) {
            $this->Flash->success(__('The companies tag has been deleted.'));
        } else {
            $this->Flash->error(__('The companies tag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
