<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * TaskTypes Controller
 *
 * @property \App\Model\Table\TaskTypesTable $TaskTypes
 *
 * @method \App\Model\Entity\TaskType[] paginate($object = null, array $settings = [])
 */
class TaskTypesController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $taskTypes = $this->paginate($this->TaskTypes);

        $this->set(compact('taskTypes'));
        $this->set('_serialize', ['taskTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Task Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $taskType = $this->TaskTypes->get($id, [
            'contain' => ['Tasks']
        ]);

        $this->set('taskType', $taskType);
        $this->set('_serialize', ['taskType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $taskType = $this->TaskTypes->newEntity();
        if ($this->request->is('post')) {
            $taskType = $this->TaskTypes->patchEntity($taskType, $this->request->getData());
            if ($this->TaskTypes->save($taskType)) {
                $this->Flash->success(__('The task type has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The task type could not be saved. Please, try again.'));
        }
        $this->set(compact('taskType'));
        $this->set('_serialize', ['taskType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Task Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $taskType = $this->TaskTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $taskType = $this->TaskTypes->patchEntity($taskType, $this->request->getData());
            if ($this->TaskTypes->save($taskType)) {
                $this->Flash->success(__('The task type has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The task type could not be saved. Please, try again.'));
        }
        $this->set(compact('taskType'));
        $this->set('_serialize', ['taskType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Task Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $taskType = $this->TaskTypes->get($id);
        if ($this->TaskTypes->delete($taskType)) {
            $this->Flash->success(__('The task type has been deleted.'));
        } else {
            $this->Flash->error(__('The task type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
    }
}
