<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Milestone;
use App\Model\Entity\Item;
use App\Model\Entity\Invoice;
use App\Model\Entity\MilestonesUser;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use PHPExcel;
use PHPExcel_IOFactory;
use Cake\I18n\I18n;
use Cake\I18n\Date;
use App\Model\Entity\Project;
use Cake\Utility\Security;
use Cake\Event\Event;
use Firebase\JWT\JWT;

/**
 * Projects Controller
 *
 * @property \App\Model\Table\ProjectsTable $Projects
 *
 * @method \App\Model\Entity\Project[] paginate($object = null, array $settings = [])
 */
class ProjectsController extends AppController
{

  public function beforeFilter(Event $event){
      $this->Auth->allow(['getProjectsUserApp', 'getProject']);

      return parent::beforeFilter($event);
  }

  public function isAuthorized($user){
    $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                        return $q->select()->where(['module' => 'Projects']);
                                                                                                    }])->count();
    if($permissions != 0){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete', 'view', 'performance', 'profiability', 'getTasksByMilestone', 'getCostByUser', 'getProject', 'getProjectsByCompany', 'getProjectsInformation', 'getProjectStatusInformation', 'getPercentagesCompletionMilestones', 'getHoursByProject', 'getDueDate', 'getCostByMilestone', 'sendEmails', 'report', 'validateDatesMilestone', 'finishProject', 'reopenProject', 'validateStatesMilestonesTasks'])){
          return true;
      }
    }

  return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $projects = $this->Projects->find()->contain(['Companies'])->order(['created_at' => 'DESC']);

        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => ['Companies', 'Invoices', 'Tasks']
        ]);
        $tableUsers = TableRegistry::get('Users');
        $users = $tableUsers->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $this->set('project', $project);
        $this->set('users', $users);
        $this->set('_serialize', ['project']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $project = $this->Projects->newEntity();
        if ($this->request->is('post')) {
            $project = $this->Projects->patchEntity($project, $this->request->getData());
            $company_id = $this->Projects->Companies->find()->select(['Companies.id', 'Companies.national_invoice'])->where(['Companies.name LIKE' => $project->company_name])->first();
            $project->company_id = $company_id->id;
            $project->created_at = Time::now();
            if(isset($project['company_id'])){
            if ($this->Projects->save($project)) {
                if(!empty($project->milestonesArray)){
                  $contDate = 0;
                  for ($i=0; $i <count($project->milestonesArray['name']); $i++) {
                    $startDate = $project->milestonesArray['start_date'][$i];
                    $endDate = $project->milestonesArray['end_date'][$i];

                    $milestone = new Milestone(['name' => $project->milestonesArray['name'][$i], 'start_date' => $startDate, 'end_date' => $endDate, 'project_id' => $project->id]);
                    $tableMilestones = TableRegistry::get('Milestones');
                    if ($this->validateDatesMilestone($milestone, $project)) {
                      if($tableMilestones->save($milestone)){

                          $tableMilestonesUsers = TableRegistry::get('MilestonesUsers');
                          $usersID = explode(',', $project->milestonesArray['user'][$i]);
                          $message = '<h2>¡Hola!</h2>Se han creado los siguientes milestones pertenecientes al proyecto '.$project->name.' los cuales se te han sido asignado :';
                          $users = array();
                          for ($j=0; $j < count($usersID)-1; $j++) {
                            $tableUsers = TableRegistry::get('Users');
                            $user = $tableUsers->get($usersID[$j]);
                            $milestoneUser = new MilestonesUser(['milestone_id' => $milestone->id, 'user_id' => $usersID[$j]]);
                            if(!$tableMilestonesUsers->save($milestoneUser)){
                                $this->Flash->error(__('Hubo errores en el guardado de datos del milestone {0} Por favor intente ingresarlo nuevamente desde la sección de milestones.', $milestone->name));
                            }
                            else{
                              $this->Flash->success(__('¡El milestone {0} ha sido salvado exitosamente!', $milestone->name));
                              if (!isset($users[$user->email])) {
                                $users[$user->email] = "<li>".$milestone->name." - Fecha de inicio: ".$milestone->start_date." - Fecha de fin: ".$milestone->end_date."</li>";
                              }else {
                                $users[$user->email] .= "<li>".$milestone->name." - Fecha de inicio: ".$milestone->start_date." - Fecha de fin: ".$milestone->end_date."</li>";
                              }
                            }
                          }
                          $this->sendEmails('Nuevo Milestone', $message, $users);
                      }
                      else{
                        $this->Flash->error(__('Hubo errores en el guardado de datos del milestone {0} Por favor intente ingresarlo nuevamente desde la sección de milestones.', $milestone->name));
                      }
                    }
                    else{

                      $this->Flash->error(__('Hay conflictos entre las fechas del milestone {0} y del proyecto. Por favor intente ingresarlo nuevamente desde la sección de milestones.', $milestone->name));
                    }
                    $contDate = $contDate+3;
                  }
                }
                if($project->invoiced==1){
                  $invoice = new Invoice(['date' => date("Y-m-d"), 'state' => 'P', 'project_id' => $project->id]);
                  $invoice->due_date = $this->getDueDate($invoice);
                  $invoice->company_id = $project->company_id;
                  $tableInvoice = TableRegistry::get('Invoices');
                  $tableItems = TableRegistry::get('Items');
                  $itemsArray = (array) json_decode($project->itemsInvoices);
                  $totalAmount = (array) $itemsArray[count($itemsArray)-1];
                  $invoice->total_amount = $totalAmount['value'];
                  $invoice->outstanding_amount = $invoice->total_amount;
                  unset($itemsArray[count($itemsArray)-1]);
                  $purchaseOrder = (array) $itemsArray[count($itemsArray)-1];
                  $invoice->purchase_order = $purchaseOrder['value'];
                  unset($itemsArray[count($itemsArray)-1]);

                  //Add the code of the invoices
                  $tableIndex = TableRegistry::get('InvoicesIndex');
                  $invoiceIndex = '';

                  if ($company_id->national_invoice) {
                    $invoiceIndex = $tableIndex->find()->where(['type' => 'N'])->first();
                    $invoice->code_invoice = 'CRC' . $invoiceIndex->value;
                  }
                  else {
                    $invoiceIndex = $tableIndex->find()->where(['type' => 'I'])->first();
                    $invoice->code_invoice = 'INT' . $invoiceIndex->value;
                  }

                  if($tableInvoice->save($invoice)){

                    $cont = 0;
                    for ($i=0; $i < count($itemsArray)/3; $i++) {
                      $description = (array) $itemsArray[$cont];
                      $price = (array) $itemsArray[$cont+1];
                      $quantity = (array) $itemsArray[$cont+2];

                      $subtotal = (int) $price['value']* (int) $quantity['value'];

                      $item = new Item(['description' => $description['value'], 'price' => $price['value'], 'quantity' => $quantity['value'], 'subtotal' => $subtotal, 'invoice_id' => $invoice->id]);

                      $tableItems->save($item);
                      $cont = $cont+3;
                    }

                    if (isset($invoiceIndex) && $invoiceIndex != '') {
                      $invoiceIndex->value = $invoiceIndex->value + 1;
                      $tableIndex->save($invoiceIndex);
                    }
                  }
                  $this->Flash->success(__('The project and invoice have been saved'));

                  return $this->redirect(['controller' =>'Invoices', 'action' => 'index']);
                }
                $this->Flash->success(__('¡El proyecto ha sido salvado exitosamente!'));

                return $this->redirect(['action' => 'view', $project->id]);
            }
            $this->Flash->error(__('El proyecto no pudo ser guardado. ¡Por favor intente de nuevo!'));
          }
          else {
            $this->Flash->error(__('El proyecto necesita una compañía asociada.'));
          }
        }
        $users = TableRegistry::get('Users')->find()->select(['id', 'username']);
        $vat = TableRegistry::get('Tax')->find('list', ['limit' => 200, 'keyField' => 'tax_value', 'valueField' => 'tax_value']);
        $companies = $this->Projects->Companies->find("list", array("fields"=>array("Companies.name")))->select(['Companies.name'])->autofields(false);
        $this->set(compact('project', 'companies', 'users', 'vat'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $project = $this->Projects->get($id, [
            'contain' => []
        ]);
        $company_name = $this->Projects->Companies->get($project->company_id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Projects->patchEntity($project, $this->request->getData());
            $company_id = $this->Projects->Companies->find()->select(['Companies.id'])->where(['Companies.name LIKE' => $project->company_name])->first();
            $project->company_id = $company_id->id;
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('¡El proyecto ha sido salvado exitosamente!'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El proyecto no pudo ser guardado. ¡Por favor intente de nuevo!'));
        }
        $companies = $this->Projects->Companies->find("list", array("fields"=>array("Companies.name")))->select(['Companies.name'])->autofields(false);
        $this->set(compact('project', 'companies', 'company_name'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Projects->get($id);
        if ($this->Projects->delete($project)) {
            $this->Flash->success(__('El proyecto ha sido eliminado'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el proyecto. Inténtelo nuevamente'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function performance(){
      if($this->request->is('post')){
        $projectName = $this->request->getData()['name'];
        $users;
        $cont = 0;
        $taskTable = TableRegistry::get('Tasks');
        $taskUsers;
        $performanceInformation;
        $tasks = TableRegistry::get('Tasks')->find()->contain(['Projects'=> function(\Cake\ORM\Query $q) use($projectName){
                                                                  return $q->select(['Projects.id'])
                                                                            ->where(['Projects.name LIKE' => $projectName]);
                                                              }, 'Users']);

        $performanceInfo = array();

        foreach ($tasks as $task) {
          isset($performanceInfo[$task->user->id]) ? $performanceInfo[$task->user->id] : $performanceInfo[$task->user->id] = '';
          isset($performanceInfo[$task->user->id]['user_info']) ? $performanceInfo[$task->user->id]['user_info'] = $task->user : $performanceInfo[$task->user->id]['user_info'] = $task->user;
          isset($performanceInfo[$task->user->id]['total_tasks']) ? $performanceInfo[$task->user->id]['total_tasks']++ : $performanceInfo[$task->user->id]['total_tasks'] = 1;
          isset($performanceInfo[$task->user->id]['pending_tasks']) ? $performanceInfo[$task->user->id]['pending_tasks'] : $performanceInfo[$task->user->id]['pending_tasks'] = 0;
          isset($performanceInfo[$task->user->id]['in_progress']) ? $performanceInfo[$task->user->id]['in_progress'] : $performanceInfo[$task->user->id]['in_progress'] = 0;
          isset($performanceInfo[$task->user->id]['pending_approve']) ? $performanceInfo[$task->user->id]['pending_approve'] : $performanceInfo[$task->user->id]['pending_approve'] = 0;
          isset($performanceInfo[$task->user->id]['finish_before']) ? $performanceInfo[$task->user->id]['finish_before'] : $performanceInfo[$task->user->id]['finish_before'] = 0;
          isset($performanceInfo[$task->user->id]['finish_after']) ? $performanceInfo[$task->user->id]['finish_after'] : $performanceInfo[$task->user->id]['finish_after'] = 0;
          isset($performanceInfo[$task->user->id]['complete_tasks']) ? $performanceInfo[$task->user->id]['complete_tasks'] : $performanceInfo[$task->user->id]['complete_tasks'] = 0;
          isset($performanceInfo[$task->user->id]['for_today']) ? $performanceInfo[$task->user->id]['for_today'] : $performanceInfo[$task->user->id]['for_today'] = 0;
          isset($performanceInfo[$task->user->id]['delay_task']) ? $performanceInfo[$task->user->id]['delay_task'] : $performanceInfo[$task->user->id]['delay_task'] = 0;
          switch ($task->state) {
            case 'P':
              $performanceInfo[$task->user->id]['pending_tasks']++;
              break;
            case 'IP':
              $performanceInfo[$task->user->id]['in_progress']++;
              break;
            case 'PA':
              $performanceInfo[$task->user->id]['pending_approve']++;
              break;
            case 'FB':
              $performanceInfo[$task->user->id]['complete_tasks']++;
              $performanceInfo[$task->user->id]['finish_before']++;
              break;
            case 'FA':
              $performanceInfo[$task->user->id]['complete_tasks']++;
              $performanceInfo[$task->user->id]['finish_after']++;
              break;
          }

          $today = Time::now();
          if ($today == new FrozenTime($task->end_date)) {
            $performanceInfo[$task->user->id]['for_today']++;
          }

          if ($today > new FrozenTime($task->end_date)) {
            $performanceInfo[$task->user->id]['delay_task']++;
          }
        }
         $this->set(compact('performanceInfo'));
         $this->set('_serialize', ['performanceInfo']);
      }
      $projects = $this->Projects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
      $this->set(compact('projects', 'performanceInfo'));
      $this->set('_serialize', ['projects', 'performanceInfo']);
    }

    public function profiability(){
      $project;
      if($this->request->is('ajax')){
        $projectName = $this->request->getData()['name'];
        $project = $this->Projects->find()->where(['Projects.name LIKE' => $projectName])->first();
        $project->profiability = $this->Projects->getProfiabilityProject($project->id);
        $project->earning = ($project->budget-$project->total_cost);
        $tasks = TableRegistry::get('Tasks')->find()->select(['Tasks.duration', 'Tasks.name', 'Tasks.id'])
                                                    ->autofields(false)
                                                    ->where(['project_id' => $project->id])
                                                    ->contain(['Users' => function(\Cake\ORM\Query $q){
                                                      return $q->select(['Users.hour_cost', 'Users.id', 'Users.username'])
                                                                ->autofields(false);
                                                    }]);
        $tasksArray = array();
        $costMilestones = array();
        $usersNames = array();
        $users = array();
        $costs = array();

        foreach ($tasks as $task) {
          array_push($tasksArray, $task->name);
          array_push($users, $task->user);
          array_push($costs, $this->getCostByTask($task));
          array_push($usersNames, $task->user->username);
        }

        $costUsers = array();
        foreach (array_unique($users) as $user) {
          array_push($costUsers, $this->getCostByUser($user->id, $project->id));
        }

        $project->dataCosts = array('tasks' => $tasksArray, 'costs' => $costs);
        $project->costUsers = ['costs' => $costUsers, 'users' => array_values(array_unique($usersNames))];
        $this->set(compact('project'));
      }
      $projects = $this->Projects->find("list", array("fields"=>array("Projects.name")))->select(['Projects.name'])->autofields(false);
      $this->set(compact('projects'));
      $this->set('_serialize', ['projects', 'project']);
    }

    private function getTasksByMilestone($milestoneID){
      $tasks = TableRegistry::get('Tasks')->find()->where(['Tasks.milestone_id' => $milestoneID])->contain(['Users']);
      return $tasks->toArray();
    }

    private function getCostByUser($userID, $projectID){
      $totalHours = 0;
      $totalMinutes = 0;
      $timesheets = TableRegistry::get('Timesheets')->find()->select(['Timesheets.duration'])
                                                            ->autofields(false)
                                                            ->contain(['Tasks' => function(\Cake\ORM\Query $q) use($userID, $projectID){
                                                                return $q->select(['Tasks.id'])
                                                                        ->where(['Tasks.user_id' => $userID, 'Tasks.project_id']);
                                                            }]);

     $user = TableRegistry::get('Users')->get($userID);
     $totalCost = 0;
     foreach ($timesheets as $timesheet) {
       $totalHours += $timesheet->duration->format('H');
       $totalMinutes += $timesheet->duration->format('i');

       if($totalMinutes == 60){
        $totalMinutes = 0;
        $totalHours++;
       }
     }

     if ($totalMinutes <= 59) {
       $subtotal = $user->hour_cost/60;
       $costMinutes = $totalMinutes * $subtotal;
     }

     $totalCost = $totalHours * $user->hour_cost;
     if ($costMinutes > 0) {
       $totalCost += $costMinutes;
     }
     // foreach ($timesheets as $timesheet) {
     //   $start = strtotime($timesheet->start_date != null ? $timesheet->start_date->format('Y-m-d H:i'): $timesheet->start_date);
     //   $end = strtotime($timesheet->end_date != null ? $timesheet->end_date->format('Y-m-d H:i'):$timesheet->end_date);
     //   $days = ($start-$end)/86400;
     //   if (($days < 0 && $days > -1) || ($days > 0 && $days < 1)) { //Verify if the difference is hours or minutes
     //     if ($timesheet->start_date->day == $timesheet->end_date->day) {
     //       $hours = abs(($start-$end)/3600);
     //     }
     //     else {
     //       $difference = 0;
     //       $differenceEndDate = 0;
     //       if (($timesheet->start_date->hour < 17) || ($timesheet->start_date->hour ==17 && $timesheet->start_date->minute == 0)) {
     //         $difference = abs((strtotime($timesheet->start_date->format('Y-m-d H:i'))-strtotime(date($timesheet->start_date->format('Y-m-d').' 17:00')))/3600);
     //       }
     //       if($timesheet->end_date->hour > 8 || ($timesheet->end_date->hour == 8 && $timesheet->end_date->minute > 0)){
     //         $differenceEndDate = abs((strtotime($timesheet->end_date->format('Y-m-d H:i'))-strtotime(date($timesheet->end_date->format('Y-m-d').' 8:00')))/3600);
     //       }
     //       $hours = abs($difference+$differenceEndDate);
     //     }
     //   }
     //   else{ //The difference is in days
     //     $hours = $days*8;
     //   }
     //   $totalCost += $hours*$user->hour_cost;
     // }

     return number_format($totalCost, 2);
    }

    public function getProject(){
      if($this->request->is('ajax')){
        $projectID = $this->request->data['project_id'];

        $project = $this->Projects->get($projectID);

        $this->set('project', $project);
        $this->set('_serialize', ['project']);
      }
    }

    public function getProjectsByCompany(){
      if ($this->request->is('ajax')) {
        $company = $this->request->data['company'];
        $company = '%'.$company.'%';

        $projects = $this->Projects->find()->select(['Projects.id', 'Projects.name'])->contain(['Companies' => function(\Cake\ORM\Query $q) use($company){
                                                                                  return $q->select(['Companies.id'])
                                                                                          ->where(['Companies.name LIKE' => $company]);
                                                                              }]);

        $this->set('projects', $projects);
        $this->set('_serialize', ['projects']);
      }
    }

    public function getProjectsInformation(){
      if ($this->request->is('ajax')) {
        $startDate = $this->request->data['start_date'];
        $endDate = $this->request->data['end_date'];
        $projects = $this->Projects->find()->where(['Projects.start_date >=' => $startDate, 'Projects.end_date <=' => $endDate]);
        $start = new FrozenTime(date('Y-m-d'));
        $end = $start->modify('-5 months');
        $projectLastSemester = $this->Projects->find()->where(['Projects.start_date >=' => $end, 'Projects.end_date <=' => $start]);
        $projectHours = array();
        $pendingProjects = 0;
        $closedProjects = 0;
        $delayedProjects = 0;

        foreach ($projects as $project) {
          if ($project->state == 'O') {
            $pendingProjects++;
          }
          else {
            $closedProjects++;
          }
          $projectEndDate = $project->end_date != null ? $project->end_date->format('Y-m-d') : $project->end_date;
          if (($projectEndDate <= Time::now()->format('Y-m-d')) && $project->state == 'O') {
            $delayedProjects++;
          }
          $projectsHours[$project->name] = $this->getHoursByProject($project->id);
        }

        $months = array();
        $monthsName = array();
        for ($i=0; $i < 6; $i++) {
          $month = $end->modify('+'.$i.' months');
          array_push($months, $month->month);
          array_push($monthsName, $month->format('F'));
        }
        $closedProjectsByMonth = [0,0,0,0,0,0];
        $openProjectsByMonth = [0,0,0,0,0,0];

        foreach ($projectLastSemester as $project) {
          for ($i=0; $i < count($months); $i++) {
            $monthEnd = ($project->final_end_date != null) ? $project->final_end_date->month:0;
            $monthStart = ($project->start_date != null) ? $project->start_date->month:0;
            if($monthEnd == $months[$i] && $project->state == 'C'){
              $closedProjectsByMonth[($i)] = $closedProjectsByMonth[($i)]+1;
            }
            if($monthStart == $months[$i]){
              $openProjectsByMonth[($i)] = $openProjectsByMonth[($i)]+1;
            }
          }
        }

        $projectsInformation = [$pendingProjects, $closedProjects, $delayedProjects];
        $this->set(compact('projectsInformation', 'monthsName', 'closedProjectsByMonth', 'openProjectsByMonth', 'projectsHours'));
        $this->set('_serialize', ['projectsInformation', 'monthsName', 'closedProjectsByMonth', 'openProjectsByMonth', 'projectsHours']);
      }
    }

    public function getProjectStatusInformation(){
      if($this->request->is('ajax')){
        $requestData = $this->request->data;
        $projectID = $requestData['project_id'];
        $projectEndDate = $requestData['end_date'];

        $taskTable = TableRegistry::get('Tasks');

        $info = $this->getInfoCompletionTasks($taskTable->find()->where(['project_id' => $projectID]));

        $start = strtotime(date('m/d/Y'));
        $end = strtotime($projectEndDate);
        $difference = $start-$end;
        $remainingDays = abs($difference/86400);

        $profiability = $this->Projects->getProfiabilityProject($projectID);

        $projectInformation = array('percentageCompletion' => number_format($info['percentage'], 2),
                                    'remainingDays' => $remainingDays,
                                    'pendingTasks' => $info['pending_tasks'],
                                    'completeTasks' => $info['complete_tasks'],
                                    'pendingApprove' => $info['pending_approve'],
                                    'inProgress' => $info['in_progress'],
                                    'totalTasks' => $info['total_tasks'],
                                    'profiability' => number_format($profiability,2));

        $this->set(compact('projectInformation'));
        $this->set('_serialize', ['projectInformation']);

      }
    }

    /*
    *Get information about percentage of completion of the milestones and
    *information about his tasks
    */
    private function getInfoCompletionTasks($tasks){
      $tasksTable = TableRegistry::get('Tasks');
      $totalTasks = $tasks->count();
      $completeTasks = 0;
      $pendingTasks = 0;
      $pendingApproveTasks = 0;
      $inProgressTasks = 0;
      $finishTasks = 0;

      foreach ($tasks as $task) {
        switch ($task->state) {
          case 'P':
            $pendingTasks++;
            break;
          case 'IP':
            $inProgressTasks++;
            break;
          case 'PA':
            $pendingApproveTasks++;
            break;
          case 'FB':
            $completeTasks++;
            break;
          case 'FA':
            $completeTasks++;
            break;
        }
      }
      $percentage = ($totalTasks == 0) ? 0 :($completeTasks/$totalTasks)*100;
      $info = ['percentage' => $percentage, 'total_tasks' => $totalTasks, 'complete_tasks' => $completeTasks, 'pending_tasks' => $pendingTasks, 'pending_approve' => $pendingApproveTasks, 'in_progress' => $inProgressTasks];

      return $info;
    }

    private function getHoursByProject($projectID){
      $timesheets = TableRegistry::get('Timesheets')->find()
                                                    ->select(['Timesheets.duration'])
                                                    ->autofields(false)
                                                    ->contain(['Tasks' => function(\Cake\ORM\Query $q) use($projectID){
                                                        return $q->select('Tasks.id')->contain(['Milestones' => function(\Cake\ORM\Query $q) use($projectID){
                                                            return $q->select('Milestones.id')->autofields(false)->where(['Milestones.project_id' => $projectID]);                                                        }]);
                                                    }]);
      $totalHours = new Time(date('Y-m-d').' 00:00');
      foreach ($timesheets as $timesheet) {
        $totalHours = $totalHours->modify('+'.$timesheet->duration->hour.' hours '.$timesheet->duration->minute.' minutes');
      }

      return ($totalHours->hour.'.'.$totalHours->minute);
    }

    private function getDueDate($invoice){
      $project = $this->Projects->get($invoice->project_id);
      $customer = TableRegistry::get('Companies')->get($project->company_id);
      $payment_terms = TableRegistry::get('PaymentTerms')->get($customer->payment_terms);
      return date('Y-m-d', strtotime('+'.$payment_terms->name.' days', strtotime($invoice->date)));
    }

    private function getCostByTask($task){
      $cost = 0;
      $totalHours = 0;
      $totalMinutes = 0;
      $timesheets = TableRegistry::get('Timesheets')->find()->where(['task_id' => $task->id]);

      foreach ($timesheets as $timesheet) {
        $totalHours += $timesheet->duration->format('H');
        $totalMinutes += $timesheet->duration->format('i');

        if($totalMinutes == 60){
         $totalMinutes = 0;
         $totalHours++;
        }
      }

      if ($totalMinutes <= 59) {
        $subtotal = $task->user->hour_cost/60;
        $costMinutes = $totalMinutes * $subtotal;
      }

      $cost = $totalHours * $task->user->hour_cost;
      if ($costMinutes > 0) {
        $cost += $costMinutes;
      }

      return number_format($cost, 2);
    }

    private function sendEmails($subject, $message, $user){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');

      if(is_array($user)){
        $to = new \SendGrid\Email('WDPM', 'wdpminfo@gmail.com');
        $content = new \SendGrid\Content("text/plain", 'hola');
        $mail = new \SendGrid\Mail($from, $subject, $to, $content);

        foreach ($user as $key => $value) {
          $personalization = new \SendGrid\Personalization();
          $email = new \SendGrid\Email(null, $key);
          $personalization->addTo($email);
          $personalization->addSubstitution('%html_content%', $value);
          $personalization->addSubstitution('%text_content%', $message);
          $personalization->addSubstitution('%button_content%', 'localhost/wdpm/tasks');
          $mail->addPersonalization($personalization);
        }
        $content = new \SendGrid\Content("text/html", $template);
        $mail->addContent($content);
      }
      else {
        $to = new \SendGrid\Email(null, $user);
        $content = new \SendGrid\Content("text/html", $template);
        $mail = new \SendGrid\Mail($from, $subject, $to, $content);
        $mail->personalization[0]->addSubstitution('%html_content%', $message);
        $mail->personalization[0]->addSubstitution('%text_content%', '<h2>¡Hola!</h2>');
        $mail->personalization[0]->addSubstitution('%button_content%', 'localhost/wdpm/tasks/');
      }
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }


    public function report(){
      $projects = $this->Projects->find();
      $organizationTable = TableRegistry::get('Organizations');
      $companyTable = TableRegistry::get('Companies');
      $organization = $organizationTable->get($this->Auth->user('organization_id'));
      $excelFile = new PHPExcel();
      $excelFile->getProperties()->setCreator("WDPM")->setLastModifiedBy("WDPM")->setTitle("ProjectsReport")
                         ->setSubject("Projects")->setDescription("Document report")->setKeywords("office")
                         ->setCategory("Report");
      $excelFile->setActiveSheetIndex(0)->setCellValue('A1', 'Reporte de proyectos')->setCellValue('A3', 'Fecha:')
                                        ->setCellValue('B3', Time::now()->format('d-m-Y H:i'))->setCellValue('A4', 'Compañía')
                                        ->setCellValue('B4', $organization->name);

      $excelFile->setActiveSheetIndex(0)->setCellValue('A6', 'Nombre')->setCellValue('B6', 'Fecha de inicio')
                                        ->setCellValue('C6', 'Fecha de fin')->setCellValue('D6', 'Presupuesto')
                                        ->setCellValue('E6', 'Compañía')->setCellValue('F6', 'Costo Total');

      $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      foreach ($projects as $project) {
        $company = $companyTable->get($project->company_id);
        $excelFile->setActiveSheetIndex(0)->setCellValue('A'.($lastRow+1), $project->name)->setCellValue('B'.($lastRow+1), ($project->start_date != null) ? $project->start_date->format('d-m-Y'): '')
                                          ->setCellValue('C'.($lastRow+1), ($project->end_date != null) ? $project->end_date->format('d-m-Y'): '')->setCellValue('D'.($lastRow+1), $project->budget)
                                          ->setCellValue('E'.($lastRow+1), $company->name)->setCellValue('F'.($lastRow+1), $project->total_cost);
        $lastRow = $excelFile->getActiveSheet()->getHighestRow();
      }
      $excelFile->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A3')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A4')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('A6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('B6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('C6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('D6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('E6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getStyle('F6')->getFont()->setBold(true)->setSize(12);
      $excelFile->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
      $excelFile->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

      $excelFile->getActiveSheet()->setTitle('Reporte de proyectos');
      $excelFile->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser (Excel5)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="projects_report.xls"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0

      $writer = \PHPExcel_IOFactory::createWriter($excelFile, 'Excel5');

      ob_end_clean();
      $writer->save('php://output');
    }

    public function validateDatesMilestone($milestone, $project){
      if (new FrozenTime($milestone->start_date) < new FrozenTime($milestone->end_date)) {
        if(new FrozenTime($milestone->start_date) >= $project->start_date && new FrozenTime($milestone->start_date) < $project->end_date && new FrozenTime($milestone->end_date) <= $project->end_date){
          return true;
        }
      }

      return false;
    }

    public function finishProject($id = null){
      $this->request->allowMethod(['post']);
      $project = $this->Projects->get($id, ['contain' => ['Companies']]);

      if ($project->state != 'C') {
        if ($this->validateStatesMilestonesTasks($project->id)) {
          $project->state = 'C';
          $project->final_end_date = new FrozenTime(date('Y-m-d'));
          if($this->Projects->save($project)){
            $this->Flash->success(__('El proyecto ha sido marcado como finalizado.'));
          }
        }
        else {
          $this->Flash->error(__('El proyecto tiene milestones y/o tareas abiertas.'));
        }
      }
      else{
        $this->Flash->error(__('El proyecto ya se encuentra finalizado.'));
      }

      return $this->redirect(['action' => 'index']);
    }

    public function reopenProject($id = null){
      $this->request->allowMethod(['post']);
      $project = $this->Projects->get($id, ['contain' => ['Companies']]);

      if ($project->state == 'C') {
        $project->state = 'O';
        if($this->Projects->save($project)){
          $this->Flash->success(__('El proyecto ha sido reabierto.'));
        }
      }
      else{
        $this->Flash->error(__('El proyecto ya se encuentra abierto.'));
      }

      return $this->redirect(['action' => 'index']);
    }

    private function validateStatesMilestonesTasks($projectID){
      $milestones = TableRegistry::get('Milestones')->find()
                                                    ->select(['Milestones.state', 'Milestones.id'])
                                                    ->autofields(false)
                                                    ->where(['Milestones.project_id' => $projectID]);
      $tableTasks = TableRegistry::get('Tasks');
      $allClosed = true;

      if ($milestones->count() != 0) {
        foreach ($milestones as $milestone) {
          if ($milestone->state != 'O') {
            $tasks = $tableTasks->find()
                                ->select(['Tasks.milestone_id', 'Tasks.state'])
                                ->autofields(false)
                                ->where(['Tasks.milestone_id' => $milestone->id]);

           if ($tasks->count() != 0) {
             foreach ($tasks as $task) {
               if ($task->state == 'TD' || $task->state == 'TA') {
                 $allClosed = true;
               }
               else {
                 return false;
               }
             }
           }
           else {
             $allClosed = true;
           }
          }
          else {
            return false;
          }
        }
      }

      return $allClosed;
    }

    public function importProjects(){
      $file = fopen(WWW_ROOT."files/Projects.csv", "r");
      $first = true;
      $companies = TableRegistry::get('Companies');
      $milestonesTable = TableRegistry::get('Milestones');
      $milestonesUsersTable = TableRegistry::get('MilestoneUsers');
      $numberProjects = 0;
      while (($line = fgetcsv($file, 1000, ";")) !== FALSE) {
        if ($first) {
          $first=false;
          continue;
        }
        $company = $companies->find()->where(['name LIKE' => utf8_decode($line[8])])->first();
        $company = $company != null ? $company->id : null;
        $project = new Project();
        $project->name = utf8_decode($line[0]);
        $project->start_date = $line[11];
        $project->budget = $line[1];
        $project->invoiced = $line[4] != 0 ? 1 : 0;
        $project->company_id = $company;
        $project->total_cost = $line[2];
        $project->end_date = $line[7];
        $project->state = $line[9] == 'Done' ? 'C' : 'O';
        $project->estimated_cost = $line[1];
        if ($this->Projects->save($project)) {
          $numberProjects++;
        }
      }
      debug("Proyectos agregados...".$numberProjects);
      die();
    }

    public function getProjectsUserApp(){
      $this->autoRender = false;
      header('Access-Control-Allow-Origin: *');

      if ($this->request->is('post')) {
        $user_id = $this->request->data['user_id'];
        $jwt = $this->request->data['jwt'];

        $key = '16cf8e473001a6ceff36ab1a24134a76';
        $data = JWT::decode($jwt, $key, array('HS256'));

        $user = TableRegistry::get('Users')->get($user_id);
        $jwtToken = $data->data->jwtToken;
        $decodeJwtToken = base64_decode($jwtToken);
        $explodeToken = explode('.', $decodeJwtToken);

        if ($explodeToken[0] == $user->id && $explodeToken[1] == $user->username) {

          if ($user->role == 'A') {
            $projects = $this->Projects->find();
            $resultJ = json_encode(array('projects' => $projects));
            $this->response->type('json');
            $this->response->body($resultJ);
            return $this->response;
          }
          else {
            $subQuery = TableRegistry::get('Tasks')->find('list', ['valueField' => 'project_id'])->select(['Tasks.project_id'])->autofields(false)->where(['user_id' => $user_id])->hydrate(false);
            $subQuery = array_values($subQuery->toArray());
            if (count($subQuery) != 0) {
              $projects = $this->Projects->find()->where(['Projects.id IN' => $subQuery]);
              $resultJ = json_encode(array('projects' => $projects));
              $this->response->type('json');
              $this->response->body($resultJ);
              return $this->response;
            }
          }
        }
      }
    }
}
