<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MilestonesUsers Controller
 *
 * @property \App\Model\Table\MilestonesUsersTable $MilestonesUsers
 *
 * @method \App\Model\Entity\MilestonesUser[] paginate($object = null, array $settings = [])
 */
class MilestonesUsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Milestones', 'Users']
        ];
        $milestonesUsers = $this->paginate($this->MilestonesUsers);

        $this->set(compact('milestonesUsers'));
        $this->set('_serialize', ['milestonesUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Milestones User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $milestonesUser = $this->MilestonesUsers->get($id, [
            'contain' => ['Milestones', 'Users']
        ]);

        $this->set('milestonesUser', $milestonesUser);
        $this->set('_serialize', ['milestonesUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $milestonesUser = $this->MilestonesUsers->newEntity();
        if ($this->request->is('post')) {
            $milestonesUser = $this->MilestonesUsers->patchEntity($milestonesUser, $this->request->getData());
            if ($this->MilestonesUsers->save($milestonesUser)) {
                $this->Flash->success(__('The milestones user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The milestones user could not be saved. Please, try again.'));
        }
        $milestones = $this->MilestonesUsers->Milestones->find('list', ['limit' => 200]);
        $users = $this->MilestonesUsers->Users->find('list', ['limit' => 200]);
        $this->set(compact('milestonesUser', 'milestones', 'users'));
        $this->set('_serialize', ['milestonesUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Milestones User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $milestonesUser = $this->MilestonesUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $milestonesUser = $this->MilestonesUsers->patchEntity($milestonesUser, $this->request->getData());
            if ($this->MilestonesUsers->save($milestonesUser)) {
                $this->Flash->success(__('The milestones user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The milestones user could not be saved. Please, try again.'));
        }
        $milestones = $this->MilestonesUsers->Milestones->find('list', ['limit' => 200]);
        $users = $this->MilestonesUsers->Users->find('list', ['limit' => 200]);
        $this->set(compact('milestonesUser', 'milestones', 'users'));
        $this->set('_serialize', ['milestonesUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Milestones User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $milestonesUser = $this->MilestonesUsers->get($id);
        if ($this->MilestonesUsers->delete($milestonesUser)) {
            $this->Flash->success(__('The milestones user has been deleted.'));
        } else {
            $this->Flash->error(__('The milestones user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
