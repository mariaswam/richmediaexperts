<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Contact;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 *
 * @method \App\Model\Entity\Contact[] paginate($object = null, array $settings = [])
 */
class ContactsController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'CRM']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $contacts =$this->Contacts->find()->contain(['Companies', 'Roles']);

        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);
    }

    /**
     * View method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => ['Companies', 'Roles']
        ]);

        $this->set('contact', $contact);
        $this->set('_serialize', ['contact']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            $company_id = $this->Contacts->Companies->find()->select(['Companies.id'])->where(['Companies.name LIKE' => $contact->company_name])->first();
            $contact->company_id = $company_id->id;
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('El contacto ha sido salvado exitosamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El contacto no se pudo salvar. Intente nuevamente'));
        }
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'code'])->order(['name' => 'ASC']);
        $companies = $this->Contacts->Companies->find("list", array("fields"=>array("Companies.name")))->select('name')->autofields(false);
        $roles = $this->Contacts->Roles->find('list', ['limit' => 200]);
        $this->set(compact('contact', 'companies', 'roles', 'countries'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => []
        ]);
        $company_name = $this->Contacts->Companies->get($contact->company_id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            $company_id = $this->Contacts->Companies->find()->select(['Companies.id'])->where(['Companies.name LIKE' => $contact->company_name])->first();
            $contact->company_id = $company_id->id;
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('El contacto ha sido salvado exitosamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El contacto no se pudo salvar. Intente nuevamente'));
        }
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'code'])->order(['name' => 'ASC']);
        $companies = $this->Contacts->Companies->find("list", array("fields"=>array("Companies.name")))->select('name')->autofields(false);
        $roles = $this->Contacts->Roles->find('list', ['limit' => 200]);
        $this->set(compact('contact', 'companies', 'roles', 'countries', 'company_name'));
        $this->set('_serialize', ['contact']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contact = $this->Contacts->get($id);
        if ($this->Contacts->delete($contact)) {
            $this->Flash->success(__('El contacto ha sido eliminado correctamente'));
        } else {
            $this->Flash->error(__('El contacto no se ha podido eliminar. Intente nuevamente'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function importContacts(){
      $file = fopen(WWW_ROOT."files/Contacts.csv", "r");
      $first = true;
      $countryTable = TableRegistry::get('Countries');
      $companies = TableRegistry::get('Companies');
      $roleTable = TableRegistry::get('Roles');
      $numberContacts = 0;
      while (($line = fgetcsv($file, 1000, ";")) !== FALSE) {
        if ($first) {
          $first=false;
          continue;
        }
        $country = $countryTable->find()->where(['code' => $line[3]])->first();
        $address = $line[2] != '' ? utf8_decode($line[1]) : "no-address";
        $city = $line[2] != '' ? utf8_decode($line[2]) : "no-city";
        $role = $roleTable->find()->where(['name LIKE' => $line[11]])->first();
        $role = $role != null ? $role->id : null;
        $company = $companies->find()->where(['name LIKE' => $line[10]])->first();
        $company = $company != null ? $company->id : null;
        $mobile = $line[9] != '' ? $line[9] : 'no-number';
        $contact = new Contact(['first_name' => $line[0], 'last_name' => $line[1], 'email' => $line[7], 'mobile' => $mobile, 'phone' => $line[8], 'gender' => $line[5], 'date_of_birth' => $line[4], 'country' => $country->name, 'address' => ($address.", ".$city), 'language' => $line[6], 'company_id' => $company, 'role_id' => $role]);
        if ($this->Contacts->save($contact)) {
          $numberContacts++;
        }

      }
      debug("Contactos agregados...".$numberContacts);
      die();
    }
}
