<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[] paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{

  public function isAuthorized($user){
      if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete'])){
          return true;
      }

      return parent::isAuthorized($user);
  }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tasks']
        ];
        $comments = $this->paginate($this->Comments);

        $this->set(compact('comments'));
        $this->set('_serialize', ['comments']);
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Tasks']
        ]);

        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $time = Time::now();
            $comment->date = $time::now();
            $task = TableRegistry::get('Tasks')->get($comment->task_id);
            $tableUsers = TableRegistry::get('Users');
            if ($this->Auth->user('id') == $task->user_id) {
              $user = $tableUsers->get($task->created_by);
            }
            else {
              $user = $tableUsers->get($task->user_id);
            }
            if ($this->Comments->save($comment)) {
              $url = Router::url(['controller' => 'Tasks', 'action' => 'view', $task->id], TRUE);
              $message = "Se ha agregado un nuevo comentario a la tarea: *".$task->name."*". PHP_EOL . ">" . $this->Auth->user('username').": ".$comment->comment. "." . PHP_EOL . $url;

              $this->sendMessageSlack($user, $message);
              $this->Flash->success(__('The comment has been saved.'));

              return $this->redirect(['controller' => 'Tasks','action' => 'view', $comment->task_id]);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            return $this->redirect(['controller' => 'Tasks','action' => 'view', $comment->task_id]);
        }
        // $tasks = $this->Comments->Tasks->find('list', ['limit' => 200]);
        // $this->set(compact('comment', 'tasks'));
        // $this->set('_serialize', ['comment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $tasks = $this->Comments->Tasks->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'tasks'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($id);
        $taskID = $comment->task_id;
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Tasks','action' => 'view', $taskID]);
    }

    private function sendEmails($subject, $message, $user, $url){
      $from = new \SendGrid\Email("WDPM", 'wdpminfo@gmail.com');
      $to = new \SendGrid\Email($user['first_name'].' '.$user['last_name'], $user['email']);
      $template = file_get_contents(WWW_ROOT.'reminder_templates/today_reminder.html');
      $content = new \SendGrid\Content("text/html", $template);

      $mail = new \SendGrid\Mail($from, $subject, $to, $content);
      $mail->personalization[0]->addSubstitution('%html_content%', $message);
      $mail->personalization[0]->addSubstitution('%text_content%', '<h2>¡Hola!</h2>');
      $mail->personalization[0]->addSubstitution('%button_content%', $url);
      $sg = new \SendGrid("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");
      $response = $sg->client->mail()->send()->post($mail);
    }

    public function sendMessageSlack($user, $message){
      $client = new \Maknz\Slack\Client('https://hooks.slack.com/services/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');
      $client->to($user->slack_user)->send($message);
    }
}
