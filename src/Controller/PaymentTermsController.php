<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * PaymentTerms Controller
 *
 * @property \App\Model\Table\PaymentTermsTable $PaymentTerms
 *
 * @method \App\Model\Entity\PaymentTerm[] paginate($object = null, array $settings = [])
 */
class PaymentTermsController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $paymentTerms = $this->paginate($this->PaymentTerms);

        $this->set(compact('paymentTerms'));
        $this->set('_serialize', ['paymentTerms']);
    }

    /**
     * View method
     *
     * @param string|null $id Payment Term id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentTerm = $this->PaymentTerms->get($id, [
            'contain' => []
        ]);

        $this->set('paymentTerm', $paymentTerm);
        $this->set('_serialize', ['paymentTerm']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paymentTerm = $this->PaymentTerms->newEntity();
        if ($this->request->is('post')) {
            $paymentTerm = $this->PaymentTerms->patchEntity($paymentTerm, $this->request->getData());
            if ($this->PaymentTerms->save($paymentTerm)) {
                $this->Flash->success(__('El término de pago fue salvado correctamente.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('El término de pago no pudo ser salvado. Por favor intente nuevamente.'));
        }
        $this->set(compact('paymentTerm'));
        $this->set('_serialize', ['paymentTerm']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment Term id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentTerm = $this->PaymentTerms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentTerm = $this->PaymentTerms->patchEntity($paymentTerm, $this->request->getData());
            if ($this->PaymentTerms->save($paymentTerm)) {
                $this->Flash->success(__('The payment term has been saved.'));

                return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
            }
            $this->Flash->error(__('The payment term could not be saved. Please, try again.'));
        }
        $this->set(compact('paymentTerm'));
        $this->set('_serialize', ['paymentTerm']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment Term id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentTerm = $this->PaymentTerms->get($id);
        if ($this->PaymentTerms->delete($paymentTerm)) {
            $this->Flash->success(__('The payment term has been deleted.'));
        } else {
            $this->Flash->error(__('The payment term could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Parametrization','action' => 'settings']);
    }
}
