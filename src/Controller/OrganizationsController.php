<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Organizations Controller
 *
 * @property \App\Model\Table\OrganizationsTable $Organizations
 *
 * @method \App\Model\Entity\Organization[] paginate($object = null, array $settings = [])
 */
class OrganizationsController extends AppController
{

    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'Config']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['edit', 'view'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $organizations = $this->paginate($this->Organizations);

        $this->set(compact('organizations'));
        $this->set('_serialize', ['organizations']);
    }

    /**
     * View method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if($this->Auth->user('organization_id') != $id){
          $this->Flash->error('¡Error! Intenta ingresar un dato que no concuerda con el registro.');
          return $this->redirect(['controller' => 'Dashboard','action' => 'index']);
        }
        $organization = $this->Organizations->get($id, [
            'contain' => []
        ]);
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'name']);
        $banks = TableRegistry::get('Banks')->find()->where(['organization_id' => $organization->id]);
        $this->set(compact('organization', 'countries', 'banks'));
        $this->set('_serialize', ['organization']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $organization = $this->Organizations->newEntity();
        if ($this->request->is('post')) {
            $organization = $this->Organizations->patchEntity($organization, $this->request->getData());
            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('The organization has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The organization could not be saved. Please, try again.'));
        }
        $this->set(compact('organization'));
        $this->set('_serialize', ['organization']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $organization = $this->Organizations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $logo = $organization->logo;
            $organization = $this->Organizations->patchEntity($organization, $this->request->getData());

              if($_FILES['logo']['size'] != 0){
                $logoPath = WWW_ROOT.'img/org/'.$organization->id;
                $imageName = addslashes($_FILES['logo']['tmp_name']);
                $name = basename($_FILES['logo']['name']);
                $imagePath = "$logoPath/$name";
                if(file_exists($logoPath)){
                  if(move_uploaded_file($imageName, $imagePath)){
                    $organization->logo = 'org/'.$organization->id.'/'.$name;
                  }
                  else{
                    debug('Problem with move file');
                  }
                }
                else{
                  if(mkdir($logoPath, 0777, true)){
                    if(move_uploaded_file($imageName, $imagePath)){
                      $organization->logo = 'org/'.$organization->id.'/'.$name;
                    }
                    else{
                      debug('Problem with move file');
                    }
                  }
                  else{
                    debug('Problem with move file directory');
                  }
                }
              }
              else {
                $organization->logo = $logo;
              }

            if ($this->Organizations->save($organization)) {
                $this->Flash->success(__('La organización ha sido salvada con éxito.'));

                return $this->redirect(['action' => 'view', $organization->id]);
            }
            $this->Flash->error(__('La organización no pudo ser salvada. Por favor intente nuevamente.'));
        }
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'name']);
        $this->set(compact('organization', 'countries', 'banks'));
        $this->set('_serialize', ['organization']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Organization id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $organization = $this->Organizations->get($id);
        if ($this->Organizations->delete($organization)) {
            $this->Flash->success(__('The organization has been deleted.'));
        } else {
            $this->Flash->error(__('The organization could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
