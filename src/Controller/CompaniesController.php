<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Model\Entity\Company;
use App\Model\Entity\CompaniesTag;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 *
 * @method \App\Model\Entity\Company[] paginate($object = null, array $settings = [])
 */
class CompaniesController extends AppController
{
    public function isAuthorized($user){
      $permissions = TableRegistry::get('PermissionsUsers')->find()->where(['user_id' => $user['id']])->contain(['Permissions'=> function(\Cake\ORM\Query $q){
                                                                                                          return $q->select()->where(['module' => 'CRM']);
                                                                                                      }])->count();
      if($permissions != 0){
        if($user['role'] == 'R' && in_array($this->request->getParam('action'), ['index', 'add', 'edit', 'delete'])){
            return true;
        }
      }

    return parent::isAuthorized($user);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $companies = $this->Companies->find()->contain(['Users']);

        $this->set(compact('companies'));
        $this->set('_serialize', ['companies']);
    }

    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users', 'Tags', 'Contacts', 'Projects']
        ]);
        $paymentTerm = TableRegistry::get('PaymentTerms')->get($company->payment_terms);
        $this->set(compact('company', 'sector', 'paymentTerm'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $company = $this->Companies->newEntity();
        if ($this->request->is('post')) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('La compañía ha sido salvada exitosamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La compañía no se pudo salvar. Intente nuevamente'));
        }
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'name'])->order(['name' => 'ASC']);
        $sectors = TableRegistry::get('Industrie_sectors')->find('list', ['limit' => 200, 'keyField' => 'name']);
        $paymentTerms = TableRegistry::get('Payment_terms')->find('list', ['limit' => 200, 'keyField' => 'id']);
        $users = $this->Companies->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $tags = $this->Companies->Tags->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'tag']);
        $this->set(compact('company', 'users', 'tags', 'countries', 'sectors', 'paymentTerms'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Tags']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('La compañía ha sido salvada exitosamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La compañía no se pudo salvar. Intente nuevamente'));
        }
        $tableCountries = TableRegistry::get('Countries');
        $countries = $tableCountries->find('list', ['keyField' => 'name'])->order(['name' => 'ASC']);
        $sectors = TableRegistry::get('Industrie_sectors')->find('list', ['limit' => 200, 'keyField' => 'id']);
        $paymentTerms = TableRegistry::get('Payment_terms')->find('list', ['limit' => 200, 'keyField' => 'id']);
        $users = $this->Companies->Users->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'username']);
        $tags = $this->Companies->Tags->find('list', ['limit' => 200, 'keyField' => 'id', 'valueField' => 'tag']);
        $this->set(compact('company', 'users', 'tags', 'countries', 'sectors', 'paymentTerms'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        try {
          if ($this->Companies->delete($company)) {
              $this->Flash->success(__('La compañía ha sido eliminada'));
          } else {
              $this->Flash->error(__('La compañía no se ha podido eliminar. Intente nuevamente'));
          }
        } catch (\PDOException  $e) {
          $this->Flash->error(__('La compañía no se puede eliminar ya que tiene registros asociados'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function importCompanies(){
      $file = fopen(WWW_ROOT."files/Companies.csv", "r");
      $first = true;
      $countryTable = TableRegistry::get('Countries');
      $paymentTermTable = TableRegistry::get('PaymentTerms');
      $companiesTagTable = TableRegistry::get('CompaniesTags');
      $tagsTable = TableRegistry::get('Tags');
      $numberCompanies = 0;
      while (($line = fgetcsv($file, 1000, ";")) !== FALSE) {
        if ($first) {
          $first=false;
          continue;
        }
        $country = $countryTable->find()->where(['code' => $line[3]])->first();
        $word = substr($line[8], 0,-1);
        $paymentTerm = $paymentTermTable->find()->where(['name LIKE' => $word])->first();
        $paymentTerm = $paymentTerm != null ? $paymentTerm->id : 2;
        $tag = $tagsTable->find()->where(['tag LIKE' => $line[6]])->first();
        $address = $line[1] != '' ? utf8_decode($line[1]) : "no-address";
        $city = $line[2] != '' ? utf8_decode($line[2]) : "no-city";
        $company = new Company(['name' => utf8_decode($line[0]), 'address' => ($address.", ".$city), 'country'=> $country->name, 'phone' => $line[5], 'user_id' => 36, 'payment_terms' => $paymentTerm]);
        if ($this->Companies->save($company)) {
          $numberCompanies++;
          $companyTag = new CompaniesTag(['company_id' => $company->id, 'tag_id' => $tag->id]);
          $companiesTagTable->save($companyTag);
        }

      }

      debug("Number of registers ".$numberCompanies);
      die();
    }
}
