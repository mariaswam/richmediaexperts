<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Nuevo Contacto');
  $companies = json_encode($companies);
?>
<div class="row quotes-form-container quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <div class="add-forms-container">
        <?= $this->Html->link(__('Back to contacts'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
        <?= $this->Form->create($contact) ?>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('first_name', ['label' => __('Nombre')]); ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('last_name', ['label' => __('Apellidos')]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('email', ['label' => __('Correo electrónico')]); ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('mobile', ['label' => __('Número de contacto')]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('company_name', ['label' => __('Compañía'), 'type' => 'text', 'id' => 'company']); ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('gender', ['label' => __('Género'), 'options' => ['F' => 'Female', 'M' => 'Male']]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label for="date_of_birth"><?=__('Fecha de nacimiento')?></label>
              <input name="date_of_birth" type="text" id="startDateBirth" data-provide="datepicker" class="form-control">
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('country', ['label' => __('País'), 'options' => $countries]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php echo $this->Form->control('address', ['label' => __('Dirección')]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('language', ['label' => __('Lenguaje'), 'options' => ['EN' => 'English', 'ES' => 'Spanish', 'PG' => 'Portugues', 'MD' => 'Chinesse']]); ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('role_id', ['options' => $roles, 'label' => __('Rol')]); ?>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-12">
            <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
          </div>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#startDateBirth').datepicker({dateFormat: 'yy-mm-dd'});
    $('#page-title').text('<?php echo $pageTitle; ?>');

    var companies = '<?php echo $companies ?>';
    var source = companies != '' ? JSON.parse(companies) : [];
    $('#company').autocomplete({
      source: source
    });
  });
</script>
