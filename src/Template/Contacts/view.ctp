<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Contact $contact
  */
  $pageTitle = h($contact->first_name) ." " . h($contact->last_name);
?>
<div class="row quotes-form-container quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <div class="row">


        <div class="col-md-6 col-sm-12">
          <?= $this->Html->link(__('Back to contacts'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
          <h2><?= __('Information') ?></h2>
          <table class="vertical-table table table-view small-view">
              <tr>
                  <th scope="row"><?= __('Nombre') ?></th>
                  <td><?= h($contact->first_name) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Apellidos') ?></th>
                  <td><?= h($contact->last_name) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Correo Electrónico') ?></th>
                  <td><?= h($contact->email) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Número de contacto') ?></th>
                  <td><?= h($contact->mobile) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Género') ?></th>
                  <td><?= $contact->gender == 'M' ? __('Masculino') : __('Femenino') ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Fecha de nacimiento') ?></th>
                  <td><?= h($contact->date_of_birth) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('País') ?></th>
                  <td><?= h($contact->country) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Dirección') ?></th>
                  <td><?= h($contact->address) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Lenguaje') ?></th>
                  <td><?= h($contact->language) ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Compañía') ?></th>
                  <td><?= $contact->has('company') ? $this->Html->link($contact->company->name, ['controller' => 'Companies', 'action' => 'view', $contact->company->id]) : '' ?></td>
              </tr>
              <tr>
                  <th scope="row"><?= __('Rol') ?></th>
                  <td><?= $contact->has('role') ? $contact->role->name : '' ?></td>
              </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
