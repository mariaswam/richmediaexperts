<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
  */
  $pageTitle = __('Contactos');
?>
<div class="row quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('Add New Contact'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
      <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped" id="table_contacts">
            <thead>
                <tr>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Apellidos') ?></th>
                    <th><?= __('Número de contacto') ?></th>
                    <th><?= __('Compañía') ?></th>
                    <th><?= __('Rol') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($contacts as $contact): ?>
                <tr>
                    <td><?= h($contact->first_name) ?></td>
                    <td><?= h($contact->last_name) ?></td>
                    <td><?= h($contact->mobile) ?></td>
                    <td><?= $contact->has('company') ? $this->Html->link($contact->company->name, ['controller' => 'Companies', 'action' => 'view', $contact->company->id]) : '' ?></td>
                    <td><?= $contact->has('role') ? $contact->role->name : '' ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $contact->id], ['class' => 'btn-view']) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $contact->id], ['class' => 'btn-edit']) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $contact->id], ['class' => 'btn-delete'], ['confirm' => __('¿Seguro que desea eliminar el contacto {0}?', $contact->first_name)]) ?>
                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <select class="form-control items-count" name="" id="numberOfRowSelect">
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('#table_contacts').footable({
      "paging": {
        "enabled": true,
        "size": localStorage.getItem('numberOfRecordsContacts') != null ? localStorage.getItem('numberOfRecordsContacts') : 10
      },
      "filtering": {
        "enabled": true
      }
    });

    if (localStorage.getItem('numberOfRecordsContacts') != null) {
      $('#numberOfRowSelect').val(localStorage.getItem('numberOfRecordsContacts'));
    }
    else{
      $('#numberOfRowSelect').val('10');
    }

    $('#numberOfRowSelect').change(function() {
      localStorage.setItem('numberOfRecordsContacts', $('#numberOfRowSelect').val());
      FooTable.get('#table_contacts').pageSize($('#numberOfRowSelect').val());
    });
  });
</script>
