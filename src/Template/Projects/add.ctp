<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Nuevo Proyecto');
  $companies = json_encode( $companies );
?>

  <div class="col-md-12 col-sm-12 main-content-wrapper form-wrapper">
    <?= $this->Html->link(__('Back to projects'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
    <?= $this->Form->create($project, ['id' => 'formProject']) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre *'), 'id' => 'projectName']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('budget', ['label' => __('Presupuesto *'), 'id' => 'budget']); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('estimated_cost', ['label' => __('Costo estimado *')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('company_name', ['label' => __('Compañía cliente *'), 'required' => true, 'type' => 'text', 'id' => 'company']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php
                echo $this->Form->control('invoiced', ['type' => 'hidden']);
                echo $this->Form->control('itemsInvoices', ['type' => 'hidden']);
            ?>
        </div>
      </div>
    </fieldset>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <button type="button" class="btn btn-success" id="btnAddMilestone"><?= __('Agregar Milestone')?></button><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div id="form-milestone"></div>
      </div>
    </div>
    <div class="button-wrapper">
      <?= $this->Form->button(__('Guardar'), ['type' => 'button', 'id' => 'btnSubmitForm', 'class' => 'btn-form']) ?>
    </div>
    <?= $this->Form->end() ?>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="display: inline-block;"><?=__('Componentes de factura')?></h5>
        <button type="button" class="btn btn-sm btn-info btn-circle" onclick="addItemInvoice()">+</button>
      </div>
      <form id="form-invoices" method="post">
      <div class="modal-body" >
        <div id="rowItems">
          <div class="row formInvoiceModal quote-item">
            <div class="col-md-4 form-group">
              <label for="price"><?=__('Descripción')?></label>
              <input type="text" class="form-control" name="description" placeholder="<?=__('Descripción')?>" id="firstDescription">
            </div>
            <div class="col-md-2 form-group">
              <label for="price"><?=__('Precio')?></label>
              <input type="number" class="form-control itemPrice calcInput" id="pI0" name="price" placeholder="<?=__('Precio')?>">
            </div>
            <div class="col-md-2 form-group">
              <label for="quantity"><?=__('Cantidad')?></label>
              <input type="number" class="form-control itemQuantity calcInput" id="qI0" name="quantity" placeholder="<?=__('Cantidad')?>">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <?php echo $this->Form->control('purchase_order', ['label' => __('Orden de compra')]); ?>
          </div>
          <div class="col-md-3" style="float:right;">
              <?php echo $this->Form->control('total_amount', ['label' => 'Total I.V.I', 'readonly', 'id' => 'subtotalInvoice']); ?>
          </div>
        </div>
      </div>
      </form>
      <div class="modal-footer" style="overflow: hidden">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <button type="button" class="btn btn-secondary" id="btnSaveAll"><?=__('Guardar')?></button>
      </div>
    </div>
  </div>

<?= $this->end(); ?>
<script>
var cont = 1;
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');

    //AutoComplete
    var companies = '<?php echo $companies ?>';
    var source = companies != '' ? JSON.parse(companies) : [];
    $('#company').autocomplete({
      source: source
    });

    //Datepickers
    $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'});
    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'});
    var count = 0;
    $("#btnAddMilestone").click(function(){
      var formText = '<div class="col-md-6 formMilestone" id="div'+count+'">'
      formText += '<legend style="display: inline-block;"><?=__('Nuevo Milestone')?>'
      formText += '<button type="button" class="btn btn-sm btn-danger btn-circle close" id="'+count+'" onclick="eliminarDiv(this)"><i class="fa fa-times" aria-hidden="true"></i></button>'
      formText += '</legend>';
      formText += '<?= $this->Form->control('milestonesArray[name][]', ['label' => __('Nombre del milestone *'), 'required']); ?> ';
      formText += '<div class="form-group"><label for="milestonesArray[start_date][]"><?=__('Fecha de inicio')?></label><input name="milestonesArray[start_date][]" type="text"  data-provide="datepicker" class="form-control startDatePickerMilestone"></div>';
      formText += '<div class="form-group"><label for="milestonesArray[end_date][]"><?=__('Fecha de fin')?></label><input name="milestonesArray[end_date][]" type="text"  data-provide="datepicker" class="form-control endDatePickerMilestone"></div>';
      formText += '<label for="milestonesArray[users][]"><?=__('Usuarios')?></label>';
      formText += '<select class="form-control sm-users" id="'+count+'" multiple onchange="addUserToMilestone(this)" required>';
      formText += '<?php
                  $options = '';
                  foreach ($users->toArray() as $user){
                      $options .= '<option value="'.$user->id.'">'.$user->username.'</option>';
                    }
                    echo $options; ?>';
      formText += '</select>';
      formText += '<input type="hidden" name="milestonesArray[user][]" id="ih'+count+'">';
      formText += '</div>';
      $('#form-milestone').append(formText);
      $('.startDatePickerMilestone').datepicker({dateFormat: 'yy-mm-dd'});
      $('.endDatePickerMilestone').datepicker({dateFormat: 'yy-mm-dd'});
      count++;
    });

    $('#btnSubmitForm').click(function(){
      swal({
        title: "<?php echo __('Desea facturar el proyecto?') ?>",
        icon: "warning",
        buttons: true,
        })
        .then((invoice) => {
        if (invoice) {
          $('#invoiced').val(1);
          $('#firstDescription').val($('#projectName').val());
          $('#pI0').val($('#budget').val());
          $('#qI0').val(1);
          $('#subtotalInvoice').val($('#budget').val());
          $('#invoiceModal').modal('show');
        } else {
          $('#invoiced').val(0);
          $('#formProject').submit();
        }
        });
    });

    $('#btnSaveAll').click(function(){
      $('#itemsinvoices').val(JSON.stringify($('#form-invoices').serializeArray()));
      $('#formProject').submit();
    });
  });

  function addUserToMilestone(val){
    var inputUser = $("#ih"+val.id);
    var users = '';
    $(val).find('option').each(function(index, element){
      if(element.selected){
        users += element.value + ',';
      }
    });
    inputUser.val(users);
  }

  function eliminarDiv(val){
    $('#div'+val.id).remove();
  }

  function addItemInvoice(){
    var itemInvoice = '<div class="row formInvoiceModal quote-item">';
    itemInvoice += '<div class="col-md-4 form-group">';
    itemInvoice += '<label for="price"><?=__('Descripción')?></label><input type="text" class="form-control" name="description" placeholder="<?=__('Descripción')?>">';
    itemInvoice += '</div>';
    itemInvoice += '<div class="col-md-2 form-group">';
    itemInvoice += '<label for="price"><?=__('Precio')?></label><input type="number" class="form-control itemPrice calcInput" id="pI'+cont+'" name="price" placeholder="<?=__('Precio')?>">';
    itemInvoice += '</div>';
    itemInvoice += '<div class="col-md-2 form-group">';
    itemInvoice += '<label for="quantity"><?=__('Cantidad')?></label><input type="number" class="form-control itemQuantity calcInput" id="qI'+cont+'" name="quantity" placeholder="<?=__('Cantidad')?>">';
    itemInvoice += '</div>';
    itemInvoice += '<div class="col-md-2 form-group">';
    itemInvoice += '<button class="btn btn-remove btn-danger" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button>';
    itemInvoice += '</div>';
    itemInvoice += '</div>';
    cont++;
    $('#rowItems').append(itemInvoice);
  }
</script>
