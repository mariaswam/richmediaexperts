<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Project $project
  */
  $pageTitle = h($project->name);
?>




<div class="col-md-12  main-content-wrapper project">
<?= $this->Html->link(__('Back to projects'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>

<div class="row">
  <div class="col-md-6">
    <h4><?= __('Detalles del proyecto') ?></h4>
    <table class="table small-view">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($project->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descripción') ?></th>
            <td><?= h($project->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Compañía') ?></th>
            <td><?= $project->has('company') ? $this->Html->link($project->company->name, ['controller' => 'Companies', 'action' => 'view', $project->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Presupuesto') ?></th>
            <td><?= $this->Number->format($project->budget) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Costo estimado') ?></th>
            <td><?= $this->Number->format($project->estimated_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('¿Facturado?') ?></th>
            <?php if($this->Number->format($project->invoiced)==0) :?>
              <td>No</td>
            <?php else : ?>
              <td><?=__('Sí')?></td>
            <?php endif ?>
        </tr>
        <tr>
            <th scope="row"><?= __('Costo Total') ?></th>
            <td><?= $this->Number->format($project->total_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de inicio') ?></th>
            <td><?= h($project->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de fin') ?></th>
            <td><?= h($project->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= ($project->state == 'O') ? __('Abierto') : __('Cerrado') ?></td>
        </tr>
    </table>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="related ">
        <h4><?= __('Facturas del proyecto') ?></h4>
        <?php if (!empty($project->invoices)): ?>
          <div class="table-responsive">

            <table cellpadding="0" cellspacing="0"  class="table table-striped invoices-table">
              <thead>
                <tr>
                    <th scope="col" class="center-col"><?= __('Date') ?></th>
                    <th scope="col" class="center-col"><?= __('State') ?></th>
                    <th scope="col" class="center-col"><?= __('Total Amount') ?></th>
                    <th scope="col" class="center-col"><?= __('Project Id') ?></th>
                    <th scope="col" class="center-col"><?= __('Due Date') ?></th>
                </tr>
              </thead>
                <?php foreach ($project->invoices as $invoices): ?>
                  <tbody>
                <tr>
                    <td class="center-col"><?= h($invoices->date) ?></td>
                    <td class="center-col"><?= h($invoices->state) ?></td>
                    <td class="center-col"><?= h($invoices->total_amount) ?></td>
                    <td class="center-col"><?= h($invoices->project_id) ?></td>
                    <td class="center-col"><?= h($invoices->due_date) ?></td>
                </tr>
              </tbody>
                <?php endforeach; ?>
            </table>
          </div>
        <?php endif; ?>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-info text-center info-project">
        <i class="fa fa-tasks fa-lg"></i>
        <b id="totalProjectTasks">0</b> <?= __('Total de tareas') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-success text-center info-project">
        <i class="fa fa-check fa-lg"></i>
        <b id="completeTasksQuantity">0</b> <?= __('Tareas completas') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-danger text-center info-project">
        <i class="fa fa-times fa-lg"></i>
        <b id="incompleteTasksQuantity">0</b> <?= __('Tareas pendientes') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-danger text-center info-project">
        <i class="fa fa-times fa-lg"></i>
        <b id="pendingTasksQuantity">0</b> <?= __('Tareas pendientes aprobación') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-success text-center info-project">
        <i class="fa fa-times fa-lg"></i>
        <b id="inProgressTasksQuantity">0</b> <?= __('Tareas en progreso') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-info text-center info-project">
        <i class="fa fa-line-chart fa-lg"></i>
        <b id="percentageCompletion">0</b> <?= __('de avance') ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-info text-center info-project">
        <i class="fa fa-usd fa-lg"></i>
        <b id="costEffectiveness">0</b> <?= __('Rentabilidad') ?>
    </div>
  </div>
  <div class="col-md-2 col-sm-12">
    <div class="alert alert-info text-center info-project">
        <i class="fa fa-calendar fa-lg"></i>
        <b id="remainingDays">0</b> <?= __('días restantes') ?>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="related">
      <h4><?= __('Desarrollo del proyecto') ?></h4>
      <div class="row">
        <div class="col-md-12 gantt-container">
          <svg id="gantt" style="pointer-events:none;"></svg>
        </div>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-md-4 col-sm-12">
    <div class="related">
        <h4 style="display: inline-block;"><?= __('Tareas del proyecto') ?></h4>
        <?php if (!empty($project->tasks)): ?>
        <table cellpadding="0" cellspacing="0" class="table">
            <tr>
                <th scope="col"><?= __('Nombre') ?></th>
                <th scope="col"><?= __('Fecha de fin') ?></th>
            </tr>
            <?php foreach ($project->tasks as $task): ?>
            <tr>
                <td><?= h($task->name) ?></td>
                <td><?= h($task->end_date) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
      <?php endif; ?>
    </div>
  </div>
</div>
</div>


<?= $this->start('scripts')?>
<?= $this->Html->script('snap.svg-min.js') ?>
<?= $this->Html->script('frappe-gantt.min.js') ?>
<?= $this->end()?>
<script>

 $(document).ready(function(){
   var datesArray = [];
   var pos = 0;
   var getMilestonesUrl = "<?= $this->Url->build(['controller' => 'Milestones', 'action' => 'getMilestonesProject'])?>";
   var milestones = '<?= json_encode($project->milestones); ?>' ;
   var projectDates = '';
   var projectActivities = '';
   var startMilestoneToday = false;

   $('#page-title').text('<?php echo $pageTitle; ?>');

   $.each(JSON.parse(milestones), function(index, value){
     datesArray[index] = {'id': value.id, 'name': value.name, 'start':new Date(value.start_date), 'end':new Date(value.end_date)};
   });

   var projectID = '<?= $project->id ?>';
   $.ajax({
     type: 'POST',
     async: true,
     dataType: 'json',
     url: "<?= $this->Url->build(['controller' => 'Projects', 'action' => 'getProjectStatusInformation'])?>",
     data: {
       project_id: projectID,
       end_date: '<?= $project->end_date != null ? $project->end_date->format('m/d/Y'):$project->end_date ?>'
     },
     success: function(data) {
       $('#percentageCompletion').text(data.projectInformation.percentageCompletion+'%');
       $('#remainingDays').text(data.projectInformation.remainingDays);
       $('#incompleteTasksQuantity').text(data.projectInformation.pendingTasks);
       $('#completeTasksQuantity').text(data.projectInformation.completeTasks);
       $('#pendingTasksQuantity').text(data.projectInformation.pendingApprove);
       $('#inProgressTasksQuantity').text(data.projectInformation.inProgress);
       $('#totalProjectTasks').text(data.projectInformation.totalTasks);
       $('#costEffectiveness').text(data.projectInformation.profiability+'%');
       var gantt = new Gantt("#gantt", datesArray);
     }
   });

   $('.invoices-table').footable({
     "paging": {
       "enabled": true,
       "size": localStorage.getItem('numberOfRecordsContacts') != null ? localStorage.getItem('numberOfRecordsContacts') : 10
     },
     "filtering": {
       "enabled": false
     }
   });

 });
</script>
