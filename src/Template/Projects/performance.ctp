<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Project[]|\Cake\Collection\CollectionInterface $projects
  */
  $pageTitle = __('Desempeño de recursos');
  $projects = empty($projects) ? null : json_encode($projects->toArray());
?>
<div class="row">
  <div class="col-md-3">
    <?= $this->Form->control('project_id', ['type' => 'text', 'label' => __('Proyecto'), 'onchange' => 'getResourcesInformation(this.value)', 'id' => 'projectName']); ?>
  </div>
</div>
<div class="row" id="contentRow">
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="ganttModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=__('Diagrama de Gantt')?></h5>
      </div>
      <div class="modal-body gantt-container" id="modalBodyGantt">
          <svg id="gantt"></svg>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<?= $this->start('scripts')?>
<?= $this->Html->script('snap.svg-min.js') ?>
<?= $this->Html->script('frappe-gantt.min.js') ?>
<?= $this->end()?>
<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    var availableProjects = '<?php echo $projects ?>';
    var source = availableProjects != '' ? JSON.parse(availableProjects) : [];
    $('#projectName').autocomplete({
      source: source
    });
  });

  function showGanttChart(userID) {
    var projectName = $('#projectName').val();
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'getTasksByUser'])?>",
      data: {
        user_id: userID,
        project_name: projectName
      },
      success: function(data){
        var gantt = new Gantt("#gantt", data.tasks);
        $('#ganttModal').modal('show');
      }
    });
  }

  function getResourcesInformation(project){
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Projects', 'action' => 'performance'])?>",
      data: {
        name: project
      },
      success: function(data){
        $('#contentRow').empty();
        $.each(data.performanceInfo, function(index, value){
          console.log(value);
          var panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5>'+value.user_info.username+'</h5></div>';
          panelInformation += '<div class="content"><table class="table">';
          panelInformation += '<tr><th><?=__('Usuario')?></th><td>'+value.user_info.first_name+' '+value.user_info.last_name+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas asignadas')?></th><td>'+value.total_tasks+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas pendientes')?></th><td>'+value.pending_tasks+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas en progreso')?></th><td>'+value.in_progress+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas pendientes aprobación')?></th><td>'+value.pending_approve+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas para hoy')?></th><td>'+value.for_today+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas finalizadas')?></th><td>'+value.complete_tasks+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas retrasadas')?></th><td>'+value.delay_task+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas entregadas antes de la fecha')?></th><td>'+value.finish_before+'</td></tr>';
          panelInformation += '<tr><th><?=__('Tareas entregadas posterior a la fecha')?></th><td>'+value.finish_after+'</td></tr>';
          panelInformation += '<tr><th><?=__('Progreso de avance')?></th><td><div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" style="width:'+((value.complete_tasks*100)/value.total_tasks)+'%">'+Math.round(((value.complete_tasks*100)/value.total_tasks));
          panelInformation += '%</div><div class="progress-bar progress-bar-danger" role="progressbar" style="width:'+(100-((value.complete_tasks*100)/value.total_tasks))+'%">'+Math.round((100-((value.complete_tasks*100)/value.total_tasks)))+'%</div></div></td>';
          panelInformation += '<tr><th><?=__('Desglose de horas')?></th><td><a class="btn btn-sm btn-info" href="/wdpm/timesheets/downloadReportTimesheetsByProject/'+$('#projectName').val()+'/'+value.user_info.id+'"><?=__('Descargar')?></a></td></tr>';
          panelInformation += '</table></div></div></div>';
          $('#contentRow').append(panelInformation);
        });
      }
    });
  }
</script>
