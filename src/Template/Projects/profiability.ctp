<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Project[]|\Cake\Collection\CollectionInterface $projects
  */
  $pageTitle = __('Reporte de Proyectos');
  $projects = empty($projects) ? null : json_encode($projects->toArray());
?>
<!-- Filter/ -->
  <div class="row">
    <div class="col-md-2 col-sm-12">
      <?= $this->Form->control('project_id', ['empty' => ['0' => __('Información General')], 'label' => __('Proyecto'), 'options' => ['0' => __('Información General'), '1' => __('Por proyecto')] , 'onchange' => 'getInformation(this.value)', 'id' => 'selProjects']); ?>
    </div>
    <div class="col-md-2" id="colSDP">
      <label for="start_date"><?=__('Fecha de inicio')?></label>
      <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control" onchange="getInformation(0)">
    </div>
    <div class="col-md-2" id="colEDP">
      <label for="end_date"><?=__('Fecha de fin')?></label>
      <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control" onchange="getInformation(0)">
    </div>
    <div class="col-md-2 specific-name">
      <?= $this->Form->control('project_name', ['label' => __('Proyecto') , 'onchange' => 'getResourcesInformation(this.value)', 'id' => 'projectName', 'type' => 'text']); ?>
    </div>
  </div>
<!-- Filter/ -->
<div class="row" id="contentRow"></div>
<div class="row" id="estimatedCost"></div>

<script>
  var chartProjectsMonth = null;
  var chartDistributionProject = null;
  var chartHours = null;

  $(document).ready(function() {
    var date = new Date();
    var endDate = new Date();
    $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(date.setMonth(date.getMonth() - 1)));
    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(endDate.setDate(endDate.getDate() + 1)));

    $('#page-title').text('<?php echo $pageTitle; ?>');
    getInformation($('#selProjects').val());

    var availableProjects = '<?php echo $projects ?>';
    var source = availableProjects != '' ? JSON.parse(availableProjects) : [];
    $('#projectName').autocomplete({
      source: source
    });
  });

  function getInformation(value) {
    if(value == '0'){
      $('#colSDP').css('display', 'inline');
      $('#colEDP').css('display', 'inline');
      getProjectsInformation();
    }
    else if (value == '1'){
      $('#colSDP').css('display', 'none');
      $('#colEDP').css('display', 'none');
      $('.specific-name').css('display', 'inline');
    }
  }

  function getResourcesInformation(projectName){
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Projects', 'action' => 'profiability'])?>",
      data: {
        name: projectName
      },
      success: function(data){
        var project = data.project;
        $('#contentRow').empty();
        $('#estimatedCost').empty();
          var panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5>'+project.name+'</h5></div>';
          panelInformation += '<div class="content"><table class="table">';
          panelInformation += '<tr><th><?=__('Descripción')?></th><td>'+project.description+'</td></tr>';
          panelInformation += '<tr><th><?=__('Presupuesto')?></th><td>'+project.budget+'</td></tr>';
          panelInformation += '<tr><th><?=__('Costo total')?></th><td>$'+project.total_cost+'</td></tr>';
          panelInformation += '<tr><th><?=__('Rentabilidad')?></th><td>'+project.profiability+'%</td></tr>';
          panelInformation += '<tr><th><?=__('Ganancia')?></th><td>$'+project.earning+'</td></tr>';
          panelInformation += '</table></div></div></div>';

          $('#contentRow').append(panelInformation);

          panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5>'+'<?= __('Consumo del presupuesto') ?>'+'</h5></div>';
          panelInformation += '<div class="content">';
          panelInformation += '<canvas id="chartBudget"></canvas>';
          panelInformation += '<div class="footer"><div class="stats"><?=__('Datos brindados en dólares')?></div></div>';
          panelInformation += '</div></div></div>';

          $('#contentRow').append(panelInformation);

          panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5>'+'<?= __('Estimaciones') ?>'+'</h5></div>';
          panelInformation += '<div class="content">';
          panelInformation += '<canvas id="chartEstimate"></canvas>';
          panelInformation += '<div class="footer"><div class="stats"><?=__('Datos brindados en dólares')?></div></div>';
          panelInformation += '</div></div></div>';

          $('#estimatedCost').append(panelInformation);

          panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5>'+'<?= __('Costo de recursos') ?>'+'</h5></div>';
          panelInformation += '<div class="content">';
          panelInformation += '<canvas id="chartCostUsers"></canvas>';
          panelInformation += '<div class="footer"><div class="stats"><?=__('Datos brindados en dólares')?></div></div>';
          panelInformation += '</div></div></div>';

          $('#estimatedCost').append(panelInformation);

            var ctx = document.getElementById("chartBudget").getContext('2d');
            var ctx2 = document.getElementById("chartEstimate").getContext('2d');
            var ctx3 = document.getElementById("chartCostUsers").getContext('2d');

            var myChart = new Chart(ctx, {
              type: 'line',
              data: {
                  labels: project.dataCosts.tasks,
                  datasets: [{
                      label: 'Tareas',
                      data: project.dataCosts.costs,
                      backgroundColor: [
                          'rgba(255, 99, 132, 0.7)',
                          'rgba(54, 162, 235, 0.7)',
                          'rgba(255, 206, 86, 0.7)',
                          'rgba(75, 192, 192, 0.7)',
                          'rgba(153, 102, 255, 0.7)',
                          'rgba(255, 159, 64, 0.7)'
                      ],
                      borderColor: [
                          'rgba(255,99,132,1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)'
                      ],
                      borderWidth: 1
                }]
              },
              options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 14
                    }
                },
                scales: {
                   yAxes: [{
                        ticks: {
                            fontColor: 'white',
                            beginAtZero:true,
                            max: project.total_cost
                        }
                  }],
                  xAxes: [{
                       ticks: {
                          beginAtZero:true,
                           fontColor: 'white'
                       }
                 }]
                }
              }
            });

          var myChart2 = new Chart(ctx2, {
              type: 'bar',
              data: {
                  labels: ['<?=__('Costo estimado')?>', '<?=__('Costo total')?>'],
                  datasets: [{
                      label: '<?=__('Costos')?>',
                      data: [project.estimated_cost, project.total_cost],
                      backgroundColor: [
                          'rgba(255, 99, 132, 0.7)',
                          'rgba(54, 162, 235, 0.7)',
                          'rgba(255, 206, 86, 0.7)',
                          'rgba(75, 192, 192, 0.7)',
                          'rgba(153, 102, 255, 0.7)',
                          'rgba(255, 159, 64, 0.7)'
                      ],
                      borderColor: [
                          'rgba(255,99,132,1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)'
                      ],
                      borderWidth: 1
                  }]
              },
              options: {
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 14
                    }
                },
                  scales: {
                      yAxes: [{
                          ticks: {
                            fontColor: 'white',
                              beginAtZero:true
                          }
                      }],
                      xAxes: [{
                          ticks: {
                              fontColor: "white",
                              fontSize: 12
                          }
                      }]
                  }
              }
    });

    var myChart3 = new Chart(ctx3, {
        type: 'bar',
        data: {
            labels: project.costUsers.users,
            datasets: [{
                label: '<?=__('Costo por horas registradas')?>',
                data: project.costUsers.costs,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 206, 86, 0.7)',
                    'rgba(75, 192, 192, 0.7)',
                    'rgba(153, 102, 255, 0.7)',
                    'rgba(255, 159, 64, 0.7)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          legend: {
              labels: {
                  fontColor: "white",
                  fontSize: 14
              }
          },
            scales: {
                yAxes: [{
                    ticks: {
                      fontColor: 'white',
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "white",
                        fontSize: 12,
                    }
                }]
            }
        }
});
    }
    });
  }

  function getProjectsInformation() {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?=$this->Url->build(['controller' => 'Projects', 'action' => 'getProjectsInformation']);?>',
      data: {
        start_date: $('#startDatePicker').val(),
        end_date: $('#endDatePicker').val()
      },
      success: function(data){
        $('#contentRow').empty();
        $('#estimatedCost').empty();
          var panelInformation = '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5><?=__('Distribución')?></h5></div>';
          panelInformation += '<div class="content"><canvas id="chartDistribution"></canvas></div></div></div>';
          panelInformation += '<div class="col-md-6"><div class="card">';
          panelInformation += '<div class="header"><h5><?=__('Proyectos de los últimos seis meses')?></h5></div>';
          panelInformation += '<div class="content"><canvas id="chartProjectsMonth"></canvas></div></div></div>';

        $('#contentRow').append(panelInformation);

        var panelInformation = '<div class="col-md-6"><div class="card">';
        panelInformation += '<div class="header"><h5><?=__('Cantidad de horas')?></h5></div>';
        panelInformation += '<div class="content"><table class="table">';
        $.each(data.projectsHours, function(index, value) {
          panelInformation += '<tr><th>'+index+'</th><td>'+value+' <?=__(' horas')?></td></tr>'
        });
        panelInformation += '</table></div></div></div>';

        $('#estimatedCost').append(panelInformation);

        if(chartProjectsMonth != null) {
          chartProjectsMonth.destroy();
        }
        if(chartDistributionProject != null) {
          chartDistributionProject.destroy();
        }
        createDistributionProjectsChart(data);
        createProjectsMonthChart(data);

      }
    });
  }

  function createDistributionProjectsChart(data) {
    var ctx = document.getElementById("chartDistribution").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
          labels: ['<?=__('Pendientes')?>', '<?=__('Cerrados')?>', '<?=__('Atrasados')?>'],
          datasets: [{
              label: 'Distribución',
              data: data.projectsInformation,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.7)',
                  'rgba(54, 162, 235, 0.7)',
                  'rgba(255, 206, 86, 0.7)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 1
        }]
      },
      options: {
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 14
            }
        }
      }
    });

    chartDistributionProject = myChart;
  }

  function createProjectsMonthChart(data) {
    console.log(data);
    var ctx2 = document.getElementById("chartProjectsMonth").getContext('2d');
    var myChart2 = new Chart(ctx2, {
      type: 'bar',
      data: {
          labels: data.monthsName,
          datasets: [{
              label: '<?=__('Abiertos')?>',
              data: data.openProjectsByMonth,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.7)',
                  'rgba(54, 162, 235, 0.7)',
                  'rgba(255, 206, 86, 0.7)',
                  'rgba(22, 160, 133, 0.7)',
                  'rgba(243, 156, 18, 0.7)',
                  'rgba(93, 109, 126, 0.7)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)'
              ],
              borderWidth: 1
        },
        {
          label: '<?=__('Cerrados')?>',
          data: data.closedProjectsByMonth,
          backgroundColor: [
              'rgba(211, 84, 0, 0.7)',
              'rgba(54, 162, 235, 0.7)',
              'rgba(255, 206, 86, 0.7)',
              'rgba(255, 206, 86, 0.7)',
              'rgba(108, 52, 131, 0.7)'
          ],
          borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
            labels: {
                fontColor: "white",
                fontSize: 14
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "white",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "white",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }]
        }
      }
    });

    chartProjectsMonth = myChart2;
  }
</script>
