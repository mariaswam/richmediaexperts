<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Project[]|\Cake\Collection\CollectionInterface $projects
  */
  $pageTitle = __('Proyectos');
?>
<div class="col-md-12 main-content-wrapper">
  <div class="row">
    <div class="col-md-12">
      <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('New Project'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
    </div>
  </div>
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-border" id="table_projects">
            <thead>
                <tr>
                    <th scope="col"><?= __('Nombre') ?></th>
                    <th scope="col" class="center-col"><?= __('Fecha de fin') ?></th>
                    <th scope="col" class="center-col"><?= __('Presupuesto') ?></th>
                    <th scope="col" class="center-col"><?= __('Costo estimado') ?></th>
                    <th scope="col" class="center-col"><?= __('¿Facturado?') ?></th>
                    <th scope="col" class="center-col"><?= __('Compañía') ?></th>
                    <th scope="col" class="center-col"><?= __('Costo Total') ?></th>
                    <th scope="col" class="center-col"><?= __('Estado') ?></th>
                    <th scope="col" class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($projects as $project): ?>
                <tr>
                    <td><?= h($project->name) ?></td>
                    <?php $dateProject =  ($project->end_date != null) ? h($project->end_date->format('d-m-Y')) : ''; ?>
                    <td  class="center-col" data-order="<?php echo strtotime($dateProject); ?>"><?= $dateProject; ?></td>
                    <td  class="center-col"><?= $this->Number->format($project->budget) ?></td>
                    <td  class="center-col"><?= $this->Number->format($project->estimated_cost) ?></td>
                    <?php if($this->Number->format($project->invoiced) == 0):?>
                      <td class="center-col">No</td>
                    <?php else :?>
                      <td class="center-col"><?=__('Sí')?></td>
                    <?php endif; ?>
                    <td class="company center-col"><?= $project->has('company') ? $this->Html->link($project->company->name, ['controller' => 'Companies', 'action' => 'view', $project->company->id]) : '' ?></td>
                    <td class="center-col"><?= $this->Number->format($project->total_cost) ?></td>
                    <td class="center-col"><?= ($project->state == 'O') ? __('Abierto') : __('Cerrado') ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">

                          <?= $this->Html->link(__('Ver'), ['action' => 'view', $project->id], ['class' => 'btn-view']) ?>
                          <?= $this->Html->link(__('Editar'), ['action' => 'edit', $project->id], ['class' => 'btn-edit']) ?>
                          <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $project->id], ['class' => 'btn-delete'], ['confirm' => __('¿Seguro que desea eliminar el proyecto {0}?', $project->name)]) ?>
                          <?php if ($project->state == 'O'):?>
                            <?= $this->Form->postLink(__('Finalizar'), ['action' => 'finishProject', $project->id], ['class' => 'btn-finish'], ['confirm' => __('¿Desea concluir el proyecto {0}?', $project->name)]) ?>
                          <?php else: ?>
                            <?= $this->Form->postLink(__('Reabrir'), ['action' => 'reopenProject', $project->id], ['class' => 'btn-reopen'], ['confirm' => __('¿Desea volver a abrir el proyecto {0}?', $project->name)]) ?>
                          <?php endif;?>

                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <select class="form-control items-count" name="" id="numberOfRowSelect">
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
    </div>
</div>
<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('#table_projects').footable({
      "paging": {
  			"enabled": true,
        "size": localStorage.getItem('numberOfRecordsProjects') != null ? localStorage.getItem('numberOfRecordsProjects') : 10
  		},
      "filtering": {
        "enabled": true
      }
    });

    if (localStorage.getItem('numberOfRecordsProjects') != null) {
      $('#numberOfRowSelect').val(localStorage.getItem('numberOfRecordsProjects'));
    }
    else{
      $('#numberOfRowSelect').val('10');
    }

    $('#numberOfRowSelect').change(function() {
      localStorage.setItem('numberOfRecordsProjects', $('#numberOfRowSelect').val());
      FooTable.get('#table_projects').pageSize($('#numberOfRowSelect').val());
    });
  });
</script>
