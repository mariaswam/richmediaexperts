<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Editar Proyecto');
  $companies = json_encode( $companies );
?>

  <div class="col-md-12 col-sm-12 main-content-wrapper form-wrapper">
    <?= $this->Html->link(__('Back to projects'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
    <?= $this->Form->create($project) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre *')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea', 'rows' => '5', 'cols' => '5']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('budget', ['label' => __('Presupuesto *')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('estimated_cost', ['label' => __('Costo estimado *')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('company_name', ['label' => __('Compañía cliente *'), 'required' => true, 'type' => 'text', 'id' => 'company', 'value' => $company_name->name]); ?>
        </div>
      </div>
    </fieldset>
    <div class="button-wrapper">
      <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    </div>

    <?= $this->Form->end() ?>
  </div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    //AutoComplete
    var companies = '<?php echo $companies ?>';
    var source = companies != '' ? JSON.parse(companies) : [];
    $('#company').autocomplete({
      source: source
    });

    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', '<?=$project->end_date != null ? $project->end_date->format('Y-m-d'):$project->end_date?>');
    $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', '<?=$project->start_date != null ? $project->start_date->format('Y-m-d'):$project->start_date?>');
  });
</script>
