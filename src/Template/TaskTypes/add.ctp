<div class="col-md-12">
    <?= $this->Form->create($taskType) ?>
    <fieldset>
        <legend><?= __('Nuevo tipo de tarea') ?></legend>
        <?php
            echo $this->Form->control('type', ['label' => __('Tipo de tarea')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
