<div class="col-md-12">
    <?= $this->Form->create($tax) ?>
    <fieldset>
        <legend><?= __('Nuevo impuesto') ?></legend>
        <?php
            echo $this->Form->control('tax_value', ['label' => __('Impuesto')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
