<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Organizations $organization
  */
  $pageTitle = $organization->name;
?>
<div class="row quote-form-container">
  <div class="col-md-6">
    <div class="add-forms-container">
      <?= $this->Form->create($organization, array('url' =>array('controller'=>'Organizations', 'action'=>'edit'), 'enctype'=>'multipart/form-data')) ?>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('company_number', ['label' => __('Cédula jurídica')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('country', ['label' => __('País'), 'options' => $countries, 'default' => $organization->country]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php echo $this->Form->control('address', ['label' => __('Dirección')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('phone', ['label' => __('Número de contacto')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('email', ['label' => __('Email')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('logo', ['label' => 'Logo', 'type' => 'file']);
                echo $this->Form->control('logo_dir', ['type' => 'hidden', 'value' => $organization->logo]);
          ?>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
  <div class="col-md-6">
    <div class="add-forms-container">
      <h4 style="display: inline-block;"><?= __('Bancos') ?></h4>
      <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i></button>
      <div class="table-responsive">
        <table class="table table-responsive table-striped" id="table_banks">
          <thead>
            <tr>
              <th><?=__('Banco')?></th>
              <th><?=__('Número de cuenta')?></th>
              <th><?=__('Dirección')?></th>
              <th><?=__('SWIFT')?></th>
              <th><?=__('ABA')?></th>
              <th><?=__('Tipo')?></th>
              <th class="actions"><?= __('Acciones') ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($banks as $bank):?>
              <tr>
                <td><?= $bank->name ?></td>
                <td><?= $bank->account ?></td>
                <td><?= $bank->address ?></td>
                <td><?= $bank->swift ?></td>
                <td><?= $bank->aba ?></td>
                <td><?= $bank->type ?></td>
                <td class="actions">
                  <div class="btns-wrapper">
                    <button type="button" class="btn btn-index download-btn" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Banks', 'action' => 'edit', $bank->id])?>" data-target="#editBankModal"><?= __('Editar') ?></button>
                    <?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Banks', 'action' => 'delete', $bank->id], ['class' => 'btn-delete'], ['confirm' => __('¿Seguro que desea eliminar el banco {0}?', $bank->name)]) ?>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=__('Nuevo Banco')?></h5>
      </div>
      <div class="modal-body">
        <?= $this->Form->create('bank', array('url' =>array('controller'=>'Banks', 'action'=>'add'))) ?>
        <fieldset>
            <?php
                echo $this->Form->control('name', ['label' => __('Nombre')]);
                echo $this->Form->control('address', ['label' => __('Dirección'), 'type' => 'textarea', 'maxlength' => '110', 'placeholder' => __('Máximo 110 caracteres'), 'rows' => '2']);
                echo $this->Form->control('swift', ['label' => __('SWIFT')]);
                echo $this->Form->control('iban', ['label' => __('IBAN')]);
                echo $this->Form->control('aba', ['label' => __('ABA')]);
                echo $this->Form->control('type', ['label' => __('Tipo de banco'), 'options' => ['B' => __('Beneficiario'), 'I' => __('Intermediario')]]);
                echo $this->Form->control('organization_id', ['type' => 'hidden', 'value' => $organization->id]);
            ?>
            <div class="row">
                <button type="button" class="btn btn-sm btn-info" onclick="addAccount()" style="float:right;margin-right:15px;"><?=__('Agregar cuenta')?></button>
            </div>
            <div id="accounts">
              <div class="row">
                <div class="col-md-6">
                  <?php echo $this->Form->control('accounts[account_number][]', ['label' => __('Número de cuenta')]);?>
                </div>
                <div class="col-md-6">
                  <?php echo $this->Form->control('accounts[account_type][]', ['label' => __('Tipo'), 'options' => ['international' => __('Internacional'), 'national' => __('Nacional')]])?>
                </div>
              </div>
            </div>
        </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editBankModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="divAdd"></div>
      </div>
      <div class="modal-footer" style="border-top:none;">
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<?= $this->start('scripts'); ?>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('#table_banks').footable({
      "paging": {
  			"enabled": false
  		},
      "filtering": {
        "enabled": false
      }
    });
  });

$('#editBankModal').on('show.bs.modal', function (e) {
    var $trigger = $(e.relatedTarget);
    $('#divAdd').load($trigger.attr('data-bmdSrc'));
})

  function addAccount() {
    var accountItem = '<div class="row"><div class="col-md-6">';
    accountItem += '<?php echo $this->Form->control('accounts[account_number][]', ['label' => __('Número de cuenta')]);?>';
    accountItem += '</div>';
    accountItem += '<div class="col-md-6">';
    accountItem += '<?php echo $this->Form->control('accounts[account_type][]', ['label' => __('Tipo'), 'options' => ['international' => __('Internacional'), 'national' => __('Nacional')]])?>';
    accountItem += '</div></div>';
    $('#accounts').append(accountItem);
  }
</script>
<?= $this->end(); ?>
