<div class="col-md-12">
    <?= $this->Form->create($tag) ?>
    <fieldset>
        <legend><?= __('Nuevo Tag') ?></legend>
        <?php
            echo $this->Form->control('tag', ['label' => __('Etiqueta')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
