<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
  <div class="col-md-12">
    <?= $this->Form->create($template) ?>
    <fieldset>
        <legend><?= __('Editar Plantilla') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Nombre')]);
            echo $this->Form->control('content', ['label' => __('Contenido'), 'type' => 'textarea']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
