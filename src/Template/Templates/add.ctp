<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
  <div class="col-md-12">
    <?= $this->Form->create($template, ['id' => 'form-template']) ?>
    <fieldset>
        <legend><?= __('Agregar Plantilla') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Nombre')]);
            echo $this->Form->control('type', ['label' => __('Tipo'), 'options' => ['message' => __('Mensaje'), 'header' => __('Encabezado')]]);
            echo $this->Form->control('content', ['type' => 'hidden', 'id' => 'content']);
        ?>
        <div id="froalaContent"></div>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#froalaContent').on('froalaEditor.contentChanged froalaEditor.initialized', function (e, editor) {
                          $('#content').val($('#froalaContent').froalaEditor('html.get'));
                        }).froalaEditor({
                          listAdvancedTypes: true,
                          toolbarButtons: ['undo', 'redo', 'bold', 'italic', 'outdent', 'indent', 'clearFormatting', 'fontSize', 'fontFamily', 'formatOL', 'formatUL'],
                          toolbarButtonsMD: ['undo', 'redo', 'bold', 'italic', 'outdent', 'indent', 'clearFormatting', 'fontSize', 'fontFamily','formatOL', 'formatUL'],
                          toolbarButtonsSM: ['undo', 'redo', 'bold', 'italic', 'outdent', 'indent', 'clearFormatting', 'fontSize', 'fontFamily', 'formatOL', 'formatUL'],
                          toolbarButtonsXS: ['undo', 'redo', 'bold', 'italic', 'outdent', 'indent', 'clearFormatting', 'fontSize', 'fontFamily', 'formatOL', 'formatUL'],
                        });
  });
</script>
