<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
  */
?>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><?=__('CRM')?></a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Etiquetas de Compañías') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Tags', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Etiqueta') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tags as $tag): ?>
                    <tr>
                        <td><?= h($tag->tag) ?></td>
                        <td class="actions">
                          <div class="dropdown">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?= __('Acciones'); ?>
                            </button>
                            <ul class="dropdown-menu">
                              <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Tags', 'action' => 'edit', $tag->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                              <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Tags', 'action' => 'delete', $tag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tag->id), 'class' => 'delete-dd']) ?></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Roles de contactos') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Roles', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Roles') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($roles as $role): ?>
                    <tr>
                        <td><?= h($role->name) ?></td>
                        <td class="actions">
                          <div class="dropdown">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?= __('Acciones'); ?>
                            </button>
                            <ul class="dropdown-menu">
                              <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Roles', 'action' => 'edit', $role->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                              <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Roles', 'action' => 'delete', $role->id], ['confirm' => __('Are you sure you want to delete # {0}?', $role->id), 'class' => 'delete-dd']) ?></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Industrias') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'IndustrieSectors', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Industria') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($industries as $industrie): ?>
                    <tr>
                        <td><?= h($industrie->name) ?></td>
                        <td class="actions">
                          <div class="dropdown">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?= __('Acciones'); ?>
                            </button>
                            <ul class="dropdown-menu">
                              <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'IndustrieSectors', 'action' => 'edit', $industrie->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                              <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'IndustrieSectors', 'action' => 'delete', $industrie->id], ['confirm' => __('Are you sure you want to delete # {0}?', $industrie->id), 'class' => 'delete-dd']) ?></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><?=__('Proyectos')?></a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Tipos de tareas') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'TaskTypes', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Tipo de tarea') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tasks as $task): ?>
                      <?php if ($task->id != 6): ?>
                        <tr>
                            <td><?= h($task->type) ?></td>
                            <td class="actions">
                              <div class="dropdown">
                                <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <?= __('Acciones'); ?>
                                </button>
                                <ul class="dropdown-menu">
                                  <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'TaskTypes', 'action' => 'edit', $task->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                                  <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'TaskTypes', 'action' => 'delete', $task->id], ['confirm' => __('Are you sure you want to delete # {0}?', $task->id), 'class' => 'delete-dd']) ?></li>
                                </ul>
                              </div>
                            </td>
                        </tr>
                      <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><?=__('Facturas')?></a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Términos de pago') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'PaymentTerms', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Término de pago') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($payment_terms as $payment): ?>
                    <tr>
                        <td><?= h($payment->name).' días' ?></td>
                        <td class="actions">
                          <div class="dropdown">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?= __('Acciones'); ?>
                            </button>
                            <ul class="dropdown-menu">
                              <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'PaymentTerms', 'action' => 'edit', $payment->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                              <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'PaymentTerms', 'action' => 'delete', $payment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $payment->id), 'class' => 'delete-dd']) ?></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Impuestos') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Tax', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= __('Impuesto') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($taxes as $tax): ?>
                    <tr>
                        <td><?= h($tax->tax_value).' %' ?></td>
                        <td class="actions">
                          <div class="dropdown">
                            <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?= __('Acciones'); ?>
                            </button>
                            <ul class="dropdown-menu">
                              <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Tax', 'action' => 'edit', $tax->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                              <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Tax', 'action' => 'delete', $tax->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tax->id), 'class' => 'delete-dd']) ?></li>
                            </ul>
                          </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Recordatorios') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Templates', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table class="table table-responsive">
              <thead>
                <tr>
                  <th><?=__('Plantilla')?></th>
                  <th><?=__('Acciones')?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($templates as $template): ?>
                  <tr>
                    <td><?=$template->name ?></td>
                    <td class="actions">
                      <div class="dropdown">
                        <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <?= __('Acciones'); ?>
                        </button>
                        <ul class="dropdown-menu">
                          <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Templates', 'action' => 'edit', $template->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                          <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Templates', 'action' => 'delete', $template->id], ['confirm' => __('Are you sure you want to delete # {0}?', $template->id), 'class' => 'delete-dd']) ?></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-6">
            <h5 style="display: inline-block;"><?= __('Assemble customers quotes templates') ?></h5>
            <button type="button" class="btn btn-sm btn-info btn-circle" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Customers', 'action' => 'add'])?>" data-target="#modalAddParametrization"><i class="fa fa-plus" aria-hidden="true"></i></button>
            <table class="table table-responsive">
              <thead>
                <tr>
                  <th><?=__('Customer')?></th>
                  <th><?=__('Master price')?></th>
                  <th><?=__('Resize price')?></th>
                  <th><?=__('Payment Terms')?></th>
                  <th><?=__('Acciones')?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($customersQuotesTemplates as $customer): ?>
                  <tr>
                    <td><?=$customer->name ?></td>
                    <td><?=$customer->master_price ?></td>
                    <td><?=$customer->resize_price ?></td>
                    <td><?=$customer->payment_terms ?></td>
                    <td class="actions">
                      <div class="dropdown">
                        <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <?= __('Acciones'); ?>
                        </button>
                        <ul class="dropdown-menu">
                          <li><button type="button" class="btn-link btn-dd" data-toggle="modal" data-bmdSrc="<?=$this->Url->build(['controller' => 'Customers', 'action' => 'edit', $customer->id])?>" data-target="#modalAddParametrization"><?= __('Editar') ?></button></li>
                          <li><?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Customers', 'action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id), 'class' => 'delete-dd']) ?></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-8">
            <h3><?=__('Plantillas')?></h3>
            <iframe src="<?=$this->request->webroot.'invoices_templates/invoice_template.html'?>" width="600" height="520"></iframe>
          </div>
          <div class="col-md-4">
            <h4><?=__('Editar')?></h4>
            <div class="row">
              <div class="col-md-10">
                <label><?=__('Información:')?> </label>
                <div class="form-group">
                  <?php if ($organization->default_beneficiary_bank != 0): ?>
                    <label><input type="checkbox" name="bB" id="bB" checked><?= __(' Banco beneficiario')?></label>
                    <?= $this->Form->control('bank_id', ['options' => $banksBeneficiary, 'label' => false, 'id' => 'selBanksBeneficiary', 'default' => $organization->default_beneficiary_bank])?>
                  <?php else: ?>
                    <label><input type="checkbox" name="bB" id="bB"><?= __(' Banco beneficiario')?></label>
                    <?= $this->Form->control('bank_id', ['options' => $banksBeneficiary, 'label' => false, 'id' => 'selBanksBeneficiary', 'default' => $organization->default_beneficiary_bank])?>
                  <?php endif; ?>
                </div>
                <div class="form-group">
                  <?php if ($organization->client_beneficiary != 0): ?>
                    <label><input type="checkbox" name="cB" id="cB" checked><?= __(' Cliente beneficiario')?></label>
                  <?php else: ?>
                    <label><input type="checkbox" name="cB" id="cB"><?= __(' Cliente beneficiario')?></label>
                  <?php endif; ?>
                </div>
                <div class="form-group">
                  <?php if ($organization->default_intermediary_bank != 0): ?>
                    <label><input type="checkbox" name="sendNotification" id="ib"value="true" checked><?= __(' Banco intermediario')?></label>
                    <?= $this->Form->control('bank_id', ['options' => $banks, 'label' => false, 'id' => 'selBanks', 'default' => $organization->default_intermediary_bank])?>
                  <?php else: ?>
                    <label><input type="checkbox" name="sendNotification" id="ib"value="true"><?= __(' Banco intermediario')?></label>
                    <?= $this->Form->control('bank_id', ['options' => $banks, 'label' => false, 'disabled', 'id' => 'selBanks', 'default' => $organization->default_intermediary_bank])?>
                  <?php endif; ?>
                </div>
                <div class="form-group">
                  <?= $this->Form->control('notes', ['label' => __('Notas'), 'type' => 'textarea', 'maxlength' => '250', 'placeholder' => __('Máximo 250 caracteres'), 'id' => 'txtNotes', 'value' => $organization->notes_invoices])?>
                </div>
                <button type="button" class="btn btn-sm btn-success" name="button" onclick="configureInvoices()"><?=__('Guardar')?></button>
                <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modalCSS"><?=__('Editar CSS')?></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="modalAddParametrization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div id="divAdd"></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalCSS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?= $this->Form->create('cssFile', array('url' =>array('controller'=>'Parametrization', 'action'=>'editCssFile'))); ?>
      <div class="modal-body">
        <?php
           echo $this->Form->control('cssFile', ['label' => __('Notas'), 'type' => 'textarea', 'value' => $cssFile, 'rows' => '50'])
        ?>
      </div>
      <div class="modal-footer">
        <?=
         $this->Form->button(__('Guardar'));
         $this->Form->end();
        ?>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<?= $this->start('scripts'); ?>
<script>
$('#ib').change(function(){
  if($('#ib').is(':checked')){
    $('#selBanks').prop('disabled', false);
  }
  else{
    $('#selBanks').prop('disabled', true);
  }
});

$('#modalAddParametrization').on('show.bs.modal', function (e) {
    var $trigger = $(e.relatedTarget);
    $('#divAdd').load($trigger.attr('data-bmdSrc'));
})

  function configureInvoices() {
    var intermediaryBank = 0;
    var beneficiaryClient = 0;
    var beneficiaryBank = 0;
    if($('#ib').is(':checked')){
      intermediaryBank = $('#selBanks').val();
    }
    if($('#bB').is(':checked')){
      console.log('Works');
      beneficiaryBank = $('#selBanksBeneficiary').val();
    }
    if($('#cB').is(':checked')){
      beneficiaryClient = 1;
    }

    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?=$this->Url->build(['controller' => 'Parametrization', 'action' => 'configureInvoices'])?>',
      data: {
        intermediaryBank: intermediaryBank,
        beneficiaryBank: beneficiaryBank,
        beneficiaryClient: beneficiaryClient,
        notes: $('#txtNotes').val()
      },
      success: function(data){
        location.reload();
      }
    });
  }
</script>
<?= $this->end(); ?>
