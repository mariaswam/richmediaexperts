<?php
$cakeDescription = 'WDPM';
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8" />

  <?= $this->Html->charset() ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      <?= $cakeDescription ?>:
      <?= $this->fetch('title') ?>
  </title>
	<link rel="icon" type="image/png" sizes="32x32" href="http://richmediaexperts.com/favicon-32x32.png">
  <?= $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') ?>
  <?= $this->Html->css('animate.min.css') ?>
	<?= $this->Html->css('custom.css') ?>
	<?= $this->Html->css('fileinput.min.css') ?>
	<?= $this->Html->css('fullcalendar.min.css') ?>
	<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
	<link href='<?php echo $this->request->webroot.'css/fullcalendar.print.min.css'?>' rel='stylesheet' media='print' />
	<?= $this->Html->script('modernizr.js') ?>
	<?= $this->Html->css('dataTables.bootstrap.min.css') ?>
	<?= $this->Html->css('jquery.bootgrid.min.css') ?>
	<?= $this->Html->css('jkanban.min.css') ?>
	<?= $this->Html->css('https://use.fontawesome.com/releases/v5.7.2/css/all.css') ?>
	<?= $this->Html->css('footable.standalone.min.css') ?>
	<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css') ?>
	<?= $this->Html->css('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css') ?>
	<?= $this->Html->css('new_style.css') ?>

	<?= $this->Html->css('https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css') ?>

    <!--     Fonts and icons     -->
  <?= $this->Html->css('pe-icon-7-stroke.css') ?>
	<?= $this->Html->script('moment.min.js') ?>

	<!--   Core JS Files   -->
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js') ?>
	<?= $this->Html->script('https://code.jquery.com/ui/1.12.1/jquery-ui.min.js') ?>
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js') ?>
	<?= $this->Html->script('piexif.min.js') ?>
	<?= $this->Html->script('sortable.min.js') ?>
	<?= $this->Html->script('purify.min.js') ?>
	<?= $this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') ?>
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js') ?>
	<?= $this->Html->script('jkanban.min.js') ?>
	<?= $this->Html->script('fileinput.min.js') ?>

	<?= $this->Html->script('https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js') ?>
</head>
<?php $class = strtolower($this->request->getParam('controller')); ?>
<body class="crm-body">
  <div class="wrapper">
		<?=  $this->element('sidebar'); ?>
    <div class="main-panel page-<?= $class; ?>">
			<?=  $this->element('navbar'); ?>
        <?= $this->Flash->render() ?>
        <div class="content h-100">
            <div class="container-fluid">
                <?= $this->fetch('content') ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>
                </p>
            </div>
        </footer>

    </div>
</div>
<div class="loader-container" id="loader">
	<div class="loader">Loading...</div>
</div>
</body>

	<?= $this->fetch('modalContent') ?>

	<!--  Checkbox, Radio & Switch Plugins -->
	<?= $this->Html->script('chartist.min.js') ?>
	<?= $this->Html->script('custom.js') ?>
	<?= $this->Html->script('fullcalendar.min.js') ?>
	<?= $this->Html->script('datatables.min.js') ?>
	<?= $this->Html->script('jquery.bootgrid.min.js') ?>
	<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js') ?>
	<?= $this->Html->script('https://unpkg.com/sweetalert/dist/sweetalert.min.js') ?>
	<?= $this->Html->script('http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js') ?>
	<?= $this->Html->script('footable.min.js') ?>
	<?= $this->Html->script('chartplugin.min.js') ?>
	<?= $this->fetch('scripts') ?>
</html>
