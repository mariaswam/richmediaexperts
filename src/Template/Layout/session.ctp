<?php
$cakeDescription = 'WDPM';
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8" />

  <?= $this->Html->charset() ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      <?= $cakeDescription ?>:
      <?= $this->fetch('title') ?>
  </title>
  <?= $this->Html->meta('icon') ?>
  <?= $this->Html->css('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') ?>
  <?= $this->Html->css('animate.min.css') ?>
  <?= $this->Html->css('light-bootstrap-dashboard.css') ?>
	<?= $this->Html->css('new_style.css') ?>
	<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css') ?>
    <!--     Fonts and icons     -->
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
  <?= $this->Html->css('pe-icon-7-stroke.css') ?>
	<!--   Core JS Files   -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js') ?>
	<?= $this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') ?>

</head>
<body class="session-body">
  <?= $this->Flash->render('auth') ?>
	<?= $this->fetch('content') ?>
</body>
</html>
