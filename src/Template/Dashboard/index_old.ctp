<style media="screen">
  .content{
    padding: 0 !important;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="marquee">
      <div class="fade-left"></div>
      <div class="fade-right"></div>
      <span class="marquee-message">
        <i class="fa fa-usd fa-lg"></i>
        <b><?=$costsInformation['projectCost']?></b> <?= __('Costos de proyectos último mes') ?>
        <span class="separator">|</span>
        <i class="fa fa-usd fa-lg"></i>
        <b><?=$costsInformation['invoicedCosts']?></b> <?= __('Facturados en el último mes') ?>
      </span>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-sm-12">
    <div class="card">
      <div class="header">
        <h4 class="title"><?= __('¡Para hoy!') ?></h4>
      </div>
      <div class="content">
        <div class="container-fluid" id="tab">
          <ul class="nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#1" data-toggle="tab"><?=__('Proyectos')?></a></li>
            <li><a href="#2" data-toggle="tab"><?=__('Milestones')?></a></li>
            <li><a href="#3" data-toggle="tab"><?=__('Tareas')?></a></li>
            <li><a href="#4" data-toggle="tab"><?=__('Facturas')?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="1">
                <table class="table table-striped table-dashboard">
                  <thead>
                    <th><?= __('Proyecto') ?></th>
                    <th><?= __('Fecha de entrega') ?></th>
                  </thead>
                  <tbody>
                    <?php foreach ($projectsInformation['projectForToday'] as $project):?>
                      <tr>
                        <td><?= $project->name ?></td>
                        <td><?= $project->end_date ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
            <div class="tab-pane" id="2">
              <table class="table table-striped table-dashboard"cellspacing="0"cellpadding="3" >
                <thead>
                  <th><?= __('Milestone') ?></th>
                  <th><?= __('Fecha de entrega') ?></th>
                  <th><?= __('Proyecto') ?></th>
                </thead>
                <tbody>
                  <?php foreach ($milestonesInformation['milestonesForToday'] as $milestone):?>
                    <tr>
                      <td><?= $milestone->name ?></td>
                      <td><?= $milestone->end_date ?></td>
                      <td><?= $milestone->project->name ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="3">
              <table class="table table-striped table-dashboard">
                <thead>
                  <th><?= __('Tarea') ?></th>
                  <th><?= __('Fecha de entrega') ?></th>
                  <th><?= __('Proyecto') ?></th>
                  <th><?= __('Usuario') ?></th>
                </thead>
                <tbody>
                  <?php foreach ($tasksInformation['tasksForToday'] as $task):?>
                    <tr>
                      <td><?= $task->name ?></td>
                      <td><?= $task->end_date ?></td>
                      <td><?= $task->milestone->project->name ?></td>
                      <td><?= $task->user->username ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <div class="tab-pane" id="4">
              <table class="table table-striped table-dashboard">
                <thead>
                  <th><?= __('Factura') ?></th>
                  <th><?= __('Monto total') ?></th>
                  <th><?= __('Monto pendiente') ?></th>
                  <th><?= __('Proyecto') ?></th>
                </thead>
                <tbody>
                  <?php foreach ($invoicesInformation['invoicesForToday'] as $invoice):?>
                    <tr>
                      <td><?= $invoice->id ?></td>
                      <td><?= $invoice->total_amount ?></td>
                      <td><?= $invoice->outstanding_amount ?></td>
                      <td><?= $invoice->project->name ?></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h4 class="title"><?= __('¡Todo el trabajo!') ?></h4>
      </div>
      <div class="content">
        <div id="kb"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h4 class="title" style="text-align:center;"><?= __('¡Mis tareas!') ?></h4>
      </div>
      <div class="content">
        <div id="kbMyTasks"></div>
      </div>
    </div>
  </div>
</div>
<div class="row pop-up">
  <div class="col-md-12">
    <span><i class="fa fa-times" aria-hidden="true"></i></span>
    <h3><?=__('Descripción')?></h3>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Tarea')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskName"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Descripción')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskDescription"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Milestone')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskMilestone"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Proyecto')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskProject"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Asignado a')?></p>
          </div>
          <div class="col-md5">
            <p id="pResourceTask"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Hora de entrega')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskFinishHour"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Comentarios')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskComments"></p>
          </div>
      </div>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="modalComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Retroalimentación')?></h4>
      </div>
      <div class="modal-body">
        <?= $this->Form->create('task', array('url' => array('controller' => 'Tasks', 'action' => 'sendFeedbackTask'))) ?>
        <fieldset>
          <?php
            echo $this->Form->control('task_id', ['type' => 'hidden', 'id' => 'taskId']);
            echo $this->Form->control('comments', ['type' => 'textarea', 'label' => __('Comentarios')]);
          ?>
        </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <?= $this->Form->button(__('Enviar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalEndDate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Fecha de finalización')?></h4>
      </div>
      <div class="alert alert-info" id="alertDuration"></div>
      <?= $this->Form->create('task', array('url' =>array('controller'=>'Tasks', 'action'=>'reopenTask'))) ?>
      <div class="modal-body" id="modalBodyInvoice">
        <?php
          echo $this->Form->control('task_id', ['type' => 'hidden', 'id' => 'taskIdReopen']);
          echo $this->Form->control('duration', ['empty' => true, 'label' => __('Duración')]);
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<script>
var pendingTasks = '<?php echo $pendingTasks ?>';
var pendingApprovedTasks = '<?php echo $pendingApprovedTasks ?>';
var finishTasks = '<?php echo $finishTasks; ?>';
var myPendingTasks = '<?php echo $myTasks; ?>';
var myFinishTasks = '<?php echo $myFinishTasks; ?>';
var sourceItem;
var sourceBoard;

$(document).ready(function() {
  initCharts();
  $('.pop-up div span, .pop-up').click(function() {
    $('.pop-up').fadeOut(300);
  });
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust().responsive.recalc();
  });

  $('#modalComments').on('hidden.bs.modal', function () {
      returnItemToSource($('#taskId').val(), sourceItem);
  });

  $('#modalEndDate').on('hidden.bs.modal', function () {
    if (sourceBoard == 'kbMyTasks') {
      returnItemToSourceMyTasks($('#taskIdReopen').val(), sourceItem);
    }
    else {
      returnItemToSource($('#taskIdReopen').val(), sourceItem);
    }
  });

});
  var kanban = new jKanban({
    element         : '#kb',
    gutter          : '15px',
    widthBoard      : '275px',
    boards          : [
      {
          "id" : "todo",
          "title"  : "<?=__('Pendiente')?>",
          "class" : "info",
          "item"  : JSON.parse(pendingTasks)
      },
      {
          "id" : "completed",
          "title"  : "<?=__('Completado')?>",
          "class" : "warning",
          "item"  : JSON.parse(pendingApprovedTasks)
      },
      {
          "id" : "done",
          "title"  : "<?=__('Aprobado')?>",
          "class" : "success",
          "item"  : JSON.parse(finishTasks)
      }
    ],
    dragendEl: function (el) {
      var action = $(el).parent().parent().attr('data-id');
      var taskID = $(el).attr('data-eid');
      if (action == 'todo' && sourceItem == 'completed') {
        $('#taskId').val(taskID);
        $('#modalComments').modal('show');
      }
      else if (action == 'completed') {
        switch (sourceItem) {
          case 'todo':
            sendTaskForApproval(taskID);
            break;
          case 'done':
          var date = $(el).attr('data-date');
          var duration = $(el).attr('data-duration');
          compareDates(taskID, date, duration);
            break;
        }
      }
      else {
        if (sourceItem == 'todo') {
          returnItemToSource(taskID, 'todo');
        }
        else if(sourceItem == 'completed'){
          var endDate = new Date();
          var date = endDate.getFullYear()+'-'+('0' + (endDate.getMonth()+1)).slice(-2)+'-'+('0' + (endDate.getDate())).slice(-2)+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2);
          finishTask(date, taskID);
        }
        else{
          returnItemToSource(taskID, sourceItem);
        }
      }
    },
    dragEl: function(el, source) {
      sourceItem = $(source).parent().attr('data-id');
      sourceBoard = $(source).parent().parent().parent().attr('id');
    },
    click: function(el) {
      getTaskInformation($(el).attr('data-eid'));
    }
  });

  var kanbanMyTasks = new jKanban({
    element         : '#kbMyTasks',
    gutter          : '15px',
    widthBoard      : '400px',
    boards          : [
      {
          "id" : "todo",
          "title"  : "<?=__('Pendiente')?>",
          "class" : "info",
          "item"  : JSON.parse(myPendingTasks)
      },
      {
          "id" : "completed",
          "title"  : "<?=__('Completado')?>",
          "class" : "warning",
          "item"  : JSON.parse(myFinishTasks)
      }
    ],
    dragendEl: function (el) {
      var action = $(el).parent().parent().attr('data-id');
      var taskID = $(el).attr('data-eid');
      if (action == 'todo' && sourceItem == 'completed') {
        var date = $(el).attr('data-date');
        var duration = $(el).attr('data-duration');
        compareDates(taskID, date, duration);
      }
      else if (action == 'completed') {
        switch (sourceItem) {
          case 'todo':
            var endDate = new Date();
            var date = endDate.getFullYear()+'-'+('0' + (endDate.getMonth()+1)).slice(-2)+'-'+('0' + (endDate.getDate())).slice(-2)+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2);
            finishTask(date, taskID);
            break;
        }
      }
    },
    dragEl: function(el, source) {
      sourceItem = $(source).parent().attr('data-id');
      sourceBoard = $(source).parent().parent().parent().attr('id');
      console.log(sourceBoard);
    },
    click: function(el) {
      getTaskInformation($(el).attr('data-eid'));
    }
  });

  function compareDates(taskID, taskDate, duration){
    var date = new Date();
    var today = date.getDate()+'/'+('0'+(date.getMonth()+1)).slice(-2)+'/'+date.getFullYear()+' '+date.getHours()+':'+('0'+date.getMinutes()).slice(-2);
    if (taskDate < today) {
      if (confirm('<?=__('La hora de entrega de la tarea ya pasó. ¿Desea agregar nuevamente la duración?')?>')) {
        $('#taskIdReopen').val(taskID);
        var endDate = new Date(taskDate);
        var formatDate = ('0' + (endDate.getDate())).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2)+'/'+endDate.getFullYear()+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2)
        $('#alertDuration').text('<?=__('La hora de la entrega de la tarea era de: ')?>'+formatDate+'<?=__('. Con una duración de: ')?>'+duration+'<?=__(' horas')?>');
        $('#modalEndDate').modal('show');
      }
      else {
        console.log(sourceBoard);
        if (sourceBoard == 'kbMyTasks') {
          returnItemToSourceMyTasks(taskID, sourceItem);
        }
        else {
          returnItemToSource(taskID, sourceItem);
        }
      }
    }
    else{
      reopenTask(taskID);
    }
  }

  function returnItemToSourceMyTasks(id, source) {
    var item = kanbanMyTasks.findElement(id);
    var itemAdd = {
      id: $(item).attr('data-eid'),
      title: $(item).text(),
      date: $(item).attr('data-date'),
      duration: $(item).attr('data-duration')
    }
    kanbanMyTasks.removeElement(id);
    kanbanMyTasks.addElement(source, itemAdd);
  }

  function initCharts() {
    var ctx = document.getElementById('bigDashboardChart').getContext("2d");
    chartColor = "#FFFFFF";

    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
            datasets: [{
                label: "Data",
                borderColor: chartColor,
                pointBorderColor: chartColor,
                pointBackgroundColor: "#1e3d60",
                pointHoverBackgroundColor: "#1e3d60",
                pointHoverBorderColor: chartColor,
                pointBorderWidth: 1,
                pointHoverRadius: 7,
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                fill: true,
                backgroundColor: gradientFill,
                borderWidth: 2,
                data: [50, 150, 100, 190, 130, 90, 150, 160, 120, 140, 190, 95]
            }]
        },
        options: {
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 0,
                    bottom: 0
                }
            },
            maintainAspectRatio: false,
            tooltips: {
              backgroundColor: '#fff',
              titleFontColor: '#333',
              bodyFontColor: '#666',
              bodySpacing: 4,
              xPadding: 12,
              mode: "nearest",
              intersect: 0,
              position: "nearest"
            },
            legend: {
                position: "bottom",
                fillStyle: "#FFF",
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 10
                    },
                    gridLines: {
                        drawTicks: true,
                        drawBorder: false,
                        display: true,
                        color: "rgba(255,255,255,0.1)",
                        zeroLineColor: "transparent"
                    }

                }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: false,

                    },
                    ticks: {
                        padding: 10,
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
  }
</script>
