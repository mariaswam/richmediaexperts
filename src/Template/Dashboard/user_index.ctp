<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h4 class="title" style="text-align:center;"><?= __('¡Para hoy!') ?></h4>
      </div>
      <div class="content">
        <div class="container-fluid" id="tab">
          <ul class="nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#1" data-toggle="tab"><?=__('Milestones')?></a></li>
            <li><a href="#2" data-toggle="tab"><?=__('Tareas')?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="1">
              <table class="table table-dashboard">
                <thead>
                  <tr>
                    <th><?= __('Milestone') ?></th>
                    <th><?= __('Proyecto') ?></th>
                  </tr>
                  <tbody>
                    <?php foreach ($milestonesInformation['milestonesForToday'] as $milestone): ?>
                      <tr>
                        <td><?= $milestone->name ?></td>
                        <td><?= $milestone->project->name ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </thead>
              </table>
            </div>
            <div class="tab-pane" id="2">
              <table class="table table-dashboard">
                <thead>
                  <tr>
                    <th><?= __('Tarea') ?></th>
                    <th><?= __('Milestone') ?></th>
                    <th><?= __('Proyecto') ?></th>
                  </tr>
                  <tbody>
                    <?php foreach ($tasksInformation['tasksForToday'] as $task): ?>
                      <tr>
                        <td><?= $task->name ?></td>
                        <td><?= $task->milestone->name ?></td>
                        <td><?= $task->milestone->project->name ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div id="kb"></div>
  </div>
</div>
<div class="row pop-up">
  <div class="col-md-12">
    <span><i class="fa fa-times" aria-hidden="true"></i></span>
    <h3><?=__('Descripción')?></h3>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Tarea')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskName"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Descripción')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskDescription"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Milestone')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskMilestone"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Proyecto')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskProject"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Asignado a')?></p>
          </div>
          <div class="col-md5">
            <p id="pResourceTask"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Hora de entrega')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskFinishHour"></p>
          </div>
      </div>
      <div class="row">
          <div class="col-md-5">
            <p><?=__('Comentarios')?></p>
          </div>
          <div class="col-md5">
            <p id="pTaskComments"></p>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
var pendingTasks = '<?php echo $pendingTasks ?>';
var pendingApprovedTasks = '<?php echo $pendingApprovedTasks ?>';
var finishTasks = '<?php echo $finishTasks; ?>';
var sourceItem;

$(document).ready(function() {
  $('.pop-up div span, .pop-up').click(function() {
    $('.pop-up').fadeOut(300);
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    $($.fn.dataTable.tables(true)).DataTable()
       .columns.adjust()
       .responsive.recalc();
  });
});

var kanban = new jKanban({
  element         : '#kb',
  gutter          : '15px',
  widthBoard      : '275px',
  boards          : [
    {
        "id" : "todo",
        "title"  : "<?=__('Pendiente')?>",
        "class" : "info",
        "item"  : JSON.parse(pendingTasks)
    },
    {
        "id" : "completed",
        "title"  : "<?=__('Completado')?>",
        "class" : "warning",
        "item"  : JSON.parse(pendingApprovedTasks)
    },
    {
        "id" : "done",
        "title"  : "<?=__('Aprobado')?>",
        "class" : "success",
        "item"  : JSON.parse(finishTasks)
    }
  ],
  dragendEl: function (el) {
    var action = $(el).parent().parent().attr('data-id');
    var taskID = $(el).attr('data-eid');
    if (action == 'todo') {
      returnItemToSource(taskID, sourceItem);
    }
    else if (action == 'completed') {
      switch (sourceItem) {
        case 'todo':
          sendTaskForApproval(taskID);
          break;
        case 'done':
          returnItemToSource(taskID, sourceItem);
      }
    }
    else {
      returnItemToSource(taskID, sourceItem);
    }
  },
  dragEl: function(el, source) {
    sourceItem = $(source).parent().attr('data-id');
  },
  click: function(el) {
    getTaskInformation($(el).attr('data-eid'));
  }
});

</script>
