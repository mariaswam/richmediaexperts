<div class="row dashboard-content">
  <div class="col-md-12 col-sm-12">

        <ul class="nav nav-tabs" data-tabs="tabs">
          <li class="active"><a href="#general" data-toggle="tab"><?=__('General')?></a></li>
          <li><a href="#today" data-toggle="tab"><?=__('Hoy')?></a></li>
        </ul>
        <div class="tab-content">
          <!-- TAB General -->
          <div class="tab-pane active" id="general">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="row info-dash">
                  <div class="col-md-4">
                    <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                  </div>
                  <div class="col-md-4">
                    <span><?=$costsInformation['invoicedCosts']?></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /TAB General -->
          <!-- TAB Today -->
          <div class="tab-pane" id="today">
            <div class="row dash-tab-content">
              <!-- Projects -->
              <div class="col-md-4 col-sm-12">
                <div class="card card-dashboard">
                  <div class="card-body">
                    <h5 class="card-title">Proyectos</h5>
                    <table class="table table-striped">
                      <thead>
                        <th><?= __('Proyecto') ?></th>
                        <th><?= __('Fecha de entrega') ?></th>
                      </thead>
                      <tbody>
                        <?php foreach ($projectsInformation['projectForToday'] as $project):?>
                          <tr>
                            <td><?= $project->name ?></td>
                            <td><?= $project->end_date ?></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /Projects -->
              <!-- Invoices -->
              <div class="col-md-4 col-sm-12">
                <div class="card card-dashboard">
                  <div class="card-body">
                    <h5 class="card-title">Facturas</h5>
                  </div>
                </div>
              </div>
              <!-- /Invoices -->
              <!-- Tasks -->
              <div class="col-md-4 col-sm-12">
                <div class="card card-dashboard">
                  <div class="card-body">
                    <h5 class="card-title">Tareas</h5>
                  </div>
                </div>
              </div>
              <!-- /Tasks -->
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card card-dashboard">
                  <div class="header">
                    <h4 class="title"><?= __('¡Todo el trabajo!') ?></h4>
                  </div>
                  <div class="content">
                    <div id="kb"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /TAB Today -->
        </div>
      </div>
  </div>
  <script>
  var pendingTasks = '<?php echo $pendingTasks ?>';
  var pendingApprovedTasks = '<?php echo $pendingApprovedTasks ?>';
  var finishTasks = '<?php echo $finishTasks; ?>';
  var myPendingTasks = '<?php echo $myTasks; ?>';
  var myFinishTasks = '<?php echo $myFinishTasks; ?>';
  var sourceItem;
  var sourceBoard;

  $(document).ready(function() {
    initCharts();
    $('.pop-up div span, .pop-up').click(function() {
      $('.pop-up').fadeOut(300);
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust().responsive.recalc();
    });

    $('#modalComments').on('hidden.bs.modal', function () {
        returnItemToSource($('#taskId').val(), sourceItem);
    });

    $('#modalEndDate').on('hidden.bs.modal', function () {
      if (sourceBoard == 'kbMyTasks') {
        returnItemToSourceMyTasks($('#taskIdReopen').val(), sourceItem);
      }
      else {
        returnItemToSource($('#taskIdReopen').val(), sourceItem);
      }
    });

  });
    var kanban = new jKanban({
      element         : '#kb',
      gutter          : '15px',
      widthBoard      : '275px',
      boards          : [
        {
            "id" : "todo",
            "title"  : "<?=__('Pendiente')?>",
            "class" : "info",
            "item"  : JSON.parse(pendingTasks)
        },
        {
            "id" : "completed",
            "title"  : "<?=__('Completado')?>",
            "class" : "info",
            "item"  : JSON.parse(pendingApprovedTasks)
        },
        {
            "id" : "done",
            "title"  : "<?=__('Aprobado')?>",
            "class" : "info",
            "item"  : JSON.parse(finishTasks)
        }
      ],
      dragendEl: function (el) {
        var action = $(el).parent().parent().attr('data-id');
        var taskID = $(el).attr('data-eid');
        if (action == 'todo' && sourceItem == 'completed') {
          $('#taskId').val(taskID);
          $('#modalComments').modal('show');
        }
        else if (action == 'completed') {
          switch (sourceItem) {
            case 'todo':
              sendTaskForApproval(taskID);
              break;
            case 'done':
            var date = $(el).attr('data-date');
            var duration = $(el).attr('data-duration');
            compareDates(taskID, date, duration);
              break;
          }
        }
        else {
          if (sourceItem == 'todo') {
            returnItemToSource(taskID, 'todo');
          }
          else if(sourceItem == 'completed'){
            var endDate = new Date();
            var date = endDate.getFullYear()+'-'+('0' + (endDate.getMonth()+1)).slice(-2)+'-'+('0' + (endDate.getDate())).slice(-2)+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2);
            finishTask(date, taskID);
          }
          else{
            returnItemToSource(taskID, sourceItem);
          }
        }
      },
      dragEl: function(el, source) {
        sourceItem = $(source).parent().attr('data-id');
        sourceBoard = $(source).parent().parent().parent().attr('id');
      },
      click: function(el) {
        getTaskInformation($(el).attr('data-eid'));
      }
    });

    var kanbanMyTasks = new jKanban({
      element         : '#kbMyTasks',
      gutter          : '15px',
      widthBoard      : '400px',
      boards          : [
        {
            "id" : "todo",
            "title"  : "<?=__('Pendiente')?>",
            "class" : "info",
            "item"  : JSON.parse(myPendingTasks)
        },
        {
            "id" : "completed",
            "title"  : "<?=__('Completado')?>",
            "class" : "warning",
            "item"  : JSON.parse(myFinishTasks)
        }
      ],
      dragendEl: function (el) {
        var action = $(el).parent().parent().attr('data-id');
        var taskID = $(el).attr('data-eid');
        if (action == 'todo' && sourceItem == 'completed') {
          var date = $(el).attr('data-date');
          var duration = $(el).attr('data-duration');
          compareDates(taskID, date, duration);
        }
        else if (action == 'completed') {
          switch (sourceItem) {
            case 'todo':
              var endDate = new Date();
              var date = endDate.getFullYear()+'-'+('0' + (endDate.getMonth()+1)).slice(-2)+'-'+('0' + (endDate.getDate())).slice(-2)+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2);
              finishTask(date, taskID);
              break;
          }
        }
      },
      dragEl: function(el, source) {
        sourceItem = $(source).parent().attr('data-id');
        sourceBoard = $(source).parent().parent().parent().attr('id');
        console.log(sourceBoard);
      },
      click: function(el) {
        getTaskInformation($(el).attr('data-eid'));
      }
    });

    function compareDates(taskID, taskDate, duration){
      var date = new Date();
      var today = date.getDate()+'/'+('0'+(date.getMonth()+1)).slice(-2)+'/'+date.getFullYear()+' '+date.getHours()+':'+('0'+date.getMinutes()).slice(-2);
      if (taskDate < today) {
        if (confirm('<?=__('La hora de entrega de la tarea ya pasó. ¿Desea agregar nuevamente la duración?')?>')) {
          $('#taskIdReopen').val(taskID);
          var endDate = new Date(taskDate);
          var formatDate = ('0' + (endDate.getDate())).slice(-2)+'/'+('0' + (endDate.getMonth()+1)).slice(-2)+'/'+endDate.getFullYear()+' '+endDate.getHours()+':'+('0' + (endDate.getMinutes())).slice(-2)
          $('#alertDuration').text('<?=__('La hora de la entrega de la tarea era de: ')?>'+formatDate+'<?=__('. Con una duración de: ')?>'+duration+'<?=__(' horas')?>');
          $('#modalEndDate').modal('show');
        }
        else {
          console.log(sourceBoard);
          if (sourceBoard == 'kbMyTasks') {
            returnItemToSourceMyTasks(taskID, sourceItem);
          }
          else {
            returnItemToSource(taskID, sourceItem);
          }
        }
      }
      else{
        reopenTask(taskID);
      }
    }

    function returnItemToSourceMyTasks(id, source) {
      var item = kanbanMyTasks.findElement(id);
      var itemAdd = {
        id: $(item).attr('data-eid'),
        title: $(item).text(),
        date: $(item).attr('data-date'),
        duration: $(item).attr('data-duration')
      }
      kanbanMyTasks.removeElement(id);
      kanbanMyTasks.addElement(source, itemAdd);
    }

    function initCharts() {
      var ctx = document.getElementById('bigDashboardChart').getContext("2d");
      chartColor = "#FFFFFF";

      var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
      gradientStroke.addColorStop(0, '#80b6f4');
      gradientStroke.addColorStop(1, chartColor);

      var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
      gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
      gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
              datasets: [{
                  label: "Data",
                  borderColor: chartColor,
                  pointBorderColor: chartColor,
                  pointBackgroundColor: "#1e3d60",
                  pointHoverBackgroundColor: "#1e3d60",
                  pointHoverBorderColor: chartColor,
                  pointBorderWidth: 1,
                  pointHoverRadius: 7,
                  pointHoverBorderWidth: 2,
                  pointRadius: 5,
                  fill: true,
                  backgroundColor: gradientFill,
                  borderWidth: 2,
                  data: [50, 150, 100, 190, 130, 90, 150, 160, 120, 140, 190, 95]
              }]
          },
          options: {
              layout: {
                  padding: {
                      left: 20,
                      right: 20,
                      top: 0,
                      bottom: 0
                  }
              },
              maintainAspectRatio: false,
              tooltips: {
                backgroundColor: '#fff',
                titleFontColor: '#333',
                bodyFontColor: '#666',
                bodySpacing: 4,
                xPadding: 12,
                mode: "nearest",
                intersect: 0,
                position: "nearest"
              },
              legend: {
                  position: "bottom",
                  fillStyle: "#FFF",
                  display: false
              },
              scales: {
                  yAxes: [{
                      ticks: {
                          fontColor: "rgba(255,255,255,0.4)",
                          fontStyle: "bold",
                          beginAtZero: true,
                          maxTicksLimit: 5,
                          padding: 10
                      },
                      gridLines: {
                          drawTicks: true,
                          drawBorder: false,
                          display: true,
                          color: "rgba(255,255,255,0.1)",
                          zeroLineColor: "transparent"
                      }

                  }],
                  xAxes: [{
                      gridLines: {
                          zeroLineColor: "transparent",
                          display: false,

                      },
                      ticks: {
                          padding: 10,
                          fontColor: "rgba(255,255,255,0.4)",
                          fontStyle: "bold"
                      }
                  }]
              }
          }
      });
    }
  </script>
