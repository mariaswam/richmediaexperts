<div class="col-md-12">
    <?= $this->Form->create($paymentTerm) ?>
    <fieldset>
        <legend><?= __('Nuevo término de pago') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Término de pago')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
