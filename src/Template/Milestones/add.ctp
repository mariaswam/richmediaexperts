<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Nuevo Milestone');
  $projects = json_encode( $projects );
?>
<div class="row form-containers">
  <div class="col-md-12 col-sm-12">
    <?= $this->Form->create($milestone) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('project_name', ['label' => __('Proyectos'), 'type' => 'text', 'id' => 'project']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('users._ids', ['options' => $users, 'label' => __('Usuarios asignados')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <button type="button" class="btn btn-success" id="btnAddTask"><?=__('Agregar Tarea')?></button><br>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div id="form-task"></div>
        </div>
      </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>

<?php $this->start('scripts');?>
<script>
var count = 0;
$(document).ready(function(){
  $('#page-title').text('<?php echo $pageTitle; ?>');

  //AutoComplete
  var projects = '<?php echo $projects ?>';
  var source = projects != '' ? JSON.parse(projects) : [];
  $('#project').autocomplete({
    source: source
  });

  //Datepcikers
  $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'});
  $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'});
  $("#btnAddTask").click(function(){
    var formText = '<div class="col-md-6 formMilestone" id="div'+count+'">'
    formText += '<legend style="display: inline-block;"><?=__('Nueva Tarea')?>'
    formText += '<button type="button" class="btn btn-sm btn-danger btn-circle close" id="'+count+'" onclick="eliminarDiv(this)"><i class="fa fa-times" aria-hidden="true"></i></button>'
    formText += '</legend>';
    formText += '<?= $this->Form->control('tasksArray[name][]', ['label' => __('Nombre de la tarea')]); ?> ';
    formText += '<?= $this->Form->control('tasksArray[description][]', ['label' => __('Descripción'), 'type' => 'textarea']); ?> ';
    formText += '<?php echo $this->Form->control('attachments[]', ['label' => 'Adjuntar archivos', 'type' => 'file', 'id' => 'attachments', 'multiple' => true]); ?>';
    formText += '<div class="form-group"><label for="end_date"><?=__('Fecha de entrega')?></label><input name="tasksArray[end_date][]" type="text" data-provide="datepicker" class="form-control startDatePickerTasks"></div>';
    formText += '<?= $this->Form->control('tasksArray[priority][]', ['label' => __('Prioridad'), 'options' => ['A' => 'Alta', 'M' => 'Media', 'B' => 'Baja']]); ?> ';
    formText += '<?= $this->Form->control('tasksArray[task_type][]', ['options' => $taskTypes, 'label' => __('Tipo de tarea')]) ?>';
    formText += '<?= $this->Form->control('tasksArray[user][]', ['options' => $users, 'label' => __('Usuario asignado')]); ?>';
    formText += '</div>';
    $('#form-task').append(formText);
    $('.startDatePickerTasks').datetimepicker({format: 'y-m-d H:i'});
    count++;
  });
});

function eliminarDiv(val){
  $('#div'+val.id).remove();
}
</script>
<?php $this->end();?>
