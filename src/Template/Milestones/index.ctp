<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Milestone[]|\Cake\Collection\CollectionInterface $milestones
  */
  $pageTitle = __('Milestones');
  $projects = json_encode( $projects );
?>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div role="alert" class="alert alert-dimissible alert-success" id="divFinishMilestone" style="display:none;">¡Se ha finalizado el milestone correctamente!</div>
  </div>
</div>
<div class="row">
  <div class="col-md-2">
    <?= $this->Form->control('project_id', ['label' => __('Proyecto'), 'id' => 'project_name', 'type' => 'text', 'onchange' => 'getMilestonesProjectIndex(this.value)']); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <table cellpadding="0" cellspacing="0" class="table table-striped" id="tbMilestones">
        <thead>
            <tr>
                <th id="name"><?= __('Nombre') ?></th>
                <th id="start_date"><?= __('Fecha de inicio') ?></th>
                <th id="end_date"><?= __('Fecha de fin') ?></th>
                <th id="project"><?= __('Proyecto') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody class="table-milestones" id="tableMilestones">
        </tbody>
    </table>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="modalEnDate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Fecha de finalización')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <?php
          echo $this->Form->control('task_id', ['type' => 'hidden', 'id' => 'milestoneId']);
        ?>
          <div class="form-group">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <button type="button" class="btn btn-secondary" id="saveEndDate"><?=__('Finalizar')?></button>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<div class="zoom">
  <a class="zoom-fab zoom-btn-large" id="zoomBtn" href="milestones/add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i></a>
</div>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');

    //AutoComplete
    var projects = '<?php echo $projects ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#project_name').autocomplete({
      source: source
    });

    //Datepickers
    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'});

    //Finish Milestone
    $('#saveEndDate').click(function(){
      var endDate = $('select[name="end_date[year]"]').val()+"-"+$('select[name="end_date[month]"]').val()+"-"+$('select[name="end_date[day]"]').val()+" "+$('select[name="end_date[hour]"]').val()+":"+$('select[name="end_date[minute]"]').val();
      finishMilestone(endDate, $('#milestoneId').val());
    });

    //Load session storage
    if (sessionStorage.projectName) {
      getMilestonesProjectIndex(sessionStorage.getItem('projectName'));
      $('#project_name').val(sessionStorage.getItem('projectName'));
    }

    //DT
    $('#tbMilestones').DataTable({
      'searching': false
    });

  });

  function deleteMilestone(value){
    if(confirm('<?=__('¿Seguro que desea eliminar el milestone?')?>')){
        document.getElementById('formDeleteMilestone'+value).submit();
    }
  }

  function getMilestonesProjectIndex(projectName){
    var getMilestonesUrl = "<?= $this->Url->build(['controller' => 'Milestones', 'action' => 'getMilestonesProject'])?>";
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: getMilestonesUrl,
      data: {
        name: projectName
      },
      success: function(data) {
        $('#tableMilestones tr').remove();
        $.each(data.milestones, function(index, value){
          startDate = new Date(value.start_date);
          endDate = new Date(value.end_date);
          start_date = startDate.getUTCDate()+'/'+(startDate.getUTCMonth()+1)+'/'+startDate.getUTCFullYear();
          end_date = endDate.getUTCDate()+'/'+(endDate.getUTCMonth()+1)+'/'+endDate.getUTCFullYear();
          var tableRow = '<tr><td>'+value.name+'</td><td>'+start_date+'</td><td>'+end_date+'</td><td>'+value.project.name+'</td>';
          tableRow += '<td class="actions"><div class="dropdown">';
          tableRow += '<button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= __('Acciones'); ?></button>';
          tableRow += '<ul class="dropdown-menu">';
          tableRow += '<li><a href='+document.URL+'/view/'+value.id+'><?=__('Ver')?></a></li>';
          tableRow += '<li><a href='+document.URL+'/edit/'+value.id+'><?=__('Editar')?></a></li>';
          tableRow += '<li><form action="'+document.URL+'/delete/'+value.id+'" method="POST" id="formDeleteMilestone'+value.id+'"><input type="hidden"></form><a href="javascript:deleteMilestone('+value.id+')"><?=__('Eliminar')?></a></li>';
          if (value.state == 'C') {
            tableRow += '<li><a onclick="reopenMilestone('+value.id+')" id="btnFinishTask"><?=__('Reabrir')?></a></li></ul></div></td></tr>';
          }
          else{
            tableRow += '<li><a onclick="confirmDate('+value.id+')" id="btnFinishTask"><?=__('Finalizar')?></a></li></ul></div></td></tr>';
          }
          $('#tableMilestones').append(tableRow);
        });
        $('#tbMilestones').DataTable();
        if (!sessionStorage.projectName) {
          sessionStorage.setItem('projectName', projectName);
        }
        else {
          sessionStorage.projectName = projectName;
        }
      }
    });
  }

  function reopenMilestone(milestoneID) {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?=$this->Url->build(['controller' => 'Milestones', 'action' => 'reopenMilestone'])?>',
      data: {
        id: milestoneID
      },
      success: function(data){
        location.reload();
      }
    });
  }

  function confirmDate(milestoneID){
    if(!confirm('<?=__('¿El milestone fue finalizado hoy?')?>')){
      $('#milestoneId').val(milestoneID);
      $('#modalEnDate').modal('show');
    }
    else{
      var date = new Date();
      finishMilestone('<?php echo date('Y-m-d') ?>'+' '+date.getHours()+':'+date.getMinutes(), milestoneID);
    }
  }
</script>
