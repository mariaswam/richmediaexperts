<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Editar Milestone');
  $projects = json_encode( $projects );
?>
<div class="row form-containers">
  <div class="col-md-12 col-sm-12">
    <?= $this->Form->create($milestone) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('state', ['label' => __('Estado'), 'options' => ['O' => __('Abierto'), 'C' => __('Cerrado')]]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('project_name', ['label' => __('Proyectos'), 'type' => 'text', 'id' => 'project', 'value' => $project_name->name]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('users._ids', ['options' => $users, 'label' => __('Usuarios asignados')]); ?>
        </div>
      </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    //AutoComplete
    var projects = '<?php echo $projects ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#project').autocomplete({
      source: source
    });

    //Datepickers
    $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', '<?= $milestone->start_date != null ? $milestone->start_date->format('Y-m-d') : $milestone->start_date?>');
    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', '<?= $milestone->end_date != null ? $milestone->end_date->format('Y-m-d') : $milestone->end_date?>');
  });
</script>
