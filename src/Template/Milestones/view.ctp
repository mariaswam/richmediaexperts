<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Milestone $milestone
  */
  $pageTitle = h($milestone->name);
?>
<div class="row">
  <div class="col-md-6 col-sm-12">
    <table class="table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($milestone->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Proyecto') ?></th>
            <td><?= $milestone->has('project') ? $this->Html->link($milestone->project->name, ['controller' => 'Projects', 'action' => 'view', $milestone->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de inicio') ?></th>
            <td><?= h($milestone->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de fin') ?></th>
            <td><?= h($milestone->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= h($milestone->state == 'O' ? __('Abierto') : __('Cerrado')) ?></td>
        </tr>
    </table>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="related">
        <h4><?= __('Usuarios asignados') ?></h4>
        <?php if (!empty($milestone->users)): ?>
        <table cellpadding="0" cellspacing="0" class="table">
            <tr>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
            </tr>
            <?php foreach ($milestone->users as $users): ?>
            <tr>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->first_name) ?></td>
                <td><?= h($users->last_name) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="related">
        <h4><?= __('Tareas del milestone') ?></h4>
        <?php if (!empty($milestone->tasks)): ?>
        <table cellpadding="0" cellspacing="0" class="table">
            <tr>
                <th scope="col"><?= __('Nombre') ?></th>
                <th scope="col"><?= __('Descripción') ?></th>
                <th scope="col"><?= __('Fecha de inicio') ?></th>
                <th scope="col"><?= __('Fecha de fin') ?></th>
                <th scope="col"><?= __('Prioridad') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($milestone->tasks as $tasks): ?>
            <tr>
                <td><?= h($tasks->name) ?></td>
                <td><?= h($tasks->description) ?></td>
                <td><?= h($tasks->start_date) ?></td>
                <td><?= h($tasks->end_date) ?></td>
                <td><?= h($tasks->priority) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Tasks', 'action' => 'view', $tasks->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
