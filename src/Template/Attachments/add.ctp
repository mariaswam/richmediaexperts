<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <?= $this->Form->create($attachment, ['enctype'=>'multipart/form-data']) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('attachments[]', ['label' => 'Adjuntar archivos', 'type' => 'file', 'id' => 'attachments', 'multiple' => true]); ?>
          <?php echo $this->Form->control('attachments[]', ['label' => 'Adjuntar archivos', 'type' => 'hidden']); ?>
        </div>
      </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-secondary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("#attachments").fileinput({'browseOnZoneClick':true, 'showUpload':false});
  });
</script>
