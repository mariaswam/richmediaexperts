<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Company $company
  */
  $pageTitle = h($company->name);
?>

<div class="col-md-12 main-content-wrapper form-wrapper">


    <?= $this->Html->link(__('Back to companies'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>


  <div class="row">
    <div class="col-md-7 col-sm-12">
      <h4><?= __('General Information') ?></h4>
      <table class="vertical-table table table-view small-view">
          <tr>
              <th scope="row"><?= __('Nombre') ?></th>
              <td><?= h($company->name) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Número de contacto') ?></th>
              <td><?= h($company->phone) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('País') ?></th>
              <td><?= h($company->country) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Dirección') ?></th>
              <td><?= h($company->address) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Sector') ?></th>
              <td><?= h($company->sector) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Términos de pago') ?></th>
              <td><?= h($paymentTerm->name).__(' días') ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Usuario administrador') ?></th>
              <td><?= $company->has('user') ? $this->Html->link($company->user->username, ['controller' => 'Users', 'action' => 'view', $company->user->id]) : '' ?></td>
          </tr>
      </table>

      <div class="related contacts-info">
          <h4><?= __('Contactos') ?></h4>
          <?php if (!empty($company->contacts)): ?>

          <div class="table-responsive">


            <table cellpadding="0" cellspacing="0" class="table table-striped footable" id="table-contacts-view">
              <thead>
                <tr class="footable-header">
                    <th scope="col"><?= __('Nombre') ?></th>
                    <th scope="col"><?= __('Apellidos') ?></th>
                    <th scope="col" class="center-col"><?= __('Correo Electrónico') ?></th>
                    <th scope="col" class="center-col"><?= __('Télefono') ?></th>
                    <th scope="col" class="center-col"><?= __('Rol') ?></th>
                    <th scope="col" class="actions"><?= __('Consultar') ?></th>
                </tr>
              </thead>
              <tbody>

                <?php foreach ($company->contacts as $contacts): ?>
                <tr>
                    <td><?= h($contacts->first_name) ?></td>
                    <td><?= h($contacts->last_name) ?></td>
                    <td class="center-col"><?= h($contacts->email) ?></td>
                    <td class="center-col"><?= h($contacts->mobile) ?></td>
                    <td class="center-col"><?= h($contacts->role_id) ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">

                        <?= $this->Html->link(__('Ver'),['controller' => 'Contacts', 'action' => 'view', $contacts->id], ['class' => 'btn-view']) ?>

                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <?php endif; ?>
      </div>
    </div>
    <div class="col-md-5 col-sm-12">
      <div class="row">
        <div class="col-md-12">
          <div class="related">
              <h4><?= __('Projects') ?></h4>
              <?php if (!empty($company->projects)): ?>
              <table cellpadding="0" cellspacing="0" class="table" id="table-projects-view">
                  <thead>
                    <tr>
                      <th scope="col"><?= __('Nombre') ?></th>
                      <th scope="col" class="center-col"><?= __('Presupuesto') ?></th>
                      <th scope="col" class="center-col"><?= __('Costo Total') ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($company->projects as $projects): ?>
                    <tr>
                        <td><?= $this->Html->link($projects->name, ['controller' => 'Projects', 'action' => 'view', $projects->id]); ?></td>
                        <td class="center-col"><?= h($projects->budget) ?></td>
                        <td class="center-col"><?= h($projects->total_cost) ?></td>

                    </tr>
                    <?php endforeach; ?>
                  </tbody>
              </table>
              <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('#table-projects-view').footable({
      "paging": {
  			"enabled": true,
        "size": 16
  		}
    });

    $('#table-contacts-view').footable({
      "paging": {
  			"enabled": true,
        "size": 5
  		}
    });
  });
</script>
