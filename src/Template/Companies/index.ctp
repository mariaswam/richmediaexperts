<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Company[]|\Cake\Collection\CollectionInterface $companies
  */
  $pageTitle = __('Compañías');
?>
<div class="row quotes-container">
  <div class="col-md-12">
    <div class="bg-white-container">
      <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('Add New Company'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
      <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped" id="table_companies">
            <thead>
                <tr>
                    <th><?= __('Nombre') ?></th>
                    <th class="center-col"><?= __('Número de contacto') ?></th>
                    <th class="center-col"><?= __('País') ?></th>
                    <th class="center-col"><?= __('Administrador') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($companies as $company): ?>
                <tr>
                    <td><?= h($company->name) ?></td>
                    <td class="center-col"><?= h($company->phone) ?></td>
                    <td class="center-col"><?= h($company->country) ?></td>
                    <td class="center-col"><?= $company->has('user') ? $this->Html->link($company->user->username, ['controller' => 'Users', 'action' => 'view', $company->user->id]) : '' ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $company->id], ['class' => 'btn-view']) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $company->id], ['class' => 'btn-edit']) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $company->id], ['class' => 'btn-delete'], ['confirm' => __('¿Seguro que desea eliminar la compañía {0}?', $company->name)]) ?>
                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <select class="form-control items-count" name="" id="numberOfRowSelect">
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="100">100</option>
        </select>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('#table_companies').footable({
      "paging": {
  			"enabled": true,
        "size": localStorage.getItem('numberOfRecordsCompanies') != null ? localStorage.getItem('numberOfRecordsCompanies') : 10
  		},
      "filtering": {
        "enabled": true
      }
    });

    if (localStorage.getItem('numberOfRecordsCompanies') != null) {
      $('#numberOfRowSelect').val(localStorage.getItem('numberOfRecordsCompanies'));
    }
    else{
      $('#numberOfRowSelect').val('10');
    }

    $('#numberOfRowSelect').change(function() {
      localStorage.setItem('numberOfRecordsCompanies', $('#numberOfRowSelect').val());
      FooTable.get('#table_companies').pageSize($('#numberOfRowSelect').val());
    });
  });
</script>
