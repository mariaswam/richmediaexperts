<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Nueva Compañía');
?>

  <div class="col-md-12 main-content-wrapper form-wrapper">

      <?= $this->Html->link(__('Back to companies'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($company) ?>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre *')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('phone', ['label' => __('Número de contacto')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('country', ['label' => __('País'), 'options' => $countries]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('sector', ['options' => $sectors]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('address', ['label' => __('Dirección')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('payment_terms', ['label' => __('Términos de pago'), 'options' => $paymentTerms]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('user_id', ['options' => $users, 'label' => __('Usuario administrador')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="checkbox-invoice">
            <label><input type="checkbox" value="true" name="national_invoice"><?= __('Factura nacional?') ?></label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('tags._ids', ['options' => $tags, 'label' => __('Etiquetas')]); ?>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>

<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
