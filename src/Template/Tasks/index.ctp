<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Task[]|\Cake\Collection\CollectionInterface $tasks
  */
  $pageTitle = __('Tareas');
  //$projects = json_encode( $projects );
  $pendingTasks = array();
  $inProgressTasks = array();
  $doneTasks = array();
  foreach ($tasks as $task) {
    switch ($task->state) {
      case 'P':
        array_push($pendingTasks, ['id' => $task->id, 'title' => $task->name]);
      break;
      case 'IP':
        array_push($inProgressTasks, ['id' => $task->id, 'title' => $task->name]);
      break;
      case 'PA':
        array_push($doneTasks, ['id' => $task->id, 'title' => $task->name]);
      break;
    }
  }
  $pendingTasks = json_encode($pendingTasks);
  $inProgressTasks = json_encode($inProgressTasks);
  $doneTasks = json_encode($doneTasks);
?>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="alert alert-success" id="divFinishTask" style="display:none;">¡Se ha finalizado la tarea correctamente!</div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
      <div class="content">
        <div id="kb"></div>
      </div>
  </div>
</div>
<div class="zoom">
  <a class="zoom-fab zoom-btn-large" id="zoomBtn" href="tasks/add"><i class="fa fa-plus fa-lg" aria-hidden="true"></i></a>
</div>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');

    //Datepickers
    $('#endDatePicker').datetimepicker({dateFormat: 'y-m-d H:i'});

    //Finish task
    $('#saveEndDate').click(function(){
      var endDate = $('#endDatePicker').val();
      finishTask(endDate, $('#taskId').val());
    });

    //Load session object
    if (sessionStorage.projectTask) {
      getTasksProject(sessionStorage.getItem('projectTask'));
      $('#selProjects').val(sessionStorage.getItem('projectTask'));
    }
    else {
      getTasksProject($('#selProjects').val());
    }

    $('#tableTasks').dataTable({
      'searching': false
    });
  });

var pendingTasks = '<?php echo $pendingTasks; ?>';
var inProgressTasks = '<?php echo $inProgressTasks ?>';
var doneTasks = '<?php echo $doneTasks ?>';
var sourceItem;
var sourceBoard;
  /**KANBAN FOR TASKS**/
  var kanban = new jKanban({
    element         : '#kb',
    gutter          : '15px',
    widthBoard      : '275px',
    boards          : [
      {
          "id" : "todo",
          "title"  : "<?=__('Pendiente')?>",
          "class" : "info",
          "item"  : JSON.parse(pendingTasks)
      },
      {
          "id" : "in_progress",
          "title"  : "<?=__('En progreso')?>",
          "class" : "info",
          "item"  : JSON.parse(inProgressTasks)
      },
      {
          "id" : "pending_apr",
          "title"  : "<?=__('Pendiente de Aprobación')?>",
          "class" : "info",
          "item"  : JSON.parse(doneTasks)
      }
    ],
    dragendEl: function (el) {
      var action = $(el).parent().parent().attr('data-id');
      var taskID = $(el).attr('data-eid');
      if (action == 'todo' && sourceItem == 'in_progress') {
        //Change status to pending
        changeStatusTask(taskID, 'P');
      }
      else if (action == 'in_progress') {
        //Change status to In progress
        changeStatusTask(taskID, 'IP');
      }
      else {
        if (sourceItem == 'todo') {
          swal('<?php echo __('Ops!') ?>', '<?php echo __('Acción no permitida') ?>', "error");
          returnItemToSource(taskID, 'todo');
        }
        else if(sourceItem == 'in_progress'){
          //Change status to pending_apr
          changeStatusTask(taskID, 'PA');
        }
        else{
          swal('<?php echo __('Ops!') ?>', '<?php echo __('Acción no permitida') ?>', "error");
          returnItemToSource(taskID, sourceItem);
        }
      }
    },
    dragEl: function(el, source) {
      sourceItem = $(source).parent().attr('data-id');
      sourceBoard = $(source).parent().parent().parent().attr('id');
    },
    click: function(el) {
      var taskID = $(el).attr('data-eid');
      location.href = '/WDPM/tasks/view/' + taskID;
    }
  });

  /**Delete task**/
  function deleteTask(){
    swal({
      title: "<?php echo __('¿Seguro que desea eliminar el tarea?') ?>",
      text: "<?php echo __('Esta acción es irreversible') ?>",
      icon: "error",
      buttons: true,
      })
      .then((deleteTask) => {
      if (deleteTask) {
        document.getElementById('formDeleteTask').submit();
      }
      });
  }

  function getTasksProject(projectId){
    var userRole = '<?php echo $user['role'] ?>';
    var getTasksUrl = "<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'getTasksProject'])?>";
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: getTasksUrl,
      data: {
        id: projectId
      },
      success: function(data) {
        $('#tbTasks tr').remove();
        $.each(data.tasks, function(index, value){
          var endDate = new Date(value.end_date);
          var end_date = ('0' + (endDate.getDate())).slice(-2)+'/'+('0'+(endDate.getMonth()+1)).slice(-2)+'/'+endDate.getFullYear()+' '+endDate.getHours()+':'+('0'+endDate.getMinutes()).slice(-2);
          var tableRow = '<tr><td>'+value.name+'</td><td>'
                                    +endDate.getDate()+'/'+(endDate.getMonth()+1)+'/'+endDate.getFullYear()+' '+endDate.getHours()+':'+('0'+endDate.getMinutes()).slice(-2)+'</td><td>'
                                    +value.user.username+'</td><td>'
                                    +(value.milestone != null ? value.milestone.name : 'NA')
                                    +'</td>';
              tableRow += '<td class="actions"><div class="dropdown">';
              tableRow += '<button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= __('Acciones'); ?></button>';
              tableRow += '<ul class="dropdown-menu">';
              tableRow += '<li><a href='+document.URL+'/view/'+value.id+'><?=__('Ver')?></a></li>';
              tableRow += '<li><a href='+document.URL+'/edit/'+value.id+'><?=__('Editar')?></a></li>';
              tableRow += '<li><form action="'+document.URL+'/delete/'+value.id+'" method="POST" id="formDeleteTask"><input type="hidden"></form><a href="javascript:deleteTask()"><?=__('Eliminar')?></a></li>';
              if (value.state == 'TD' || value.state == 'TA') {
                if (userRole == 'A') {
                  tableRow += '<li><button class="btn-link btn-dd" type="button" id="dropdownMenuButton" data-toggle="dropdown" data-date="'+end_date+'" data-duration="'+value.duration+'" data-id="'+value.id+'" onclick="compareDate(this)" style="padding:5px 15px;"><?=__('Reabrir')?></button></li></ul></div></td></tr>';
                }
              }
              else if (value.state == 'PA') {
                tableRow += '<li><a><?=__('Pendiente aprobación')?></a></li></ul></div></td></tr>';
              }
              else {
                if (userRole == 'A') {
                  tableRow += '<li><a onclick="confirmDate('+value.id+')" id="btnFinishTask"><?=__('Finalizar')?></a></li></ul></div></td></tr>';
                }
                else {
                  tableRow += '<li><a onclick="sendTaskForApproval('+value.id+')"><?=__('Completar')?></a></li></ul></div></td></tr>';
                }
              }

          $('#tbTasks').append(tableRow);
          $('#tableTasks').dataTable();
          if (!sessionStorage.projectTask) {
            sessionStorage.setItem('projectTask', projectId);
          }
          else {
            sessionStorage.projectTask = projectId;
          }
        });
      }
    });
  }

  function compareDate(value){
    var date = new Date();
    var taskID = $(value).attr('data-id');
    var today = ('0' + (date.getDate())).slice(-2)+'/'+('0'+(date.getMonth()+1)).slice(-2)+'/'+date.getFullYear()+' '+('0'+date.getHours()).slice(-2)+':'+('0'+date.getMinutes()).slice(-2);

    if ($(value).attr('data-date') < today) {
      swal({
        title: "<?php echo __('La fecha de fin de la tarea ya pasó. ¿Desea agregar nuevamente la duración?') ?>",
        icon: "warning",
        buttons: true,
        })
        .then((newDuration) => {
        if (newDuration) {
          $('#taskIdReopen').val(taskID);
          $('#alertDuration').text('<?=__('La duración de la tarea era de: ')?>'+$(value).attr('data-duration')+'<?=__(' horas')?>');
          $('#modalEndDate').modal('show');
        }
        });
    }
    else{
      reopenTask($(value).attr('data-id'));
    }
  }

  function confirmDate(taskID){
    swal({
      title: "<?php echo __('¿La tarea fue finalizada hoy?') ?>",
      icon: "warning",
      buttons: true,
      })
      .then((today) => {
        if (today) {
          $('#taskId').val(taskID);
          $('#modalEnDate').modal('show');
        }
        else {
          var date = new Date();
          finishTask('<?php echo date('Y-m-d') ?>'+' '+date.getHours()+':'+date.getMinutes(), taskID);
        }
      });
  }

  function disableProjectInput(value){
    if(value == '1'){
      $('.specific-name').css('display', 'inline');
    }
    else {
      $('.specific-name').css('display', 'none');
      getTasksProject(value);
    }
  }

  function changeStatusTask(taskID, state) {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'changeStatusTask'])?>',
      data: {
        id: taskID,
        state: state
      },
      success: function(data) {
        if (data.result == true) {
          swal('<?php echo __('¡Excelente!') ?>', '<?php echo __('Se ha actualizado el estado de la tarea') ?>', "success");
        }
        else {
          swal('<?php echo __('Ops!') ?>', '<?php echo __('Algo ha ido mal. Por favor, intentalo de nuevo') ?>', "error");
        }
      }
    });
  }
</script>
