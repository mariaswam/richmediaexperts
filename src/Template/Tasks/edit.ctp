<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __($task->name);
  $projects = json_encode( $projects );
?>
<div class="row form-containers">
  <div class="col-md-12">
    <?= $this->Form->create($task, ['enctype'=>'multipart/form-data']) ?>
    <fieldset>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('name', ['label' => __('Nombre')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('state', ['label' => __('Estado'), 'options' => ['P' => __('Pendiente'), 'IP' => __('En progreso'),'PA' => __('Pendiente aprobación'), 'F' => __('Finalizada')]]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('attachments[]', ['label' => 'Adjuntar archivos', 'type' => 'file', 'id' => 'attachments', 'multiple' => true]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="end_date"><?=__('Fecha y hora de entrega')?></label>
            <input name="end_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('user_id', ['options' => $users, 'label' => __('Usuario')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('project_name', ['label' => __('Proyecto'), 'onchange' => 'getMilestonesProject(this.value)', 'id' => 'projectName', 'type' => 'text', 'value' => $task->project->name]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('milestone_id', ['label' => __('Milestones'), 'id' => 'milestone-id']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('task_type_id', ['options' => $taskTypes, 'label' => __('Tipo de tarea'), 'onchange' => 'verifyValue(this.value)']); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('priority', ['label' => __('Prioridad'), 'options' => ['A' => 'Alta', 'M' => 'Media', 'B' => 'Baja']]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('duration', ['label' => __('Duración'), 'type' => 'number', 'placeholder' => __('Dato brindado en horas...')]); ?>
        </div>
      </div>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
    var date = '<?= $task->end_date != null ? $task->end_date->format('Y-m-d H:i') : $task->end_date?>';
    $('#startDatePicker').datetimepicker({format: 'y-m-d H:i'});
    $('#startDatePicker').val(date);
    $("#attachments").fileinput({'browseOnZoneClick':true, 'showUpload':false});

    //AutoComplete
    var projects = '<?php echo $projects ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#projectName').autocomplete({
      source: source
    });
  });
</script>
