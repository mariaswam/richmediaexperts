<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Task $task
  */
  $pageTitle = h($task->name);
?>
<!-- Content -->
<div class="row">
  <!-- Left side / Comments -->
  <div class="col-md-6 col-sm-12">
    <?php foreach ($comments as $comment):?>
      <div class="comments">
        <?php if ($comment->user_id == $loggedUser['id']): ?>
          <div class="i-author">
            <div class="head-comment">
              <h5 class="author"><i class="fa fa-user"></i> <?= $comment->user->username; ?></h5>
              <span class="date"><?= $comment->date ?></span>
              <?= $this->Form->postLink(__('<i class="fa fa-trash"></i>'), ['controller' => 'Comments', 'action' => 'delete', $comment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $comment->comment), 'class' => 'delete-dd comment-del', 'escape' => false]) ?>
            </div>
            <p class="comment">
              <?= $comment->comment; ?>
            </p>
          </div>
        <?php else: ?>
          <div class="another-author">
            <div class="head-comment">
              <h5 class="author"><i class="fa fa-user"></i> <?= $comment->user->username; ?></h5>
              <span class="date"><?= $comment->date ?></span>
            </div>
            <p class="comment">
              <?= $comment->comment; ?>
            </p>
          </div>
        <?php endif; ?>
      </div>
    <?php endforeach; ?>
    <div class="add-comment">
      <?= $this->Form->create('comment', array('url' =>array('controller'=>'Comments', 'action'=>'add'))) ?>
      <fieldset>
          <?php
              echo $this->Form->control('comment', ['label' => false, 'type' => 'textarea', 'placeholder' => __('Nuevo comentario...')]);
              echo $this->Form->control('task_id', ['label' => false, 'type' => 'hidden', 'value' => $task->id]);
              echo $this->Form->control('user_id', ['label' => false, 'type' => 'hidden', 'value' => $loggedUser['id']]);
          ?>
      </fieldset>
      <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
      <?= $this->Form->end() ?>
    </div>
  </div>
  <!-- Right side / Info -->
  <div class="col-md-6 col-sm-12 pull-right">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title down-files"><?php echo __('Información') ?></h5>
            <button type="button" class="btn btn-sm btn-form" name="button" data-toggle="modal" data-target="#modalEditInvoice"><i class="fa fa-edit"></i></button>
            <h6 class="card-subtitle mb-2 text-muted"><?php echo __('Descripción') ?></h6>
            <p class="card-text"><?= h($task->description) ?></p>
            <h6 class="card-subtitle mb-2 text-muted"><?= __('Fecha de fin') ?></h6>
            <p class="card-text"><?= h($task->end_date) ?></p>
            <h6 class="card-subtitle mb-2 text-muted"><?php echo __('Prioridad') ?></h6>
            <p class="card-text"><?= $task->priority == 'A' ? __('Alta') : ($task->priority == 'M' ? __('Medio') : __('Bajo')) ?></p>
            <h6 class="card-subtitle mb-2 text-muted"><?= __('Estado') ?></h6>
            <p class="card-text"><?= $task->state == 'P' ? __('Pendiente') : ($task->state == 'PA' ? __('Pendiente aprobación') : ($task->state == 'FA' ? __('Finalizado atrasado') : ($task->state == 'FB' ? __('Finalizado antes') : __('En progreso')))) ?></p>
            <h6 class="card-subtitle mb-2 text-muted"><?= __('Tipo de tarea') ?></h6>
            <p class="card-text"><?= $task->has('task_type') ? $task->task_type->type : '' ?></p>
            <h6 class="card-subtitle mb-2 text-muted"><?= __('Asignada a:') ?></h6>
            <p class="card-text"><?= $task->user->username ?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title down-files" ><?php echo __('Archivos') ?></h5>
            <button type="button" class="btn btn-sm btn-secondary" name="button" data-toggle="modal" data-target="#modalUploadFiles"><i class="fa fa-upload"></i></button>
            <?= $this->Html->link(__('<i class="fa fa-download"></i>'), ['controller' => 'Attachments', 'action' => 'downloadAllFiles', $task->id], ['escape' => false, 'class' => 'btn btn-sm btn-secondary']) ?>
            <?php foreach ($attachments as $attachment):?>
              <div class="row item-attach">
                <div class="col-md-6 col-sm-12">
                  <?= $this->Html->link(__($attachment->name), ['controller' => 'Attachments', 'action' => 'downloadAttachment', $attachment->id], ['class' => 'download-item text-center']) ?>
                </div>
                <div class="col-md-6 col-sm-12 pull-right">
                  <?= $this->Form->postLink(__('<i class="fa fa-trash"></i>'), ['controller' => 'Attachments', 'action' => 'delete', $attachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $attachment->id), 'class' => 'delete-dd', 'escape' => false]) ?>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /Content -->
<?= $this->start('modalContent'); ?>
<!-- Modal edit task -->
<div class="modal fade" id="modalEditInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" id="edit_task"></div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Modal upload file -->
<div class="modal fade" id="modalUploadFiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <fieldset>
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <?php echo $this->Form->control('attachments[]', ['label' => 'Adjuntar archivos', 'type' => 'file', 'id' => 'attachments', 'multiple' => true]); ?>
                </div>
              </div>
            </fieldset>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#saveEndDate').click(function(){
      var endDate = $('select[name="end_date[year]"]').val()+"-"+$('select[name="end_date[month]"]').val()+"-"+$('select[name="end_date[day]"]').val()+" "+$('select[name="end_date[hour]"]').val()+":"+$('select[name="end_date[minute]"]').val();
      finishTask(endDate, '<?php echo $task->id ?>');
    });

    $('#edit_task').load('<?=$this->Url->build(['controller' => 'Tasks', 'action' => 'edit', $task->id])?>');
    $('#upload_files').load('<?=$this->Url->build(['controller' => 'Attachments', 'action' => 'add'])?>');
    $("#attachments").fileinput({
      'browseOnZoneClick':true,
      'uploadUrl': '<?=$this->Url->build(['controller' => 'Attachments', 'action' => 'add'])?>',
      'uploadExtraData': {
        task_id: '<?php echo $task->id ?>'
      }
    });
  });

      function openTrackWindow(){
        window.open('<?=$this->Url->build(['controller' => 'Timesheets', 'action' => 'timeTracking', $task->id])?>', 'targetWindow',
                    'toolbar=no,location=no,status=no,menubar=no,width=400,height=100');
      }
</script>
