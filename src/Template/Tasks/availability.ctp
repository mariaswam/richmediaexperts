<div class="row">
  <div class="col-md-6">
    <?php
      if($user['role'] == 'A'){
          echo $this->Form->control('users', ['empty' => true, 'label' => __('Usuarios'), 'options' => $users, 'onchange' => 'getWeekWorkUser(this.value)', 'id' => 'userSelect']);
      }
    ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="calendar-container" id="divCalendar">
    </div>
  </div>
</div>
<?= $this->start('scripts')?>
<?= $this->Html->script('snap.svg-min.js') ?>
<?= $this->Html->script('frappe-gantt.min.js') ?>
<?= $this->end()?>
<script>
  $(document).ready(function(){
    var data = '<?php echo $workOfTheWeek ?>';
    $('#divCalendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek'
      },
      defaultView: 'agendaWeek',
      minTime: '08:00:00',
      maxTime: '18:00:00',
      navLinks: true, // can click day/week names to navigate views
      eventLimit: true, // allow "more" link when too many events
      events: JSON.parse(data)
    });
  });

  function getWeekWorkUser(userID) {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Tasks', 'action' => 'availability'])?>",
      data: {
        id: userID
      },
      success: function(data) {
        $('#divCalendar').fullCalendar( 'destroy' );
        $('#divCalendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek'
          },
          defaultView: 'agendaWeek',
          navLinks: true, // can click day/week names to navigate views
          eventLimit: true, // allow "more" link when too many events
          events: JSON.parse(data.workOfTheWeek)
        });
      }
    });
  }
</script>
