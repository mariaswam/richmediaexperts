<div class="col-md-12">
    <?= $this->Form->create($role) ?>
    <fieldset>
        <legend><?= __('Nuevo Rol') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Rol')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
