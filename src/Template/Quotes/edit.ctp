<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Edit Quote');
?>
<div class="col-md-12  main-content-wrapper quote-form-container" >
<div class="row">
  <div class="col-md-12">

      <?= $this->Html->link(__('Back to quotes'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($quote) ?>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('name'); ?>
        </div>
        <?php if ($user['role'] != 'E'): ?>
          <div class="col-md-6">
            <?php echo $this->Form->control('company_id', ['options' => $companies]); ?>
          </div>
        <?php else: ?>
          <div class="col-md-6">
            <?php echo $this->Form->control('company_id', ['type' => 'hidden', 'value' => $user['company_id']]); ?>
          </div>
        <?php endif; ?>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php echo $this->Form->control('description'); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('purchase_order'); ?>
        </div>
      </div>
      <?php if ($user['role'] != 'E'): ?>
        <div class="row">
          <div class="col-md-5">
            <label for="templates"><?= __('Cargar encabezado desde plantilla') ?></label>
            <select class="form-control" id="selectTemplate" name="templates">
              <option value="0"><?= __('Seleccione una plantilla') ?></option>
              <?php foreach ($templates as $template): ?>
                <option value="<?= $template->id ?>" data-html='<?= $template->content ?>'><?= $template->name ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="row wysywig-editor">
          <div class="col-md-12 col-sm-12">
            <label><?= __('Encabezado de la cotización') ?></label>
            <textarea id="header-editor" name="header"></textarea>
          </div>
        </div>
      <?php endif; ?>

      <div class="add-forms-container">

      <div class="row">
        <div class="col-md-12 col-sm-12">
          <legend id="itemLegend"><?= __('Project items') ?>
            <button type="button" class="btn btn-sm plus-btn btn-circle" id="btnAddItems"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </legend>
          <div class="row">
            <div class="col-md-8">
              <div id="rowItems">
                <div class="row">
                  <div class="col-md-6"><label><?=__('Descripción')?></label></div>
                  <div class="col-md-2"><label><?=__('Precio')?></label></div>
                  <div class="col-md-2"><label><?=__('Cantidad')?></label></div>
                </div>
                <?php $cont = 0; foreach ($items as $item): ?>
                  <div class="row quote-item" id="<?=$item->id?>">
                    <?php echo  $this->Form->control('itemsArray[id][]', ['value' => $item->id, 'type' => 'hidden']);?>
                    <div class="col-md-6">
                      <?php echo  $this->Form->control('itemsArray[description][]', ['value' => $item->description, 'label' => false, 'required']);?>
                    </div>
                    <div class="col-md-2">
                      <?php echo  $this->Form->control('itemsArray[price][]', ['value' => $item->price, 'label' => false, 'id' => 'pI'.$cont, 'class' => 'itemPrice calcInput', 'required']);?>
                    </div>
                    <div class="col-md-2">
                      <?php echo  $this->Form->control('itemsArray[quantity][]', ['value' => $item->quantity, 'label' => false, 'id' => 'qI'.$cont, 'class' => 'itemQuantity calcInput', 'required', 'min' => '1']);?>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-danger btn-md delete-account" name="button" onclick="deleteItem('<?=$item->id?>')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>
                  </div>
                <?php $cont++; endforeach; ?>
              </div>
            </div>
            <div class="col-md-4 subtotal-content">
              <label for="total_amount"><?= __('Total') ?></label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">$</span>
                </div>
                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="total_amount" readonly id="subtotalInvoice" value="<?= $quote->total_amount; ?>">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?php echo $this->Form->control('items_delete', ['type' => 'hidden', 'id' => 'itemsDelete'])?>
          <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
</div>
<script>
  var itemsToDelete = [];
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#header-editor').summernote('code', '<?= $quote->header; ?>');
  });
  var cont = '<?= $items->count();?>';
  $('#btnAddItems').click(function(){
    var items = '<div class="row quote-item">';
    items += '<div class="col-md-6"><div class="form-group"><input class="form-control" name="itemsArray[description][]" required></div></div>';
    items += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" required></div></div>';
    items += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1"></div></div>';
    items += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
    items += '</div>';
    cont++;
    $('#rowItems').append(items);
  });

    function deleteItem(itemID) {
      itemsToDelete.push(itemID);
      $('#'+itemID).remove();
      $('#itemsDelete').val(itemsToDelete);
      calcSubtotal();
    }
</script>
