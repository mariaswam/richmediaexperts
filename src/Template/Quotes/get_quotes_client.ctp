<?php
  $pageTitle = __('Quotes');
  $userCompany = $user['company_id'] != null ?  $user['company_id'] : '';
?>
<div class="row quotes-container">
  <div class="col-md-12 col">
    <div class="bg-white-container">
      <div class="row">
        <div class="col-md-6 info-invoicing">
          <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('Add New Quote'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
          <h4 class="d-none"><?= __('Total amount: ') ?> &nbsp;<span id="totalAmount"></span></h4>
          <h4 class="d-none"><?= __('Pending amount: ') ?> &nbsp;<span id="pendingAmount"></span></h4>
        </div>
        <div class="col-md-6 align-items-end download-elements">
          <button type="button" class="btn btn-success invoice-panel btn-form btn-sm download-btn" onclick="downloadReportQuotes()" title="Download Report Quotes"><i class="fa fa-download" aria-hidden="true"></i></button>
        </div>
        <div class="col-md-6 filter-column d-none">
          <button type="button" class="btn btn-success invoice-panel btn-form btn-sm download-btn" onclick="downloadReport()" title="Download Report"><i class="fa fa-download" aria-hidden="true"></i></button>
          <div class="form-group select d-none date-filter">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control" onchange="filterByCompany()">
          </div>
          <div class="form-group select d-none date-filter">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control" onchange="filterByCompany()">
          </div>
          <?= $this->Form->control(__('Time'), ['options' => ['0' => __('Todas'), '1' => __('Por fecha'), '2' => __('Mes actual'), '3' => __('Mes pasado')], 'onchange' => 'disableDatePicker(this.value)', 'id' => 'selOptionsDate', 'default' => '0'])?>
          <?= $this->Form->control(__('Estado'), ['options' => ['0' => __('Todas'), '1' => __('Vencidas'), '3' => __('Pendientes')], 'id' => 'selOptions', 'default' => '3', 'onchange' => 'filterByCompany()'])?>
          <?= $this->Form->control(__('Filtrar'), ['type' => 'text', 'onchange' => 'filterByCompany()', 'id' => 'companyInput'])?>
        </div>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link quote-tab active" href="#sent" role="tab" data-toggle="tab" data-id="sent"><?= __('Sent for approval') ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link quote-tab" href="#approved" role="tab" data-toggle="tab" data-id="approved"><?= __('Approved by RME') ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link quote-tab" href="#invoices" role="tab" data-toggle="tab" data-id="invoicing"><?= __('Invoicing Information') ?></a>
        </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Invoices tab -->
        <div role="tabpanel" class="tab-pane" id="invoices">
          <table cellpadding="0" cellspacing="0" class="table table-striped table-border" id="invoicesAssembleTable"></table>
          <select class="form-control items-count" name="" id="numberOfRowSelectAssmbl" data-store="numberOfRecordsInvoices">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
        <!-- Sent for approval quotes -->
        <div role="tabpanel" class="tab-pane in active" id="sent">
          <table cellpadding="0" cellspacing="0" class="table table-striped table-border table-index" id="tableSent">
              <thead>
                  <tr>
                      <th scope="col"><?= __('id') ?></th>
                      <th scope="col"><?= __('name') ?></th>
                      <th scope="col" class="center-col"><?= __('total amount') ?></th>
                      <th scope="col" class="center-col"><?= __('state') ?></th>
                      <th scope="col" class="actions"><?= __('Actions') ?></th>
                  </tr>
              </thead>
              <tbody>
                  <?php foreach ($quotes as $quote): $disabled = $quote->state == 'draft' ? false : true; ?>
                  <tr>
                      <td><?= $this->Number->format($quote->id) ?></td>
                      <td><?= $quote->name ?></td>
                      <td class="center-col"><?= $this->Number->format($quote->total_amount) ?></td>
                      <td class="quote-state center-col"><?= $quote->state ?></td>
                      <td class="actions">
                        <div class="btns-wrapper">

                          <?= $this->Html->link(__('View'), ['action' => 'view', $quote->id], ['class' => 'btn-view']) ?>
                          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $quote->id], ['class' => 'btn-edit']) ?>
                          <?= $this->Form->postLink(__('Resend'), ['action' => 'resendQuoteForApproval', $quote->id], ['confirm' => __('Are you sure you want to resend # {0}?', $quote->id), 'class' => 'btn-reopen']) ?>
                          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $quote->id], ['confirm' => __('Are you sure you want to delete {0}?', $quote->name), 'class' => 'btn-delete']) ?>
                          <?= $this->Html->link($this->Html->image("icons/download.png", ['width' => '15']), ['action' => 'downloadQuote', $quote->id], ['class' => 'download-btn btn btn-sm white', 'escape'=>false]) ?>
                        </div>
                      </td>
                  </tr>
                  <?php endforeach; ?>
              </tbody>
          </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="approved">
          <table cellpadding="0" cellspacing="0" class="table table-striped table-border" id="approvedQuotesTable"></table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var invoicesTable;
$(document).ready(function () {
  $('#page-title').text('<?php echo $pageTitle; ?>');

  if (localStorage.getItem('numberOfRecords') != null) {
    $('#numberOfRowSelectAssmbl').val(localStorage.getItem('numberOfRecords'));
  }
  else{
    $('#numberOfRowSelectAssmbl').val('100');
  }

  invoicesTable = FooTable.init('#invoicesAssembleTable', {
		// we only load the column definitions as the row data is loaded through the button clicks
		"columns": [
      {"name":"id","title": "ID","breakpoints":"xs sm"},
      {"name":"customer","title": "Client"},
      {"name":"assembleCustomer","title": "Project Client"},
      {"name":"project","title": "Project"},
      {"name":"dueDate","title": "Due Date RM", "type": "date"},
      {"name":"dueDateC","title": "Due Date Client", "type": "date"},
      {"name":"state","title": "State"},
      {"name":"totalAmount","title": "Total Amount"},
      {"name":"outstandingAmount","title": "Pending Amount"},
      {"name":"action","title": "Actions"}
    ],
    "paging": {
      "enabled": true,
      "size": localStorage.getItem('numberOfRecords') != null ? localStorage.getItem('numberOfRecords') : 100
    },
    "filtering": {
      "enabled": true
    }
	});

  approvedQuotes = FooTable.init('#approvedQuotesTable', {
    "columns": [
      {"name":"id","title": "ID","breakpoints":"xs sm"},
      {"name":"name","title": "Name"},
      {"name":"totalAmount","title": "Total Amount"},
      {"name":"state","title": "State"},
      {"name":"action","title": "Actions"}
    ],
    "filtering": {
      "enabled": true
    }
  });

  $('#numberOfRowSelectAssmbl').change(function() {
    localStorage.setItem('numberOfRecords', $('#numberOfRowSelectAssmbl').val());
    FooTable.get('#invoicesAssembleTable').pageSize($('#numberOfRowSelectAssmbl').val());
  });
});

  //GET ALL INVOICES METHOD
  function filterByCompany() {
    $('#loader').css('display', 'inline-block');
    localStorage.setItem('filter_value', $('#selOptions').val());
    localStorage.setItem("company", $('#companyInput').val());
    var selectOptionDate = $('#selOptionsDate').val();
    var selectOptions = $('#selOptions').val();
    var data = { startDate: null, endDate: null, state: null, downloaded: null, company: '', type: '0'};

    switch (selectOptionDate) {
      case '0':
        data.startDate = null;
        data.endDate = null;
      break;
      case '1':
        data.startDate = $('#startDatePicker').val();
        data.endDate = $('#endDatePicker').val();
      break;
      case '2':
        var date = new Date();
        data.startDate = date.getUTCFullYear()+'-'+('0'+(date.getMonth()+1)).slice(-2)+'-01';
        var lastDate = new Date(date.getFullYear(), ('0'+(date.getMonth()+1)).slice(-2), 0);
        data.endDate = lastDate.getFullYear()+'-'+('0'+(lastDate.getMonth()+1)).slice(-2)+'-'+lastDate.getDate();
      break;
      case '3':
        var date = new Date();
        var month = date.getMonth()+1 == 1 ? '12' : ('0'+(date.getMonth())).slice(-2);
        var year = date.getMonth()+1 == 1 ? date.getUTCFullYear()-1 : date.getUTCFullYear();
        var lastDate = new Date(date.getFullYear(), ('0'+(date.getMonth())).slice(-2), 0);
        data.startDate = year+'-'+month+'-01';
        data.endDate = lastDate.getFullYear()+'-'+('0'+(lastDate.getMonth()+1)).slice(-2)+'-'+lastDate.getDate();
      break;
    }

    switch (selectOptions) {
      case '0':
        data.state = null;
        data.downloaded = null;
      break;
      case '1':
        data.state = 'V';
        data.downloaded = null;
      break;
      case '2':
        data.downloaded = true;
        data.state = null;
      break;
      case '3':
        data.state = 'P';
        data.downloaded = null;
      break;
    }
    data.company = $('#companyInput').val();
    data.type = '2';
    sendAjaxInvoices(data);
  }

  function sendAjaxInvoices(data){
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'getInvoicesCompany'])?>",
      data: data,
      success: function(data){
        var invoices = [];
        var rowJson = [];
       $.each(data.invoices, function(index, value){
         // console.log(value);
         date = new Date(value.date);
         dueDate = new Date(value.due_date);
         dueDateC = new Date(value.due_date);
         dueDateC = value.customer != null ? dueDateC.setDate(dueDate.getDate() + parseInt(value.customer.payment_terms)) : dueDateC;
         totalAmount = '$' + value.total_amount;
         outstandingAmount = '$' + value.outstanding_amount;

         var project = (value.project != null) ? value.project.name:'';
         var classState = '';
         var state = '';

         switch (value.state) {
           case 'V':
             classState = 'due';
             state = '<?php echo __('Vencida') ?>';
           break;
           case 'P':
              state = '<?php echo __('Pendiente') ?>';
           break;
           case 'D':
             classState = 'discard';
             state = '<?php echo __('Discard') ?>';
           break;
           case 'C':
              state = '<?php echo __('Cancelado') ?>';
           break;
         }

         var actionLink = classState != 'discard' ?  "<a href='<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'view'])?>/"+value.id+"' class='action-btn btn btn-sm'>View</a>" : "<a href='<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'view'])?>/"+value.id+"' class='action-btn btn btn-sm disabled'>View</a>";
         actionLink += '<a href="invoices/downloadInvoice/'+value.id+'/national" class="btn btn-sm download-btn">' + '<?= $this->Html->image("icons/download.png", ['width' => '15']); ?>' + "</a>";
         var dataJson = {
           "id": value.code_invoice,
           "customer": value.company.name,
           "assembleCustomer": value.customer != null ? value.customer.name : value.company.name,
           "project": project,
           "dueDate": dueDate,
           "dueDateC": dueDateC,
           "state": state,
           "totalAmount": totalAmount,
           "outstandingAmount": outstandingAmount,
           "action": actionLink
         };

         //Save invoices id on local storage for download purpouses
         invoices.push(value.id);
         //Create json for table
         rowJson.push(dataJson);
       });
       invoicesTable.rows.load(rowJson);

       $('#loader').css('display', 'none');
       if (localStorage.getItem("invoices") == null) {
         localStorage.setItem("invoices", invoices)
       }
       else {
         localStorage.setItem("invoices", invoices)
       }

       $('#totalAmount').text('$' + data.totalCosts.toFixed(2));
       $('#pendingAmount').text('$' + data.totalPendingCosts.toFixed(2));
      }
    });
  }

  function convertProject(element) {
    var id = $(element).attr('data-quote');
    swal({
      title: "<?php echo __('Do you want to approve the quote?') ?>",
      icon: "warning",
      buttons: true,
      })
      .then((confirm) => {
        if (confirm) {
          $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'changeStateQuote'])?>",
            data: {
              quote_id: id,
              state: 'invoice'
            },
            success: function(data){
              if (data.message == "success") {
                $(element).parent().parent().remove();
                swal({
                  title: "<?php echo __('The quote has been approved') ?>",
                  icon: "success",
                });
              }
            }
          });
        }
      });
  }

  function test(data) {
    convertProject(data);
  }

  function loadApprovedQuotes() {
    $('#loader').css('display', 'inline-block');
    $.ajax({
      type: 'GET',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['action' => 'getApprovedQuotes'])?>",
      success: function(data){
        var rowJson = [];
        $.each(data, function(index, value){
          date = new Date(value.date);

          var actionLink = '<a href="<?= $this->Url->build(['action' => 'view'])?>/'+value.id+'" class="action-btn btn btn-sm">Review</a>';
          actionLink += '<a href="<?= $this->Url->build(['action' => 'edit'])?>/'+value.id+'" class="edit-btn btn btn-sm white">Edit</a>';
          actionLink += '<button class="download-btn btn btn-sm convertButton" data-quote="'+value.id+'" onclick="test(this)">Approved by client</button>';
          actionLink += '<button class="delete-btn btn btn-sm white" data-quotedelete="'+value.id+'" onclick="deleteQuote(this)">Delete</button>';
          actionLink += '<a href="<?= $this->Url->build(['action' => 'downloadQuote'])?>/'+value.id+'" class="download-btn btn btn-sm white"><?= $this->Html->image("icons/download.png", ['width' => '15']) ?></a>'

          var dataJson = {
            "id": value.id,
            "name": value.name,
            "totalAmount": value.total_amount,
            "state": value.state,
            "action": actionLink
          };

          //Create json for table
          rowJson.push(dataJson);
        });
        approvedQuotes.rows.load(rowJson);

        $('#loader').css('display', 'none');
      }
    });
  }

  function deleteQuote(value) {
    var id = $(value).data('quotedelete');
    swal({
      title: "<?php echo __('Do you want to delete the quote?') ?>",
      icon: "warning",
      buttons: true,
      })
      .then((confirm) => {
        if (confirm) {
          $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'delete'])?>/" + id,
            data: {
              id: id
            },
            success: function(data){
              $(value).parent().parent().remove();
              swal({
                title: "<?php echo __('The quote has been approved') ?>",
                icon: "success",
              });
            }
          });
        }
      });
  }

  function downloadReportQuotes() {
    var activeTab = $('.quote-tab.active').data('id');
    var quotes = [];
    if (activeTab == 'sent') {
      $('#tableSent tbody tr td:first-child').each(function (index, value) {
        quotes.push($(value).text());
      });
    }
    else{
      $('#approvedQuotesTable tbody tr td:first-child').each(function (index, value) {
        quotes.push($(value).text());
      });
    }

    location.href = 'quotes/report/?quotes='+quotes;
  }
</script>
