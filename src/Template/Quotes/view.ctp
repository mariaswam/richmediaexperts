<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Quote $quote
  */
  $pageTitle = __('Quote #').$quote->id;
?>
<div class="col-md-12  main-content-wrapper" >
  <div class="row">
    <div class="col-sm-12">
      <?= $this->Html->link(__('Back to quotes'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
    </div>
  </div>
<div class="row">
  <div class="col-md-6 col-sm-12">

    <h2><?= __('Information') ?></h2>
    <table class="vertical-table table table-view small-view">
        <tr>
            <th scope="row" width="15%"><?= __('Company:') ?></th>
            <td><?= $quote->has('company') ? $this->Html->link($quote->company->name, ['controller' => 'Companies', 'action' => 'view', $quote->company->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Name:') ?></th>
            <td><?= $quote->name ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Total Amount:') ?></th>
            <td><?= $this->Number->format($quote->total_amount) ?></td>
        </tr>
        <tr>
            <th scope="row" width="20%"><?= __('Purchase order:') ?></th>
            <td><?= $quote->purchase_order ?></td>
        </tr>
        <tr>
          <th scope="row" width="15%"><?= __('Description:') ?></th>
          <td><?= $this->Text->autoParagraph(h($quote->description)); ?></td>
        </tr>
    </table>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="related">
        <h2><?= __('Items') ?></h2>
        <?php if (!empty($quote->components)): ?>
          <div class="table-responsive">


            <table cellpadding="0" cellspacing="0" class="table table-striped table-border table-items">
              <thead>
                <tr>
                    <th scope="col"><?= __('Descripción') ?></th>
                    <th scope="col" class="center-col"><?= __('Precio') ?></th>
                    <th scope="col" class="center-col"><?= __('Cantidad') ?></th>
                    <th scope="col" class="center-col"><?= __('Subtotal') ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($quote->components as $items): ?>
                <tr>
                    <td><?= h($items->description) ?></td>
                    <td class="center-col"><?= h($items->price) ?></td>
                    <td class="center-col"><?= h($items->quantity) ?></td>
                    <td class="center-col"><?= h($items->subtotal) ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('.table-items').footable({
      "paging": {
        "enabled": true,
        "size": localStorage.getItem('numberOfRecordsQuotes') != null ? localStorage.getItem('numberOfRecordsQuotes') : 10
      },
      "filtering": {
        "enabled": false
      }
    });
  });
</script>
