<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('New Quote');
  $templatesArray = json_encode($templates->toArray());
?>
<div class="col-md-12  main-content-wrapper quote-form-container" >
<div class="row">
  <div class="col-md-12">

      <?= $this->Html->link(__('Back to quotes'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($quote) ?>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('name'); ?>
        </div>
        <?php if ($user['role'] != 'E'): ?>
          <div class="col-md-6">
            <?php echo $this->Form->control('company_id', ['options' => $companies]); ?>
          </div>
        <?php else: ?>
          <div class="col-md-6">
            <?php
              echo $this->Form->control('company_id', ['type' => 'hidden', 'value' => $user['company_id']]);
              echo $this->Form->control('customer_id', ['type' => 'hidden', 'id' => 'customerInput']);
            ?>

          </div>
        <?php endif; ?>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php echo $this->Form->control('description'); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('purchase_order'); ?>
        </div>
      </div>
      <?php if ($user['role'] != 'E'): ?>
        <div class="row">
          <div class="col-md-6">
            <label for="templates"><?= __('Template header') ?></label>
            <select class="form-control" id="selectTemplate" name="templates">
              <option value="0"><?= __('Seleccione una plantilla') ?></option>
              <?php foreach ($templates as $template): ?>
                <option value="<?= $template->id ?>" data-html='<?= $template->content ?>'><?= $template->name ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="row wysywig-editor">
          <div class="col-md-12 col-sm-12 form-group">
            <label><?= __('Quote header') ?></label>
            <textarea id="header-editor" name="header"></textarea>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="customer_id"><?= __('Quote Template') ?></label>
            <select class="form-control" name="customer_id" id="customerSelect">
              <option value="0"><?= __('Select a customer template quote') ?></option>
              <?php foreach ($customers as $customer): ?>
                <option value="<?= $customer->id ?>" data-master="<?= $customer->master_price ?>" data-resize="<?= $customer->resize_price ?>" data-customer="<?= $customer->id ?>" data-mstrtime="<?= $customer->master_eta ?>" data-rsztime="<?= $customer->resizes_eta ?>"><?= $customer->name ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
      </div>
      <div class="add-forms-container">


        <div class="row">
          <div class="col-md-12 col-sm-12">
            <legend id="itemLegend"><?= __('Project items') ?>
              <button type="button" class="btn btn-sm plus-btn btn-circle" id="btnAddItems"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </legend>
            <div class="row">
              <div class="col-md-8">
                <div id="rowItems">
                  <div class="row header-items">
                    <div class="col-md-6"><label><?=__('Descripción')?></label></div>
                    <div class="col-md-2"><label><?=__('Precio')?></label></div>
                    <div class="col-md-2"><label><?=__('Cantidad')?></label></div>
                  </div>
                  <div class="row quote-item">
                      <div class="col-md-6">
                        <?php echo $this->Form->control('itemsArray[description][]', ['label' => '', 'required', 'id' => 'descriptionI0']); ?>
                      </div>
                      <div class="col-md-2">
                        <?php echo $this->Form->control('itemsArray[price][]', ['label' => '', 'id' => 'pI0',  'class' => 'itemPrice calcInput', 'required']); ?>
                      </div>
                      <div class="col-md-2">
                        <?php echo $this->Form->control('itemsArray[quantity][]', ['label' => '', 'type' => 'number', 'class' => 'itemQuantity calcInput', 'id' => 'qI0', 'required', 'value' => '1', 'min' => '1']); ?>
                      </div>
                      <div class="col-md-2">
                        <?= $this->Form->button(__('<i class="fa fa-trash" aria-hidden="true"></i>'), ['class' => 'btn btn-danger btn-md btn-remove-row', 'type' => 'button', 'onclick' => 'removeRow(this)'], ['escape' => false]) ?>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 subtotal-content">
                <label for="total_amount" ><?= __('Total') ?></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                  </div>
                  <input type="text" class="form-control subtotal-input" aria-label="Amount (to the nearest dollar)" name="total_amount" readonly id="subtotalInvoice">
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Assets</span>
                  </div>
                  <input type="text" class="form-control subtotal-input" aria-label="Amount (to the nearest dollar)" name="total_assets" value="1" name="total_assets" readonly id="subtotalAssets">
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">ETA</span>
                  </div>
                  <input type="text" class="form-control subtotal-input" aria-label="Amount (to the nearest dollar)" name="eta" value="-" name="eta" readonly id="subtotalETA">
                  <input type="hidden" id="useTemplate">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-12">
            <?= $this->Form->button(__('Submit'), ['class' => 'btn-form']) ?>
          </div>
        </div>
      </div>
      <?= $this->Form->end() ?>

  </div>
</div>
<script type="text/javascript">
var cont = 1;
$(document).ready(function () {
  $('#page-title').text('<?php echo $pageTitle; ?>');
  $('#header-editor').summernote();

  $('#selectTemplate').change(function (element) {
    var htmlCode = $(this).find(':selected').data('html');
    if (htmlCode != undefined) {
        $('#header-editor').summernote('code', htmlCode);
    }
    else{
      $('#header-editor').summernote('code', '');
    }
  });

  $('#customerSelect').change(function () {
    if ($(this).val() != 0) {
      removePreviousTemplate();
      $('#customerInput').val($(this).find(':selected').data('customer'));
        if ($('#descriptionI0').val() == "") {
          $('#descriptionI0').val('Master');
          $('#descriptionI0').parent().parent().parent().addClass('template-items');
          $('#pI0').val($(this).find(':selected').data('master'));
          $('#qI0').val(1);
          $('#qI0').attr('data-type', 'master');
        }
        else{
          if ($('#descriptionI'+cont).length == 0) {
            var itemMaster = '<div class="row quote-item template-items">';
            itemMaster += '<div class="col-md-6"><div class="form-group"><input class="form-control" id="descriptionI'+cont+'" name="itemsArray[description][]" required value="Master"></div></div>';
            itemMaster += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" value="'+$(this).find(':selected').data('master')+'" required></div></div>';
            itemMaster += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1"></div></div>';
            itemMaster += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
            itemMaster += '</div>';
            $('#qI'+cont).attr('data-type', 'master');
            cont++;
          }
        }

        var items = '<div class="row quote-item template-items">';
        items += '<div class="col-md-6"><div class="form-group"><input class="form-control" id="descriptionI'+cont+'" name="itemsArray[description][]" required value="Resizes"></div></div>';
        items += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" value="'+$(this).find(':selected').data('resize')+'" required></div></div>';
        items += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1" data-type="resizes"></div></div>';
        items += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
        items += '</div>';
        cont++;

        var itemsFeedback = '<div class="row quote-item template-items">';
        itemsFeedback += '<div class="col-md-6"><div class="form-group"><input class="form-control" id="descriptionI'+cont+'" name="itemsArray[description][]" required value="Feedback"></div></div>';
        itemsFeedback += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" value="45" required></div></div>';
        itemsFeedback += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="10" min="1" data-type="feedback"></div></div>';
        itemsFeedback += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
        itemsFeedback += '</div>';
        cont++;

        $('#rowItems').append(itemMaster);
        $('#rowItems').append(items);
        $('#rowItems').append(itemsFeedback);
        $('#useTemplate').val(true);
        calcSubtotal();
        getTotalAssets();
        calcETA();
    }
    else{
      $('#subtotalAssets').val(0);
      $('#subtotalInvoice').val(0);
      removePreviousTemplate();
    }
  });
});

   $('#btnAddItems').click(function(){
     var items = '<div class="row quote-item">';
     items += '<div class="col-md-6"><div class="form-group"><input class="form-control" name="itemsArray[description][]" required></div></div>';
     items += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" required></div></div>';
     items += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1"></div></div>';
     items += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
     items += '</div>';
     cont++;
     $('#rowItems').append(items);
     getTotalAssets();
   });

   function removePreviousTemplate() {
    $('.template-items').each(function () {
      $(this).remove();
    });
   }
</script>
