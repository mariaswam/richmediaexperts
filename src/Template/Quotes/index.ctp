<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Quote[]|\Cake\Collection\CollectionInterface $quotes
  */
  $pageTitle = __('Quotes');
?>
<div class="col-md-12  main-content-wrapper" >
<div class="row">
  <div class="col-md-12">

      <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('Add New Quote'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>

      <div class="table-responsive">


        <table cellpadding="0" cellspacing="0" class="table table-striped table-border tb-quotes-admin">
            <thead>
                <tr>
                    <th scope="col"><?= __('id') ?></th>
                    <th scope="col"><?= __('Nombre') ?></th>
                    <th scope="col" class="center-col"><?= __('Monto total') ?></th>
                    <th scope="col" class="center-col"><?= __('Compañía') ?></th>
                    <th scope="col" class="center-col"><?= __('Estado') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                    <th scope="col" class="actions"><?= __('Download') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($quotes as $quote): $disabled = $quote->state == 'draft' ? false : ($quote->state == 'approved' ? false: true); ?>
                <tr>
                    <td><?= $this->Number->format($quote->id) ?></td>
                    <td><?= $quote->name ?></td>
                    <td class="center-col"><?= $this->Number->format($quote->total_amount) ?></td>
                    <td class="center-col"><?= $quote->has('company') ? $this->Html->link($quote->company->name, ['controller' => 'Companies', 'action' => 'view', $quote->company->id]) : '' ?></td>
                    <td class="quote-state center-col"><?= $quote->state ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">


                        <?= $this->Html->link(__('View'), ['action' => 'view', $quote->id], ['class' => 'btn-view']) ?>
                        <?php if (!$disabled): ?>
                          <?= $this->Html->link(__('Edit'), ['action' => 'edit', $quote->id], ['class' => 'btn-edit']) ?>
                          <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $quote->id], ['confirm' => __('Are you sure you want to delete {0}?', $quote->name), 'class' => 'btn-delete']) ?>
                        <?php endif; ?>
                        <?= $this->Html->link(__('Approve'), '#', ['data-quote' => $quote->id, 'onclick' => 'test(this)', 'class' => 'btn-finish']) ?>
                      </div>
                    </td>
                    <td><?= $this->Html->link($this->Html->image("icons/download.png", ['width' => '15']), ['action' => 'downloadQuote', $quote->id], ['class' => 'download-btn btn btn-sm white', 'escape'=>false]) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
      </div>
      <select class="form-control items-count" name="" id="numberOfRowSelect">
        <option value="5">5</option>
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>

  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#page-title').text('<?php echo $pageTitle; ?>');

    $('.tb-quotes-admin').footable({
      "paging": {
  			"enabled": true,
        "size": localStorage.getItem('numberOfRecordsQuotes') != null ? localStorage.getItem('numberOfRecordsQuotes') : 10
  		},
      "filtering": {
        "enabled": true
      }
    });

    if (localStorage.getItem('numberOfRecordsQuotes') != null) {
      $('#numberOfRowSelect').val(localStorage.getItem('numberOfRecordsQuotes'));
    }
    else{
      $('#numberOfRowSelect').val('10');
    }

    $('#numberOfRowSelect').change(function() {
      localStorage.setItem('numberOfRecordsQuotes', $('#numberOfRowSelect').val());
      FooTable.get('.tb-quotes-admin').pageSize($('#numberOfRowSelect').val());
    });

  });

  function convertProject(id) {
    swal({
      title: "<?php echo __('Do you want to approve the quote?') ?>",
      icon: "warning",
      buttons: true,
      })
      .then((confirm) => {
        if (confirm) {
          $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'changeStateQuote'])?>",
            data: {
              quote_id: id,
              state: 'approved'
            },
            success: function(data){
              if (data.message == "success") {
                swal({
                  title: "<?php echo __('The quote has been approved') ?>",
                  icon: "success",
                });
              }
            }
          });
        }
      });
  }

  function test(data) {
    convertProject($(data).attr('data-quote'));
  }
</script>
