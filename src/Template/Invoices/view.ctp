<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Invoice $invoice
  */
  $pageTitle = __('Invoice: ').$invoice->code_invoice;
  $returnLink = $user['role'] == 'E' ? ['controller' => 'Quotes', 'action' => 'index'] : ['controller' => 'Invoices', 'action' => 'index'];
?>

<div class="row quote-form-container quotes-container ">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <div class="row">


      <div class="col-md-6">
    <?= $this->Html->link(__('Back to dashboard'), $returnLink, ['class' => 'btn btn-sm btn-go-back']) ?>
    <h2><?= __('Information') ?></h2>
    <table class="vertical-table table table-view small-view invoices-table">
        <tr>
            <th scope="row" width="15%"><?= __('ID') ?></th>
            <td><?= $invoice->code_invoice ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Company:') ?></th>
            <td><?= $invoice->company->name ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Estado') ?></th>
            <td><?= ($invoice->state == 'P') ? __('Pendiente') : __('Cancelado'); ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Proyecto') ?></th>
            <td><?= $invoice->has('project') ? $this->Html->link($invoice->project->name, ['controller' => 'Projects', 'action' => 'view', $invoice->project->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Monto total') ?></th>
            <td><?= $this->Number->format($invoice->total_amount) ?></td>
        </tr>
        <tr>
            <th scope="row" width="20%"><?= __('Monto pendiente') ?></th>
            <td><?= $this->Number->format($invoice->outstanding_amount) ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Fecha') ?></th>
            <td><?= h($invoice->date) ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Fecha de pago') ?></th>
            <td><?= h($invoice->due_date) ?></td>
        </tr>
        <tr>
            <th scope="row" width="15%"><?= __('Orden de compra') ?></th>
            <td><?= h($invoice->purchase_order) ?></td>
        </tr>
    </table>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <?= $this->Html->link('<i class="fas fa-download"></i>', ['controller' => 'Invoices', 'action' => 'downloadInvoice', $invoice->id, 'international'], ['class' => 'btn btn-info btn-md ddPaid btn-form', 'escape' => false]) ?>
        <?php if ($user['role'] != 'E'): ?>
          <button type="button" class="btn btn-success ddPaid btn-form" data-toggle="modal" data-target="#modalTypeInvoice"><i class="fa fa-download" aria-hidden="true"></i></button>
          <button type="button" class="btn btn-success ddPaid btn-form" data-toggle="modal" data-target="#modalReminder"><i class="fa fa-envelope" aria-hidden="true"></i></button>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="related invoices-items">
            <h2><?= __('Items') ?></h2>
            <?php if (!empty($invoice->items)): ?>
            <div class="table-responsive">


              <table cellpadding="0" cellspacing="0" class="table table-view table-striped table-components underline-thead" id="invoice-items-table">
                <thead>
                  <tr>
                    <th scope="col"><?= __('Descripción') ?></th>
                    <th scope="col" class="center-col"><?= __('Precio') ?></th>
                    <th scope="col" class="center-col"><?= __('Cantidad') ?></th>
                    <th scope="col" class="center-col"><?= __('Subtotal i.v.i') ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($invoice->items as $items): ?>
                  <tr>
                      <td><?= h($items->description) ?></td>
                      <td class="center-col"><?= h($items->price) ?></td>
                      <td class="center-col"><?= h($items->quantity) ?></td>
                      <td class="center-col"><?= h($items->subtotal) ?></td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Realizar pago de factura')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <?= $this->Form->create(null, array('url' =>array('controller'=>'Invoices', 'action'=>'makePaid'))) ?>
        <fieldset>
          <div class="row">
            <div class="col-md-10">
              <label><?=__('Tipo de pago:')?> </label>
              <div class="form-group">
                <label class="radio-inline rbPaid"><input type="radio" name="paidType" value="0" required><?= __('Pago completo')?></label>
                <label class="radio-inline rbPaid"><input type="radio" name="paidType" value="1"><?= __('Pago parcial')?></label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <?php echo $this->Form->control('paidAmount', ['label' => __('Monto a pagar'), 'type' => 'number', 'id' => 'paidAmount']); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label><input type="checkbox" name="sendNotification" value="true"><?= __('Enviar notificación de pago')?></label>
            </div>
          </div>
          <?php echo $this->Form->control('invoice_id', ['type' => 'hidden', 'value' => $invoice->id]); ?>
        </fieldset>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <?= $this->Form->button(__('Guardar')) ?>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalTypeInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Tipo de factura')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <div class="row">
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" style="border:none;" id="international" onclick="downloadInvoice(this.id)">
              <i class="fa fa-globe fa-5x iconInvoice" aria-hidden="true"></i>
              <h4 style="align:center;"><?=__('Internacional')?></h4>
            </button>
          </div>
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" style="border:none;" id="national" onclick="downloadInvoice(this.id)">
              <i class="fa fa-home fa-5x iconInvoice" aria-hidden="true"></i>
              <h4><?=__('Nacional')?></h4>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalReminder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Recordatorio')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <div class="row">
          <div class="col-md-6">
            <?php echo $this->Form->control('email', ['empty' => ['null' => __('Seleccione un destinatario')], 'label' => __('Correo electrónico'), 'options' => $contacts, 'id' => 'emails']);?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <?php echo $this->Form->control('template', ['empty' => ['0' => 'Manual'],'label' => __('Plantilla'), 'options' => $invoice->templates, 'onchange' => 'getContentTemplate(this.value)']); ?>
          </div>
          <div class="col-md-6">
            <?php echo $this->Form->control('subject', ['label' => __('Asunto'), 'id' => 'subject']); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php echo $this->Form->control('content', ['label' => __('Contenido'), 'type' => 'textarea', 'id' => 'txtContentTemplate', 'style' => 'white-space: pre-wrap;']); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <button type="button" class="btn btn-secondary" onclick="sendReminder()"><?=__('Enviar')?></button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<?= $this->end(); ?>
<script>
$(document).ready(function(){
  $('#page-title').text('<?php echo $pageTitle; ?>');
  $('.navBtn').css('display', 'inline');
  $('#prev').append('<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-angle-left')), ['controller' => 'Invoices', 'action' => 'view', $prev], array('escape'=>false)); ?>');
  $('#next').append('<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-angle-right')), ['controller' => 'Invoices', 'action' => 'view', $next], array('escape'=>false)); ?>');
  var invoiceState = '<?php echo $invoice->state ?>';

  if (invoiceState == 'C') {
    $('#btnDownload').prop('disabled', true);
  }

  $('.rbPaid').change(function(){
      if($('input[name=paidType]:checked').val()==0){
        $('#paidAmount').prop('disabled', true);
      }
      else{
        $('#paidAmount').prop('disabled', false);
      }
  });

  $('#ib').change(function(){
    if($('#ib').is(':checked')){
      $('#selBanks').prop('disabled', false);
    }
    else{
      $('#selBanks').prop('disabled', true);
    }
  });

  $('.rbReminder').change(function(){
      if($('input[name=sendType]:checked').val()==1){
        $('#messageEmail').prop('disabled', true);
      }
      else{
        $('#messageEmail').prop('disabled', false);
      }
  });
});


function downloadInvoice(value) {
  invoiceID = '<?php echo $invoice->id ?>';
  if (value === 'international') {
    $('#modalTypeInvoice').modal('hide');
    location.href = '../downloadInvoice/'+invoiceID+'/international';
  }
  else{
    $('#modalTypeInvoice').modal('hide');
    location.href = '../downloadInvoice/'+invoiceID+'/national';
  }
}

function download() {
  if ($('#selBanks').prop('disabled')) {
    $('#modalPersonalizeInvoice').modal('hide');
    location.href = '../downloadInvoice/'+'<?= $invoice->id?>'+'/'+null+'/'+$('#txtNotes').val();
  }
  else {
    $('#modalPersonalizeInvoice').modal('hide');
    location.href = '../downloadInvoice/'+'<?= $invoice->id?>'+'/'+$('#selBanks').val()+'/'+$('#txtNotes').val();
  }
}

  function sendReminder(){
    var message =  $('#txtContentTemplate').val() != '' ? btoa($('#txtContentTemplate').val()) : 'no_message';
    var subject = $('#subject').val() != '' ? $('#subject').val() : 'no_subject';
    var email = $('#emails').val();
    location.href = '../sendInvoiceEmail/<?=$invoice->id?>/'+subject+'/'+message+'/'+email;
  }

  function getContentTemplate(template) {
    if (template != '0') {
      $.ajax({
        type: 'post',
        async: 'true',
        dataType: 'json',
        url: '<?= $this->Url->build(['action' => 'getContentTemplate'])?>',
        data: {
          template: template,
          invoice_id: '<?php echo $invoice->id; ?>'
        },
        success: function(data) {
          $('#txtContentTemplate').val(data);
        }
      });
    }
    $('#txtContentTemplate').val('');
  }

</script>
