<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Invoice[]|\Cake\Collection\CollectionInterface $invoices
  */
  $pageTitle = __('Invoices');
?>
<div class="row quotes-container">
  <div class="col-md-12">
    <div class="bg-white-container">
      <div class="row">
        <div class="col-md-6 info-invoicing">
          <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('New Invoice'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
          <h4><?= __('Total amount: ') ?> &nbsp;<span id="totalAmount">$0.00</span></h4>
          <h4><?= __('Pending amount: ') ?> &nbsp;<span id="pendingAmount">$0.00</span></h4>
        </div>
        <div class="col-md-6 filter-column">
          <button type="button" class="btn invoice-panel btn-sm download-btn rounded-circle d-none multiple-btn" id="bntDownloadInvoices" title="Download multiple invoices"><i class="fas fa-file-archive"></i></button>
          <button type="button" class="btn invoice-panel btn-sm download-btn rounded-circle d-none multiple-btn" onclick="paidMultipleInvoices()" title="Paid invoices"><i class="fa fa-credit-card"></i></button>
          <button type="button" class="btn invoice-panel btn-sm download-btn rounded-circle" onclick="downloadReport()" title="Download Report"><i class="fa fa-download" aria-hidden="true"></i></button>
          <div class="form-group select d-none date-filter">
            <label for="end_date"><?=__('Fecha de fin')?></label>
            <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control" onchange="filterByCompany()">
          </div>
          <div class="form-group select d-none date-filter">
            <label for="start_date"><?=__('Fecha de inicio')?></label>
            <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control" onchange="filterByCompany()">
          </div>
          <?= $this->Form->control(__('Time'), ['options' => ['0' => __('Todas'), '1' => __('Por fecha'), '2' => __('Mes actual'), '3' => __('Mes pasado')], 'onchange' => 'disableDatePicker(this.value)', 'id' => 'selOptionsDate', 'default' => '0'])?>
          <?= $this->Form->control(__('Tipo'), ['options' => ['0' => __('Todas'), '1' => __('Costa Rica'), '2' => __('Internacional')], 'id' => 'selTypes', 'onchange' => 'setType(this.value)'])?>
          <?= $this->Form->control(__('Estado'), ['options' => ['0' => __('Todas'), '1' => __('Vencidas'), '3' => __('Pendientes')], 'id' => 'selOptions', 'default' => '0', 'onchange' => 'filterByCompany()'])?>
          <?= $this->Form->control(__('Filtrar'), ['type' => 'text', 'onchange' => 'filterByCompany()', 'id' => 'companyInput'])?>
        </div>
      </div>
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link invoice-tab active" href="#invoices" role="tab" data-toggle="tab" data-id="invoices"><?= __('Invoices') ?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link invoice-tab" href="#pendingInvoices" role="tab" data-toggle="tab" data-id="pending"><?= __('Pending Invoicing') ?></a>
        </li>
      </ul>

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active invoicesTab" id="invoices">
          <table cellpadding="0" cellspacing="0" class="table table-striped table-border" id="invoicesIndexTable"></table>
          <select class="form-control items-count" name="" id="numberOfRowSelect" data-store="numberOfRecordsInvoices">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
        <div role="tabpanel" class="tab-pane" id="pendingInvoices">
          <table cellpadding="0" cellspacing="0" class="table table-striped table-border" id="invoicesPendingTable"></table>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<div class="modal fade" id="modalTypeInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Tipo de factura')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <div class="row">
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" style="border:none;" id="international" onclick="downloadInvoice(this)">
              <i class="fa fa-globe fa-5x iconInvoice" aria-hidden="true"></i>
              <h4 style="align:center;"><?=__('Internacional')?></h4>
            </button>
          </div>
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" style="border:none;" id="national" onclick="downloadInvoice(this)">
              <i class="fa fa-home fa-5x iconInvoice" aria-hidden="true"></i>
              <h4><?=__('Nacional')?></h4>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Multiple Downloads -->
<div class="modal fade" id="modalTypeMultipleInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Tipo de factura')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <div class="row">
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" id="international" onclick="downloadMultipleInvoices(this)">
              <i class="fa fa-globe fa-5x iconInvoice" aria-hidden="true"></i>
              <h4 style="align:center;"><?=__('Internacional')?></h4>
            </button>
          </div>
          <div class="col-md-6" style="text-align: center;">
            <button type="button" class="btn typeInvoice" name="button" style="border:none;" id="national" onclick="downloadMultipleInvoices(this)">
              <i class="fa fa-home fa-5x iconInvoice" aria-hidden="true"></i>
              <h4><?=__('Nacional')?></h4>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- MODAL PAID -->
<div class="modal fade" id="modal-paid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><?=__('Realizar pago de factura')?></h4>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <?= $this->Form->create(null, array('url' =>array('controller'=>'Invoices', 'action'=>'makePaid'))) ?>
        <fieldset>
          <div class="row">
            <div class="col-md-10">
              <label><?=__('Tipo de pago:')?> </label>
              <div class="form-group">
                <label class="radio-inline rbPaid"><input type="radio" name="paidType" value="0" required><?= __('Pago completo')?></label>
                <label class="radio-inline rbPaid"><input type="radio" name="paidType" value="1"><?= __('Pago parcial')?></label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <?php echo $this->Form->control('paidAmount', ['label' => __('Monto a pagar'), 'type' => 'number', 'id' => 'paidAmount']); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label class="radio-inline "><input type="checkbox" name="sendNotification" value="true"><?= __('Enviar notificación de pago')?></label>
            </div>
          </div>
          <?php echo $this->Form->control('invoice_id', ['type' => 'hidden', 'id' => 'invoice-id-paid']); ?>
        </fieldset>
      </div>
      <div class="modal-footer">
        <div class="footer-btn-container">
          <div class="btns-wrapper">


            <button type="button" class="action-btn btn btn-sm" data-dismiss="modal"><?=__('Cerrar')?></button>
            <?= $this->Form->button(__('Guardar'), ['class' => 'btn-finish btn btn-sm white']) ?>
          </div>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<script id="row" type="text/html">
  <tr class="{{state_class}}">
    <td>{{id}}</td>
    <td>{{customer}}</td>
    <td>{{project}}</td>
    <td data-order="{{number}}">{{dueDate}}</td>
    <td>{{state}}</td>
    <td>{{totalAmount}}</td>
    <td>{{outstandingAmount}}</td>
    <td class="hidden">{{code}}</td>
    <td>{{actions}}</td>
  </tr>
</script>
<script>
var table;
var typeInvoice = '0';
var invoicesTable;

$(document).ready(function(){
  $('#page-title').text('<?php echo $pageTitle; ?>');
  var date = new Date();
  var beginMonth = new Date(date.getUTCFullYear(),('0'+(date.getMonth())).slice(-2), '01');

  //Set dates for datepickers
  $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(beginMonth));
  $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date());
  $('#companyInput').val(localStorage.getItem("company"));
  if (localStorage.getItem("filter_value") != null) {
    $('#selOptions').val(localStorage.getItem("filter_value"));
  }
  else {
    $('#selOptions').val(2);
  }
  if (localStorage.getItem("typeInvoice") != null) {
    $('#selTypes').val(localStorage.getItem("typeInvoice"));
    typeInvoice = localStorage.getItem("typeInvoice");
  }
  else {
    $('#selTypes').val(0);
    typeInvoice = '0';
  }

  if (localStorage.getItem('numberOfRecords') != null) {
    $('#numberOfRowSelect').val(localStorage.getItem('numberOfRecords'));
  }
  else{
    $('#numberOfRowSelect').val('10');
  }

  //Initialize the datatable
  invoicesTable = FooTable.init('#invoicesIndexTable', {
    // we only load the column definitions as the row data is loaded through the button clicks
    "columns": [
      {"name":"id","title": "ID","breakpoints":"xs sm"},
      {"name":"customer","title": "Client"},
      {"name":"project","title": "Project"},
      {"name":"dueDate","title": "Due Date", "type": "date"},
      {"name":"state","title": "State"},
      {"name":"totalAmount","title": "Total Amount"},
      {"name":"outstandingAmount","title": "Pending Amount"},
      {"name":"action","title": "Actions"}
    ],
    "paging": {
      "enabled": true,
      "size": localStorage.getItem('numberOfRecords') != null ? localStorage.getItem('numberOfRecords') : 100
    },
    "filtering": {
      "enabled": false
    }
  }, function () {
    filterByCompany();
  });

  quotesTable = FooTable.init('#invoicesPendingTable', {
    "columns": [
      {"name":"id","title": "ID","breakpoints":"xs sm"},
      {"name":"name","title": "Name"},
      {"name":"totalAmount","title": "Total Amount"},
      {"name":"company","title": "Company"},
      {"name":"state","title": "State"},
      {"name":"action","title": "Actions"}
    ],
    "filtering": {
      "enabled": false
    }
  });

  $('#numberOfRowSelect').change(function() {
    localStorage.setItem('numberOfRecords', $('#numberOfRowSelect').val());
    FooTable.get('#invoicesIndexTable').pageSize($('#numberOfRowSelect').val());
  });

  //Exec the method
  //filterByCompany();

  $('body #invoicesIndexTable').on('click', 'tbody tr', function() {
    $(this).toggleClass('selected');

    if ($('#invoicesIndexTable .selected').length >= 1) {
      $('.multiple-btn').removeClass('d-none');
    }
    else {
      $('.multiple-btn').addClass('d-none');
    }
  });

  $('#bntDownloadInvoices').click(function() {
    $('#modalTypeMultipleInvoice').modal('show');
  });

  //Change for modal paid
  $('.rbPaid').change(function(){
      if($('input[name=paidType]:checked').val()==0){
        $('#paidAmount').prop('disabled', true);
      }
      else{
        $('#paidAmount').prop('disabled', false);
      }
  });
});

//GET ALL INVOICES METHOD
  function filterByCompany() {
    $('#loader').css('display', 'inline-block');
    localStorage.setItem('filter_value', $('#selOptions').val());
    localStorage.setItem("company", $('#companyInput').val());
    var selectOptionDate = $('#selOptionsDate').val();
    var selectOptions = $('#selOptions').val();
    var data = { startDate: null, endDate: null, state: null, downloaded: null, company: '', type: '0'};

    switch (selectOptionDate) {
      case '0':
        data.startDate = null;
        data.endDate = null;
      break;
      case '1':
        data.startDate = $('#startDatePicker').val();
        data.endDate = $('#endDatePicker').val();
      break;
      case '2':
        var date = new Date();
        data.startDate = date.getUTCFullYear()+'-'+('0'+(date.getMonth()+1)).slice(-2)+'-01';
        var lastDate = new Date(date.getFullYear(), ('0'+(date.getMonth()+1)).slice(-2), 0);
        data.endDate = lastDate.getFullYear()+'-'+('0'+(lastDate.getMonth()+1)).slice(-2)+'-'+lastDate.getDate();
      break;
      case '3':
        var date = new Date();
        var month = date.getMonth()+1 == 1 ? '12' : ('0'+(date.getMonth())).slice(-2);
        var year = date.getMonth()+1 == 1 ? date.getUTCFullYear()-1 : date.getUTCFullYear();
        var lastDate = new Date(date.getFullYear(), ('0'+(date.getMonth())).slice(-2), 0);
        data.startDate = year+'-'+month+'-01';
        data.endDate = lastDate.getFullYear()+'-'+('0'+(lastDate.getMonth()+1)).slice(-2)+'-'+lastDate.getDate();
      break;
    }

    switch (selectOptions) {
      case '0':
        data.state = null;
        data.downloaded = null;
      break;
      case '1':
        data.state = 'V';
        data.downloaded = null;
      break;
      case '2':
        data.downloaded = true;
        data.state = null;
      break;
      case '3':
        data.state = 'P';
        data.downloaded = null;
      break;
    }
    data.company = $('#companyInput').val();
    data.type = localStorage.getItem("typeInvoice") != null ? localStorage.getItem("typeInvoice") : '0';
    sendAjaxInvoices(data);
  }

  //Set type of invoices
  function setType(type) {
    typeInvoice = type;
    localStorage.setItem('typeInvoice', type);
    filterByCompany();
  }

  function disableDatePicker(value){
    if(value == '1'){
      $('.date-filter').css('display', 'inline');
    }
    else {
      $('.date-filter').css('display', 'none');
      filterByCompany();
    }
  }

  function cancelInvoice(invoiceID){
    $('#invoice-id-paid').val(invoiceID);
    $('#modal-paid').modal('show');
  }

  function discardInvoice(invoiceID) {
    swal({
      title: "<?php echo __('¿Seguro que desea descartar la factura?') ?>",
      text: 'Esta acción es irreversible',
      icon: "error",
      buttons: true,
      })
      .then((discard) => {
      if (discard) {
        $.ajax({
          type: 'POST',
          async: true,
          dataType: 'json',
          url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'discardInvoice'])?>",
          data: {
            id: invoiceID
          },
          success: function(data){
            location.reload();
          }
        });
      }
      });
  }

  function openModalDownload(id) {
    $('#international').attr('data-id', id);
    $('#national').attr('data-id', id);
    $('#modalTypeInvoice').modal('show');
  }

  function downloadInvoice(value) {
    invoiceID = $(value).attr('data-id');
    if ($(value).attr('id') === 'international') {
      $('#modalTypeInvoice').modal('hide');
      location.href = 'invoices/downloadInvoice/'+invoiceID+'/international';
    }
    else{
      $('#modalTypeInvoice').modal('hide');
      location.href = 'invoices/downloadInvoice/'+invoiceID+'/national';
    }
  }

    function downloadMultipleInvoices(value) {
      var data = $('#invoicesIndexTable .selected');
      var invoices = [];
      data.each(function (index, value) {
        var invoice = $(this).find('td:eq(0)').text();
        invoices.push(invoice);
      });
      if ($(value).attr('id') === 'international') {
        $('#modalTypeMultipleInvoice').modal('hide');
        location.href = 'invoices/downloadMultipleInvoices/'+invoices+'/international';
      }
      else{
        $('#modalTypeMultipleInvoice').modal('hide');
        location.href = 'invoices/downloadMultipleInvoices/'+invoices+'/national';
      }
    }

    function paidMultipleInvoices() {
      var data = table.rows('.selected').data();
      var invoices = [];
      for (var i = 0; i < data.length; i++) {
        invoices[i] = data[i][7];
      }
      swal({
        title: "<?php echo __('Do you want paid all the invoices?') ?>",
        icon: "success",
        buttons: true,
        })
        .then((success) => {
        if (success) {
          $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'paidMultipleInvoices'])?>",
            data: {
              invoices: invoices
            },
            success: function(data){
              location.reload();
            }
          });
        }
        });
    }

    function sendAjaxInvoices(data){
      $.ajax({
        type: 'POST',
        async: true,
        dataType: 'json',
        url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'getInvoicesCompany'])?>",
        data: data,
        success: function(data){
          var invoices = [];
          var rowJson = [];

         $.each(data.invoices, function(index, value){
           date = new Date(value.date);
           dueDate = new Date(value.due_date);
           dateFormat = date.getUTCDate()+'/'+(date.getUTCMonth()+1)+'/'+date.getUTCFullYear();
           dueDateFormat = dueDate.getUTCDate()+'/'+(dueDate.getUTCMonth()+1)+'/'+dueDate.getUTCFullYear();
           totalAmount = '$' + value.total_amount;
           outstandingAmount = '$' + value.outstanding_amount;
           var project = (value.project != null) ? value.project.name:'';
           var classState = '';
           var state = '';

           switch (value.state) {
             case 'V':
               classState = 'due';
               state = '<?php echo __('Vencida') ?>';
             break;
             case 'P':
                state = '<?php echo __('Pendiente') ?>';
             break;
             case 'D':
               classState = 'discard';
               state = '<?php echo __('Descartada') ?>';
             break;
             case 'C':
                state = '<?php echo __('Cancelado') ?>';
             break;
           }

           var actionLink = classState != 'discard' ?  "<a href='<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'view'])?>/"+value.id+"' class='action-btn btn btn-sm'>View</a>" : "<a href='<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'view'])?>/"+value.id+"' class='action-btn btn btn-sm disabled'>View</a>";
           actionLink += '<button type="button" class="btn btn-index download-btn" onclick="openModalDownload('+value.id+')"><?= $this->Html->image("icons/download.png", ['width' => '15']); ?></button>';
           actionLink += '<a class="edit-btn btn btn-sm white" href="<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'edit'])?>/'+value.id+'"><?=__('Editar')?></a>';
           if (value.state == 'C') {
             actionLink += '<a class="btn btn-sm action-btn" onclick="discardInvoice('+value.id+')"><?=__('Discard')?></a>'
           }
           else{
             actionLink += '<a class="btn btn-sm delete-btn" onclick="cancelInvoice('+value.id+')"><?=__('Pay')?></a>'
             actionLink += '<a class="btn btn-sm action-btn" onclick="discardInvoice('+value.id+')"><?=__('Discard')?></a>'
           }

           var dataJson = {
             "id": value.code_invoice,
             "customer": value.company.name,
             "project": project,
             "dueDate": dueDate,
             "state": state,
             "totalAmount": totalAmount,
             "outstandingAmount": outstandingAmount,
             "action": actionLink
           };


           //Save invoices id on local storage for download purpouses
           invoices.push(value.id);
           //Create json for table
           rowJson.push(dataJson);
         });
         invoicesTable.rows.load(rowJson);

         $('#loader').css('display', 'none');
         if (localStorage.getItem("invoices") == null) {
           localStorage.setItem("invoices", invoices)
         }
         else {
           localStorage.setItem("invoices", invoices)
         }

         $('#totalAmount').text('$' + data.totalCosts.toFixed(2));
         $('#pendingAmount').text('$' + data.totalPendingCosts.toFixed(2));
        }
      });
    }

    function convertProject(id) {
      swal({
        title: "<?php echo __('Do you want to create a project for this quote?') ?>",
        text: "<?php echo __('This will create a new project.') ?>",
        icon: "warning",
        buttons: true,
        })
        .then((confirm) => {
          if (confirm) {
            $.ajax({
              type: 'POST',
              async: true,
              dataType: 'json',
              url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'convertQuoteInProject'])?>",
              data: {
                quote_id: id
              },
              success: function(data){
                if (data.message == "success") {
                  var projectID = data.project_id;
                  var quoteID = data.quote_id;
                  swal({
                    title: "<?php echo __('Project has been created') ?>",
                    text: "<?php echo __('Do you want to create an invoice?') ?>",
                    icon: "success",
                    buttons: true,
                    })
                    .then((invoice) => {
                      if (invoice) {
                        var url = "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'add'])?>"+ '/' + projectID + '/' + quoteID;
                        location.href = url;
                      }
                    });
                }
              }
            });
          }
        });
    }

    function test(data) {
      convertProject($(data).attr('data-quote'));
    }

    function loadPendingQuotes() {
      $('#loader').css('display', 'inline-block');
      $.ajax({
        type: 'GET',
        async: true,
        dataType: 'json',
        url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'getPendingInvoiceQuotes'])?>",
        success: function(data){
          console.log(data);
          var rowJson = [];
          $.each(data, function(index, value){
            date = new Date(value.date);

            var actionLink = '<button class="download-btn btn btn-sm convertButton btn-default" data-quote="'+value.id+'" onclick="test(this)">Invoice</button>';
            actionLink += '<button class="delete-btn btn btn-sm white" data-quotereturn="'+value.id+'" onclick="returnQuote(this)">Return to approved</button>';

            var dataJson = {
              "id": value.id,
              "name": value.name,
              "totalAmount": value.total_amount,
              "company": value.company.name,
              "state": value.state,
              "action": actionLink
            };

            //Create json for table
            rowJson.push(dataJson);
          });
          quotesTable.rows.load(rowJson);

          $('#loader').css('display', 'none');
        }
      });
    }

    function returnQuote(value) {
      var id = $(value).data('quotereturn');
      swal({
        title: "<?php echo __('Do you want to return to approved the quote?') ?>",
        icon: "warning",
        buttons: true,
        })
        .then((confirm) => {
          if (confirm) {
            $.ajax({
              type: 'POST',
              async: true,
              dataType: 'json',
              url: "<?= $this->Url->build(['controller' => 'Quotes', 'action' => 'returnQuoteToApprove'])?>/" + id,
              data: {
                id: id
              },
              success: function(data){
                $(value).parent().parent().remove();
                swal({
                  title: "<?php echo __('The quote has been returned to approved') ?>",
                  icon: "success",
                });
              }
            });
          }
        });
    }
</script>
