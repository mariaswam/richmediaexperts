<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Nueva Factura');
  $companies = json_encode($companies);
  $exist = $quote == null ? 'false' : 'true';
  $projectID = isset($project) ? $project->id : null;
  $contBegin = isset($quote->components) && !empty($quote->components) ? count($quote->components) : 1;
?>
<div class="row quote-form-container quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <div class="add-forms-container">
        <?= $this->Html->link(__('Back to invoice'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
        <?= $this->Form->create($invoice) ?>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label for="date"><?=__('Fecha')?></label>
              <input name="date" type="text" id="datePicker" data-provide="datepicker" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('company_id', ['label' => __('Compañía'), 'type' => 'text', 'onchange' => 'getProjectsCompany(null)', 'id' => 'company', 'value' => isset($quote) ? $quote->company->name : null]); ?>
            <?php echo $this->Form->control('customer_id', ['type' => 'hidden', 'value' => isset($quote) ? $quote->customer_id : null]); ?>
          </div>
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('project_id', ['empty' => true, 'label' => __('Proyecto'), 'id' => 'selProjects']); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <?php echo $this->Form->control('purchase_order', ['label' => __('Orden de compra'), 'value' => isset($quote) ? $quote->purchase_order : null]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <?php echo $this->Form->control('notes', ['label' => __('Notas'), 'type' => 'textarea', 'value' => isset($quote) ? $quote->description : null]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <legend id="itemLegend"><?= __('Items') ?>
              <button type="button" class="btn btn-sm plus-btn btn-circle" id="btnAddItems"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </legend>
            <div class="row">
              <div class="col-md-8">
                <div id="rowItems">
                  <div class="row header-items">
                    <div class="col-md-6"><label><?=__('Descripción')?></label></div>
                    <div class="col-md-2"><label><?=__('Precio')?></label></div>
                    <div class="col-md-2"><label><?=__('Cantidad')?></label></div>
                  </div>
                  <?php if (isset($quote)): ?>
                    <?php $cont = 0; foreach ($quote->components as $item): ?>
                      <div class="row quote-item">
                          <div class="col-md-6">
                            <?php echo $this->Form->control('itemsArray[description][]', ['label' => '', 'required', 'value' => $item->description]); ?>
                          </div>
                          <div class="col-md-2">
                            <?php echo $this->Form->control('itemsArray[price][]', ['label' => '', 'id' => 'pI' . $cont,  'class' => 'itemPrice calcInput', 'required', 'value' => $item->price]); ?>
                          </div>
                          <div class="col-md-2">
                            <?php echo $this->Form->control('itemsArray[quantity][]', ['label' => '', 'type' => 'number', 'class' => 'itemQuantity calcInput', 'id' => 'qI' . $cont, 'required', 'value' => '1', 'value' => $item->quantity]); ?>
                          </div>
                          <div class="col-md-2">
                            <?= $this->Form->button(__('<i class="fa fa-trash" aria-hidden="true"></i>'), ['class' => 'btn btn-danger btn-md btn-remove-row', 'type' => 'button', 'onclick' => 'removeRow(this)'], ['escape' => false]) ?>
                          </div>
                      </div>
                    <?php $cont++; endforeach; ?>
                  <?php else: ?>
                    <div class="row quote-item">
                        <div class="col-md-6">
                          <?php echo $this->Form->control('itemsArray[description][]', ['label' => '', 'required']); ?>
                        </div>
                        <div class="col-md-2">
                          <?php echo $this->Form->control('itemsArray[price][]', ['label' => '', 'id' => 'pI0',  'class' => 'itemPrice calcInput', 'required']); ?>
                        </div>
                        <div class="col-md-2">
                          <?php echo $this->Form->control('itemsArray[quantity][]', ['label' => '', 'type' => 'number', 'class' => 'itemQuantity calcInput', 'id' => 'qI0', 'required', 'value' => '1']); ?>
                        </div>
                        <div class="col-md-2">
                          <?= $this->Form->button(__('<i class="fa fa-trash" aria-hidden="true"></i>'), ['class' => 'btn btn-danger btn-md btn-remove-row', 'type' => 'button', 'onclick' => 'removeRow(this)'], ['escape' => false]) ?>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
              <div class="col-md-4 subtotal-content">
                <label for="total_amount"><?= __('Total') ?></label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                  </div>
                  <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="total_amount" readonly id="subtotalInvoice" value="<?= isset($quote) ? $quote->total_amount : 0 ?>">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-end">
          <div class="col-md-12">
            <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
          </div>
        </div>
        <?= $this->Form->end() ?>
      </div>

    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#datePicker').datepicker({dateFormat: 'yy-mm-dd'});

    if ('<?php echo $exist ?>' == 'true') {
      getProjectsCompany('<?= $projectID; ?>');
    }

    var companies = '<?php echo $companies ?>';
    var source = companies != '' ? JSON.parse(companies) : [];
    $('#company').autocomplete({
      source: source
    });

  });
 var cont = '<?php echo $contBegin ?>';
 $('#btnAddItems').click(function(){
   var items = '<div class="row quote-item">';
   items += '<div class="col-md-6"><div class="form-group"><input class="form-control" name="itemsArray[description][]" required></div></div>';
   items += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" required></div></div>';
   items += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1"></div></div>';
   items += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
   items += '</div>';
   cont++;
   $('#rowItems').append(items);
 });
</script>
