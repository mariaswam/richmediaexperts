<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Edit Invoice');
  //$companies = json_encode($companies);
?>
<div class="row quote-form-container quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
    <div class="add-forms-container">
      <?= $this->Html->link(__('Back to invoice'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($invoice) ?>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <div class="form-group">
            <label for="date"><?=__('Fecha')?></label>
            <input name="date" type="text" id="datePicker" data-provide="datepicker" class="form-control">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('company_id', ['label' => __('Compañía'), 'type' => 'text', 'onchange' => 'getProjectsCompany()', 'id' => 'company', 'value' => $invoice->company->name]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('project_id', ['options' => $projects, 'label' => __('Proyecto')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('purchase_order', ['label' => __('Orden de compra')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <?php echo $this->Form->control('notes', ['label' => __('Notas'), 'type' => 'textarea']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <legend id="itemLegend"><?= __('Items') ?>
            <button type="button" class="btn btn-sm plus-btn btn-circle" id="btnAddItems"><i class="fa fa-plus" aria-hidden="true"></i></button>
          </legend>
          <div class="row">
            <div class="col-md-8">
              <div id="rowItems">
                <div class="row header-items">
                  <div class="col-md-6"><label><?=__('Descripción')?></label></div>
                  <div class="col-md-2"><label><?=__('Precio')?></label></div>
                  <div class="col-md-2"><label><?=__('Cantidad')?></label></div>
                </div>
                <?php $cont = 0; foreach ($items->toArray() as $item): ?>
                  <div class="row quote-item" id="<?=$item->id?>">
                    <?php echo  $this->Form->control('itemsArray[id][]', ['value' => $item->id, 'type' => 'hidden']);?>
                    <div class="col-md-6">
                      <?php echo  $this->Form->control('itemsArray[description][]', ['value' => $item->description, 'label' => false, 'required']);?>
                    </div>
                    <div class="col-md-2">
                      <?php echo  $this->Form->control('itemsArray[price][]', ['value' => $item->price, 'label' => false, 'id' => 'pI'.$cont, 'class' => 'itemPrice calcInput', 'required']);?>
                    </div>
                    <div class="col-md-2">
                      <?php echo  $this->Form->control('itemsArray[quantity][]', ['value' => $item->quantity, 'label' => false, 'id' => 'qI'.$cont, 'class' => 'itemQuantity calcInput', 'required']);?>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-danger btn-md delete-account" name="button" onclick="deleteItem('<?=$item->id?>')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>
                  </div>
                <?php $cont++; endforeach; ?>
              </div>
            </div>
            <div class="col-md-4 subtotal-content">
              <label for="total_amount"><?= __('Total') ?></label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">$</span>
                </div>
                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="total_amount" readonly id="subtotalInvoice" value="<?= $invoice->total_amount ?>">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->control('items_delete', ['type' => 'hidden', 'id' => 'itemsDelete'])?>
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
  </div>
</div>
<script>
var itemsToDelete = [];
$(document).ready(function() {
  $('#page-title').text('<?php echo $pageTitle; ?>');
  $('#datePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', '<?= $invoice->date != null ? $invoice->date->format('Y-m-d'):$invoice->date?>');
});
var cont = '<?= count($items->toArray());?>';
$('#btnAddItems').click(function(){
  var items = '<div class="row quote-item">';
  items += '<div class="col-md-6"><div class="form-group"><input class="form-control" name="itemsArray[description][]" required></div></div>';
  items += '<div class="col-md-2"><div class="form-group"><input class="form-control itemPrice calcInput"  type="number" name="itemsArray[price][]"  id="pI'+cont+'" required></div></div>';
  items += '<div class="col-md-2"><div class="form-group"><input class="itemQuantity form-control calcInput" type="number" name="itemsArray[quantity][]"  id="qI'+cont+'" required value="1" min="1"></div></div>';
  items += '<div class="col-md-2"><button class="btn btn-danger btn-md btn-remove-row-seconds" type="button" onclick="removeRow(this)"><i class="fa fa-trash"></i></button></div>';
  items += '</div>';
  cont++;
  $('#rowItems').append(items);
});

$('#btnPaid').click(function(){
  alert('Works');
});

  function discardItem(value) {
    $(value).parent().parent().remove();
  }

  function deleteItem(itemID) {
    itemsToDelete.push(itemID);
    $('#'+itemID).remove();
    $('#itemsDelete').val(itemsToDelete);
    calcSubtotal();
  }
</script>
