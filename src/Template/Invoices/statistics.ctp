<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Estadísticas');
  $companies = json_encode($companies);
?>
<div class="row">
  <div class="col-md-2">
    <?php echo $this->Form->control('companies', ['empty' => ['0' =>__('Todos')], 'options' => ['0' => __('Todos'), '1' => __('Por compañía')], 'id' => 'companies', 'onchange' => 'displayFilter(this.value)']); ?>
  </div>
  <div class="col-md-2 specific-name">
    <?php echo $this->Form->control('companies', ['type' =>'text', 'label' => __('Compañía'),'id' => 'companyName', 'onchange' => 'filterStatistics(this.value)']); ?>
  </div>
  <div class="col-md-2 dates">
    <label for="start_date"><?=__('Fecha de inicio')?></label>
    <input name="start_date" type="text" id="startDatePicker" data-provide="datepicker" class="form-control" onchange="filterStatistics()">
  </div>
  <div class="col-md-2 dates">
    <label for="end_date"><?=__('Fecha de fin')?></label>
    <input name="end_date" type="text" id="endDatePicker" data-provide="datepicker" class="form-control" onchange="filterStatistics()">
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="header">
          <h4 class="title"><?=__('Estadísticas')?></h4>
      </div>
      <div class="content">
        <table class="table">
          <tr>
            <th><?= __('Total de ingresos') ?></th>
            <td id="totalAmount"></td>
          </tr>
          <tr>
            <th><?= __('Monto adeudado') ?></th>
            <td id="amountOwed"></td>
          </tr>
          <tr>
            <th><?= __('Monto pagado') ?></th>
            <td id="amountPaid"></td>
          </tr>
          <tr>
            <th><?= __('Facturas pagadas') ?></th>
            <td id="paidInvoices"></td>
          </tr>
          <tr>
            <th><?= __('Facturas pendientes') ?></th>
            <td id="pendingInvoices"></td>
          </tr>
          <tr>
            <th><?= __('Facturas vencidas') ?></th>
            <td id="dueInvoices"></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="header">
          <h4 class="title"><?= __('Facturas') ?></h4>
      </div>
      <div class="content">
        <canvas id="invoicesChart"></canvas>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="header">
          <h4 class="title"><?= __('Ingresos') ?></h4>
      </div>
      <div class="content">
        <canvas id="earningsChart"></canvas>
        <div class="footer">
          <div class="stats">
            <?=__('Datos brindados en dólares')?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var chartCreated = null;
var earningsChartCreated = null;
  $(document).ready(function(){
    var date = new Date();
    var endDate = new Date();
    $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(date.setMonth(date.getMonth() - 1)));
    $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(endDate.setDate(endDate.getDate() + 1)));
    $('#page-title').text('<?php echo $pageTitle; ?>');
    filterStatistics();

    var companies = '<?php echo $companies ?>';
    var source = companies != '' ? JSON.parse(companies) : [];
    $('#companyName').autocomplete({
      source: source
    });
  });

  function displayFilter(value) {
    if(value == '0'){
      $('.specific-name').css('display', 'none');
      filterStatistics();
    }
    else if (value == '1'){
      $('.specific-name').css('display', 'inline');
    }
  }

  function filterStatistics(){
    var startDate = $('#startDatePicker').val();
    var endDate = $('#endDatePicker').val();
    var company;
    if ($('#companies').val() == '0') {
      company = 0;
    }
    else{
      company = $('#companyName').val();
    }
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Invoices', 'action' => 'statistics'])?>",
      data: {
        company: company,
        start_date: startDate,
        end_date: endDate
      },
      success: function(data){
        var statistics = data.data;
        $('#totalAmount').text('$ '+data.data.totalAmount);
        $('#amountOwed').text('$ '+data.data.amountOwed);
        $('#amountPaid').text('$ '+data.data.amountPaid);
        $('#paidInvoices').text(data.data.paidInvoices);
        $('#pendingInvoices').text(data.data.pendingInvoices);
        $('#dueInvoices').text(data.data.dueInvoices);
        if(chartCreated != null){
          chartCreated.destroy();
        }
        if(earningsChartCreated != null){
          earningsChartCreated.destroy();
        }
        createInvoicesChart(data.data);
        createEarningsChart(data.data);
      }
    });
  }

  function createInvoicesChart(data){
      var ctx = document.getElementById("invoicesChart").getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: ['<?=__("Pagadas")?>', '<?=__("Pendientes")?>', '<?=__("Vencidas")?>'],
              datasets: [{
                  data: [data.paidInvoices, data.pendingInvoices, data.dueInvoices],
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.7)',
                    'rgba(54, 162, 235, 0.7)',
                    'rgba(255, 206, 86, 0.7)'
                  ],
                  borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                  ]
              }]
          },
          options: {
            legend: {
                labels: {
                    fontColor: "white",
                    fontSize: 14
                }
            },
            scales: {
               yAxes: [{
                    ticks: {
                        fontColor: 'white',
                    }
              }],
              xAxes: [{
                   ticks: {
                       fontColor: 'white'
                   }
             }]
            }
          }
      });

      chartCreated = myChart;
  }

  function createEarningsChart(data){
    var ctx = document.getElementById("earningsChart").getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: data.dates,
          datasets: [{
            label: '<?= __('Montos') ?>',
            data: data.amountsByDate,
            backgroundColor: [
              'rgba(133, 193, 233, 0.7)',
            ],
            borderColor: [
              'rgba(52,152,219,1)',
            ]
          }]
        },
        options: {
          legend: {
              labels: {
                  fontColor: "white",
                  fontSize: 14
              }
          },
          scales: {
             yAxes: [{
                  ticks: {
                      fontColor: 'white'
                  }
            }],
            xAxes: [{
                 ticks: {
                     fontColor: 'white'
                 }
           }]
          }
        }
    });
    earningsChartCreated = myLineChart;
  }
</script>
