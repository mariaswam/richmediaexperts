<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Time Tracking</title>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js'); ?>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3 timer form-group">
          <input type="text" class="form-control" name="duration" id="timer-id" style="margin:10px 0 10px 0;" disabled>
          <button type="button" class="btn btn-warning" onclick="pauseTimer()" id="btnPause"><i class="fa fa-pause pause" aria-hidden="true" id="faPause"></i><?=__(' Pausar')?></button>
          <button type="button" class="btn btn-danger" onclick="stopTimer()" id="btnStop"><i class="fa fa-stop" aria-hidden="true"></i><?=__(' Detener')?></button>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"><?=__('Fecha de finalización')?></h4>
          </div>
          <?= $this->Form->create('timesheet', array('url' =>array('controller'=>'Timesheets', 'action'=>'timeTracking'))) ?>
          <div class="modal-body" id="modalBodyInvoice">
            <?php
            echo $this->Form->control('task_id', ['type' => 'hidden', 'value' => $task->id]);
            echo $this->Form->control('performed_activity', ['options' => $taskTypes, 'label' => __('Actividad realizada')]);
            echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea']);
            echo $this->Form->control('duration', ['type' => 'hidden', 'id' => 'durationTask']);
            echo $this->Form->control('start_date', ['type' => 'hidden', 'id' => 'startDateTracking']);
            ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
            <?= $this->Form->button(__('Agregar')) ?>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </body>
  <script type="text/javascript">
  var timer;
      $(document).ready(function() {
        if (!sessionStorage.start_date_tracking) {
          sessionStorage.setItem('start_date_tracking', '<?=date('Y-m-d H:i')?>');
        }

        $(window).resize(function() {
          if (!$('#myModal').is(':visible')) {
            window.resizeTo(300 ,100);
          }
        });
        timer = $('#timer-id').timer();

        $( "#myModal" ).on('shown.bs.modal', function(e){
            window.resizeTo(400 ,450);
        });

        $('#myModal').on('hidden.bs.modal', function () {
            window.resizeTo(300 ,100);
        });
      });

      function pauseTimer() {
        if (timer.data('state') === 'running') {
          timer.timer('pause');
          $('#btnPause').removeClass('btn-warning');
          $('#btnPause').addClass('btn-success');
          $('#btnPause').html('<i class="fa fa-play" aria-hidden="true"></i> Reanudar');
        }
        else{
          timer.timer('resume');
          $('#btnPause').removeClass('btn-success');
          $('#btnPause').addClass('btn-warning');
          $('#btnPause').html('<i class="fa fa-pause" aria-hidden="true"></i> Pausar');
        }
      }

      function stopTimer() {
        var seconds = timer.data('seconds');
        var data = secondsToHms(seconds);
        var duration = '<?=date('Y-m-d')?>'+' '+data;
        console.log(duration);

        if (confirm('<?=__('¿Desea registrar el tiempo recorrido?')?>')) {
          $('#myModal').modal('show');
          $('#btnPause').removeClass('btn-warning');
          $('#btnPause').addClass('btn-success');
          $('#btnPause').text('Iniciar');
          $('#btnPause').attr('disabled', 'disabled');
          $('#btnStop').attr('disabled', 'disabled');
          $('#durationTask').val(data);
          console.log(sessionStorage.start_date_tracking);
          $('#startDateTracking').val(sessionStorage.start_date_tracking);
          timer.timer('remove');
        }
      }

      function secondsToHms(d) {
          d = Number(d);
          var h = Math.floor(d / 3600);
          var m = Math.floor(d % 3600 / 60);
          var s = Math.floor(d % 3600 % 60);

          var hDisplay = h > 0 ? (h < 10 ? ('0'+h) : h) : "00";
          var mDisplay = m > 0 ? (m < 10 ? ('0'+m): m) : "00";
          var sDisplay = s > 0 ? (s < 10 ? ('0'+s): s) : "00";
          return hDisplay+':'+mDisplay;
      }
  </script>
</html>
