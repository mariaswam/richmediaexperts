<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Timesheet $timesheet
  */
  $pageTitle = __('Hora de trabajo');
?>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <table class="table">
        <tr>
            <th scope="row"><?= __('Descripción') ?></th>
            <td><?= h($timesheet->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tarea') ?></th>
            <td><?= $timesheet->has('task') ? $this->Html->link($timesheet->task->name, ['controller' => 'Tasks', 'action' => 'view', $timesheet->task->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de inicio') ?></th>
            <td><?= h($timesheet->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Duración') ?></th>
            <td><?= h($timesheet->duration->hour.' horas y '.$timesheet->duration->minute.' minutos') ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha de fin') ?></th>
            <td><?= h($timesheet->end_date) ?></td>
        </tr>
        <tr>
          <th scope="row"><?=__('Acciones') ?></th>
          <td>
            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $timesheet->id]) ?>
            <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $timesheet->id], ['confirm' => __('¿Seguro que desea eliminar {0}?', $timesheet->description)]) ?>
          </td>
        </tr>
    </table>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
