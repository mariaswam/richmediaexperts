<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Timesheet[]|\Cake\Collection\CollectionInterface $timesheets
  */
  $pageTitle = __('Timetracking');
?>
<style media="screen">
  .content{
    padding: 0 !important;
  }
</style>
<div class="row">
  <div class="col-md-2 pull-right">
    <div class="marquee">
      <div class="fade-left"></div>
      <div class="fade-right"></div>
      <span class="marquee-message">
        <i class="fa fa-clock-o" aria-hidden="true"></i>
        <b id="totalHours">0</b><?= __(' horas') ?>
        <?php if ($user['role'] == 'A'): ?>
          <span class="separator">|</span>
          <i class="fa fa-usd" aria-hidden="true"></i>
          <b id="totalCost">0</b><?=__(' total')?>
        <?php endif; ?>
      </span>
    </div>
  </div>
</div>
<?php if ($user['role'] == 'A'):?>
<div class="row">
  <div class="col-md-2">
    <?php echo $this->Form->control('users', ['empty' => ['null' => ''], 'label' => __('Usuarios'), 'options' => $users, 'onchange' => 'getTimesheetsUser(this.value)', 'id' => 'userSelect']); ?>
  </div>
</div>
<?php endif; ?>
<div class="row">
  <div class="col-md-6 pull-right">
    <button type="button" class="btn btn-sm btn-success btn-form" data-toggle="modal" data-target="#download-report" style="float:right; margin-top: 29px"><i class="fa fa-download" aria-hidden="true"></i></button>
  </div>
</div>
<div class="row" style="margin-top:25px;">
  <div class="col col-md-4 col-sm-12">
    <div id='divCalendar'></div>
  </div>
  <div class="col col-md-8 col-sm-12">
    <table class="table table-striped table-index">
      <thead>
        <tr>
          <th scope="col"><?= $this->Paginator->sort('description', [__('Descripción')]) ?></th>
          <th scope="col"><?= $this->Paginator->sort('performed_activity', [__('Actividad realizada')]) ?></th>
          <th scope="col" class="actions"><?= __('Acciones') ?></th>
        </tr>
      </thead>
      <tbody id="tbTimesheets">
          <?php foreach (json_decode($timesheets) as $timesheet): ?>
          <tr>
              <td><?= h($timesheet->description) ?></td>
              <td><?= h($timesheet->task->name) ?></td>
              <td class="actions">
                <div class="dropdown">
                  <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= __('Acciones'); ?>
                  </button>
                  <ul class="dropdown-menu">
                    <li><?= $this->Html->link(__('Ver'), ['action' => 'view', $timesheet->id]) ?></li>
                    <li><?= $this->Html->link(__('Editar'), ['action' => 'edit', $timesheet->id]) ?></li>
                    <li><?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $timesheet->id], ['confirm' => __('Are you sure you want to delete # {0}?', $timesheet->id)]) ?></li>
                  </ul>
                </div>
              </td>
          </tr>
          <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="timesheetModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=__('Horas de trabajo')?></h5>
      </div>
      <div class="modal-body" id="modalBodyInvoice"></div>
    </div>
  </div>
</div>

<!-- Modal downloadReport -->
<div class="modal fade" id="download-report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=__('Horas de trabajo')?></h5>
      </div>
      <div class="modal-body" id="modalBodyInvoice">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="start_date"><?=__('Fecha y hora de inicio')?></label>
              <input name="start_date" type="text" id="startDatePickerDownload" data-provide="datepicker" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="end_date"><?=__('Fecha y hora de inicio')?></label>
              <input name="end_date" type="text" id="endDatePickerDownload" data-provide="datepicker" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <?= $this->Form->button(__('Descargar'), ['onclick' => 'downloadReport()']) ?>
      </div>
    </div>
  </div>
</div>
<?= $this->end(); ?>
<script>
$(document).ready(function() {
  $('#page-title').text('<?php echo $pageTitle; ?>');
  $('#startDatePicker').datetimepicker({format: 'y-m-d H:i'});
  $('#startDatePickerDownload').datepicker({format: 'y-m-d'});
  $('#endDatePickerDownload').datepicker({format: 'y-m-d'});
  $('#modalBodyInvoice').load('<?=$this->Url->build(['controller' => 'Timesheets', 'action' => 'add'])?>');
  var timesheets = '<?= $timesheets ?>';
  var events = [];

  $.each(JSON.parse(timesheets), function(index, value){
    events[index] = {
                        title: value.description,
                        start: value.start_date,
                        end: value.end_date,
                        url: '<?= $this->Url->build('/timesheets/view/'); ?>'+value.id
                    };
  });
  $('#divCalendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,basicWeek,basicDay'
    },
    navLinks: true, // can click day/week names to navigate views
    eventLimit: true, // allow "more" link when too many events
    events: events,
    dayClick: function(date, jsEvent, view) {
      swal({
        title: "<?php echo __('¿Desea agregar horas de trabajo?') ?>",
        icon: "warning",
        buttons: true,
        })
        .then((add) => {
        if (add) {
          $('#timesheetModal').modal('show');
          $('#startDatePicker').val(date.format());
        }
        });
    },
    viewRender: function(view, element) {
      var dates = getCalendarDateRange();
      var startDate = dates.start.getFullYear()+'-'+('0' + (dates.start.getMonth()+1)).slice(-2)+'-'+('0' + (dates.start.getDate())).slice(-2)+' 23:59';
      var endDate = dates.end.getFullYear()+'-'+('0' + (dates.end.getMonth()+1)).slice(-2)+'-'+('0' + (dates.end.getDate())).slice(-2)+' 23:59';
      if ($('#userSelect').length) {
        getTotalHoursByUser($('#userSelect').val(), startDate, endDate);
      }
      else {
        getTotalHoursByUser('null', startDate, endDate);
      }
    }
  });

});

function getCalendarDateRange() {
      var calendar = $('#divCalendar').fullCalendar('getCalendar');
      var view = calendar.view;
      var start = new Date(view.intervalStart);
      var end = new Date(view.intervalEnd);
      var dates = { start: start, end: end };
      return dates;
  }

  function deleteTimesheet(){
    swal({
      title: "<?php echo __('¿Desea agregar horas de trabajo?') ?>",
      text: "<?php echo __('Esta acción es irreversible') ?>",
      icon: "error",
      buttons: true,
      })
      .then((deleteTime) => {
      if (deleteTime) {
        document.getElementById('formDeleteTimesheet').submit();
      }
      });
  }

  function getTimesheetsUser(userID){
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'getTimesheetByUser'])?>",
      data: {
        id: userID
      },
      success: function(data){
        $('#tbTimesheets').empty();
        $('#divCalendar').fullCalendar( 'destroy' );
        var events = [];
        $.each(JSON.parse(data.timesheets), function(index, value){
          events[index] = {
                              title: value.description,
                              start: value.start_date,
                              end: value.end_date,
                              url: '<?= $this->Url->build('/timesheets/view/'); ?>'+value.id
                          };
          var row = "<tr><td>"+value.description+"</td><td>"+value.start_date+"</td><td>"+value.end_date+"</td><td>"+value.task.name+"</td>";
          row += '<td class="actions"><div class="dropdown"><button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= __('Acciones'); ?></button>';
          row += '<ul class="dropdown-menu">';
          row += '<li><a href=/timesheets/view/'+value.id+'><?=__('Ver')?></a></li>';
          row += '<li><a href=/timesheets/edit/'+value.id+'><?=__('Editar')?></a></li>';
          row += '<li><form action="/timesheets/delete/'+value.id+'" method="POST" id="formDeleteTimesheet"><input type="hidden"></form><a href="javascript:deleteTimesheet()"><?=__('Eliminar')?></a></li>';
          row += '</ul></div></td></tr>';
          $('#tbTimesheets').append(row);
        });
        $('#divCalendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
          },
          navLinks: true, // can click day/week names to navigate views
          eventLimit: true, // allow "more" link when too many events
          events: events,
          dayClick: function(date, jsEvent, view) {
            swal({
              title: "<?php echo __('¿Desea agregar horas de trabajo?') ?>",
              icon: "warning",
              buttons: true,
              })
              .then((add) => {
              if (add) {
                $('#timesheetModal').modal('show');
                $('#startDatePicker').val(date.format());
              }
              });
          },
          viewRender: function(view, element) {
            var dates = getCalendarDateRange();
            var startDate = dates.start.getFullYear()+'-'+('0' + (dates.start.getMonth()+1)).slice(-2)+'-'+('0' + (dates.start.getDate())).slice(-2)+' 23:59';
            var endDate = dates.end.getFullYear()+'-'+('0' + (dates.end.getMonth()+1)).slice(-2)+'-'+('0' + (dates.end.getDate())).slice(-2)+' 23:59';
            getTotalHoursByUser($('#userSelect').val(), startDate, endDate);
          }
        });

      }
    });
  }
  function downloadReport(){
    var startDate = new Date($('#startDatePickerDownload').val());
    var endDate = new Date($('#endDatePickerDownload').val());
    var start = startDate.getFullYear()+'-'+('0' + (startDate.getMonth()+1)).slice(-2)+'-'+('0' + startDate.getDate()).slice(-2)+' 00:01';
    var end = endDate.getFullYear()+'-'+('0' + (endDate.getMonth()+1)).slice(-2)+'-'+('0' + endDate.getDate()).slice(-2)+' 23:59';
    if($('#userSelect').length > 0){
      location.href = 'timesheets/report/'+$('#userSelect').val()+'/'+start+'/'+end;
    }
    else{
      location.href = 'timesheets/report/'+null+'/'+start+'/'+end;
    }
  }

  function getTotalHoursByUser(userID, startDate, endDate) {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'getTotalHoursByUser'])?>",
      data: {
        id: userID,
        start_date: startDate,
        end_date: endDate
      },
      success: function(data){
          $('#totalHours').text(data.totalHoursUser);
          $('#totalCost').text(data.totalAmount.toFixed(2));
      }
    });
  }
</script>
