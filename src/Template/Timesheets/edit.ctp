<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Edit Timetracking');
  $projects = empty($projects) ? null : json_encode($projects->toArray());
?>
<div id="form-timesheet">
  <div class="col-md-12 main-content-wrapper edit-timesheet quote-form-container">
    <div class="add-forms-container">
      <?= $this->Html->link(__('Back to timesheets'), '/tracking', ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($timesheet) ?>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="start_date"><?=__('Date')?></label>
            <input name="date" type="text" id="date" data-provide="datepicker" class="form-control" required>
          </div>
        </div>

        <div class="col-md-6">
          <div class="row">
            <div class="col-md-4">
              <?php echo $this->Form->control('duration', ['label' => __('Hours'), 'type' => 'number', 'required' => 'true']); ?>
            </div>
            <div class="col-md-8">
              <label><?= __('Type') ?></label><br>
              <div class="radio-type-container">
                <input type="radio" name="performed_activity" value="regular" id="radio-regular" required>
                <label><?= __('Regular') ?></label>
                <input type="radio" name="performed_activity" value="feedback" id="radio-feedback" required>
                <label><?= __('Feedback') ?></label>
                <input type="radio" name="performed_activity" value="overtime" id="radio-overtime" required>
                <label><?= __('Overtime') ?></label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('project', ['label' => __('Projects'), 'id' => 'projects_select', 'type' => 'text', 'value' => $timesheet->project != null ? $timesheet->project->name: '', 'required' => 'true']); ?>
        </div>
        <div class="col-md-6">
          <?php echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea']); ?>
        </div>
      </div>


      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Submit'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#date').datetimepicker({format: 'm-d-y'});
    $('#date').val('<?= $timesheet->date != null ? $timesheet->date->format('Y-m-d'):$timesheet->date?>');
    $('#radio-'+'<?= $timesheet->performed_activity; ?>').attr('checked', true);

    var projects = '<?php echo $projects ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#projects_select').autocomplete({
      source: source
    });
  });
</script>
