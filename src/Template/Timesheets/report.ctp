<?php
  $pageTitle = __('Timesheets report');
  $projects = count($projects->toArray()) > 0 ? json_encode($projects->toArray()) : '';
?>
<div class="reports">

<div class="row timesheets-details-admin" id="filter-timesheets">
        <div id="filter-panel" class="filter-panel">
          <div class="row">
            <div class="col-md-2">
              <?php echo $this->Form->control('project', ['label' => 'Project filter', 'id' => 'project_filter', 'type' => 'text']); ?>
              <button type="button" class="btn download-btn btn-md" onclick="downloadReportTimesheetsProject()" name="button"><?= $this->Html->image("icons/download.png", ['width' => '15']); ?></button>
            </div>
            <div class="col-md-2">
              <label><?= __('Filter') ?></label>
              <select class="form-control user-select" id="filter_timesheets" onchange="filterReport()">
                <option value="0"><?= __('All timesheets') ?></option>
                <option value="1"><?= __('This week') ?></option>
                <option value="2"><?= __('Last week') ?></option>
                <option value="3"><?= __('By date') ?></option>
              </select>
            </div>
            <div class="col-md-2">
              <div class="form-group select d-none timesheets-date">
                <label for="start_date"><?=__('Fecha de inicio')?></label>
                <input name="start_date" type="text" id="startdateTimesheet" data-provide="datepicker" class="form-control" onchange="filterDateReport()" autocomplete="off">
              </div>
              <div class="form-group select d-none timesheets-date">
                <label for="end_date"><?=__('Fecha de fin')?></label>
                <input name="end_date" type="text" id="enddateTimesheet" data-provide="datepicker" class="form-control" onchange="filterDateReport()" autocomplete="off">
              </div>
            </div>
            <div class="col-md-2">
              <label><?= __('Users') ?></label>
              <select class="form-control user-select" id="select_user" onchange="filterReport()">
                <option value="0" selected><?= __('All') ?></option>
                <?php foreach ($usersArray as $userData): ?>
                  <option value="<?= $userData->id ?>"><?= $userData->username; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
        </div>

	</div>
<div class="row quote-form-container">
  <div class="col-md-6">
    <div class="add-forms-container">
      <div class="scroll-wrapper"><table class="table table-striped tb-quotes-admin" cellspacing="0" cellpadding="0" id="timesheetTable"></table></div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="add-forms-container">
      <div class="row">
        <div class="col-sm-12"><h3><?= __('Summary time') ?> - <?= __('Regular') ?> <span id="regular_hour">0</span> / <?= __('Feedback') ?> <span id="feedback_hour">0</span> / <?= __('Overtime') ?> <span id="overtime_hour">0</span></h3></div>
        <div class="col-md-8">
          <canvas id="times_chart"></canvas>
        </div>
        <div class="col-md-4">
          <table class="table">
            <tr>
              <th><?= __('Estimated feedback:') ?></th>
              <td><span id="hour_estimated">0h</span></td>
            </tr>
            <tr>
              <th><?= __('Current feedback:') ?></th>
              <td><span id="hour_feedback">0h</span></td>
            </tr>
            <tr>
              <th><?= __('Exceeded feedback:') ?> </th>
              <td><span id="hour_residue">0h</span></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row quote-form-container">
  <div class="col-md-6">
    <div class="add-forms-container">
      <h3><?= __('Resource Summary') ?></h3>
      <canvas id="times_chart_resource"></canvas>
    </div>
  </div>
</div>
<div class="row quote-form-container">
  <div class="col-md-12">
    <div class="add-forms-container">
      <h3><?= __('Projects with few feedback time') ?></h3>
      <canvas id="hot_projects"></canvas>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
var chartCreated = null;
var chartResources = null;
var ctx;
  $(document).ready(function () {
    var projects = '<?= $projects; ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#startdateTimesheet').datepicker({dateFormat: 'yy-mm-dd'});
    $('#enddateTimesheet').datepicker({dateFormat: 'yy-mm-dd'});

    $('#project_filter').autocomplete({
      source: source
    });

    timesheetsTable = FooTable.init('#timesheetTable', {
      // we only load the column definitions as the row data is loaded through the button clicks
      "columns": [
        {"name":"project","title": "Project","breakpoints":"xs sm"},
        {"name":"date","title": "Date", "type": "date"},
        {"name":"description","title": "Description"},
        {"name":"user","title": "Resource"},
        {"name":"hours","title": "Hours"},
        {"name":"type","title": "Type"}
      ],
      "paging": {
        "enabled": false
      },
      "filtering": {
        "enabled": false
      }
    }, function() {
        filterReport();
    });

    $('#project_filter').change(function () {
      filterReport();
    });
  });

  function getReportProjects(data) {
    $('#loader').css('display', 'inline-block');
    // var data = {
    //   project: $('#project_filter').val()
    // };

    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'report'])?>",
      data: data,
      success: function(data){
        var rowJson = [];
        var timesheets = [];
         $.each(data.timesheets, function(index, value){
           date = new Date(value.date);

           var dataJson = {
             "project": value.project != undefined ? value.project.name : '',
             "date": date,
             "description": value.description,
             "user": value.user.username,
             "hours": value.duration,
             "type": '<span class="cap-text">' + value.performed_activity + '</span>'
           };

           //Create json for table
           rowJson.push(dataJson);
           timesheets.push(value.id);
         });
         timesheetsTable.rows.load(rowJson);

         $('#loader').css('display', 'none');
         localStorage.setItem("timesheets", timesheets);

         $('#regular_hour').text(data.regulartime + 'h');
         $('#feedback_hour').text(data.feedbacktime + 'h');
         $('#overtime_hour').text(data.overtime + 'h');
         $('#hour_estimated').text(data.feedbackEstimatedTime + 'h');
         $('#hour_feedback').text(data.feedbacktime + 'h');
         var exceed = (data.feedbackEstimatedTime - data.feedbacktime) < 0 ? Math.abs((data.feedbackEstimatedTime - data.feedbacktime)) : 0;
         $('#hour_residue').text(exceed + 'h');
         if (data.feedbacktime > data.feedbackEstimatedTime) {
           if ($('#project_filter').val() != '') {
             $('#hour_estimated').addClass('exceed');
           }
         }
         else{
           $('#hour_estimated').removeClass('exceed');
         }
         loadChart(data.regulartime, data.feedbacktime, data.overtime);
         loadChartResource(data.users, data.userStatistic);
         loadChartFeedback(data.dataHotFeedback);
      }
    });
  }

  function loadChart(regular, feedback, overtime) {
    chartCreated != null ? chartCreated.destroy() : chartCreated;

    ctx = document.getElementById("times_chart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ['<?=__("Regular")?>', '<?=__("Feedback")?>', '<?=__("Overtime")?>'],
            datasets: [{
                data: [regular, feedback, overtime],
                backgroundColor: [
                  'rgba(241,90,36,0.7)',
                  'rgba(41,171,226,0.7)',
                  'rgba(103,65,135,0.7)'
                ],
                borderColor: [
                  'rgba(241,90,36,1)',
                  'rgba(41,171,226,1)',
                  'rgba(103,65,135,1)'
                ]
            }]
        },
        options: {
          legend: {
              labels: {
                  fontColor: "black",
                  fontSize: 14
              }
          },
          scales: {
             yAxes: [{
                  ticks: {
                      display: false,
                  },
                  gridLines:{
                    display: false
                  }
            }],
            xAxes: [{
                  barPercentage: 0.1,
                 ticks: {
                     display: false,
                 },
                 gridLines:{
                   display: false
                 }
           }]
         },
         plugins: {
            labels: {
              render: 'value',
              fontSize: 14,
              fontColor: '#FFF'
            }
          }
        }
    });

    chartCreated = myChart;
  }

  function loadChartResource(users, times){
    chartResources != null ? chartResources.destroy() : chartResources;
    var ctx2 = document.getElementById("times_chart_resource").getContext('2d');
    var datasets = [];

    $.each(times, function(index, value) {
      var dataset = {
        label: index,
        data: value,
        backgroundColor: [
            'rgba(255, 99, 132, 0.7)',
            'rgba(54, 162, 235, 0.7)',
            'rgba(255, 206, 86, 0.7)',
            'rgba(22, 160, 133, 0.7)',
        ],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
        ],
        borderWidth: 1
      };
      datasets.push(dataset);
    });

    var myChart2 = new Chart(ctx2, {
      type: 'bar',
      data: {
          labels: ['<?= __('Regular') ?>', '<?= __('Feedback') ?>', '<?= __('Overtime') ?>'],
          datasets: datasets
      },
      options: {
        legend: {
            labels: {
                fontColor: "black",
                fontSize: 14,
                boxWidth: 0,
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "black",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "black",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }]
        },
        plugins: {
           labels: {
             render: 'value',
             fontSize: 14,
             fontColor: '#000'
           }
         }
      }
    });

    chartResources = myChart2;
  }

  function loadChartFeedback(data){
    var ctx2 = document.getElementById("hot_projects").getContext('2d');
    var datasetOne = [];
    var datasetTwo = [];
    var labels = [];

    $.each(data, function(index, value) {
      datasetOne.push(value[0]);
      datasetTwo.push(value[1]);
      labels.push(index);
    });

    var myChart2 = new Chart(ctx2, {
      type: 'bar',
      data: {
          labels: labels,
          datasets: [{
        				label: '<?= __('Estimated feedback') ?>',
                backgroundColor: 'rgba(54, 162, 235, 0.7)',
        				borderColor: 'rgba(54, 162, 235, 1)',
        				borderWidth: 1,
        				data: datasetOne
        			}, {
        				label: '<?= __('Current feedback') ?>',
                backgroundColor: 'rgba(255, 99, 132, 0.7)',
        				borderColor: 'rgba(255,99,132,1)',
        				borderWidth: 1,
        				data: datasetTwo
        			}]
      },
      options: {
        legend: {
            labels: {
                fontColor: "black",
                fontSize: 14,
                boxWidth: 0,
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: "black",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: "black",
                    fontSize: 12,
                    stepSize: 5,
                    beginAtZero: true
                }
            }]
        },
        plugins: {
           labels: {
             render: 'value',
             fontSize: 14,
             fontColor: '#FFF',
             textMargin: -20
           }
         }
      }
    });

    chartFeedback = myChart2;
  }

  function downloadReportTimesheetsProject() {
    var timesheets = localStorage.getItem("timesheets");
    location.href = '/timesheets/downloadReportTimesheetsByProject/' + timesheets;
  }

  function filterReport() {
    let filter = $('#filter_timesheets').val();
    let today = new Date();
    let data = { date: null, endDate: null, project: $('#project_filter').val(), user: $('#select_user').val()};
    var first, last, startDate, endDate;

    switch (filter) {
      case '0':
        data.date = null;
        data.endDate = null;
        $('.timesheets-date').addClass('d-none');
        getReportProjects(data);
      break;
      case '1':
        first = today.getDate() - today.getDay();
        last = first + 6;
        startDate = new Date(today.setDate(first));
        endDate = new Date(today.setDate(today.getDate()+6));
        data.date = startDate.getUTCFullYear() + '-' + ('0'+(startDate.getMonth()+1)).slice(-2) + '-' + ('0'+startDate.getDate()).slice(-2) + ' 00:00';
        data.endDate = endDate.getUTCFullYear() + '-' + ('0'+(endDate.getMonth()+1)).slice(-2) + '-' + ('0'+endDate.getDate()).slice(-2) + ' 23:59';
        $('.timesheets-date').addClass('d-none');
        getReportProjects(data);
      break;
      case '2':
        first = today.getDate() - today.getDay();
        last = first + 6;
        startDate = new Date(today.setTime(today.getTime() - (today.getDay() ? today.getDay() : 7) * 24 * 60 * 60 * 1000));
        endDate = new Date(today.setTime(today.getTime() - 6 * 24 * 60 * 60 * 1000));
        data.endDate = startDate.getUTCFullYear() + '-' + ('0'+(startDate.getMonth()+1)).slice(-2) + '-' + ('0'+(startDate.getDate()-1)).slice(-2) + ' 23:59';
        data.date = endDate.getUTCFullYear() + '-' + ('0'+(endDate.getMonth()+1)).slice(-2) + '-' + ('0'+endDate.getDate()).slice(-2) + ' 00:00';
        $('.timesheets-date').addClass('d-none');
        getReportProjects(data);
      break;
      case '3':
        $('.timesheets-date').removeClass('d-none');
      break;
    }
  }

  function filterDateReport() {
    var data = {
      date: $('#startdateTimesheet').val(),
      endDate: $('#enddateTimesheet').val(),
      project: $('#project_filter').val(),
      user: $('#select_user').val()
    };

    getReportProjects(data);
  }

  var scrollbar = new SimpleBar($('.scroll-wrapper')[0], { autoHide: false, forceVisible: true, scrollbarMinSize: 20 });
</script>
