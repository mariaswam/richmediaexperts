<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Add Timetracking');
  $projects = empty($projects) ? null : json_encode($projects->toArray());
?>
<div class="col-md-12 main-content-wrapper">

<div class="row quote-form-container" id="form-timesheet">
  <div class="col-md-12">
    <div class="add-forms-container">
      <legend><?= __('New entry') ?></legend>
      <?= $this->Form->create($timesheet) ?>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="start_date"><?=__('Date')?></label>
            <input name="date" type="text" id="date" data-provide="datepicker" class="form-control">
          </div>
        </div>

        <div class="col-md-6">
          <div class="row">
            <div class="col-md-4">
              <?php echo $this->Form->control('duration', ['label' => __('Hours'), 'type' => 'number', 'required' => 'true']); ?>
            </div>
            <div class="col-md-8">
              <label><?= __('Type') ?></label><br>
              <div class="radio-type-container">
                <input type="radio" name="performed_activity" value="regular" required>
                <label><?= __('Regular') ?></label>
                <input type="radio" name="performed_activity" value="feedback" required>
                <label><?= __('Feedback') ?></label>
                <input type="radio" name="performed_activity" value="overtime" required>
                <label><?= __('Overtime') ?></label>
              </div>
            </div>
          </div>
        </div>


      </div>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('project', ['label' => __('Project'), 'id' => 'projects_select', 'type' => 'text', 'required' => 'true']); ?>
        </div>

        <div class="col-md-6">
          <?php echo $this->Form->control('description', ['label' => __('Descripción'), 'type' => 'textarea']); ?>
        </div>


      </div>

      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Submit'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>

</div>

<div class="row timesheets-details-admin">
  <div class="col-md-2 filter-all">
    <label><?= __('Filter') ?></label>
    <select class="form-control user-select" id="filter_timesheets" onchange="filterTimesheets()">
      <option value="0"><?= __('All timesheets') ?></option>
      <option value="1"><?= __('This week') ?></option>
      <option value="2"><?= __('Last week') ?></option>
      <option value="3"><?= __('By date') ?></option>
    </select>
  </div>
  <div class="col-md-2">
    <div class="form-group select d-none timesheets-date">
      <label for="start_date"><?=__('Fecha de inicio')?></label>
      <input name="start_date" type="text" id="startdateTimesheet" data-provide="datepicker" class="form-control" onchange="filterDateTimesheets()" autocomplete="off">
    </div>
    <div class="form-group select d-none timesheets-date">
      <label for="end_date"><?=__('Fecha de fin')?></label>
      <input name="end_date" type="text" id="enddateTimesheet" data-provide="datepicker" class="form-control" onchange="filterDateTimesheets()" autocomplete="off">
    </div>
  </div>
  <div class="col-md-2">
    <?php if ($user['role'] == 'A'): ?>
    <label><?= __('Users') ?></label>
    <select class="form-control user-select" id="select_user">
      <?php foreach ($users as $userData): ?>
        <option value="<?= $userData->id ?>"><?= $userData->username; ?></option>
      <?php endforeach; ?>
    </select>
    <?php endif; ?>
  </div>
  <div class="col-md-6 text-right hours">
    <h3><?= __('Summary') ?></h3>
    <div class="hours-details">
      Regular: <span id="regular_hour">0</span> /
      Feedback: <span id="feedback_hour">0</span> /
      Overtime: <span id="overtime_hour">0</span>
    </div>
    <button type="button" class="btn download-btn btn-sm" onclick="downloadReportTimesheets()" name="button"><?= $this->Html->image("icons/download.png", ['width' => '15']); ?></button>
  </div>
</div>

<div class="row recorded-times">
  <div class="col-md-12">
    <table class="table table-striped tb-quotes-admin" cellspacing="0" cellpadding="0" id="timesheetTable"></table>
    <select class="form-control items-count" name="" id="numberOfRowSelectTimesheet" data-store="numberOfRecordsInvoices">
      <option value="5">5</option>
      <option value="10">10</option>
      <option value="25">25</option>
      <option value="50">50</option>
      <option value="100">100</option>
    </select>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    var projects = '<?php echo $projects; ?>';
    var source = projects != '' ? JSON.parse(projects) : [];
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $('#date').datepicker().datepicker("setDate", new Date());
    $('#startdateTimesheet').datepicker({dateFormat: 'yy-mm-dd'});
    $('#enddateTimesheet').datepicker({dateFormat: 'yy-mm-dd'});

    $('#projects_select').autocomplete({
      source: source
    });

    if (localStorage.getItem('numberOfRecords') != null) {
      $('#numberOfRowSelectTimesheet').val(localStorage.getItem('numberOfRecordsTimesheet'));
    }
    else{
      $('#numberOfRowSelectTimesheet').val('100');
    }

    timesheetsTable = FooTable.init('#timesheetTable', {
  		// we only load the column definitions as the row data is loaded through the button clicks
  		"columns": [
        {"name":"project","title": "Project","breakpoints":"xs sm"},
        {"name":"date","title": "Date", "type": "date"},
        {"name":"description","title": "Description"},
        {"name":"hours","title": "Hours"},
        {"name":"type","title": "Type"},
        {"name":"action","title": "Actions"}
      ],
      "paging": {
        "enabled": true,
        "size": localStorage.getItem('numberOfRecordsTimesheet') != null ? localStorage.getItem('numberOfRecordsTimesheet') : 10
      },
      "filtering": {
        "enabled": true
      }
  	}, function() {
        filterTimesheets();
    });

    $('#numberOfRowSelectTimesheet').change(function() {
      localStorage.setItem('numberOfRecordsTimesheet', $('#numberOfRowSelectTimesheet').val());
      FooTable.get('#timesheetTable').pageSize($('#numberOfRowSelectTimesheet').val());
    });

    $('#select_user').change(function () {
      filterTimesheets();
    });

  });

  function filterDateTimesheets() {
    var role = '<?= $user['role'] ?>';
    var data = {
      date: $('#startdateTimesheet').val(),
      endDate: $('#enddateTimesheet').val(),
      user: null
    };

    if (role != 'A') {
      data.user = '<?= $user['role'] ?>';
    }
    else{
      data.user = $('#select_user').val();
    }
    getTimesheetsUser(data);
  }

  function filterTimesheets() {
    let filter = $('#filter_timesheets').val();
    let today = new Date();
    let data = { date: null, endDate: null, user: null };
    var first, last, startDate, endDate;
    var role = '<?= $user['role'] ?>';
    if (role != 'A') {
      data.user = '<?= $user['id'] ?>';
    }
    else{
      data.user = $('#select_user').val();
    }

    switch (filter) {
      case '0':
        data.date = null;
        data.endDate = null;
        $('.timesheets-date').addClass('d-none');
        getTimesheetsUser(data);
      break;
      case '1':
        first = today.getDate() - today.getDay();
        last = first + 6;
        startDate = new Date(today.setDate(first));
        endDate = new Date(today.setDate(last));
        data.date = startDate.getUTCFullYear() + '-' + ('0'+(startDate.getMonth()+1)).slice(-2) + '-' + ('0'+startDate.getDate()).slice(-2) + ' 00:00';
        data.endDate = endDate.getUTCFullYear() + '-' + ('0'+(endDate.getMonth()+1)).slice(-2) + '-' + ('0'+endDate.getDate()).slice(-2) + ' 23:59';
        $('.timesheets-date').addClass('d-none');
        getTimesheetsUser(data);
      break;
      case '2':
        first = today.getDate() - today.getDay();
        last = first + 6;
        startDate = new Date(today.setTime(today.getTime() - (today.getDay() ? today.getDay() : 7) * 24 * 60 * 60 * 1000));
        endDate = new Date(today.setTime(today.getTime() - 6 * 24 * 60 * 60 * 1000));
        data.endDate = startDate.getUTCFullYear() + '-' + ('0'+(startDate.getMonth()+1)).slice(-2) + '-' + ('0'+(startDate.getDate()-1)).slice(-2) + ' 23:59';
        data.date = endDate.getUTCFullYear() + '-' + ('0'+(endDate.getMonth()+1)).slice(-2) + '-' + ('0'+endDate.getDate()).slice(-2) + ' 00:00';
        $('.timesheets-date').addClass('d-none');
        getTimesheetsUser(data);
      break;
      case '3':
        $('.timesheets-date').removeClass('d-none');
      break;
    }
  }

  function getTimesheetsUser(data) {
    $('#loader').css('display', 'inline-block');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'getTimesheetsUser'])?>",
      data: data,
      success: function(data){
        var rowJson = [];
        var timesheets = [];
         $.each(data.timesheets, function(index, value){
           date = new Date(value.date);

           var actionLink = "<a href='<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'edit'])?>/"+value.id+"' class='action-btn btn btn-sm'>Edit</a>";
           actionLink += '<button class="edit-btn btn btn-sm white" onclick="loadProject('+value.project.id+')">Add hours</button>';
           actionLink += '<button class="delete-btn btn btn-sm white" data-timedelete="'+value.id+'" onclick="deleteTimesheet(this)">Delete</button>';

           var dataJson = {
             "project": value.project != undefined ? value.project.name : '',
             "date": date,
             "description": value.description,
             "hours": value.duration,
             "type": '<span class="cap-text">' + value.performed_activity + '</span>',
             "action": actionLink
           };

           //Create json for table
           rowJson.push(dataJson);
           timesheets.push(value.id);
         });
         timesheetsTable.rows.load(rowJson);

         $('#loader').css('display', 'none');
         localStorage.setItem("timesheets", timesheets);

         $('#regular_hour').text(data.regulartime);
         $('#feedback_hour').text(data.feedbacktime);
         $('#overtime_hour').text(data.overtime);
      }
    });
  }

  function loadProject(id) {
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Projects', 'action' => 'getProject'])?>",
      data: {project_id: id},
      success: function(data){
        $('#projects_select').val(data.project.name);
      }
    });
  }

  function downloadReportTimesheets() {
    var timesheets = localStorage.getItem("timesheets");
    location.href = '/timesheets/download_timesheets/' + timesheets;
  }

  function deleteTimesheet(value) {
    var id = $(value).data('timedelete');
    swal({
      title: "<?php echo __('Do you want to delete the time?') ?>",
      icon: "warning",
      buttons: true,
      })
      .then((confirm) => {
        if (confirm) {
          $.ajax({
            type: 'POST',
            async: true,
            dataType: 'json',
            url: "<?= $this->Url->build(['controller' => 'Timesheets', 'action' => 'delete'])?>/" + id,
            data: {
              id: id
            },
            success: function(data){
              filterTimesheets();
              swal({
                title: "<?php echo __('The time has been deleted') ?>",
                icon: "success",
              });
            }
          });
        }
      });
  }
</script>
