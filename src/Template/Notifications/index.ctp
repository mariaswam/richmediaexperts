<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Notification[]|\Cake\Collection\CollectionInterface $projects
  */
?>
<div class="row">
  <div class="col-md-12">
    <h2><?= __('Notificaciones') ?></h2>
  </div>
</div>
<div class="row">
  <table class="table table-responsive table-striped">
    <tbody>
      <?php foreach ($notifications as $notification): ?>
        <tr>
          <td><?= $notification->message ?></td>
          <td><?= $notification->date ?></td>
          <td><a href="<?=$notification->url?>"><?= __('Ver') ?></a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
