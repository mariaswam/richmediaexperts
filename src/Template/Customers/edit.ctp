<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="customers form large-9 medium-8 columns content">
    <?= $this->Form->create($customer) ?>
    <fieldset>
        <legend><?= __('Edit Customer') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('master_price');
            echo $this->Form->control('resize_price');
            echo $this->Form->control('payment_terms');
            echo $this->Form->control('master_eta');
            echo $this->Form->control('resizes_eta');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
