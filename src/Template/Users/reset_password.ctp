<?= $this->Form->create($user) ?>
<fieldset>
    <legend><?= __('Reestablecer contraseña') ?></legend>
    <?= $this->Form->input('new_password',['type'=>'password', 'label' => false, 'placeholder' => 'Nueva contraseña']) ?>
    <?= $this->Form->input('confirm_password',['type'=>'password', 'label' => false, 'placeholder' => 'Confirmar contraseña']) ?>
</fieldset>
<?= $this->Form->button(__('Reestablecer')) ?>
<?= $this->Form->end() ?>
