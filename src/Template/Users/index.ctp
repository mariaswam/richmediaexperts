<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
  */
  $pageTitle = __('Usuarios');
?>
<div class="row quotes-container">
  <div class="col-md-12 col-sm-12">
    <div class="bg-white-container">
      <?= $this->Html->link('<span class="plus-btn"><i class="fas fa-plus"></i></span>'.__('Add New User'), ['action' => 'add'], ['escape' => false, 'class' => 'add-link']) ?>
      <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-index">
            <thead>
                <tr>
                    <th><?= __('Usuario') ?></th>
                    <th><?= __('Nombre') ?></th>
                    <th><?= __('Apellidos') ?></th>
                    <th><?= __('Correo electrónico') ?></th>
                    <th><?= __('Rol') ?></th>
                    <th><?= __('Costo por hora') ?></th>
                    <th class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= h($user->username) ?></td>
                    <td><?= h($user->first_name) ?></td>
                    <td><?= h($user->last_name) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= $user->role=='R' ? __('Recurso') : ($user->role== 'E' ? __('Client') : __('Administrador')) ?></td>
                    <td><?= $this->Number->format($user->hour_cost) ?></td>
                    <td class="actions">
                      <div class="btns-wrapper">
                        <?= $this->Html->link(__('Ver'), ['action' => 'view', $user->id], ['class' => 'btn-view']) ?>
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn-edit']) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id], ['class' => 'btn-delete'], ['confirm' => __('Desea eliminar el usuario {0}?', $user->username)]) ?>
                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
