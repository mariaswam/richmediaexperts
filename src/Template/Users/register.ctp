<?php
/**
  * @var \App\View\AppView $this
  */
?>

<?= $this->Form->create($user) ?>
    <legend><?= __('Registro') ?></legend>
    <fieldset>
        <?php
            echo $this->Form->control('username', ['label' => false, 'placeholder' => __('Nombre de usuario'), 'class' => 'register-input']);
            echo $this->Form->control('first_name', ['label' => false, 'placeholder' => __('Nombre')]);
            echo $this->Form->control('last_name', ['label' => false, 'placeholder' => __('Apellidos')]);
            echo $this->Form->control('email', ['label' => false, 'placeholder' => __('Correo electrónico')]);
            echo $this->Form->control('organization_name', ['label' => false, 'placeholder' => __('Nombre de la organización'), 'type' => 'text']);
            echo $this->Form->control('password', ['label' => false, 'placeholder' => __('Contraseña')]);
            echo $this->Form->control('confirm_password', ['label' => false, 'placeholder' => __('Confirmar contraseña'), 'type' => 'password']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Registrar')) ?>
<?= $this->Form->end() ?>
