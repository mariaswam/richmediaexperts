<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User $user
  */
  $pageTitle = h($user->first_name . ' ' . $user->last_name);
?>
<div class="row quotes-container">
  <div class="col-md-12">
    <div class="bg-white-container">
    <?= $this->Html->link(__('Back to dashboard'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
    <h2><?= __('Information') ?></h2>
      <table class="vertical-table table table-view small-view">
          <tr>
              <th scope="row"><?= __('Nombre de usuario') ?></th>
              <td><?= h($user->username) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Nombre') ?></th>
              <td><?= h($user->first_name) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Apellidos') ?></th>
              <td><?= h($user->last_name) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Correo electrónico') ?></th>
              <td><?= h($user->email) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Rol') ?></th>
              <td><?= h($user->role) ?></td>
          </tr>
          <tr>
              <th scope="row"><?= __('Costo por hora') ?></th>
              <td><?= $this->Number->format($user->hour_cost) ?></td>
          </tr>
      </table>
  </div>
  </div>

</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
