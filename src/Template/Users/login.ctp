<div class="container session-container">
  <div class="row justify-content-center align-items-center h-100">
    <div class="col-md-7 col-sm-12 form-session">
      <?= $this->Form->create() ?>
          <legend class="text-center"><?= __('Log In') ?></legend>
          <fieldset>
            <div class="form-group row">
              <div class="col-md-2">
                <?php echo $this->Form->label('username', __('Username'), ['class' => 'col-form-label']); ?>
              </div>
              <div class="col-sm-10">
                <?php echo $this->Form->control('username', ['label' => false, 'class' => 'form-control-sm']); ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-2">
                <?php echo $this->Form->label('password', __('Password'), ['class' => 'col-form-label']); ?>
              </div>
              <div class="col-sm-10">
                <?php echo $this->Form->control('password', ['label' =>  false, 'class' => 'form-control-sm']); ?>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-8 text-center">
                <?= $this->Html->link(__('Forgot your password?'), ['controller' => 'Users', 'action' => 'forgotPassword']) ?>
              </div>
              <div class="col-md-4">
                <?= $this->Form->button(__('Log In'), ['class' => 'btn btn-sm']) ?>
              </div>
            </div>
          </fieldset>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
