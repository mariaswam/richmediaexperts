<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Users $user
  */
  $pageTitle = __('Perfil');
?>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <?php if($user->first_login == 0): ?>
      <div class="alert alert-warning">
        <?= __('Es tu primer ingreso al sistema, por favor cambia tu contraseña.') ?>
      </div>
    <?php endif;?>
  </div>
</div>
<div class="row quote-form-container">
  <div class="col-md-12">
    <div class="add-forms-container">
      <?= $this->Form->create($user) ?>
      <div class="row">
        <div class="col-md-6">
          <?php echo $this->Form->control('username', ['label' => __('Nombre de usuario')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('first_name', ['label' => __('Nombre')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('last_name', ['label' => __('Apellidos')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('email', ['label' => __('Correo electrónico'), 'type' => 'email']); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('gender', ['label' => __('Género'), 'options' => ['M' => __('Masculino'), 'F' => __('Femenino')]]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('password', ['label' => __('Contraseña')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('confirm_password', ['label' => __('Confirmar contraseña'), 'type' => 'password']); ?>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6 col-sm-12 slack">
          <img src="<?= $this->request->webroot.'img/slack-icon.png'?>" alt="slack" width="42">
          <?php echo $this->Form->control('slack_username', ['label' => false, 'placeholder' => 'Slack username']); ?>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
        <?php
            echo $this->Form->control('first_login', ['type' => 'hidden', 'value' => '1']);
        ?>
        <?= $this->Form->end() ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
