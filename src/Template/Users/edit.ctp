<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Editar usuario');
?>
<div class="row quotes-container">
  <div class="col-md-12">
    <div class="bg-white-container">
    <div class="add-forms-container">
      <?= $this->Html->link(__('Back to users'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?php if ($user->role != 'E'): ?>
        <button type="button" class="btn btn-sm btn-form" data-toggle="modal" data-target="#permissionsModal"><i class="fa fa-key"></i></button>
      <?php endif; ?>
      <?= $this->Form->create($user) ?>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('username', ['label' => __('Nombre de usuario')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('first_name', ['label' => __('Nombre')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('last_name', ['label' => __('Apellidos')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('email', ['label' => __('Correo electrónico')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('role', ['label' => __('Rol'), 'options' => ['R' => 'Recurso', 'A' => 'Administrador', 'E' => 'Client']]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('hour_cost', ['label' => __('Costo por hora')]); ?>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
<?= $this->start('modalContent'); ?>
<!-- Modal -->
<div class="modal fade" id="permissionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?=__('Permisos')?></h5>
      </div>
      <form id="form-invoices" method="post">
      <div class="modal-body" id="modalBodyInvoice">
        <?php foreach ($permissions as $permission): ?>
          <div class="row permissions">
            <div class="col-md-5">
              <label><?= __($permission->module) ?></label>
            </div>
            <div class="col-md-4">
              <?= $this->Form->button(__('<i class="fa fa-lock"></i>'), ['type' => 'button', 'class' => 'btn btn-danger btnToggle', 'id' => $permission->id, 'data-id' => 0])?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?=__('Cerrar')?></button>
        <button type="button" class="btn btn-secondary" id="btnSaveAll"><?=__('Guardar')?></button>
      </div>
    </div>
  </div>
</div>
</div>
<?= $this->end(); ?>
<script>
  $(document).ready(function() {
    $('#page-title').text('<?php echo $pageTitle; ?>');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: "<?= $this->Url->build(['controller' => 'Users', 'action' => 'getUsersPermissions'])?>",
      data: {
        user_id : '<?php echo $user->id ?>'
      },
      success: function(data){
        $.each(data, function(indx, val){
          var buttonPermission = $('.permissions #'+val.permission_id);
          if(buttonPermission.length != 0){
            buttonPermission.removeClass('btn-danger');
            buttonPermission.addClass('btn-success');
            buttonPermission.html('<i class="fa fa-unlock"></i>');
            buttonPermission.attr('data-id', 1);
          }
        });
        }
    });

    $('.btnToggle').click(function(){
      if ($(this).attr('data-id') == 1) {
        $(this).removeClass('btn-success');
        $(this).addClass('btn-danger');
        $(this).html('<i class="fa fa-lock"></i>');
        $(this).attr('data-id', 0);
      }
      else{
        $(this).removeClass('btn-danger');
        $(this).addClass('btn-success');
        $(this).html('<i class="fa fa-unlock"></i>');
        $(this).attr('data-id', 1);
      }
    });

    $('#btnSaveAll').click(function(){
      var permissions = [];
      $.each($('.btnToggle'), function(index, value){
        var modulePermission = value.id;
        if ($(this).attr('data-id') == 1) {
          permissions[index] = {module : value.id, permission: true};
        }
        else{
          permissions[index] = {module : value.id, permission: false};
        }
      });

      $.ajax({
        type: 'POST',
        async: true,
        dataType: 'json',
        url: "<?= $this->Url->build(['controller' => 'Users', 'action' => 'permissions'])?>",
        data: {
          user_id : '<?php echo $user->id ?>',
          permissions: permissions
        },
        success: function(data){
          location.reload();
        }
      });
    });
  });
</script>
