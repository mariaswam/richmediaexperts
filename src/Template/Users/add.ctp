<?php
/**
  * @var \App\View\AppView $this
  */
  $pageTitle = __('Agregar usuario');
?>
<div class="row quotes-container">
  <div class="col-md-12">
    <div class="bg-white-container">
    <div class="add-forms-container">
      <?= $this->Html->link(__('Back to users'), ['action' => 'index'], ['class' => 'btn btn-sm btn-go-back']) ?>
      <?= $this->Form->create($user) ?>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('username', ['label' => __('Nombre de usuario')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('first_name', ['label' => __('Nombre')]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('last_name', ['label' => __('Apellidos')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('email', ['label' => __('Correo electrónico')]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('role', ['label' => __('Rol'), 'options' => ['R' => 'Recurso', 'A' => 'Administrador', 'E' => 'Cliente']]); ?>
        </div>
        <div class="col-md-6 col-sm-12">
          <?php echo $this->Form->control('hour_cost', ['label' => __('Costo por hora')]); ?>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-12">
          <?= $this->Form->button(__('Save'), ['class' => 'btn-form']) ?>
        </div>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#page-title').text('<?php echo $pageTitle; ?>');
  });
</script>
