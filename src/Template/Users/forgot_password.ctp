<?= $this->Form->create() ?>
    <legend><?= __('Reestablecer contraseña') ?></legend>
    <fieldset>
        <?php
            echo $this->Form->control('email', ['label' => __('Ingrese el correo electrónico registrado'), 'placeholder' => __('Correo Electrónico')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar enlace')) ?>
<?= $this->Form->end() ?>
