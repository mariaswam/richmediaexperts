<nav id="sidebar">
  <div class="sidebar-header">
    <?= $this->Html->image("richmedialogo.png", ['width' => '66']); ?>
  </div>
  <!-- Sidebar Links -->
        <ul class="list-unstyled components nav">
          <?php if ($user['role'] != 'E'): ?>
            <!-- Home -->
              <li class="<?php echo $this->request->params['controller'] == 'Dashboard' ? 'active' : ''; ?>">
                <?= $this->Html->image("icons/home.png", ['width' => '20']); ?>
                <?php if($user['role'] == 'A'): ?>
                  <?php echo $this->Html->link(__(' Inicio'), ['controller' => 'Dashboard', 'action' => 'index']); ?>
                <?php else:  ?>
                  <?php echo $this->Html->link(__(' Inicio'), ['controller' => 'Dashboard', 'action' => 'userIndex']); ?>
                <?php endif; ?>
              </li>
            <!-- /Home -->
            <!-- CRM -->
              <?php if($isAuthorizedCRM || $user['role'] == 'A'): ?>
                <li class="<?php echo ($this->request->params['controller'] == 'Companies' || $this->request->params['controller'] == 'Contacts') ? 'active' : ''; ?>">
                  <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-users"></i>
                    <?= __('CRM')?>
                  </a>
                  <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li><?= $this->Html->link(__('Compañías'), ['controller' => 'Companies', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('Contactos'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
                  </ul>
                </li>
              <?php endif; ?>
            <!-- /CRM -->
            <!-- Projects -->
              <?php if($isAuthorizedProjects || $user['role'] == 'A'): ?>
                <li class="<?php echo ($this->request->params['controller'] == 'Projects' && $this->request->params['action'] != 'profiability' && $this->request->params['action'] != 'performance') ? 'active' : ''; ?>">
                  <?= $this->Html->image("icons/projects.png", ['width' => '20']); ?>
                  <?php echo $this->Html->link(__(' Proyectos'), ['controller' => 'Projects', 'action' => 'index']); ?>
                </li>
              <?php endif; ?>
            <!-- /Projects -->
            <!-- Tasks -->
              <?php if($isAuthorizedTasks || $user['role'] == 'A'): ?>
                <li class="<?php echo ($this->request->params['controller'] == 'Tasks' && $this->request->params['action'] != 'availability') ? 'active' : ''; ?>">
                  <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-tasks')).__(' Tareas'), ['controller' => 'Tasks', 'action' => 'index'], array('escape'=>false)); ?>
                </li>
              <?php endif; ?>
            <!-- /Tasks -->
            <!-- Invoices -->
              <?php if($isAuthorizedInvoices || $user['role'] == 'A'):?>
                <li class="<?php echo ($this->request->params['controller'] == 'Invoices' && $this->request->params['action'] != 'statistics') ? 'active' : ''; ?>">
                  <?= $this->Html->image("icons/invoices.png", ['width' => '20']); ?>
                  <?php echo $this->Html->link(__('Invoices'), ['controller' => 'Invoices', 'action' => 'index']); ?>
                </li>
                <li class="<?php echo ($this->request->params['controller'] == 'Quotes' && $this->request->params['action'] != 'statistics') ? 'active' : ''; ?>">
                  <?= $this->Html->image("icons/quotes.png", ['width' => '20']); ?>
                  <?php echo $this->Html->link(__('Quotations'), ['controller' => 'Quotes', 'action' => 'index']); ?>
                </li>
              <?php endif; ?>
            <!-- /Invoices -->
            <!-- Timesheets -->
              <?php if($isAuthorizedTimesheets || $user['role'] == 'A'): ?>
                <li class="<?php echo ($this->request->params['controller'] == 'Timesheets' || ($this->request->params['action'] == 'availability')) ? 'active' : ''; ?>">
                  <i class="fa fa-calendar"></i>
                  <?= $this->Html->link(__('Horas de trabajo'), '/tracking') ?>
                </li>
              <?php endif; ?>
            <!-- /Timesheets -->
            <!-- Reports -->
              <?php if($isAuthorizedReports || $user['role'] == 'A'): ?>
                <li class="<?php echo (($this->request->params['controller'] == 'Projects' && $this->request->params['action'] == 'profiability') || ($this->request->params['controller'] == 'Invoices' && $this->request->params['action'] == 'statistics') || ($this->request->params['controller'] == 'Projects' && $this->request->params['action'] == 'performance')) ? 'active' : ''; ?>">
                  <a href="#pageSubmenuReports" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-bar-chart"></i>
                    <?= __('Reportes')?>
                  </a>
                  <ul class="collapse list-unstyled" id="pageSubmenuReports">
                    <?php if ($user['role'] == 'E' || $user['role'] == 'A'): ?>
                      <li><?= $this->Html->link(__('Timesheet report'), ['controller' => 'Timesheets', 'action' => 'report']) ?></li>
                    <?php endif; ?>
                    <?php if ($user['role'] == 'A'): ?>
                      <li><?= $this->Html->link(__('Reporte de proyectos'), ['controller' => 'Projects', 'action' => 'profiability']) ?></li>
                      <li><?= $this->Html->link(__('Reporte de facturas'), ['controller' => 'Invoices', 'action' => 'statistics']) ?></li>
                      <li><?= $this->Html->link(__('Reporte general de facturas'), ['controller' => 'Invoices', 'action' => 'reportHistoricByClient']) ?></li>
                      <li><?= $this->Html->link(__('Reporte de desempeño'), ['controller' => 'Projects', 'action' => 'performance']) ?></li>
                    <?php endif; ?>
                  </ul>
                </li>
              <?php endif; ?>
            <!-- /Reports -->
            <!-- Config -->
            <?php if($isAuthorizedConfig || $user['role'] == 'A'): ?>
              <li class="<?php echo (($this->request->params['controller'] == 'Users') || ($this->request->params['controller'] == 'Parametrization') || ($this->request->params['controller'] == 'Organizations')) ? 'active' : ''; ?>">
                <a href="#pageSubmenuConfig" data-toggle="collapse" aria-expanded="false">
                  <?= $this->Html->image("icons/config.png", ['width' => '20']); ?>
                  <?= __('Config')?>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenuConfig">
                  <li><?= $this->Html->link(__('RRHH'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                  <li><?= $this->Html->link(__('Parametrización'), ['controller' => 'Parametrization', 'action' => 'settings']) ?></li>
                  <li><?= $this->Html->link(__('Organización'), ['controller' => 'Organizations', 'action' => 'view', $Auth->user()['organization_id']]) ?></li>
                </ul>
              </li>
            <?php endif; ?>
            <!-- /Config -->
            <?php else: ?>
              <?php if ($user['id'] != 50): ?>
                <li class="<?php echo ($this->request->params['controller'] == 'Quotes' && $this->request->params['action'] != 'statistics') ? 'active' : ''; ?>">
                  <?= $this->Html->image("icons/quotes.png", ['width' => '20']); ?>
                  <?php echo $this->Html->link(__('Quotations'), ['controller' => 'Quotes', 'action' => 'index']); ?>
                </li>
              <?php endif; ?>
              <li><?= $this->Html->link(__('Timesheet report'), ['controller' => 'Timesheets', 'action' => 'report']) ?></li>
            <?php endif; ?>
        </ul>
</nav>
