<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#" id="page-title"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto navbar-right">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle new-items" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-plus"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php if ($user['role'] != 'E'): ?>
            <?= $this->Html->link(__('Agregar proyecto'), ['controller' => 'Projects', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
            <?= $this->Html->link(__('Agregar factura'), ['controller' => 'Invoices', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
          <?php endif; ?>
          <?= $this->Html->link(__('Add new quote'), ['controller' => 'Quotes', 'action' => 'add'], ['class' => 'dropdown-item']) ?>
        </div>
      </li>
      <?php if ($user['role'] != 'E'): ?>
        <li class="nav-item active">
          <?= $this->Form->postLink($this->Html->image("icons/english.png", ["alt" => "english", 'width' => '34']), ['controller' => 'Internationalizations', 'action' => 'changeLocale', 'en_US'], ['escape' => false]) ?>
        </li>
        <li class="nav-item">
          <?= $this->Form->postLink($this->Html->image("icons/spanish.png", ["alt" => "english", 'width' => '34']), ['controller' => 'Internationalizations', 'action' => 'changeLocale', 'es'], ['escape' => false]) ?>
        </li>
      <?php endif; ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="sessionDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?= $this->Html->image("icons/profile.png", ["alt" => "english", 'width' => '35']) ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="sessionDropdown">
          <?php if ($user['role'] != 'E'): ?>
            <?= $this->Html->link(__('My profile'), ['controller' => 'Users', 'action' => 'accountSettings'], ['class' => 'dropdown-item']) ?>
          <?php endif; ?>
          <?= $this->Html->link(__('Log out'), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item']) ?>
        </div>
      </li>
    </ul>
  </div>
</nav>
