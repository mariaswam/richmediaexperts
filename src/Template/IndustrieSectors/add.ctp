<div class="col-md-12">
    <?= $this->Form->create($industrieSector) ?>
    <fieldset>
        <legend><?= __('Nuevo Sector') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Industria')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar'), ['class' => 'btn-form']) ?>
    <?= $this->Form->end() ?>
</div>
