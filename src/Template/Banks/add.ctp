<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-12">
    <?= $this->Form->create($bank) ?>
    <fieldset>
        <legend><?= __('Nuevo Banco') ?></legend>
        <?php
            echo $this->Form->control('name', ['label' => __('Nombre')]);
            echo $this->Form->control('address', ['label' => __('Dirección')]);
            echo $this->Form->control('account', ['label' => __('Número de cuenta')]);
            echo $this->Form->control('bic', ['label' => __('BIC')]);
            echo $this->Form->control('iban', ['label' => __('IBAN')]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
