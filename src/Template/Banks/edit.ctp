<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-md-12">
    <?= $this->Form->create($bank) ?>
    <fieldset>
        <legend><?= __('Editar Banco') ?></legend>
        <?php
        echo $this->Form->control('name', ['label' => __('Nombre')]);
        echo $this->Form->control('address', ['label' => __('Dirección'), 'type' => 'textarea', 'maxlength' => '110', 'placeholder' => __('Máximo 110 caracteres')]);
        echo $this->Form->control('swift', ['label' => __('SWIFT')]);
        echo $this->Form->control('iban', ['label' => __('IBAN')]);
        echo $this->Form->control('aba', ['label' => __('ABA')]);
        echo $this->Form->control('type', ['label' => __('Tipo de banco'), 'options' => ['B' => __('Beneficiario'), 'I' => __('Intermediario')]]);
        echo $this->Form->control('organization_id', ['type' => 'hidden', 'value' => $bank->organization_id]);
        ?>
        <legend><?=__('Cuentas')?>
          <button type="button" class="btn btn-sm btn-info btn-circle"  id="addAccount"><i class="fa fa-plus" aria-hidden="true"></i></button>
        </legend>
        <div id="accountsRow">
          <?php foreach ($accounts as $account): ?>
            <div id="<?=$account->id?>">
              <div class="col-md-6">
                <?php echo $this->Form->control('accounts[account_number][]', ['label' => __('Número de cuenta'), 'value' => $account->account_number]);?>
                <?php echo $this->Form->control('accounts[id][]', ['type' => 'hidden', 'value' => $account->id]);?>
              </div>
              <div class="col-md-4">
                <?php echo $this->Form->control('accounts[account_type][]', ['label' => __('Tipo'), 'options' => ['international' => __('Internacional'), 'national' => __('Nacional')] , 'value' => $account->type])?>
              </div>
              <div class="col-md-2">
                <label for="actions"><?=__('Acciones')?></label>
                <button type="button" class="btn btn-danger btn-sm delete-account" name="button" onclick="deleteAccount('<?=$account->id?>')"><i class="fa fa-trash" aria-hidden="true"></i></button>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
        <?php echo $this->Form->control('accounts_delete', ['type' => 'hidden', 'id' => 'accountDelete']);?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
<script type="text/javascript">
var accountsToDelete = [];
$(document).ready(function() {
  $('#addAccount').click(function(){
    var accountItem = '<div><div class="col-md-6">';
    accountItem += '<?php echo $this->Form->control('accounts[account_number][]', ['label' => __('Número de cuenta')]);?>';
    accountItem += '</div>';
    accountItem += '<div class="col-md-4">';
    accountItem += '<?php echo $this->Form->control('accounts[account_type][]', ['label' => __('Tipo'), 'options' => ['international' => __('Internacional'), 'national' => __('Nacional')]])?>';
    accountItem += '</div>';
    accountItem += '<div class="col-md-2"><label><?=__('Acciones')?></label><button type="button" class="btn btn-danger btn-sm delete-account" name="button" onclick="discardAccount(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></div></div>';
    $('#accountsRow').append(accountItem);
  });
});

  function discardAccount(value) {
    $(value).parent().parent().remove();
  }

  function deleteAccount(accountId) {
    accountsToDelete.push(accountId);
    $('#'+accountId).remove();
    $('#accountDelete').val(accountsToDelete);
  }
</script>
