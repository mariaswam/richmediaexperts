<?php
use Migrations\AbstractMigration;

class CreateContactsSeedMigration extends AbstractMigration
{
    public function up(){
      $generator = \Faker\Factory::create();
      $populator = new Faker\ORM\CakePHP\Populator($generator);

      $populator->addEntity('contacts', 100, [
          'first_name' => function() use($generator){
            return $generator->firstName;
          },
          'last_name' => function() use($generator){
            return $generator->lastName;
          },
          'email' => function() use($generator){
            return $generator->safeEmail;
          },
          'mobile' => function() use($generator){
            return $generator->phoneNumber;
          },
          'date_of_birth' => function() use($generator){
            return $generator->date($format = 'Y/m/d', $max='now');
          },
          'country' => function() use($generator){
            return $generator->country;
          },
          'address' => function() use($generator){
            return $generator->address;
          },
          'company_id' => function() use($generator){
            return $generator->numberBetween($min=1, $max=99);
          },
          'role_id' => function() use($generator){
            return $generator->numberBetween($min=1, $max=2);
          }
      ]);

      $populator->execute();
    }
}
