<?php
use Migrations\AbstractMigration;

class CreateCompaniesSeedMigration extends AbstractMigration
{
  public function up(){
    $faker = \Faker\Factory::create();
    $faker->addProvider(new Faker\Provider\en_US\Company($faker));
    $populator = new Faker\ORM\CakePHP\Populator($faker);

    $populator->addEntity('Companies', 100, [
          'name' => function () use($faker){
            return $faker->company;
          },
          'phone' => function() use($faker){
            return $faker->phoneNumber;
          },
          'country' => function() use($faker){
            return $faker->country;
          },
          'address' => function() use($faker){
            return $faker->address;
          },
          'sector' => function() use($faker){
            return $faker->catchPhrase;
          },
          'payment_terms' => function() use($faker){
            return $faker->numberBetween($min = 15, $max = 60);
          },
          'user_id' => '1'
      ]);

      $populator->execute();
  }
}
