$(document).ready(function () {
  var date = new Date();
  var beginMonth = new Date(date.getUTCFullYear(),('0'+(date.getMonth())).slice(-2), '01');

  $('.table-index').footable({
    "paging": {
			"enabled": true,
      "size": 10
		},
    "filtering": {
      "enabled": true
    }
  });

  //Set dates for datepickers
  $('#startDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date(beginMonth));
  $('#endDatePicker').datepicker({dateFormat: 'yy-mm-dd'}).datepicker('setDate', new Date());

  if (localStorage.getItem('active') && localStorage.getItem('active') == 'true') {
    $('#sidebar').addClass('active');
  }
  else {
    $('#sidebar').removeClass('active');
  }

  $('#sidebarCollapse').on('click', function () {
       $('#sidebar').toggleClass('active');
       if ($('#sidebar').hasClass('active')) {
         localStorage.setItem('active', true);
         $('#sidebarCollapse').html('<i class="fa fa-toggle-off"></i>');
       }
       else {
         localStorage.setItem('active', false);
         $('#sidebarCollapse').html('<i class="fa fa-toggle-on"></i>');
       }
   });


  $('#accordion-menu > li > a').click(function(){
    if ($(this).attr('class') != 'active'){
      $('#accordion-menu li ul').slideUp();
      $(this).next().slideToggle();
      $('#accordion-menu li a').removeClass('active');
      $(this).addClass('active');
    }
  });

$('#zoomBtn').click(function() {
  $('.zoom-btn-sm').toggleClass('scale-out');
  if (!$('.zoom-card').hasClass('scale-out')) {
    $('.zoom-card').toggleClass('scale-out');
  }
});

$('.zoom-btn-sm').click(function() {
  var btn = $(this);
  var card = $('.zoom-card');
  if ($('.zoom-card').hasClass('scale-out')) {
    $('.zoom-card').toggleClass('scale-out');
  }
  if (btn.hasClass('zoom-btn-person')) {
    card.css('background-color', '#d32f2f');
  } else if (btn.hasClass('zoom-btn-doc')) {
    card.css('background-color', '#fbc02d');
  } else if (btn.hasClass('zoom-btn-tangram')) {
    card.css('background-color', '#388e3c');
  } else if (btn.hasClass('zoom-btn-report')) {
    card.css('background-color', '#1976d2');
  } else {
    card.css('background-color', '#7b1fa2');
  }
});

var subtotalItems = [];
$('#rowItems').on('keyup click', '.calcInput', function(){
  calcSubtotal();
});

$('#rowItems').on('keyup click', '.itemQuantity', function () {
  getTotalAssets();
  calcETA();
});

$('.table-dashboard').dataTable({
  "scrollY":"200px",
  "scrollCollapse": true,
  "paging": false,
  "searching": false,
  "info": false
});

  $('.quote-tab').on('click', function() {
    switch ($(this).attr('data-id')) {
      case "invoicing":
          $('.footable-filtering-search').css('display', 'none');
          $('.filter-column').removeClass('d-none');
          $('.info-invoicing h4').removeClass('d-none');
          $('.download-elements').css('display', 'none');
          filterByCompany();
        break;
      case "sent":
          $('.footable-filtering-search').css('display', 'inline-block');
          $('.download-elements').css('display', 'inline-block');
          $('.filter-column').addClass('d-none');
          $('.info-invoicing h4').addClass('d-none');
          location.reload();
        break;
      case "approved":
          $('.download-elements').css('display', 'inline-block');
          $('.footable-filtering-search').css('display', 'inline-block');
          $('.filter-column').addClass('d-none');
          $('.info-invoicing h4').addClass('d-none');
          loadApprovedQuotes();
        break;

    }
  });
  
    $('.invoice-tab').on('click', function() {
    switch ($(this).attr('data-id')) {
      case "invoices":
          $('.footable-filtering-search').css('display', 'none');
          $('.filter-column').removeClass('d-none');
          $('.info-invoicing h4').removeClass('d-none');
          filterByCompany();
        break;
      case "pending":
          $('.footable-filtering-search').css('display', 'inline-block');
          $('.filter-column').addClass('d-none');
          $('.info-invoicing h4').addClass('d-none');
          loadPendingQuotes();
        break;
    }
  });

});

function getMilestonesProject(projectName){
  var getMilestonesUrl = '/milestones/getMilestonesProject';
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: getMilestonesUrl,
    data: {
      name: projectName
    },
    success: function(data) {
      $('#milestone-id option').remove();
      var optionSelectTasks = '<option value="">Seleccionar milestone</option>';
      $('#milestone-id').append(optionSelectTasks);
      $.each(data.milestones, function(index, value){
        var optionSelectMilestone = '<option value="'+value.id+'">'+value.name+'</option>';
        $('#milestone-id').append(optionSelectMilestone);
      });
    }
  });
}

function getTasksByMilestone(milestoneID){
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: '/tasks/getTasksByMilestone',
    data: {
      id: milestoneID
    },
    success: function(data){
      $('#task-id option').remove();
      var optionSelectTasks = '<option value="">Seleccionar tarea</option>';
      $('#task-id').append(optionSelectTasks);
      $.each(data.tasks, function(index, value){
        optionSelectTasks = '<option value="'+value.id+'">'+value.name+'</option>';
        $('#task-id').append(optionSelectTasks);
      });
    }
  });
}

function setDates(taskID){
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: '/tasks/getTaskById',
    data: {
      id: taskID
    },
    success: function(data){
      var startDate = new Date(data.task.start_date);
      var endDate = new Date(data.task.end_date);
      var selectStartDateYear = $('select[name="start_date[year]"] option');
      var selectStartDateMonth = $('select[name="start_date[month]"] option');

      $.each(selectStartDateYear, function(index, value){
        if(value.value < startDate.getUTCFullYear() || value.value > endDate.getUTCFullYear()){
          value.disabled = true;
        }
        else{
          value.disabled = false;
        }
      });
      $.each(selectStartDateMonth, function(index, value){
        if(value.value < startDate.getUTCMonth()+1 || value.value > endDate.getUTCMonth()+1){
          value.disabled = true;
        }
        else{
          value.disabled = false;
        }
      });
    }
  });
}

function finishTask(endDate, taskID){
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: 'tasks/finishTask',
    data: {
      id: taskID,
      end_date: endDate
    },
    success: function(data){
      location.reload();
    }
  });
}

function finishMilestone(endDate, milestoneID){
  console.log(document.URL);
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: document.URL+'/finishMilestone',
    data: {
      id: milestoneID,
      end_date: endDate
    },
    success: function (data){
      location.reload();
    }
  });
}

function sendTaskForApproval(taskID) {
 $.ajax({
   type: 'POST',
   async: true,
   dataType: 'json',
   url: '/tasks/sendTaskForApproval',
   data: {
     id: taskID
   },
   success: function (data){
     location.reload();
   },
   error: function (data) {
     console.log(data);
   }
 });
}

function reopenTask(taskID) {
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: document.URL+'/reopenTask',
    data: {
      task_id: taskID,
      duration: null
    },
    success: function(data){
      location.reload();
    }
  });
}

function returnItemToSource(id, source) {
  var item = kanban.findElement(id);
  var itemAdd = {
    id: $(item).attr('data-eid'),
    title: $(item).text(),
    date: $(item).attr('data-date'),
    duration: $(item).attr('data-duration')
  }
  kanban.removeElement(id);
  kanban.addElement(source, itemAdd);
}

function getTaskInformation(taskID) {
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: '/tasks/getTaskById',
    data: {
      id: taskID
    },
    success: function(data){
      var hour = new Date(data.task.end_date);
      $('.pop-up').fadeIn(100);
      $('#pTaskName').text(data.task.name);
      $('#pTaskDescription').text(data.task.description);
      $('#pTaskProject').text(data.task.milestone != null ? data.task.milestone.project.name : 'NA');
      $('#pTaskMilestone').text(data.task.milestone != null ? data.task.milestone.name : 'NA');
      $('#pResourceTask').text(data.task.user.username);
      $('#pTaskComments').text(data.task.comments);
      $('#pTaskFinishHour').text(('0'+(hour.getHours())).slice(-2)+':'+('0'+(hour.getMinutes())).slice(-2));
    }
  });
}

function verifyNotifications(){
  var count = 0;
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: '/notifications/verifyNotifications',
    success: function(data) {
      $('#ddNotifications').empty();
      $.each(data.notifications, function(index, value) {
        var item = '<li><a href="'+value.url+'" class="item"><span class="item-info"><span>'+value.message+'</span></span></a></li>';
        $('#ddNotifications').append(item);
        count++;
      });
      var last = '<li class="divisor"></li><li><a href="/notifications/" class="text-center">See all</a></li>';
      $('#ddNotifications').append(last);
      $('#quantityNot').text(count);
    }
  });
  setTimeout(verifyNotifications, 600000);
}

function readNotifications() {
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: '/notifications/markAsReadNotifications',
    success: function(data) {
      $('#quantityNot').text('0');
    }
  });
}

function getProjectsCompany(defaultValue) {
  var company = $('#company').val();

  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: "/projects/getProjectsByCompany",
    data: {
      company: company
    },
    success: function(data) {
      console.log(data);
      $('#selProjects option').remove();
      var optionSelectProject = '<option value="">Seleccionar Proyecto</option>';
      $('#selProjects').append(optionSelectProject);
      $.each(data.projects, function(index, value){
        optionSelectProject = '<option value="'+value.id+'">'+value.name+'</option>';
        $('#selProjects').append(optionSelectProject);
      });
      if (defaultValue != null) {
        $('#selProjects').val(defaultValue);
      }
    }
  });
}

function getMilestoneByProjectName(project) {
  $.ajax({
    type: 'POST',
    async: true,
    dataType: 'json',
    url: "/milestones/getMilestonesByProjectName",
    data: {
      project_name: project
    },
    success: function(data) {
      $('#milestone-id option').remove();
      var optionSelectTasks = '<option value="">Seleccionar milestone</option>';
      $('#milestone-id').append(optionSelectTasks);
      $.each(data.milestones, function(index, value){
        var optionSelectMilestone = '<option value="'+value.id+'">'+value.name+'</option>';
        $('#milestone-id').append(optionSelectMilestone);
      });
    }
  });
}

function removeRow(value) {
  $(value).parent().parent().remove();
  calcSubtotal();
}

function calcSubtotal() {
  var subtotalInvoice = 0;
  $('#rowItems .quote-item .itemPrice').each(function (index, value) {
    var numberId = $(value).attr('id').substring(2);
    subtotalInvoice += parseInt($('#qI'+numberId).val()) * parseInt($(value).val());
  });
  $('#subtotalInvoice').val(subtotalInvoice);
}

function disableDatePicker(value){
  console.log('Entra');
  if(value == '1'){
    $('.date-filter').removeClass('d-none');
  }
  else {
    $('.date-filter').addClass('d-none');
    filterByCompany();
  }
}

//FUNCTION FOR DOWNLOAD REPORT
  function downloadReport(){
    var invoices = localStorage.getItem("invoices");
    location.href = 'invoices/report/?invoices='+invoices;
  }

function getTotalAssets() {
  var totalAssets = 0;
  $('.itemQuantity').each(function (index, value) {
    if ($(this).data('type') != 'feedback') {
      totalAssets += parseInt($(this).val());
    }
  });

  $('#subtotalAssets').val(totalAssets);
}

function calcETA() {
  var subtotalETA = 0;
  var masterData = $('#customerSelect option:selected').data('mstrtime');
  var resizeData = $('#customerSelect option:selected').data('rsztime');
  $('#rowItems .quote-item .itemQuantity').each(function (index, value) {
    var numberId = $(value).attr('id').substring(2);

    switch ($('#qI'+numberId).attr('data-type')) {
      case 'master':
        subtotalETA += parseInt($('#qI'+numberId).val()) * masterData;
      break;
      case 'resizes':
        subtotalETA += parseInt($('#qI'+numberId).val()) * resizeData;
      break;
      case 'feedback':
        subtotalETA += parseInt($('#qI'+numberId).val());
      break;
    }
  });

  $('#subtotalETA').val(subtotalETA + 'h');
}
